/**********************************************************************************************************************************/
/*! @file sldsdiff.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Differentiation routines.
 *
 *  Differentiation routines for numeric differentiation. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <vector>
#include <petsc.h>
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::diff
 *
 *  @brief Differentiation routines.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *                                                                                                                                */
namespace slds::diff
{
    /*! @class dfctx
     *
     *  @brief Options and parameters for finite difference approximation.
     *
     *  Options and parameters for finite difference approximation. A default constructor is provided for later initialization.
     *                                                                                                                            */
    class dfctx
    {
        public:
            int d = 0; /*!< The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$. */
            void *fctx = NULL; /*!< Function context that can be used to pass additional arguments to \f$f\f$ if required. This
                                *   is passed to \f$f\f$ on evaluation. */
            PetscErrorCode (*f)(SNES, Vec, Vec, void*) = NULL; /*!< The function
                                                                *   \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$. */
            PetscErrorCode (*initnz)(Mat, int,
                                     PetscErrorCode (*)(int*, int*, int, int)) = NULL; /*!< Function to initialize the nonzero
                                                                                        *   structure of the derivative. In case
                                                                                        *   the nonzero structure is not known
                                                                                        *   a default routine is provided that
                                                                                        *   intializes all matrix entries to 1.
                                                                                        *   Preallocation is handled by this
                                                                                        *   routine. If this is NULL a default
                                                                                        *   routine will be used. */
            PetscReal zerotol = 0; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            MatColoringType colortype = MATCOLORINGJP; /*!< Matrix coloring algorithm used for finite differences. The resulting
                                                        *   matrix coloring indicates which rows can be worked on in parallel. */
            PetscErrorCode (*split)(int*, int*, int, int) = slds::util::splitdefault; /*!< Function to determine the number of
                                                                                       *   rows owned by a process. */
            dfctx() {};
            dfctx(int dd, void *fctxx, PetscErrorCode (*ff)(SNES, Vec, Vec, void*),
                  PetscErrorCode (*initnzz)(Mat, int, PetscErrorCode (*)(int*, int*, int, int))): d(dd), fctx(fctxx), f(ff),
                                                                                                  initnz(initnzz) {};
            dfctx(int dd, void *fctxx, PetscErrorCode (*ff)(SNES, Vec, Vec, void*),
                  PetscErrorCode (*initnzz)(Mat, int, PetscErrorCode (*)(int*, int*, int, int)), PetscReal ztol,
                  MatColoringType ct, PetscErrorCode (*s)(int*, int*, int, int)): d(dd), fctx(fctxx), f(ff), initnz(initnzz),
                                                                                  zerotol(ztol), colortype(ct), split(s) {};
    };
    /*! @brief Initializes all values of a matrix to 1.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Initializes all values of a matrix to 1. Intended to initialize the nonzero structure of a matrix.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A     The matrix \f$A \in \mathbb{R}^{d \times d})\f$. This is parameter is used to return the result.
     *  @param d     The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param split Function to determine the number of rows owned by a process.
     *                                                                                                                             */
    PetscErrorCode initnzfull(Mat A, int d, PetscErrorCode (*split)(int*, int*, int, int) = slds::util::splitdefault);
    /*! @brief Forward difference approximation of derivatives \f$Dfx \in L(\mathbb{R}^{d})\f$, where
     *         \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Forward difference approximation of derivatives \f$Dfx \in L(\mathbb{R}^{d})\f$, where
     *  \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$. This function uses Petsc's built in finite difference approximation
     *  methods.
     *
     *  @return Error code. Returns 0 if successful.
     *
        @param snes Nonlinear solver context. Can be NULL in case the routine is not used in a nonlinear solver context.
     *  @param x    The point \f$x \in \mathbb{R}^{d}\f$ where the derivative should be computed.
     *  @param Dfx  The derivative \f$Df(x) \in L(\mathbb{R}^{d})\f$. This is parameter is used to return the result.
     *  @param Dfxx The matrix used to build the preconditioner in a nonlinear solver context. This parameter is ignored.
     *  @param ctx  Function context containing options and information for the differentiation routine. Should have type dfctx.
     *                                                                                                                             */
    PetscErrorCode df(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *ctx);
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
