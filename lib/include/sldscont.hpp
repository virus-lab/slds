/**********************************************************************************************************************************/
/*! @file sldscont.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Continuation routines.
 *
 *  Continuation routines. Intended for periodic solutions to periodic difference equations or equilibria of differential equations.
 *  Based on PETSc (https://petsc.org/) and SLEPc (https://slepc.upv.es/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <climits>
#include <cstdio>
#include <deque>
#include <vector>
#include <H5Cpp.h>
#include <petsc.h>
#include <slepc.h>
#include "sldsdiff.hpp"
#include "sldsla.hpp"
#include "sldsslv.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::cont
 *
 *  @brief Continuation routines.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *  @warning Requires SLEPc (https://slepc.upv.es/).
 *  @warning Requires MUMPS (https://graal.ens-lyon.fr/MUMPS/index.php).
 *                                                                                                                                */
namespace slds::cont
{
    /* Forward declarations */
    class mpcctx;
    class mpcpctx;
    class mpcoutctx;
    class mpch5bpt;
    class mpch5cpt;
    class mpch5ppt;
    class mpch5vpt;
    class mpch5tpt;
    PetscErrorCode peq(SNES, Vec, Vec, void*);
    PetscErrorCode dpeq(SNES, Vec, Mat, Mat, void*);
    PetscErrorCode ppd(SNES, Vec, Vec, void*);
    PetscErrorCode dppd(SNES, Vec, Mat, Mat, void*);
    PetscErrorCode txdefault(Mat, Mat, slds::cont::mpcpctx);
    PetscErrorCode knulleqdefault(Mat, Mat, int, int, PetscReal, PetscReal, void*);
    PetscErrorCode knullpddefault(Mat, Mat, int, int, PetscReal, PetscReal, void*);
    PetscErrorCode lsf(std::vector<Vec>, Mat, Mat, PetscReal, int*, int, void*, void*);
    PetscErrorCode swtype(Mat, int*, slds::cont::mpcpctx);
    PetscErrorCode swnull(Vec, Mat, Mat, Mat, PetscReal, int, int, slds::cont::mpcpctx);
    PetscErrorCode swtx(Vec, Mat, int, int, slds::cont::mpcpctx);
    PetscErrorCode xevsdefault(Mat, std::vector<PetscReal>*, std::vector<PetscReal>*, slds::cont::mpcpctx);
    PetscErrorCode xidxsdefault(Mat, std::vector<int>*, slds::cont::mpcpctx);
    PetscErrorCode initidxt(std::vector<Vec>, Vec, Vec, Mat, Mat, std::vector<int>*, std::vector<PetscReal>*,
                            std::vector<PetscReal>*, slds::cont::mpcpctx);
    PetscErrorCode nb(Vec, std::vector<int>*, PetscReal*, int, int, int, slds::cont::mpcctx, slds::cont::mpcoutctx*);
    PetscErrorCode iscovered(Vec, Vec, Mat, PetscReal, int*, int, slds::cont::mpcctx);
    PetscErrorCode isinside(Vec, PetscReal*, PetscReal*, int*, int, slds::cont::mpcctx);
    PetscErrorCode copystep(std::vector<std::vector<Vec>>, std::vector<Mat>, std::vector<int>*, std::vector<std::vector<Vec>>,
                            std::vector<Mat>, std::vector<int>*, int, int, slds::cont::mpcpctx);
    PetscErrorCode step(std::vector<Vec>, Vec, Mat, PetscReal*, PetscReal*, PetscReal*, int, int, slds::cont::mpcpctx*,
                        slds::cont::mpcoutctx*);
    PetscErrorCode fcycdefault(SNES, Vec, Vec, void*);
    PetscErrorCode dfcycdefault(SNES, Vec, Mat, Mat, void*);
    PetscErrorCode pfdefault(Vec, Mat, void*);
    PetscErrorCode pf2default(std::vector<Vec>*, Mat, void*);
    PetscErrorCode vecsetdefault(Vec*, int, void*);
    PetscErrorCode matsetdefault(Mat*, int, int, void*);
    PetscErrorCode matsettdefault(Mat*, int, int, void*);
    PetscErrorCode matsetcycdefault(Mat*, int, int, void*);
    PetscErrorCode matsetcycpdefault(Mat*, int, int, void*);
    PetscErrorCode writeouth5defaultb(H5::H5File*, slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode writeouth5defaultc(H5::H5File*, slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode writeouth5defaultp(H5::H5File*, slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode writeouth5defaultv(H5::H5File*, slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode writeouth5defaultt(H5::H5File*, slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode writeouth5default(slds::cont::mpcpctx, slds::cont::mpcoutctx*);
    PetscErrorCode mpcl1snesoptions(SNES, void *);
    PetscErrorCode mpclinfsnesoptions(SNES, void *);
    PetscErrorCode mpcsetoptions(slds::cont::mpcpctx*);
    PetscErrorCode mpcsetdata(slds::cont::mpcpctx*);
    PetscErrorCode mpcupdatedata(slds::cont::mpcpctx*);
    PetscErrorCode mpccleandata(slds::cont::mpcpctx*);
    /*! @class mpcoutctx
     *
     *  @brief Output for multiparameter continuation.
     *
     *  @warning The resources allocated to this class should be released by calling free() when they are no longer needed.
     *
     *  Output for multiparameter continuation.
     *                                                                                                                            */
    class mpcoutctx
    {
        public:
            std::vector<std::vector<Vec>> centers; /*!< Centers of the balls. */
            std::vector<Mat> tangents; /*!< Tangent spaces at the centers of the balls. */
            std::vector<PetscReal> radii; /*!< Radii of the balls. */
            std::vector<int> bidx; /*!< Branch indices of the balls. Only meaningful for one parameter problems. */
            std::vector<int> periods; /*!< Periods of the solutions corresponding to the centers. */
            std::vector<std::vector<int>> idx; /*!< Indices of the solutions corresponding to the centers. */
            std::vector<std::vector<PetscReal>> ev; /*!< Eigenvalues of the solutions corresponding to the centers. */
            std::vector<std::vector<PetscReal>> iev; /*!< Eigenvalues of the solutions corresponding to the centers. */
            std::vector<std::vector<Vec>> points; /*!< Sampled points. */
            std::vector<int> pbidx; /*!< Branch indices of the sampled points. Only meaningful for one parameter problems. */
            std::vector<int> pparents; /*!< Parents of the sampled points. Indicates which ball generated the sample. */
            std::vector<int> pperiods; /*!< Periods of the solutions corresponding to the sampled points. */
            std::vector<std::vector<Vec>> bpoints; /*!< Bifurcation points. Only set if bifurcation tracking is enabled. */
            std::vector<int> btypes; /*!< Generic types of the bifurcation points. Only set if bifurcation tracking is enabled. */
            std::vector<int> bbidx; /*!< Branch indices of the bifurcation points. Only meaningful for one parameter problems.
                                     *   Only set if bifurcation tracking is enabled. */
            std::vector<int> bparents; /*!< Parents of the bifurcation points. Indicates which ball generated the point.
                                        *   Only set if bifurcation tracking is enabled. */
            std::vector<int> bperiods; /*!< Periods of the solutions corresponding to the bifurcation points.
                                        *   Only set if bifurcation tracking is enabled. */
            PetscErrorCode free(); /*!< Releases allocated resources. */
    };
    /*! @class mpcdatactx
     *
     *  @brief Data structure storage for multiparameter continuation.
     *
     *  Data structure storage for multiparameter continuation.
     *                                                                                                                            */
    class mpcdatactx
    {
        public:
            BV bv; /*!< Data structure storing \f$ k \f$ \f$ d \f$-dimensional parallel vectors for orthonormalization. */
            Vec xt; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec fxt; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec vd; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec wd; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec xd; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec yd; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec zd; /*!< \f$ d \f$-dimensional parallel vector. */
            Vec xpd; /*!< \f$ p(d - k) + k \f$-dimensional parallel vector. */
            Vec ypd; /*!< \f$ p(d - k) + k \f$-dimensional parallel vector. */
            Vec zpd; /*!< \f$ p(d - k) + k \f$-dimensional parallel vector. */
            Vec xk; /*!< \f$ k \f$-dimensional parallel vector. */
            Vec yk; /*!< \f$ k \f$-dimensional parallel vector. */
            Vec ydloc; /*!< \f$ d \f$-dimensional local vector. */
            Mat Dfx; /*!< \f$ d \f$-dimensional parallel matrix. */
            Mat Dfxt; /*!< \f$ d \f$-dimensional parallel matrix. */
            Mat Pd; /*!< \f$ d \f$-dimensional parallel matrix to store matrix products. */
            Mat Dfxp; /*!< \f$(d - k)\f$-dimensional parallel matrix. */
            Mat Tx; /*!< \f$ (d, k) \f$-dimensional parallel matrix. */
            Mat Dfxpd; /*!< \f$ p(d - k) + k \f$-dimensional parallel matrix. */
            Mat Ppd; /*!< \f$ p(d - k) + k \f$-dimensional parallel matrix to store matrix products. */
            Mat Dfxpdp; /*!< \f$ p(d - k) \f$-dimensional parallel matrix. */
            VecScatter dscatterctx; /*!< Scatter for \f$ d \f$-dimensional parallel vectors. */
            std::vector<Vec> sd; /* \f$ d \f$-dimensional parallel vectors to store subspaces. */
            std::vector<Vec> spd; /* \f$ p(d - k) + k \f$-dimensional parallel vectors to store subspaces. */
            std::vector<std::vector<Vec>> v; /*!< Step vertices. */
            std::vector<std::vector<Vec>> vs; /*!< Saved step vertices. */
            std::vector<Mat> Dfv; /*!< Step derivatives. */
            std::vector<Mat> Dfvs; /*!< Saved step derivatives. */
            std::vector<int> vadd; /*!< Step indicators. */
            std::vector<int> vadds; /*!< Saved step indicators. */
            std::vector<Mat> Tv; /*!< Step tangent spaces. */
            std::vector<std::vector<int>> idxv; /*!< Step indices. */
            std::vector<std::vector<PetscReal>> evv; /*!< Step eigenvalues. */
            std::vector<std::vector<PetscReal>> ievv; /*!< Step eigenvalues. */
            std::deque<std::vector<Vec>> *xboundary; /*!< Active boundary points. */
            std::deque<Mat> *Txboundary; /*!< Tangent spaces for boundary points. */
            std::deque<int> *pboundary;/*!< Periods for active boundary points. */
            std::deque<int> *bidxboundary;/*!< Branch indices for active boundary points. */
            std::deque<PetscReal> *rboundary;/*!< Initial radii for active boundary points. */
            std::deque<std::vector<int>> *idxboundary;/*!< Indices for active boundary points. */
            std::deque<std::vector<PetscReal>> *evboundary;/*!< Eigenvalues for active boundary points. */
            std::deque<std::vector<PetscReal>> *ievboundary;/*!< Eigenvalues for active boundary points. */
            std::vector<PetscReal> *distb;/*!< Norms of bifurcation points. */
            std::vector<PetscReal> *distc;/*!< Norms of centers. */
    };
    /*! @class mpcidxctx
     *
     *  @brief Options for multiparameter continuation bifurcation tracking.
     *
     *  @warning Enabling trackhopf (tracking of Hopf) or trackns (tracking of Neimark-Sacker) may significantly reduce
     *           performance.
     *
     *  Set various options for multiparameter continuation bifurcation tracking. All values have defaults so mpcidxctx() provides
     *  an instance with sensible defaults.
     *                                                                                                                            */
    class mpcidxctx
    {
        public:
            int trackfold = 0; /*!< Option to track fold bifurcations and perform branch switching. */
            int trackflip = 0; /*!< Option to track flip bifurcations and perform branch switching. */
            int trackposre = 0; /*!< Option to track bifurcations via the positive real part index. Only available for
                                 *   for equilibria.*/
            int trackmorse = 0; /*!< Option to track bifurcations via the Morse index. Only available for periodic difference
                                 *   equations. */
            int maxit = 1e4; /*!< Maximum iterations for line search. */
            int step = 2; /*!< Initial guess for the depth of the solver. Used for Morse or positive real part tracking. */
            int step0 = 1; /*!< Stepsize for the computation. Used for Morse or positive real part tracking .*/
            int computeall = 0; /*!< Option to compute all eigenvalues in one step. Used for Morse or positive real part
                                 *   tracking. */
            PetscReal evmax = 0.1; /*!< Maximum number of eigenvalues that may computed before the computation is aborted. Used
                                    *   for Morse or positive real part tracking. */
            PetscReal tol = 1e-6; /*!< Tolerance for line search. */
    };
    /*! @class mpcswctx
     *
     *  @brief Options for multiparameter continuation branch switching.
     *
     *  Set various options for multiparameter continuation branch switching. All values have defaults so mpcswctx() provides an
     *  instance with sensible defaults.
     *                                                                                                                            */
    class mpcswctx
    {
        public:
            int sw = 1; /*!< Option to enable branch switching at bifurcation points. */
            int maxrn = 4; /*!< Maximum order of the roots of unity checked when switching at Neimark-Sacker points. */
            PetscReal evtol = 1e-6; /*!< Eigenvalue tolerance for determining bifurcation types. */
            PetscReal ds = 0.5; /*!> Perturbation factor used in branch switching. Scales the radius of the ball containing the
                                 *   bifurcation point. */
            PetscReal rs = 0.5; /*!> Radius factor for bifurcation points used in branch switching. Scales the radius of the ball
                                 *   containing the bifurcation point. Any bifurcation in the radius of a known bifurcation
                                 *   point is discarded. */
    };
    /*! @class mpcfctx
     *
     *  @brief Subroutines for multiparameter continuation.
     *
     *  Subroutines for multiparameter continuation. All values have defaults so mpcfctx() provides an instance with
     *  sensible defaults.
     *                                                                                                                            */
    class mpcfctx
    {
        public:
            PetscErrorCode (*projecteq)(std::vector<Vec>, Mat, PetscReal, int,
                                        slds::cont::mpcpctx*) = NULL; /*!< Projection for equilibria. If this is NULL a default
                                                                       *   routine will be used. */
            PetscErrorCode (*projectpd)(std::vector<Vec>, Mat, PetscReal, int,
                                        slds::cont::mpcpctx*) = NULL; /*!< Projection for periodic difference equations.
                                                                       *   If this is NULL a default routine will be used. */
            PetscErrorCode (*tx)(Mat, Mat, slds::cont::mpcpctx) = NULL; /*!< Routine to compute tangent spaces. If this is
                                                                         *   NULL a default routine will be used.*/
            void *txctx = NULL; /*!< Function context for computing tangent spaces. */
            PetscErrorCode (*knulleq)(Mat, Mat, int, int, PetscReal, PetscReal,
                                      void*) = NULL; /*!< Routine to compute nullspaces for equilibria. If this is NULL a
                                                      *   default routine will be used. */
            void *knulleqctx = NULL; /*!< Function context for computing nullspaces for equilibria. */
            PetscErrorCode (*knullpd)(Mat, Mat, int, int, PetscReal, PetscReal,
                                      void*) = NULL; /*!< Routine to compute nullspaces for periodic difference equations.
                                                      *   If this is NULL a default routine will be used. */
            void *knullpdctx = NULL; /*!< Function context for computing nullspaces for periodic difference equations. */
            PetscErrorCode (*fcyc)(SNES, Vec, Vec, void*) = NULL; /*!< Function to evaluate cyclic systems. Used for periodic
                                                                   *   difference equations. If this is NULL a default routine
                                                                   *   will be used. */
            PetscErrorCode (*dfcyc)(SNES, Vec, Mat, Mat, void*) = NULL; /*!< Function to evaluate derivatives of cyclic systems.
                                                                         *   Used for periodic difference equations. If this is
                                                                         *   NULL a default routine will be used. */
            void *fcycctx = NULL; /*!< Function context for cyclic systems. */
            PetscErrorCode (*pf)(Vec, Mat, void*) = NULL; /*!< Function to compute period operators. Used for periodic
                                                           *   difference equations. If this is NULL a default routine
                                                           *   will be used. */
            PetscErrorCode (*pf2)(std::vector<Vec>*, Mat, void*) = NULL; /*!< Function to compute period operators.
                                                                          *   Used for periodic difference equations. If this is
                                                                          *   NULL a default routine will be used. */
            void *pfctx = NULL; /*!< Function context for computing period operators. */
            PetscErrorCode (*xevs)(Mat, std::vector<PetscReal>*, std::vector<PetscReal>*,
                                   slds::cont::mpcpctx) = NULL; /*!< Routine to compute eigenvalues.
                                                                 *   If this is NULL a default routine will be used. */
            void *xevsctx = NULL; /*!< Function context for computing eigenvalues. */
            PetscErrorCode (*xidxs)(Mat, std::vector<int>*,
                                    slds::cont::mpcpctx) = NULL; /*!< Routine to compute bifurcation indices.
                                                                  *   If this is NULL a default routine will be used. */
            void *xidxsctx = NULL; /*!< Function context for computing bifurcation indices. */
            PetscErrorCode (*vecset)(Vec*, int, void*) = NULL; /*!< Routine for vector creation. If this is NULL a default
                                                                *   routine will be used. */
            void *vecctx = NULL; /*!< Options context for vector creation. */
            PetscErrorCode (*matset)(Mat*, int, int, void*) = NULL; /*!< Routine for derivative creation. */
            void *matctx = NULL; /*!< Options context for derivative creation. */
            PetscErrorCode (*matsetp)(Mat*, int, int, void*) = NULL; /*!< Routine for derivative without parameters creation.
                                                                      *   If this is NULL a default routine will be used. */
            void *matpctx = NULL; /*!< Options context for derivative without parameters creation. */
            PetscErrorCode (*matsett)(Mat*, int, int, void*) = NULL; /*!< Routine for tangent space creation.
                                                                      *   If this is NULL a default routine will be used. */
            void *mattctx = NULL; /*!< Options context for tangent space creation. */
            PetscErrorCode (*matsetcyc)(Mat*, int, int, void*) = NULL; /*!< Routine for cyclic derivative creation.
                                                                        *   If this is NULL a default routine will be used. */
            void *matcycctx = NULL; /*!< Options context for cyclic derivative creation. */
            PetscErrorCode (*matsetcycp)(Mat*, int, int, void*) = NULL; /*!< Routine for cyclic derivative without parameters
                                                                         *   creation. If this is NULL a default routine will
                                                                         *   be used. */
            void *matcycpctx = NULL; /*!< Options context for cyclic derivative without parameters creation. */
            PetscErrorCode (*sw)(Mat, Mat, PetscReal, int*, int, int, int, int, int, slds::cont::mpcpctx,
                                 slds::cont::mpcoutctx*) = NULL; /*!< Routine for branch switching. If this is NULL a default
                                                                  *   routine will be used. */
            void *swctx = NULL; /*!< Function context for branch switching. */
            PetscErrorCode (*writeout)(slds::cont::mpcpctx,
                                       slds::cont::mpcoutctx*) = NULL; /*!< Routine for writing output. If this is
                                                                        *   NULL a default routine will be used. */
            void *writectx = NULL; /*!< Function context for writing output. */
    };
    /*! @class mpcslvctx
     *
     *  @brief Solvers for multiparameter continuation.
     *
     *  Solvers for multiparameter continuation. All values have defaults so mpcslvctx() provides an instance with
     *  sensible defaults.
     *                                                                                                                            */
    class mpcslvctx
    {
        public:
            slds::diff::dfctx diffctx = slds::diff::dfctx(); /*!< Finite difference context. */
            slds::solve::fsolvectx fctx = slds::solve::fsolvectx(); /*!< Nonlinear solver context. */
            slds::solve::fsolvectx fctx2 = slds::solve::fsolvectx(); /*!< Second nonlinear solver context. Used for setup of
                                                                      *   custom convergence tests. */
            slds::la::epsnullctx knullctx = slds::la::epsnullctx(); /*!< Nullspace context. May be ignored if custom routines
                                                                     *   are used for computing nullspaces. */
            slds::la::eigenctx ectx = slds::la::eigenctx(); /*!< Eigenvalue solver context. Used for Morse or positive real part
                                                             *   tracking. */
    };
    /*! @class mpcctx
     *
     *  @brief Options for multiparameter continuation.
     *
     *  Provides various options for multiparameter continuation. Includes options for the solvers used at various stages of the
     *  algorithm. All values have defaults so mpcctx() provides an instance with sensible defaults.
     *                                                                                                                            */
    class mpcctx
    {
        public:
            int verbose = 0; /*!< Option to print various updates to stdout. */
            int type = 0; /*!< Option to indicate the type of continuation (0 := equilibria, 1 := periodic difference equations). */
            int writeout = 1; /*!< Write output after computation. */
            int nvertex = 1; /*!< Discretization paramter for the boundary of the balls. Represents the coefficients \f$[-n : n]\f$
                              *   in the vertexbr() function. */
            int usecyclic = 1; /*!< Option to instruct the eigenvalue and nullspace solvers to work with cyclic matrices. */
            int copyectx = 1; /*!< Option to use the same eigenvalue solver for all computations. If this is set this overrides
                               *   any settings in knullctx.ctx(). */
            int createbv = 1; /*!< Option to create a data structure for orthonormalization of vectors. */
            int computeev = 0; /*!< Option to compute the first n dominant eigenvalues at each of the centers. */
            int nev = 1; /*!< Option to specify how many eigenvalues to compute. */
            PetscReal rmin = 1e-6; /*!< Minimum radius of the balls used for covering the manifold. */
            PetscReal rmax = 1; /*!< Maximum radius of the balls used for covering the manifold. */
            PetscReal rdec = 0.75; /*!< Radius decrement for stepsize control. */
            PetscReal rinc = 1.25; /*!< Radius increment for stepsize control. */
            PetscReal eps = 1e-2; /*!< Approximation error tolerated for projected distances. */
            PetscReal zerotol = 0; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            PetscReal nulltol = 1; /*!< Nullspace tolerance. Any eigenvalue or singular value with absolute value less
                                    *   than this is considered zero. Set to a large value to bypass checks if a computed
                                    *   value is zero. */
            VecType vtype = VECSTANDARD; /*!< Vector type. */
            MatType mtype = MATDENSE; /*!< Derivative type. For default routines should be of type DENSE. */
            MatType mttype = MATDENSE; /*!< Tangent space type. For default routines should be of type DENSE. */
            MatType mcyctype = MATAIJ; /*!< Cyclic derivative type. For default routines should be of type AIJ. */
            NormType ntype = NORM_INFINITY; /*!< The norm on \f$\mathbb{R}^{d}\f$. */
            std::string fname = "mpcout.h5"; /*!< Path to the output file. Only used if writeout is set. */
            slds::cont::mpcidxctx idxctx = slds::cont::mpcidxctx(); /*!< Options for bifurcation tracking and line search. */
            slds::cont::mpcswctx swctx = slds::cont::mpcswctx(); /*!< Options for branch switching. */
            slds::cont::mpcfctx fctx = slds::cont::mpcfctx(); /*!< Subroutines. */
            slds::cont::mpcslvctx slvctx = slds::cont::mpcslvctx(); /*!< Solvers. */
            slds::cont::mpcdatactx datactx = slds::cont::mpcdatactx(); /*!< Data structure storage. */
    };
    /* @class mpcpctx
     *
     * @brief Function context for multiparameter continuation projection.
     *
     * Function context for multiparameter continuation projection.
     *                                                                                                                            */
    class mpcpctx
    {
        public:
            int d; /*!< Problem dimension. */
            int k; /*!< Number of parameters. */
            std::vector<Vec> x; /*!< Center of the ball. */
            Vec vk; /*!< Predictor direction. */
            Mat Tx; /*!< Tangent space at the center. */
            slds::cont::mpcctx ctx; /*!< Options for multiparameter continuation. */
            PetscErrorCode (*feq)(SNES, Vec, Vec, void*);/*!< Function for projection of equilibria. May be NULL. */
            PetscErrorCode (*dfeq)(SNES, Vec, Mat, Mat, void*);/*!< Derivative for projection of equilibria. May be NULL. */
            void *feqctx; /*!< Function context for projection of equilibria. May be NULL. */
            PetscErrorCode (**fpd)(SNES, Vec, Vec, void*);/*!< Functions for projection of periodic difference equations.
                                                           *   May be NULL. */
            PetscErrorCode (**dfpd)(SNES, Vec, Mat, Mat, void*);/*!< Derivatives for projection of periodic difference
                                                                 *   equations. May be NULL. */
            void **fpdctx; /*!< Function contexts for projection of periodic difference equations. May be NULL. */
            int nb = 0; /*!< Number of branches tracked. */
            int bidx = 0; /*!< Index of the current branch. */
            int period = 0; /*!< Period of the solution. Only used for periodic difference equations. */
            int pdperiod = 0; /*!< Period of the difference equation. Only used for periodic difference equations. */
            int update = 0; /*!< Indicates the period dependendent data structures should be updated. Only used for
                             *   periodic difference equations. */
            int xswitch = 0; /*!< Option to indicate that the object is used in a branch switching context. */
            int chkdist = 0; /*!< Option to check the projection distance. */
            int distpass = 0; /*!< Result of the distance check. Only set if chkdist is set. */
            mpcpctx(int dd, int kk, std::vector<Vec> xx, Vec vvk, Mat Txx, slds::cont::mpcctx ctxx,
                    PetscErrorCode (*ffeq)(SNES, Vec, Vec, void*), PetscErrorCode (*dffeq)(SNES, Vec, Mat, Mat, void*),
                    void *feqctxx, PetscErrorCode (**ffpd)(SNES, Vec, Vec, void*),
                    PetscErrorCode (**dffpd)(SNES, Vec, Mat, Mat, void*), void **fpdctxx): d(dd), k(kk), x(xx), vk(vvk),
                                                                                           Tx(Txx), ctx(ctxx),
                                                                                           feq(ffeq), dfeq(dffeq),
                                                                                           feqctx(feqctxx), fpd(ffpd),
                                                                                           dfpd(dffpd), fpdctx(fpdctxx) {};
    };
    /*! @brief Computes the index function for an equilibrium fold bifurcation.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the index function for an equilibrium fold bifurcation. This is given by \f$ \det (Df_{x}) \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx Derivative of the operator.
     *  @param idx Index value. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode idxeqfold(Mat Dfx, int *idx, int d);
    /*! @brief Computes the index function for an equilibrium Hopf bifurcation.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning May be very expensive to compute.
     *
     *  Computes the index function for an equilibrium Hopf bifurcation. This is given by \f$ \det (2Df_{x}(\cdot)\text{id}_{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx Derivative of the operator.
     *  @param idx Index value. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param ctx Context provided by mpc.
     *                                                                                                                            */
    PetscErrorCode idxeqposre(Mat Dfx, int *idx, int d, mpcctx ctx);
    /*! @brief Computes the index function for a fold bifurcation.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the index function for a fold bifurcation. This is given by \f$ \det (Df_{x} - \text{id}_{d}) \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx Derivative of the period operator.
     *  @param idx Index value. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode idxpdfold(Mat Dfx, int *idx, int d);
    /*! @brief Computes the index function for a flip bifurcation.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the index function for a flip bifurcation. This is given by \f$ \det (Df_{x} + \text{id}_{d}) \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx Derivative of the period operator.
     *  @param idx Index value. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode idxpdflip(Mat Dfx, int *idx, int d);
    /*! @brief Computes the Morse index function for a bifurcation.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the morse index function for a bifurcation. This is given by
     *  \f$ | \{\lambda \in \sigma (Df_{x}) | \lambda \in \mathbb{C} \setminus \overline{B_{1}(0)}\} | \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx Derivative of the period operator.
     *  @param idx Index value. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param ctx Context provided by mpc.
     *                                                                                                                            */
    PetscErrorCode idxpdmorse(Mat Dfx, int *idx, int d, mpcctx ctx);
    /*! @brief Computes a vertex of the polyhedron whose vertices are given by all possible linear combinations of the basis
     *         vectors, scaled to be on the boundary of \f$ B_{r}(0)\f$.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes a vertex of the polyhedron whose vertices are given by all possible linear combinations of the basis
     *  vectors with coefficients \f$ [-n:n] \f$, excluding null. The total number of vertices is given by
     *  \f$ 3^{k} - 1 \f$, where \f$ k \f$ is the dimension of the space the polyhedron is in. All vertices are scaled so that
     *  the lie on the boundary of \f$ B_{r}(0) \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param vk     The vertex. This parameter is used to return the result.
     *  @param i      Index of the vertex, where \f$ 0 \leq i < (2n + 1)^{k}-1 \f$.
     *  @param r      Radius of the ball.
     *  @param n      The limit for \f$ [-n:n] \f$. See slds::cont::vertexbr().
     *  @param k      The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param ntype  The type of norm used for scaling.
     *                                                                                                                            */
    PetscErrorCode vertexbr(Vec vk, int i, PetscReal r, int n, int k, NormType ntype);
    /*! @brief Computes the derivative at a point.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the derivative of a point. In case of periodic points this routine returns the cyclic derivative or the period
     *  operator (constructed via iteration).
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x    Point where the derivative is evaluated.
     *  @param Dfx  Derivative. This parameter is used to return the result.
     *  @param ctx  Context provided by mpc.
     *                                                                                                                            */
    PetscErrorCode df(Vec x, Mat Dfx, mpcpctx ctx);
    /*! @brief Projects an initial guess onto a manifold.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Projects an initial guess onto a manifold orthonormal to the tangent space at a specified point. Intended for equilibria.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param px        Projected point. This parameter is used to return the result.
     *  @param Dfx       Derivative at the projected point. This parameter is used to return the result.
     *  @param t         Scale parameter to allow for projection along the line \f$x + tv\f$. Should be a value in \f$[0,1]\f$.
     *  @param returndfx Option to return the derivative.
     *  @param ctxx      Context provided by mpc.
     *                                                                                                                            */
    PetscErrorCode projecteqdefault(std::vector<Vec> px, Mat Dfx, PetscReal t, int returndfx, mpcpctx *ctxx);
    /*! @brief Projects an initial guess onto a manifold.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Projects an initial guess onto a manifold orthonormal to the tangent space at a specified point. Intended for periodic
     *  difference equations.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param px        Projected point. This parameter is used to return the result.
     *  @param Dfx       Derivative at the projected point. This parameter is used to return the result.
     *  @param t         Scale parameter to allow for projection along the line \f$x + tv\f$. Should be a value in \f$[0,1]\f$.
     *  @param returndfx Option to return the derivative.
     *  @param ctxx      Context provided by mpc.
     *                                                                                                                            */
    PetscErrorCode projectpddefault(std::vector<Vec> px, Mat Dfx, PetscReal t, int returndfx, mpcpctx *ctxx);
    /*! @brief Line search to find bifurcation points.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Finds a bifurcation point along the line connecting the center with a vertex on the boundary. A bifurcation point is
     *  assumed to be present. Since the vertex is provided by the projection context it is not explicitly required as an argument.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See SNESConvergedReason.
     *
     *  @param v       Vector to store points.
     *  @param Dfv     Matrix to store derivatives.
     *  @param Dfvp    Matrix to store derivatives without parameters.
     *  @param r       Radius of the ball.
     *  @param t       Solution. This parameter is used to return the result.
     *  @param idx0    Index value at the center.
     *  @param idx1    Index value at the vertex.
     *  @param idxtype The type of index search to be performed. (0 := Fold, 1 := Positive real part),
     *                 (0:= Fold, 1 := Flip, 2 := Morse).
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k       The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param pctx    Context for projection onto the manifold.
     *  @param outctx  Context for writing to multiparameter continuation output.
     *                                                                                                                            */
    PetscErrorCode lsidxdefault(std::vector<Vec> v, Mat Dfv, Mat Dfvp, PetscReal r, PetscReal *t, int idx0, int idx1, int idxtype,
                                int d, int k, slds::cont::mpcpctx pctx, slds::cont::mpcoutctx *outctx);
    /*! @brief Branch switching at bifurcation points.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *  @warning May be incompatible with custom routines.
     *
     *  Branch switching at bifurcation points.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Tx      Tangent space at the center.
     *  @param Tv      Tangent space at the vertex.
     *  @param r       Radius of the ball.
     *  @param bidxpp  Flag indicating that sw has found a new branch.
     *  @param idx0    Index value at the center.
     *  @param idx1    Index value at the vertex.
     *  @param idxtype The type of index search to be performed. (0 := Fold, 1 := Positive real part),
     *                 (0:= Fold, 1 := Flip, 2 := Morse).
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k       The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param pctx    Context for projection onto the manifold.
     *  @param outctx  Context for writing to multiparameter continuation output.
     *                                                                                                                            */
    PetscErrorCode swdefault(Mat Tx, Mat Tv, PetscReal r, int *bidxpp, int idx0, int idx1, int idxtype, int d, int k,
                             slds::cont::mpcpctx pctx, slds::cont::mpcoutctx *outctx);
    /*! @brief Numerical continuation.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Numerical continuation.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param feq    The function. SNES is ignored. Only used for equilibria.
     *  @param dfeq   The derivative. SNES is ignored. Only used for equilibria.
     *  @param fpd    The functions. SNES is ignored. Only used for periodic difference equations.
     *  @param dfpd   The derivatives. SNES is ignored. Only used for periodic difference equations.
     *  @param x0     Initial point on the manifold.
     *  @param a      Region of \f$d\f$ of \f$\mathbb{R}^{d}\f$ that should be covered. Left interval limits.
     *  @param b      Region of \f$d\f$ of \f$\mathbb{R}^{d}\f$ that should be covered. Right interval limits.
     *  @param p0     Period of the solution corresponding to the initial point. Only used for periodic difference equations.
     *  @param pd     Period of the difference equation. Only used for periodic difference equations.
     *  @param d      The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k      The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param outctx Output. This parameter is used to return the result.
     *  @param feqctx Function context that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *                to \f$f\f$ on evaluation. Only used for equilibria.
     *  @param fpdctx Function contexts that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *                to \f$f\f$ on evaluation. Only used for periodic difference equations.
     *  @param ctx    Context for multiparameter continuation.
     *                                                                                                                            */
    PetscErrorCode mpc(PetscErrorCode (*feq)(SNES, Vec, Vec, void*), PetscErrorCode (*dfeq)(SNES, Vec, Mat, Mat, void*),
                       PetscErrorCode (**fpd)(SNES, Vec, Vec, void*), PetscErrorCode (**dfpd)(SNES, Vec, Mat, Mat, void*), Vec x0,
                       PetscReal *a, PetscReal *b, int p0, int pd, int d, int k, slds::cont::mpcoutctx *outctx,
                       void *feqctx = NULL, void **fpdctx = NULL, slds::cont::mpcctx ctx = slds::cont::mpcctx());
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
