/**********************************************************************************************************************************/
/*! @file sldsslv.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Linear and nonlinear solvers for systems of equations.
 *
 *  Linear and nonlinear solvers for systems of equations. Additionally implements linear least squares for under- or
 *  overdetermined systems. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsdiff.hpp"
#include "sldsutil.hpp"
#include <cstdio>
#include <vector>
#include <petsc.h>
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::solve
 *
 *  @brief Linear and nonlinear solvers for systems of equations.
 *
 *  @todo Implement minsolve.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *  @warning Requires MUMPS (https://graal.ens-lyon.fr/MUMPS/index.php).
 *                                                                                                                                */
namespace slds::solve
{
    /*! @class minsolvectx
     *
     *  @brief Options for a linear minimum norm solver.
     *
     *  @warning Not all preconditioners work with all matrix types. MPIAIJ should work for most.
     *
     *  Set various options for minimum norm. All values have defaults so minsolvectx() provides an instance with
     *  sensible defaults for many applications.
     *                                                                                                                            */
    class minsolvectx
    {
        public:
            int safe = 1; /*!< Use MUMPS LU with safe pivoting as a direct solver. Useful for singular or ill-conditioned normal
                           *   equations. */
            TaoType taotype = TAOALMM; /*!< Optimization solver type. Should accept equality constraints. */
            KSPType ksptype = KSPGMRES; /*!< Linear solver. */
            PCType pctype = PCNONE; /*!< Preconditioner. */
            PetscReal zerotol = 0; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            MatColoringType colortype = MATCOLORINGJP; /*!< Matrix coloring algorithm used for finite differences. The resulting
                                                        *   matrix coloring indicates which rows can be worked on in parallel. */
            PetscReal taogatol; /*!< Absoulte convergence tolerance of the optimization solver. */
            PetscReal taogrtol; /*!< Relative convergence tolerance of the optimization solver. */
            PetscReal taogttol; /*!< Tolerance of the optimization solver. The solver stops if the norm of the gradient is
                                 *   reduced by this factor. */
            int taomaxit = PETSC_DEFAULT; /*!< Maximum iterations of the optimization solver. */
            int taomaxf = -1; /*!< Maximum function evaluations of the optimization solver. */
            PetscReal kspatol = PETSC_DEFAULT; /*!< Absoulte convergence tolerance of the linear solver. */
            PetscReal ksprtol = PETSC_DEFAULT; /*!< Relative convergence tolerance of the linear solver. */
            PetscReal kspdtol = PETSC_DEFAULT; /*!< Divergence tolerance of the linear solver. */
            int kspmaxit = PETSC_DEFAULT; /*!< Maximum iterations of the linear solver. */
            PetscErrorCode (*taooptions)(Tao, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                               *  optimization solver. */
            PetscErrorCode (*kspoptions)(KSP, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   linear solver. */
            PetscErrorCode (*pcoptions)(PC, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                            *   preconditioner. */
            void *taoctx = NULL; /*!< Function context for the user options routine of the optimization solver. Used to pass
                                   *  additional parameters if required. */
            void *kspctx = NULL; /*!< Function context for the user options routine of the linear solver. Used to pass additional
                                  *   parameters if required. */
            void *pcctx = NULL; /*!< Function context for the user options routine of the preconditioner. Used to pass additional
                                 *   parameters if required. */
    };
    /*! @class lsqsolvectx
     *
     *  @brief Options for a linear least squares solver.
     *
     *  @warning Not all preconditioners work with all matrix types. MPIAIJ should work for most.
     *
     *  Set various options for linear least squares. All values have defaults so lsqsolvectx() provides an instance with
     *  sensible defaults for many applications.
     *                                                                                                                            */
    class lsqsolvectx
    {
        public:
            int safe = 1; /*!< Use MUMPS LU with safe pivoting as a direct solver. Useful for singular or ill-conditioned normal
                           *   equations. */
            PetscReal fill = -1; /*!< Expected fill ratio of the product \f$ A^{T}A \f$. Negative value indicates that default
                                  *   options should be used.*/
            MatProductAlgorithm alg = MATPRODUCTALGORITHMDEFAULT; /*!< Matrix product algorithm used for computing
                                                                   *   \f$ A^{T}A \f$. */
            PCType pctype = PCNONE; /*!< Preconditioner. Only used if safe = 0. */
            PetscReal kspatol = 1e-6; /*!< Absolute convergence tolerance of the linear solver. */
            PetscReal ksprtol = 1e-6; /*!< Relative convergence tolerance of the linear solver. */
            PetscReal kspdtol = PETSC_DEFAULT; /*!< Divergence tolerance of the linear solver. */
            int kspmaxit = PETSC_DEFAULT; /*!< Maximum iterations of the linear solver. */
            PetscErrorCode (*kspoptions)(KSP, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   linear solver. */
            PetscErrorCode (*pcoptions)(PC, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                            *   preconditioner. */
            void *kspctx = NULL; /*!< Function context for the user options routine of the linear solver. Used to pass additional
                                  *   parameters if required. */
            void *pcctx = NULL; /*!< Function context for the user options routine of the preconditioner. Used to pass additional
                                 *   parameters if required. */
    };
    /*! @class lsolvectx
     *
     *  @brief Options for a linear solver.
     *
     *  @warning Not all solvers and preconditioners work with all matrix types. MPIAIJ should work for most.
     *
     *  Set various options for linear solve. All values have defaults so lsolvectx() provides an instance with sensible defaults
     *  for many applications.
     *                                                                                                                            */
    class lsolvectx
    {
        public:
            KSPType ksptype = KSPGMRES; /*!< Linear solver. */
            PCType pctype = PCNONE; /*!< Preconditioner. */
            PetscReal kspatol = 1e-6; /*!< Absolute convergence tolerance of the linear solver. */
            PetscReal ksprtol = 1e-6; /*!< Relative convergence tolerance of the linear solver. */
            PetscReal kspdtol = PETSC_DEFAULT; /*!< Divergence tolerance of the linear solver. */
            int kspmaxit = PETSC_DEFAULT; /*!< Maximum iterations of the linear solver. */
            PetscErrorCode (*kspoptions)(KSP, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   linear solver. */
            PetscErrorCode (*pcoptions)(PC, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                            *   preconditioner. */
            void *kspctx = NULL; /*!< Function context for the user options routine of the linear solver. Used to pass additional
                                  *   parameters if required. */
            void *pcctx = NULL; /*!< Function context for the user options routine of the preconditioner. Used to pass additional
                                 *   parameters if required. */
    };
    /*! @class fsolvectx
     *
     *  @brief Options for a nonlinear solver.
     *
     *  @warning Not all solvers and preconditioners work with all matrix types. MPIAIJ should work for most.
     *
     *  Set various options for nonlinear solve. All values have defaults so fsolvectx() provides an instance with sensible
     *  defaults for many applications.
     *                                                                                                                            */
    class fsolvectx
    {
        public:
            SNESType snestype = SNESNEWTONLS; /*!< Nonlinear solver. */
            KSPType ksptype = KSPGMRES; /*!< Linear solver. */
            PCType pctype = PCNONE; /*!< Preconditioner. */
            PetscReal zerotol = 0; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            MatColoringType colortype = MATCOLORINGJP; /*!< Matrix coloring algorithm used for finite differences. The resulting
                                                        *   matrix coloring indicates which rows can be worked on in parallel. */
            int monitorsnes = 0; /*!< Monitor solver convergence. */
            PetscReal snesatol = 1e-6; /*!< Absoulte convergence tolerance of the nonlinear solver. */
            PetscReal snesrtol = 1e-6; /*!< Relative convergence tolerance of the nonlinear solver. */
            PetscReal snesstol = 1e-6; /*!< Convergence tolerance of the nonlinear solver given by
                                        *   \f$||\delta x|| < \epsilon x\f$. */
            PetscReal snesdtol = PETSC_DEFAULT; /*!< Divergence tolerance of the nonlinear solver. */
            int snesmaxit = PETSC_DEFAULT; /*!< Maximum iterations of the nonlinear solver. */
            int snesmaxf = -1; /*!< Maximum function evaluations of the nonlinear solver. */
            PetscReal kspatol = 1e-6; /*!< Absoulte convergence tolerance of the linear solver. */
            PetscReal ksprtol = 1e-6; /*!< Relative convergence tolerance of the linear solver. */
            PetscReal kspdtol = PETSC_DEFAULT; /*!< Divergence tolerance of the linear solver. */
            int kspmaxit = PETSC_DEFAULT; /*!< Maximum iterations of the linear solver. */
            PetscErrorCode (*initnz)(Mat, int, PetscErrorCode (*)(int*, int*, int, int)) =
                slds::diff::initnzfull; /*!< Function to initialize the nonzero structure of the derivative. Used for
                                         *   finite differences. */
            PetscErrorCode (*split)(int*, int*, int, int) = slds::util::splitdefault; /*!< Function to determine the number of
                                                                                       *   rows owned by a process. Used
                                                                                       *   for finite differences. */
            PetscErrorCode (*snesoptions)(SNES, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                               *   nonlinear solver. */
            PetscErrorCode (*kspoptions)(KSP, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   linear solver. */
            PetscErrorCode (*pcoptions)(PC, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                            *   preconditioner. */
            PetscErrorCode (*monitor)(SNES, int, PetscReal, void*) = NULL; /*!< Optional user supplied monitoring routine. If
                                                                            *   this is NULL a basic built-in routine is used. */
            void *snesctx = NULL; /*!< Function context for the user options routine of the nonlinear solver. Used to pass
                                   *   additional parameters if required. */
            void *kspctx = NULL; /*!< Function context for the user options routine of the linear solver. Used to pass additional
                                  *   parameters if required. */
            void *pcctx = NULL; /*!< Function context for the user options routine of the preconditioner. Used to pass additional
                                 *   parameters if required. */
            void *monitorctx = NULL; /*!< Function context for the user provided monitoring routine. Used to pass additional
                                      *   parameters if required. */
    };
    /*! @brief Convergence test for nonlinear solver using the 1-norm.
     *
     *  Convergence test for nonlinear solver using the 1-norm. Modifies SNESConvergedDefault().
     *
     *  @warning PETSc must be initialized externally.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes   Nonlinear solver.
     *  @param it     Current iteration.
     *  @param xnorm  2-norm of current iterate. Ignored.
     *  @param snorm  2-norm of current step. Ignored.
     *  @param fnorm  2-norm of function. Ignored.
     *  @param reason Reason for convergence or divergence.
     *  @param ctx    Options for the convergence test. Ignored.
     *                                                                                                                            */
    PetscErrorCode snesconvergedl1(SNES snes, PetscInt it, PetscReal xnorm, PetscReal snorm, PetscReal fnorm,
                                   SNESConvergedReason *reason, void *cctx);
    /*! @brief Convergence test for nonlinear solver using the max-norm.
     *
     *  Convergence test for nonlinear solver using the max-norm. Modifies SNESConvergedDefault().
     *
     *  @warning PETSc must be initialized externally.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes   Nonlinear solver.
     *  @param it     Current iteration.
     *  @param xnorm  2-norm of current iterate. Ignored.
     *  @param snorm  2-norm of current step. Ignored.
     *  @param fnorm  2-norm of function. Ignored.
     *  @param reason Reason for convergence or divergence.
     *  @param ctx    Options for the convergence test. Ignored.
     *                                                                                                                            */
    PetscErrorCode snesconvergedlinf(SNES snes, PetscInt it, PetscReal xnorm, PetscReal snorm, PetscReal fnorm,
                                     SNESConvergedReason *reason, void *cctx);
    /*! @brief Solves \f$\min f(x) s.t. c(x) = 0, h(x) >= 0, l <= x <= u\f$.
     *
     *  Solves \f$\min f(x) s.t. c(x) = 0\f$ using PETSC's optimization solvers.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See KSPConvergedReason.
     *
     *  @param f     The function.
     *  @param gradf Gradient evaluation routine.
     *  @param hf    Hessian evalutaion routine.
     *  @param c     Equality constaint evaluation routine.
     *  @param dc    Jacobian of equality constraint evaluation routine.
     *  @param h     Inequality constaint evaluation routine.
     *  @param dh    Jacobian of inequality constraint evaluation routine.
     *  @param x     Intial guess for the solution. This parameter is used to to return the result.
     *  @param l     Lower bound on \f$x\f$.
     *  @param u     Upper bound on \f$x\f$.
     *  @param fctx  Function context that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *               to \f$f\f$ on evaluation.
     *  @param ctx   Options for the solver.
     *                                                                                                                            */
    PetscErrorCode minsolve(PetscErrorCode (*f)(SNES, Vec, PetscReal, void*), PetscErrorCode (*gradf)(SNES, Vec, Vec, void*),
                            PetscErrorCode (*hf)(SNES, Vec, Mat, Mat, void*), PetscErrorCode (*c)(SNES, Vec, Vec, void*),
                            PetscErrorCode (*dc)(SNES, Vec, Mat, Mat, void*), PetscErrorCode (*h)(SNES, Vec, Vec, void*),
                            PetscErrorCode (*dh)(SNES, Vec, Mat, Mat, void*), Vec x, Vec l, Vec u, void *fctx = NULL,
                            minsolvectx ctx = minsolvectx());
    /*! @brief Solves \f$\min ||Ax - b||\f$.
     *
     *  Solves \f$\min ||Ax - b||\f$ using LSQR.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See KSPConvergedReason.
     *
     *  @param A   The matrix \f$ A \in \mathbb{R}^{d \times k} \f$.
     *  @param x   The solution \f$ x \in \mathbb{R}^{d}\f$. This parameter is used to return the result.
     *  @param b   The right hand side \f$ b \in \mathbb{R}^{d}\f$.
     *  @param ctx Options for the solver.
     *                                                                                                                            */
    PetscErrorCode lsqsolve(Mat A, Vec x, Vec b, lsqsolvectx ctx = lsqsolvectx());
    /*!  @brief Solves \f$Ax = b\f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning Not all solvers are suitable for all problem types or matrix types.
     *
     *  Solves \f$Ax = b\f$ using a solver of type KSP. Uses GMRES with block Jacobi preconditioning by default.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See KSPConvergedReason.
     *
     *  @param A   The matrix \f$ A \in \mathbb{R}^{d \times k} \f$.
     *  @param x   The solution \f$ x \in \mathbb{R}^{d}\f$. This parameter is used to return the result.
     *  @param b   The right hand side \f$ b \in \mathbb{R}^{d}\f$.
     *  @param ctx Options for the solver.
     *                                                                                                                            */
    PetscErrorCode lsolve(Mat A, Vec x, Vec b, lsolvectx ctx = lsolvectx());
    /*! @brief Solves \f$f(x) = 0\f$ for a map \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning Not all solvers are suitable for all problem types or matrix types.
     *
     *  Solves \f$f(x) = 0\f$, for a map \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$, using a solver of type SNES.
     *  Uses Newton's method with cubic backtracking line search, GMRES and block Jacobi preconditioning by default.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See SNESConvergedReason.
     *
     *  @param f    The function \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *  @param df   Routine to compute the derivative \f$Dfx \in L(\mathbb{R}^{d})\f$. If this is NULL, finite difference
     *              approximation will be used.
     *  @param x0   Initial guess \f$x_{0} \in \mathbb{R}^{d}\f$ for the solution.
     *  @param x    The computed solution \f$x \in \mathbb{R}^{d}\f$. This parameter is used to return the result.
     *  @param fx   Function value \f$fx \in \mathbb{R}^{d}\f$ of \f$f\f$ at the computed solution
     *              \f$x \in \mathbb{R}^{d}\f$.
     *  @param Dfx  The derivative \f$Dfx \in L(\mathbb{R}^{d})\f$. This is parameter is used internally to store the
     *              derivative in each step.
     *  @param d    The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param fctx Function context that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *              to \f$f\f$ on evaluation.
     *  @param ctx  Options for the solver.
     *                                                                                                                            */
    PetscErrorCode fsolve(PetscErrorCode (*f)(SNES, Vec, Vec, void*), PetscErrorCode (*df)(SNES, Vec, Mat, Mat, void*), Vec x0,
                          Vec x, Vec fx, Mat Dfx, int d, void *fctx = NULL, fsolvectx ctx = fsolvectx());
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
