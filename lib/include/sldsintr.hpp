/**********************************************************************************************************************************/
/*! @file sldsintr.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Integration routines.
 *
 *  Integration routines for numeric integration. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <string>
#include <vector>
#include <petsc.h>
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::intr
 *
 *  @brief Integration routines.
 *
 *  @warning Requires PETSc (https://petsc.org/) for multiple precision via PetscReal.
 *                                                                                                                                */
namespace slds::intr
{
    /* Forward declarations */
    class h5node;
    class h5weight;
    /*! @class intrule
     *
     *  @brief Abstract representation of an integration rule for functions \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  Abstract representation of an integration rule for functions \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$. This
     *  class provides no explicit method for integration. It only stores the necessary components to perform integration when
     *  required. Integration is carried out via \f$ Qf := \sum_{i} w_{i}f(\gamma_{i})\f$, where \f$w_{i}\f$ are the weights
     *  of the rule and \f$\gamma_{i}\f$ are the nodes of the rule.
     *                                                                                                                            */
    class intrule
    {
        public:
                int k;                                /*!< Dimension \f$k\f$ of \$f\mathbb{R}^{k}\f$. */
                int d;                                /*!< Dimension \f$d\f$ of \$f\mathbb{R}^{d}\f$. */
                int n;                                /*!< Number of nodes in the integration grid. */
                std::vector<PetscReal> nodes;         /*!< Nodes in the integration grid. Array of size kn. */
                std::vector<PetscReal> weights;       /*!< Weights in the integration grid. Array of size n. */
    };
    /*! @brief Creates an instance of a product composite midpoint rule.
     *
     *  Creates an instance of a product composite midpoint rule for functions
     *  \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q  The resulting integration rule. This parameter is used to return the result.
     *  @param a  Left interval limits of the integration region. Array of size k.
     *  @param b  Right interval limits of the integration region. Array of size k.
     *  @param nk Number of subintervals in each dimension. The total number of nodes in each dimension is \f$ n_{i} \f$.
     *  @param k  The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode compositemidpoint(intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d);
    /*! @brief Creates an instance of a product composite trapeziodal rule.
     *
     *  Creates an instance of a product composite trapezoidal rule for functions
     *  \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q  The resulting integration rule. This parameter is used to return the result.
     *  @param a  Left interval limits of the integration region. Array of size k.
     *  @param b  Right interval limits of the integration region. Array of size k.
     *  @param nk Number of subintervals in each dimension. The total number of nodes in each dimension is \f$ n_{i} + 1 \f$.
     *  @param k  The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode compositetrapezoid(intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d);
    /*! @brief Creates an instance of a product composite Simposon's rule.
     *
     *  Creates an instance of a product composite Simposon's rule for functions
     *  \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q  The resulting integration rule. This parameter is used to return the result.
     *  @param a  Left interval limits of the integration region. Array of size k.
     *  @param b  Right interval limits of the integration region. Array of size k.
     *  @param nk Number of subintervals in each dimension. The total number of nodes in each dimension is \f$ 2n_{i} + 1 \f$.
     *  @param k  The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode compositesimpson(intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d);
    /*! @brief Creates an instance of a product composite Clenshaw-Curtis rule.
     *
     *  Creates an instance of a product composite Clenshaw-Curtis rule for functions
     *  \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q  The resulting integration rule. This parameter is used to return the result.
     *  @param a  Left interval limits of the integration region. Array of size k.
     *  @param b  Right interval limits of the integration region. Array of size k.
     *  @param nk Number of subintervals in each dimension. The total number of nodes in each dimension is
     *            \f$ (n_{r} + 1)n_{i} \f$.
     *  @param nr Orders of the Clenshaw-Curtis rule in each dimension. Must be even numbers.
     *  @param k  The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode compositecc(intrule *Q, PetscReal *a, PetscReal *b, int *nk, int *nr, int k, int d);
    /*! @brief Creates an instance of a product composite Gauss-Legendre rule.
     *
     *  @warning Since fastgl is limited to double precision quad precision is not (properly) supported.
     *
     *  Creates an instance of a product composite Gauss-Legendre rule for functions
     *  \f$f: \mathbb{R}^{k} \rightarrow \mathbb{R}^{d}\f$. Based on fastgl
     *  (https://people.sc.fsu.edu/~jburkardt/cpp_src/fastgl/fastgl.html).
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q  The resulting integration rule. This parameter is used to return the result.
     *  @param a  Left interval limits of the integration region. Array of size k.
     *  @param b  Right interval limits of the integration region. Array of size k.
     *  @param nk Number of subintervals in each dimension. The total number of nodes in each dimension is
     *            \f$ n_{r}n_{i} \f$.
     *  @param nc Orders of the Gauss-Legendre rule in each dimension. Must be even numbers.
     *  @param k  The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    PetscErrorCode compositegl(intrule *Q, PetscReal *a, PetscReal *b, int *nk, int *nr, int k, int d);
    /*! @brief Writes the nodes and weights of an integration rule to a HDF5 file.
     *
     *  Writes the nodes and weights of an integration rule to a HDF5 file.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Q The integration rule.
     *  @param f Output file. May be a filename, a relative path or an absolute path. Should have the extension '.h5'.
     *                                                                                                                            */
    PetscErrorCode writeh5(intrule *Q, std::string f = "intrule.h5");
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
