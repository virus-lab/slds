/**********************************************************************************************************************************/
/*! @file sldsla.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Linear algebra routines.
 *
 *  Linear algebra routines. Additionally implements solvers for eigenvalue problems. Based on PETSc (https://petsc.org/) and
 *  SLEPc (https://slepc.upv.es/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <vector>
#include <petsc.h>
#include <slepc.h>
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::la
 *
 *  @brief Linear algebra routines.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *  @warning Requires SLEPc (https://slepc.upv.es/).
 *  @warning Requires MUMPS (https://graal.ens-lyon.fr/MUMPS/index.php).
 *                                                                                                                                */
namespace slds::la
{
    /*! @class eigenctx
     *
     *  @brief Options for an eigenvalue problem solver.
     *
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *  @warning By default this computes the largest eigenvalues. If other eigenvalues the user must set select to the desired
     *           value, e.g. EPS_SMALLEST_MAGNITUDE for the smallest eigenvalues. See EPSWhich.
     *  @warning Returning an invariant subspace requires the computed eigenvalues to be real.
     *
     *  Set various options for eigenvalue problems. All values have defaults so eigenctx() provides an instance with sensible
     *  defaults. In many cases the user may want to set select to the desired value. See EPSWhich.
     *                                                                                                                            */
    class eigenctx
    {
        public:
            EPSType epstype = EPSKRYLOVSCHUR; /*!< Eigenvalue problem solver. */
            EPSWhich select = EPS_LARGEST_MAGNITUDE; /*!< Eigenvalues to compute. */
            EPSProblemType type = EPS_NHEP; /*!< Eigenvalue problem type. Default non-hermitian. */
            int maxit = PETSC_DEFAULT; /*!< Maximum iterations of the eigenvalue problem solver. */
            int mpd = -1; /*!< Maximum dimension for projected problems. A negative value indictates default options should be
                           *   used. */
            int abs = 0; /*!< Use absolute instead of relative error for convergence testing. */
            int trueres = 0; /*!< Force the solver to compute the true residual instead of an estimate. */
            int balance = 0; /*!< Enable balancing for non hermitian problems. */
            int harmonic = 1; /*!< Use harmonic extraction for EPS_TARGET_MAGNITUDE and EPS_TARGET_REAL. */
            int returninvsub = 0; /*!< Extract an orthonormal basis for the computed invariant subspace instead eigenvectors. */
            int deflate = 0; /*!< Specifies if a subspace should be set for deflation. If this option is set a suitable vector
                              *   must be provided in via dsub. */
            int setcomparemagnitude = 0; /*!< Sets the selection criterion to smallest |v| - t, where t is the target value.
                                          *   Overrides the select option. */
            PetscReal target = 0; /*!< Target value for selection. */
            PetscReal tol = 1e-6; /*!< Tolerance of the eigenvalue problem solver. */
            std::vector<Vec> *dsub = NULL; /*!< Deflation subspace for the solver. */
            PetscErrorCode (*epsoptions)(EPS, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   eigenvalue problem solver. */
            void *epsctx = NULL; /*!< Function context for the user options routine of the eigenvalue problem solver. Used to pass
                                  *   additional parameters if required. */
            PetscErrorCode (*vecset)(Vec*, int, void*) = slds::util::vecsetdefault; /*!< Routine for vector creation. */
            void *vecctx = NULL; /*!< Options context for vector creation. */
    };
    /*! @class svdctx
     *
     *  @brief Options for a singular value problem solver.
     *
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Set various options for singular value problems. All values have defaults so svdctx() provides an instance with sensible
     *  defaults.
     *                                                                                                                            */
    class svdctx
    {
        public:
            SVDType svdtype = SVDCYCLIC; /* Singular value problem solver. */
            EPSType epstype = EPSKRYLOVSCHUR; /*!< Eigenvalue problem solver. */
            SVDWhich select = SVD_LARGEST; /*!< Singular triplets to compute. */
            int maxit = PETSC_DEFAULT; /*!< Maximum iterations of the eigenvalue problem solver. */
            int abs = 1; /*!< Use absolute instead of relative error for convergence testing. */
            int explicitmat = 0; /*!< Computes matrices explicitly instead of handling them implictly. */
            int trueres = 0; /*!< Force the eigenvalue solver to compute the true residual instead of an estimate. */
            int epstarget = 0; /*!< Option to set a target magnitude for the eigenvalue solver. */
            PetscReal target = 0; /*!< Target value for the eigenvalue solver. */
            PetscReal tol = 1e-6; /*!< Tolerance of the singular problem solver. */
            PetscErrorCode (*svdoptions)(SVD, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   singular problem solver. */
            void *svdctx = NULL; /*!< Function context for the user options routine of the singular value problem solver. Used
                                  *   to pass additional parameters if required. */
            PetscErrorCode (*epsoptions)(EPS, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                              *   eigenvalue problem solver. */
            void *epsctx = NULL; /*!< Function context for the user options routine of the eigenvalue problem solver. Used to pass
                                  *   additional parameters if required. */
            PetscErrorCode (*vecset)(Vec*, int, void*) = slds::util::vecsetdefault; /*!< Routine for vector creation. */
            void *vecctx = NULL; /*!< Options context for vector creation. */
    };
    /*! @class epsnullctx
     *
     *  @brief Options for an eigenvalue based nullspace solver.
     *
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Set various options for eigenvalue based nullspace solvers. All values have defaults so epsnullctx() provides an instance
     *  with sensible defaults.
     *                                                                                                                            */
    class epsnullctx
    {
        public:
            int useata = 1; /*!< Option to compute the nullspace via \f$N(A^{T}A)\f$. */
            int usetarget = 0; /*!< Option to use zero as a target value for extraction. Useful if zero is interior to the
                                *   spectrum. */
            int useinvsub = 1; /*!< Option to compute an invariant subspace instead of eigenvectors. */
            int orthonormalize = 1; /*!< Option to enable orthonormalization of the computed vectors. */
            int setmat = 1; /*!< Option to store the vectors in the output matrix. This should only be disabled if a vector
                             *   to store the computed subspace is passed via s. Otherwise the routine will not return a valid
                             *   result. */
            PetscReal fill = -1; /*!< Expected fill ratio of the product \f$ A^{T}A \f$. Negative value indicates that default
                                  *   options should be used. */
            BV *bv = NULL; /*!< Optional data structure for orthonormalization. If this is NULL it will be created. */
            Mat *AtA = NULL; /*!< Optional matrix to store AtA. If this is NULL it will be created. */
            std::vector<Vec> *s = NULL; /*!< Optional vector to store the computed subspace. If this is NULL it will be
                                         *   created. */
            MatProductAlgorithm alg = MATPRODUCTALGORITHMDEFAULT; /*!< Matrix product algorithm used for computing
                                                                   *   \f$ A^{T}A \f$. */
            eigenctx ctx = eigenctx(); /*!< Options for the solver. Select is ignored and always set to EPS_SMALLEST_MAGNITUDE.
                                        *   Always uses absolute error for convergence testing regardless of what the user set. */
    };
    /*! @brief Product of two matrices A and B.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the matrix product \f$ AB = C \f$ using PETSc.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix A.
     *  @param B       The matrix B.
     *  @param C       Product matrix C.
     *  @param type    Matrix product type.
     *  @param alg     Matrix product algorithm.
     *  @param fill    Expected fill ratio nnz(C)/(nnz(A) + nnz(B)). Negative value indicates that default options should be used.
     *  @param cexists Option to indicate that the product matrix C already exists and should not be created.
     *                                                                                                                            */
    PetscErrorCode matmult(Mat A, Mat B, Mat *C, MatProductType type, MatProductAlgorithm alg = MATPRODUCTALGORITHMDEFAULT,
                           PetscReal fill = -1, int cexists = 0);
    /*! @brief Sorting function to sort eigenvalues by distance to a target magnitude.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Sorting function to sort eigenvalues by distance to a target magnitude. For use with EPSSetEigenvalueComparison.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param v      Real part of the first eigenvalue.
     *  @param iv     Imaginary part of the first eigenvalue.
     *  @param w      Real part of the second eigenvalue.
     *  @param iw     Imaginary part of the second eigenvalue.
     *  @param result Result indicating which eigenvalue is preferred. This is parameter is used to return the result.
     *  @param ctx    Function context. Should be of type EPS.
     *                                                                                                                            */
    PetscErrorCode comparetargetmagnitude(PetscScalar v, PetscScalar iv, PetscScalar w, PetscScalar iw, PetscInt *result,
                                          void *ctx);
    /*! @brief Eigenpairs of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Computes the first \f$n\f$ eigenpairs \f$ (\lambda, v) \f$ of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$ using
     *  Krylov Schur methods by default. The eigenpairs returned are determined by the selection criterion. For supported options
     *  see EPSWhich.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A      The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param evals  Real parts of the computed eigenvalues. This is parameter is used to return the result.
     *  @param ievals Imaginary parts of the computed eigenvalues. This is parameter is used to return the result.
     *  @param evecs  Real parts of the computed eigenvectors. This is parameter is used to return the result.
     *  @param ievecs Imaginary parts of the computed eigenvectors. This is parameter is used to return the result.
     *  @param d      The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param n      The number of eigenpairs to compute.
     *  @param ctx    Options for the solver.
     *                                                                                                                            */
    PetscErrorCode eigen(Mat A, std::vector<PetscReal> *evals, std::vector<PetscReal> *ievals, std::vector<Vec> *evecs,
                         std::vector<Vec> *ievecs, int d, int n, eigenctx ctx = eigenctx());
    /*! @brief Singular triplets of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *  @warning Experimental.
     *
     *  Computes the first \f$n\f$ singular triplets \f$ (u, \sigma, v) \f$ of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  The singular triplets returned are determined by the selection criterion. For supported options see SVDWhich.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A   The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param s   Singular values. This is parameter is used to return the result.
     *  @param u   Left singular vectors. This is parameter is used to return the result.
     *  @param v   Right singular vectors. This is parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param n   The number of singular triplets to compute.
     *  @param ctx Options for the solver.
     *                                                                                                                            */
    PetscErrorCode svd(Mat A, std::vector<PetscReal> *s, std::vector<Vec> *u, std::vector<Vec> *v, int d, int n,
                       svdctx ctx = svdctx());
    /*! @brief Computes the largest or smallest eigenvalue of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Computes the largest or smallest eigenvalue of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$ using Krylov Schur methods
     *  by default.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param v       Real part of the eigenvalue. This is parameter is used to return the result.
     *  @param iv      Imaginary part of the eigenvalue. This is parameter is used to return the result.
     *  @param ctx     Options for the solver. Select is ignored and always set to EPS_LARGEST_MAGNITUDE.
     *                                                                                                                            */
    PetscErrorCode spectralr(Mat A, PetscReal *v, PetscReal *iv, eigenctx ctx = eigenctx());
    /*! @brief Computes the Morse or positive real part index of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *  @warning In case the eigenvalues are computed with deflation the specified solver must support the routines
     *           EPSGetInvariantSubspace() and EPSSetDeflationSpace().
     *
     *  Computes the Morse or positive real part index index of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  Repeatedly computes the largest eigenvalues or largest real parts using deflation until one inside the unit circle
     *  or one with negative real part is found. An initial guess for the depth may be supplied to speed up this process.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A          The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param idx        Morse index.
     *  @param d          The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param evmax      Maximum number of eigenvalues that may computed before the computation is aborted. Specified as a
     *                    percentage of the total amount of eigenvalues.
     *  @param step0      Initial guess for the depth of the solver. A good guess should be greater than index + 1.
     *  @param step       Stepsize for the computation. If more eigenvalues are needed the solver will compute the
     *                    next step eigenvalues.
     *  @param computeall Option to compute all eigenvalues in one step. Intended for use with small problems and appropriate
     *                    eigensolvers such as LAPACK.
     *  @param idxtype    Index to be computed (0 := Positive real part, 1 := Morse).
     *  @param ctx        Options for the solver.
     *                                                                                                                            */
    PetscErrorCode morseposre(Mat A, int *idx, int d, PetscReal evmax = 0.1, int step0 = 1, int step = 2, int computeall = 0,
                              int idxtype = 0, eigenctx ctx = eigenctx());
    /*! @brief Nullspace of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$. Requires the dimension to be known.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *  @warning Currently under developtment.
     *
     *  Computes an orthonormal basis of the \f$k\f$-dimensional nullspace of \f$ A \in \mathbb{R}^{d \times d} \f$ by computing
     *  the partial singular value decomposition corresponding to the smallest singular values. The dimension \f$k\f$ of the
     *  nullspace is assumed to be known. This allows for efficient computation of the nullspace since only the \f$k\f$ smallest
     *  singular triplets \f$ (u, \sigma, v) \f$ have to be computed.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param B       Orthogonal matrix \f$ A \in \mathbb{R}^{d \times k} \f$ representing an orthonormal basis for the
     *                 nullspace.
     *                 This is parameter is used to return the result.
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k       The dimension \f$k\f$ of the nullspace.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *  @param nulltol Nullspace tolerance. Any singular value with absolute value less than this is considered zero.
     *  @param ctxx    Options for the solver. Expected to be of type svdctx. Select is ignored and always set to SVD_SMALLEST.
     *                 Always uses absolute error for convergence testing regardless of what the user set.
     *                                                                                                                            */
    PetscErrorCode knull(Mat A, Mat B, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctxx);
    /*! @brief Nullspace of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$. Requires the dimension to be known.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Computes an orthonormal basis of the \f$k\f$-dimensional nullspace of \f$ A \in \mathbb{R}^{d \times d} \f$ by computing
     *  the invariant subspace associated with the zero eigenvalues. Uses Krylov Schur methods by default. The dimension
     *  \f$k\f$ of the nullspace is assumed to be known. This allows for efficient computation of the nullspace since only the
     *  \f$k\f$ smallest eigenpairs \f$ (\lambda, v) \f$ have to be computed.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param B       Orthogonal matrix \f$ A \in \mathbb{R}^{d \times k} \f$ representing an orthonormal basis for the
     *                 nullspace.
     *                 This is parameter is used to return the result.
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k       The dimension \f$k\f$ of the nullspace.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *  @param nulltol Nullspace tolerance. Any eigenvalue with absolute value less than this is considered zero.
     *  @param useata  Option to compute the nullspace via \f$N(A^{T}A)\f$.
     *  @param ctxx    Options for the solver. Expected to be of type epsnullctx.
     *                                                                                                                            */
    PetscErrorCode knull2(Mat A, Mat B, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctxx);
    /*! @brief Determinant of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the determiant of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$ via LU factorization.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A   The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param val The value of the determinant. This parameter is used to return the result.
     *                                                                                                                            */
    PetscErrorCode det(Mat A, PetscReal *val);
    /*! @brief Computes the 2-norm for a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Computes the 2-norm for a matrix \f$ A \in \mathbb{R}^{d \times d} \f$ via \f$ \sqrt{r(A^{T}A)}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A    The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param norm The 2-norm of the matrix. This parameter is used to return the result.
     *  @param alg  Matrix product algorithm used for computing \f$ A^{T}A \f$.
     *  @param fill Expected fill ratio of the product \f$ A^{T}A \f$. Negative value indicates that default options should be
     *              used.
     *  @param ctx  Options for the solver. Select is ignored and always set to EPS_SMALLEST_MAGNITUDE or EPS_LARGEST_MAGNITUDE.
     *              If the smallest eigenvalue should be computed this always uses absolute error for convergence testing
     *              regardless of what the user set.
     *                                                                                                                            */
    PetscErrorCode norm2(Mat A, PetscReal *norm, MatProductAlgorithm alg = MATPRODUCTALGORITHMDEFAULT, PetscReal fill = -1,
                         eigenctx ctx = eigenctx());
    /*! @brief Orthonormalizes a collection of vectors \f$ x \in \mathbb{R}^{d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning SLEPc must be initialized externally.
     *  @warning PETSc should be compiled without complex support. Complex support changes the way SLEPc behaves,
     *           possibly producing unexpected results.
     *
     *  Orthonormalizes a collection of vectors \f$ x \in \mathbb{R}^{d} \f$ using SLEPc.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x  The vectors \f$ x \in \mathbb{R}^{d} \f$. This parameter is used to return the result.
     *  @param d  The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param n  The number of vectors.
     *  @param bv Optional data structure for orthonormalization. If this is NULL it will be created.
     *                                                                                                                            */
    PetscErrorCode orthonormalize(std::vector<Vec> *x, int d, int n, BV *bv = NULL);
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
