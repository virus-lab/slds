/**********************************************************************************************************************************/
/*! @file sldsutil.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Miscellaneous utilities.
 *
 *  Miscellaneous utilities. Additionally implements matrix creation routines for various applications.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <vector>
#include <petsc.h>
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::util
 *
 *  @brief Miscellaneous utilities.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *                                                                                                                                */
namespace slds::util
{
    /*! @class vecctx
     *
     *  @brief Function context for creating vectors.
     *
     *  Function context for creating vectors. Used to specify information relevant for vector creation.
     *                                                                                                                            */
    class vecctx
    {
        public:
            VecType type = VECSTANDARD; /* Vector type. May be any of PETSc's supported types. */
            int local = 0; /*!< Use the local instead of the global communicator. */
            vecctx() {};
            vecctx(VecType t): type(t) {};
            vecctx(VecType t, int l): type(t), local(l) {};
    };
    /*! @class matctx
     *
     *  @brief Function context for creating matrices.
     *
     *  Function context for creating matrices. Used to specify information relevant for matrix creation. For sequential
     *  AIJ matrices only dnz or dnnz is used. See MatXXXAIJSetPreallocation().
     *                                                                                                                            */
    class matctx
    {
        public:
            MatType type = MATDENSE; /* Matrix type. May be any of PETSc's supported types. */
            int local = 0; /*!< Use the local instead of the global communicator. */
            int noset = 0; /*!< Option to skip matrix setup. Intended to be used when the matrix preallocation is done later. */
            matctx() {};
            matctx(MatType t): type(t) {};
            matctx(MatType t, int l): type(t), local(l) {};
    };
    /*! @brief Gets the rows (or columns) of a matrix belonging to an MPI process.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector context must be set properly.
     *
     *  Gets the rows (or columns) of a matrix belonging to an MPI process under the default splitting scheme.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param n0   Index of the first row (or column) belonging to the process.
     *  @param n1   One more than the index of the last row (or column) belonging to the process.
     *  @param n    Total number of rows (or columns).
     *  @param rank The process rank in the communicator.
     *                                                                                                                            */
    PetscErrorCode splitdefault(int *n0, int *n1, int n, int rank);
    /*! @brief Sets up a vector for use with PETSc.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector context must be set properly.
     *
     *  Sets up a vector for use with PETSc. Always sets up the vector on PETSC_COMM_WORLD.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x   The vector.
     *  @param n   Vector size.
     *  @param ctx Function context for vector creation.
     *                                                                                                                            */
    PetscErrorCode vecsetdefault(Vec *x, int n, void* ctx);
    /*! @brief Sets up a matrix for use with PETSc.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The matrix context must be set properly.
     *
     *  Sets up a matrix for use with PETSc. For sparse AIJ matrices this routine optionally preallocates memory as well.
     *  Always sets up the matrix on PETSC_COMM_WORLD.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A   The matrix.
     *  @param n   Number of matrix rows.
     *  @param m   Number of matrix columns.
     *  @param ctx Function context for matrix creation.
     *                                                                                                                            */
    PetscErrorCode matsetdefault(Mat *A, int n, int m, void* ctx);
    /*! @brief Sets a subvector of a vector \f$ x \in \mathbb{R}^{d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Sets a subvector of a vector \f$ x \in \mathbb{R}^{d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x       The vector \f$ x \in \mathbb{R}^{d} \f$. This parameter is used to return the result.
     *  @param y       The subvector.
     *  @param n0      The source start index.
     *  @param n1      The source end index.
     *  @param p0      The target start index.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode subvecset(Vec x, Vec y, int n0, int n1, int p0, PetscReal zerotol);
    /*! @brief Gets a subvector of a vector \f$ x \in \mathbb{R}^{d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Gets a subvector of a vector \f$ x \in \mathbb{R}^{d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x       The vector \f$ x \in \mathbb{R}^{d} \f$.
     *  @param y       The subvector. This parameter is used to return the result.
     *  @param n0      The source start index.
     *  @param n1      The source end index.
     *  @param p0      The target start index.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode subvecget(Vec x, Vec y, int n0, int n1, int p0, PetscReal zerotol);
    /*! @brief Sets a submatrix of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Sets a submatrix of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$. This parameter is used to return the result.
     *  @param B       The submatrix.
     *  @param n0      The source start row index.
     *  @param m0      The source start column index.
     *  @param n1      The source end row index.
     *  @param m1      The source end column index.
     *  @param p0      The target start row index.
     *  @param q0      The target start column index.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode submatset(Mat A, Mat B, int n0, int m0, int n1, int m1, int p0, int q0, PetscReal zerotol);
    /*! @brief Gets a submatrix of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Gets a submatrix of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *  @param B       The submatrix. This parameter is used to return the result.
     *  @param n0      The source start row index.
     *  @param m0      The source start column index.
     *  @param n1      The source end row index.
     *  @param m1      The source end column index.
     *  @param p0      The target start row index.
     *  @param q0      The target start column index.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode submatget(Mat A, Mat B, int n0, int m0, int n1, int m1, int p0, int q0, PetscReal zerotol);
    /*! @brief Sets rows of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Sets rows of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$. This parameter is used to return the result.
     *  @param x       The row vectors \f$ x \in \mathbb{R}^{d} \f$.
     *  @param idxi    The row indices.
     *  @param n       The number of rows to set.
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode matrowsset(Mat A, std::vector<Vec> *x, std::vector<int> *idxi, int n, int d, PetscReal zerotol);
    /*! @brief Sets columns of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Sets columns of a matrix \f$ A \in \mathbb{R}^{d \times d} \f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A       The matrix \f$ A \in \mathbb{R}^{d \times d} \f$. This parameter is used to return the result.
     *  @param x       The column vectors \f$ x \in \mathbb{R}^{d} \f$.
     *  @param idxj    The column indices.
     *  @param n       The number of columns to set.
     *  @param d       The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param zerotol Zero tolerance. Any value with absolute value less than this is considered zero.
     *                                                                                                                            */
    PetscErrorCode matcolumnsset(Mat A, std::vector<Vec> *x, std::vector<int> *idxj, int n, int d, PetscReal zerotol);
    /*! @brief Adds a scalar to the diagonal of a (sub)matrix.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Adds a scalar to the diagonal of a (sub)matrix.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param A The matrix A.
     *  @param v The value to add.
     *  @param d The dimension \f$d\f$ of the (sub)matrix.
     *                                                                                                                            */
    PetscErrorCode matdiagonaladd(Mat A, PetscReal v, int d);
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
