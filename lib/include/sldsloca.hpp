/**********************************************************************************************************************************/
/*! @file sldsloca.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2024
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of local attractors.
 *
 *  Module for numerical analysis of local attractors. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2023  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <random>
#include <string>
#include <vector>
#include <petsc.h>
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::loca
 *
 *  @brief Module for numerical analysis of local attractors.
 *
 *  @warning Requires PETSc (https://petsc.org/) for multiple precision via PetscReal.
 *                                                                                                                                */
namespace slds::loca
{
    /* Forward declarations */
    class h5box;
    /* @class box
     *
     * @brief Box in \f$\mathbb{R}^{d}\f$.
     *
     * Class representing a box in \f$\mathbb{R}^{d}\f$.
     *                                                                                                                            */
    class box
    {
        public:
            int d; /*!< Problem dimension. */
            std::vector<PetscReal> a; /*!< Lower interval bounds of the box. */
            std::vector<PetscReal> b; /*!< Upper interval bounds of the box. */
            box() {};
            box(int dd, std::vector<PetscReal> aa, std::vector<PetscReal> bb): d(dd), a(aa), b(bb) {};
    };
    /*! @class locactx
     *
     *  @brief Options for local attractor computation.
     *
     *  Provides various options for local attractor computation. All values have defaults so locactx() provides an instance with
     *  sensible defaults.
     *                                                                                                                            */
    class locactx
    {
        public:
            int verbose = 0; /*!< Option to print various updates to stdout. */
            int ns = 16; /*!< Number of subdivision steps. */
            int nsinit = 1; /*!< Number of initial subdivision steps. Used to ensure each process is loaded from the start. Should
                             *   give approximately as many boxes as the number of processes. */
            int nt = 100; /*!< Number of test points per box. */
    };
    /*! @brief Samples a random test point in a box in \f$\mathbb{R}^{d}\f$.
     *
     *  Samples a random test point in a box in \f$\mathbb{R}^{d}\f$ using a uniform distribution.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param box The box in \f$\mathbb{R}^{d}\f$.
     *  @param tx  The test point. This parameter is used to return the result.
     *                                                                                                                            */
    PetscErrorCode boxtpoint(slds::loca::box box, Vec tx);
    /*! @brief Computes the box a point \f$x \in \mathbb{R}^{d}\f$ is contained in. The index is assumed to be initialized.
     *
     *  Computes the box a point \f$x \in \mathbb{R}^{d}\f$ is contained in.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x   The point \f$x \in \mathbb{R}^{d}\f$.
     *  @param idx The box containing \f$x \in \mathbb{R}^{d}\f$ represented by its index. This parameter is used to return the
     *             result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param n   The number of subdivisions in each axis.
     *  @param u   The initial box before subdivision.
     *                                                                                                                            */
    PetscErrorCode getbox(Vec x, std::vector<int> *idx, int d, std::vector<int> n, slds::loca::box u);
    /*! @brief Computes the box corresponding to a given index.
     *
     *  Computes the box corresponding to a given index. The box is assumed to be initialized.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @parax idx The index of the box.
     *  @param box The box in \f$\mathbb{R}^{d}\f$. This parameter is used to return the result.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param n   The number of subdivisons in each axis.
     *  @param u   The initial box before subdivision.
     *                                                                                                                            */
    PetscErrorCode idxbox(std::vector<int> idx, slds::loca::box *box, int d, std::vector<int> n, slds::loca::box u);
    /*! @brief Writes a list of boxes to a HDF5 file.
     *
     *  Writes a list of boxes to a HDF5 file.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param boxes The boxes.
     *  @param f     Output file. May be a filename, a relative path or an absolute path. Should have the extension '.h5'.
     *                                                                                                                            */
    PetscErrorCode writeh5(std::vector<slds::loca::box> *boxes, std::string f = "boxes.h5");
    /*! @brief Computes the local attractor of a box \f$U \subset \mathbb{R}^{d}\f$.
     *
     *  Computes the local attractor of a box \f$U \subset \mathbb{R}^{d}\f$ for a difference equation \f$x' = f(x)\f$ with a map
     *  \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param f    The map \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$. This function should be evaluated on the local
     *              process and will be given a sequential vector of type SEQVEC when called.
     *  @param u    The initial box \f$U \subset \mathbb{R}^{d}\f$.
     *  @param au   The boxes covering the local attractor of \f$U \subset \mathbb{R}^{d}\f$. This parameter is used to return
     *              the result.
     *  @param d    The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param fctx Function context that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *              to \f$f\f$ on evaluation.
     *  @param ctx  Context for local attractor computation.
     *                                                                                                                            */
    PetscErrorCode loca(PetscErrorCode (*f)(Vec, Vec, void*), slds::loca::box u, std::vector<slds::loca::box> *au, int d,
                        void *fctx = NULL, slds::loca::locactx ctx = slds::loca::locactx());
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
