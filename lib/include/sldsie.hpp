/**********************************************************************************************************************************/
/*! @file sldsie.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of integral equations.
 *
 *  Module for numerical analysis of integral equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <vector>
#include <petsc.h>
#include <slepc.h>
#include "sldsintr.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::ie
 *
 *  @brief Module for numerical analysis of integrodifference equations.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *  @warning Requires SLEPc (https://slepc.upv.es/).
 *  @warning Requires MUMPS (https://graal.ens-lyon.fr/MUMPS/index.php).
 *                                                                                                                                */
namespace slds::ie
{
    /*! @class ukernelctx
     *
     *  @brief Function context for Urysohn operator kernels.
     *
     *  Function context for Urysohn operator kernels
     *  \f$f: \mathbb{R}^{k} \times \mathbb{R}^{k} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *                                                                                                                            */
    class ukernelctx
    {
        public:
            int k = 0; /*!< Number of parameters for the kernel. */
            void *fctx = NULL; /* Function context that can be used to pass additional arguments to \f$f\f$ if required. */
            std::vector<PetscReal> *lambda = NULL; /*!< Parameters for the kernel. */
    };
    /*! @class urysohnctx
     *
     *  @brief Function context for Urysohn operators.
     *
     *  Function context for Uryson equations with kernel
     *  \f$f: \mathbb{R}^{k} \times \mathbb{R}^{k} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$. The user should always
     *  set \f$ f \f$ and \f$ Q \f$ after calling urysohnctx().
     *                                                                                                                            */
    class urysohnctx
    {
        public:
            int parameters = 0; /* Specifies if derivatives of kernel parameters should be computed. */
            PetscReal zerotol = 0; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            Vec *uloc = NULL; /*!< Option to supply a scatter vector. If this is NULL a scatter vector will be constructed. */
            VecScatter *scatterctx = NULL; /*!< Option to supply a scatter context. If this is NULL a scatter vector will be
                                            *   constructed. */
            slds::intr::intrule *Q = NULL; /*!< Integration rule used for numerical integration. */
            PetscErrorCode (*f)(const PetscReal*, const PetscReal*, const PetscReal*, PetscReal*, int, int,
                                ukernelctx) = NULL; /*!< The kernel. */
            PetscErrorCode (*d3f)(const PetscReal*, const PetscReal*, const PetscReal*, PetscReal*, int, int,
                                  ukernelctx) = NULL; /*!< The third partial derivative \f$ D_{3}f \f$ of the kernel. Should
                                                       *   return the matrix in row major order. */
            PetscErrorCode (**dlf)(const PetscReal*, const PetscReal*, const PetscReal*, PetscReal*, int, int,
                                   ukernelctx) = NULL; /*!< The partial derivatives \f$ D_{\lambda_{i}}f \f$ of the kernel. */
            ukernelctx fctx = ukernelctx(); /*!< Function context that can be used to pass additional arguments to \f$f\f$
                                             *   if required. */
    };
    /*! @brief Evaluates an Urysohn operator.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning \f$f\f$ and \f$Q\f$ must be set in uctx.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Evaluates an Urysohn operator with kernel
     *  \f$f: \mathbb{R}^{k} \times \mathbb{R}^{k} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$ using Nyström methods.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes Dummy variable. Used for compatibility with PETSc's SNES solvers.
     *  @param u    Function represented by its values at the nodes of /f$Q/f$.
     *  @param fu   Result represented by its values at the nodes of /f$Q/f$. This parameter is used to return the result.
     *  @param uctx Function context of type urysohnctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode furysohn(SNES snes, Vec u, Vec fu, void *uctx);
    /*! @brief Computes the derivative of an Urysohn operator.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning \f$Df\f$ and \f$Q\f$ must be set in uctx.
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Computes the derivative of an Urysohn operator with kernel
     *  \f$f: \mathbb{R}^{k} \times \mathbb{R}^{k} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$ using Nyström methods.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes Dummy variable. Used for compatibility with PETSc's SNES solvers.
     *  @param u    Function represented by its values at the nodes of /f$Q/f$.
     *  @param Dfu  The derivative. This parameter is used to return the result.
     *  @param Dfuu Matrix used for constructing the preconditioner in Newton type methods. This parameter is a dummy and will
     *              be ignored.
     *  @param uctx Function context of type urysohnctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode dfurysohn(SNES snes, Vec u, Mat Dfu, Mat Dfuu, void *uctx);
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
