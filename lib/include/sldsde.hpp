/**********************************************************************************************************************************/
/*! @file sldsde.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of difference equations.
 *
 *  Module for numerical analysis of difference equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <string>
#include <vector>
#include <petsc.h>
#include "sldsdiff.hpp"
#include "sldsslv.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::de
 *
 *  @brief Module for numerical analysis of difference equations.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *                                                                                                                                */
namespace slds::de
{
    /* Forward declarations */
    class h5it;
    /* @class cycctx
     *
     * @brief Function context for cyclic systems.
     *
     * Function context for cyclic systems.
     *                                                                                                                            */
    class cycctx
    {
        public:
            int d; /*!< Problem dimension. */
            int k; /*!< Number of parameters. */
            int period; /*!< Period of the solution. */
            int pdperiod; /*!< Period of the difference equation. */
            PetscReal zerotol; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            Vec xt; /*!< Vector to store a single iterate. */
            Vec fxt; /*!< Vector to store a single function value. */
            Mat Dfxt; /*!< Matrix to store a single derivative. */
            MatType mcyctype; /*!< Matrix type for the cyclic matrix. Used in default creation routine. Should be of
                               *   type AIJ. */
            slds::diff::dfctx diffctx; /*!< Options for finite differences. May be NULL. */
            PetscErrorCode (**fpd)(SNES, Vec, Vec, void*);/*!< Functions. May be NULL. */
            PetscErrorCode (**dfpd)(SNES, Vec, Mat, Mat, void*);/*!< Derivatives. May be NULL. */
            void **fpdctx; /*!< Function contexts for projection of periodic difference equations. May be NULL. */
            cycctx(int dd, int kk, int pp, int pdpp, PetscReal ztol, Vec xtt, Vec fxtt, Mat Dfxtt, MatType mtype,
                   slds::diff::dfctx diffctxx, PetscErrorCode (**ffpd)(SNES, Vec, Vec, void*),
                   PetscErrorCode (**dffpd)(SNES, Vec, Mat, Mat, void*),
                   void **fpdctxx): d(dd), k(kk), period(pp), pdperiod(pdpp), zerotol(ztol), xt(xtt), fxt(fxtt), Dfxt(Dfxtt),
                                    mcyctype(mtype), diffctx(diffctxx), fpd(ffpd), dfpd(dffpd), fpdctx(fpdctxx) {};
    };
    /* @class pfctx
     *
     * @brief Context for computing period operators.
     *
     * Function context for cyclic systems.
     *                                                                                                                            */
    class pfctx
    {
        public:
            cycctx ctx; /*!< Context for the associated cyclic system. */
            PetscReal fill = -1; /*!< Expected fill ratio of the product. Negative value indicates that default
                                  *   options should be used.*/
            Mat *P = NULL; /*!< Matrix to store a matrix product. Used for computing period operators. */
            MatProductAlgorithm alg = MATPRODUCTALGORITHMDEFAULT; /*!< Matrix product algorithm used for computing
                                                                   *   period operators. */
            pfctx(cycctx ctxx): ctx(ctxx) {};
    };
    /* @class cycslvctx
     *
     * @brief Function context for solving cyclic systems.
     *
     * Function context for solving cyclic systems.
     *                                                                                                                            */
    class cycslvctx
    {
        public:
            int d; /*!< Problem dimension. */
            int k; /*!< Number of parameters. */
            int period; /*!< Period of the solution. */
            int pdperiod; /*!< Period of the difference equation. */
            PetscReal zerotol; /*!< Zero tolerance. Any value with absolute value less than this is considered zero. */
            MatType mcyctype; /*!< Matrix type for the cyclic matrix. Used in default creation routine. Should be of
                               *   type AIJ. */
            slds::diff::dfctx diffctx; /*!< Options for finite differences. May be NULL. */
            PetscErrorCode (**fpd)(SNES, Vec, Vec, void*);/*!< Functions. May be NULL. */
            PetscErrorCode (**dfpd)(SNES, Vec, Mat, Mat, void*);/*!< Derivatives. May be NULL. */
            void **fpdctx; /*!< Function contexts for projection of periodic difference equations. May be NULL. */
            slds::solve::fsolvectx ctx = slds::solve::fsolvectx();
            PetscErrorCode (*vecset)(Vec*, int, void*) = slds::util::vecsetdefault; /*!< Routine for vector creation. */
            void *vecctx = NULL; /*!< Options context for vector creation. */
            PetscErrorCode (*matset)(Mat*, int, int, void*) = slds::util::matsetdefault; /*!< Routine for matrix creation. */
            void *matctx = NULL; /*!< Options context for matrix creation. */
            PetscErrorCode (*matsetcyc)(Mat*, int, int, void*) = NULL; /*!< Routine for cyclic matrix creation.
                                                                        *   If this is NULL a default routine will be used. */
            void *matcycctx = NULL; /*!< Options context for cyclic matrix creation. */
            cycslvctx(int dd, int kk, int pp, int pdpp, PetscReal ztol, MatType mtype, slds::diff::dfctx diffctxx,
                      PetscErrorCode (**ffpd)(SNES, Vec, Vec, void*), PetscErrorCode (**dffpd)(SNES, Vec, Mat, Mat, void*),
                      void **fpdctxx): d(dd), k(kk), period(pp), pdperiod(pdpp), zerotol(ztol), mcyctype(mtype),
                                       diffctx(diffctxx), fpd(ffpd), dfpd(dffpd), fpdctx(fpdctxx) {};
    };
    /*! @class feigenctx
     *
     *  @brief Options for computing Feigenbaum diagrams.
     *
     *  Set various options for computing Feigenbaum diagrams. All values have defaults so feigenctx() provides an instance with
     *  sensible defaults.
     *                                                                                                                            */
    class feigenctx
    {
        public:
            int verbose = 0; /*!< Option to print various updates to stdout. */
            int reset = 0; /*!< Option to reset the initial iterate each step. Uses initit in each step. */
            int initit = 1e4; /*!< Number of burn-in iterations for the initial step. */
            int it = 100; /*!< Number of burn-in iterations for subsequent steps. */
            int t = 50; /*!< Number of iterates returned in each step. */
            int nstep = 100; /*!< Number of parameter steps. */
            PetscReal eps = 1e-6; /*!< Equality tolerance. Two iterates \f$x, y\f$ are considered equal if
                                   *   \f$ ||x - y|| < \epsilon \f$. */
            NormType ntype = NORM_INFINITY; /*!< The type of norm used for equality comparison. */
    };
    /*! @brief Iterates a periodic difference equation.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The \f$k\f$ parameters \f$\lambda_{i}\f$ must be the last k values of the input vectors.
     *
     *  Iterates \f$ t \f$-steps of the periodic difference equation \f$ x_{t+1} = f_{t}x_{t}\f$, where
     *  \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d-k}\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param f      The functions. SNES is ignored.
     *  @param x0     Initial point.
     *  @param xt     The iterates. This parameter is used to return the result.
     *  @param t      Number of steps to iterate.
     *  @param init   Number of iterations to ignore before iterates are stored. Set to 0 to save every iterate.
     *  @param period Period of the difference equation. f and fctx should be arrays of this size.
     *  @param d      The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param k      The dimension \f$k\f$ of \f$\mathbb{R}^{k}\f$.
     *  @param fctx   Function contexts that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *                to \f$f\f$ on evaluation.
     *                                                                                                                            */
    PetscErrorCode it(PetscErrorCode (**f)(SNES, Vec, Vec, void*), Vec x0, std::vector<Vec> *xt, int t, int init, int period,
                      int d, int k, void **fctx);
    /*! @brief Attempts to find the period of a sequence of iterates.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Attempts to find the period of a sequence of iterates. The number of iterates provided need not be a multiple of
     *  the determined period.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param xt     The iterates.
     *  @param period Period of the iterates. This parameter is used to return the result.
     *  @param eps    Equality tolerance. Two iterates \f$x, y\f$ are considered equal if \f$ ||x - y|| < \epsilon \f$.
     *  @param ntype  The type of norm used for equality comparison.
     *                                                                                                                            */
    PetscErrorCode xtperiod(std::vector<Vec> *xt, int *period, PetscReal eps, NormType ntype);
    /*! @brief Computes the Feigenbaum diagram for a one parameter family of periodic difference equations.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The parameter \f$\lambda\f$ must be the last value of the input vectors.
     *
     *  Computes the Feigenbaum diagram for a one parameter family of periodic difference equations
     *  \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d-1}\f$. The diagram is computed by iterating the equation for multiple
     *  values of \f$\lambda\f$.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param f        The functions. SNES is ignored.
     *  @param x0       Initial value.
     *  @param xtlambda The parameter dependent iterates. This parameter is used to return the result.
     *  @param lambda0  Start parameter value.
     *  @param lambda1  End parameter value.
     *  @param period   Period of the difference equation. f and fctx should be arrays of this size.
     *  @param d        The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param fctx     Function contexts that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *                  to \f$f\f$ on evaluation.
     *  @param ctx      Context containing various options for computation.
     *                                                                                                                            */
    PetscErrorCode feigen(PetscErrorCode (**f)(SNES, Vec, Vec, void*), Vec x0, std::vector<std::vector<Vec>> *xtlambda,
                          PetscReal lambda0, PetscReal lambda1, int period, int d, void **fctx, feigenctx ctx = feigenctx());
    /*! @brief Evaluates a cyclic system.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *
     *  Evaluates a cyclic system
     *  \f$f: \mathbb{R}^{p(d - k) + k} \rightarrow \mathbb{R}^{p(d - k)}\f$, where \f$p\f$ denotes the period of the system and
     *  \f$k\f$ denotes the number of parameters.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes Dummy variable. Used for compatibility with PETSc's SNES solvers.
     *  @param x    Point for evaluation.
     *  @param fx   Result. This parameter is used to return the result.
     *  @param ctxx Function context of type cycctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode fcyc(SNES snes, Vec x, Vec fx, void *ctxx);
    /*! @brief Computes the derivative of a cyclic system.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Computes the derivative of a cyclic system
     *  \f$f: \mathbb{R}^{p(d - k) + k} \rightarrow \mathbb{R}^{p(d - k)}\f$, where \f$p\f$ denotes the period of the system and
     *  \f$k\f$ denotes the number of parameters.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param snes Dummy variable. Used for compatibility with PETSc's SNES solvers.
     *  @param x    Point for evaluation.
     *  @param Dfx  The derivative. This parameter is used to return the result.
     *  @param Dfxx Matrix used for constructing the preconditioner in Newton type methods. This parameter is a dummy and will
     *              be ignored.
     *  @param ctxx Function context of type cycctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode dfcyc(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *ctxx);
    /*! @brief Initializes the derivative of a cyclic system.
     *
     *  @warning PETSc must be initialized externally.
     *  @warning The vector type must support VecGetOwnershipRange().
     *  @warning The matrix type must support MatGetOwnershipRange().
     *
     *  Initializes the derivative of a cyclic system.
     *  \f$f: \mathbb{R}^{p(d - k) + k} \rightarrow \mathbb{R}^{p(d - k)}\f$, where \f$p\f$ denotes the period of the system and
     *  \f$k\f$ denotes the number of parameters. Preallocates and fills space for nonzero entries.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param Dfx  The derivative. Should be an empty matrix. This parameter is used to return the result.
     *  @param ctxx Function context of type cycctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode initdfcycaijdense(Mat *Dfx, void *ctxx);
    /*! @brief Computes the linearized period operator of a periodic solution.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the linearized period operator of a periodic solution.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x    Point where the derivative is evaluated. Should have dimension \f$p(d - k) + k\f$.
     *  @param Dfx  Period operator. Should have dimension \f$d\f$. This parameter is used to return the result.
     *  @param ctxx Function context of type pfctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode pf(Vec x, Mat Dfx, void *ctxx);
    /*! @brief Computes the linearized period operator of a periodic solution.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes the linearized period operator of a periodic solution.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param x    Point where the derivative is evaluated. Should contain \f$p\f$ iterates.
     *  @param Dfx  Period operator. Should have dimension \f$d\f$. This parameter is used to return the result.
     *  @param ctxx Function context of type pfctx. Required for evaluation.
     *                                                                                                                            */
    PetscErrorCode pf2(std::vector<Vec> *xt, Mat Dfx, void *ctxx);
    /*! @brief Computes a periodic solution to a periodic difference equation.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Computes a periodic solution to a periodic difference equation.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See SNESConvergedReason.
     *
     *  @param x0  Initial guess.
     *  @param xs  Solution. This parameter is used to return the result.
     *  @param ctx Context.
     *                                                                                                                            */
    PetscErrorCode cycsolve(Vec x0, std::vector<Vec> *xs, cycslvctx ctx);
    /*! @brief Writes a sequence of iterates to an HDF5 file.
     *
     *  Writes a sequence of iterates of a map \f$f: \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$ to an HDF5 file.
     *
     *  @return Error code. Returns 0 if successful.
     *
     *  @param xt  The iterates.
     *  @param d   The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param f   Output file. May be a filename, a relative path or an absolute path. Should have the extension '.h5'.
     *                                                                                                                            */
    PetscErrorCode writeitsh5(std::vector<std::vector<Vec>> xt, int d, std::string f = "its.h5");
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
