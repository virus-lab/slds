/**********************************************************************************************************************************/
/*! @file sldsode.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2024
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of ordinary differential equations.
 *
 *  Module for numerical analysis of ordinary differential equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Preprocessor Instructions */
/**********************************************************************************************************************************/
#pragma once
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <cstdio>
#include <string>
#include <vector>
#include <petsc.h>
#include "sldsslv.hpp"
/**********************************************************************************************************************************/
/* Namespaces */
/**********************************************************************************************************************************/
/*! @namespace slds::ode
 *
 *  @brief Module for numerical analysis of ordinary differential equations.
 *
 *  @warning Requires PETSc (https://petsc.org/).
 *                                                                                                                                */
namespace slds::ode
{
    /*! @class odesolvectx
     *
     *  @brief Options for an ODE solver.
     *
     *  Set various options for ODE solve. All values have defaults so odesolvectx() provides an instance with sensible
     *  defaults for many applications.
     *                                                                                                                            */
    class odesolvectx
    {
        public:
            int local = 0; /*!< Use the local instead of the global communicator. */
            PetscReal dt = 1e-6; /*!< Time step. */
            TSType tstype = TSRK; /*!< ODE solver type. */
            TSProblemType tsptype = TS_NONLINEAR; /*!< ODE problem type. */
            TSExactFinalTimeOption tsftime = TS_EXACTFINALTIME_MATCHSTEP; /*!< Method to evaluate the final timestep. */
            PetscReal tsatol = 1e-6; /*!< Absolute local truncation error tolerance for adaptive methods. */
            PetscReal tsrtol = 1e-6; /*!< Relative local truncation error tolerance for adaptive methods. */
            Vec fx = NULL; /*!< Optional vector to store function evaluations. Will be created if this is NULL. */
            Mat Dfx = NULL; /*!<  Matrix to store derivative evaluations. Must be set if derivative is set. */
            PetscErrorCode (*df)(TS, PetscReal, Vec, Mat, Mat, void*) = NULL;/*!< Derivative of the right hand side. */
            PetscErrorCode (*tsoptions)(TS, void*) = NULL; /*!< Optional user supplied routine to set additional options of the
                                                            *   ODE solver. */
            void *tsctx = NULL; /*!< Function context for the user options routine of the ODE solver. Used to pass additional
                                 *   parameters if required. */
    };
    /*! @brief Solves \f$\dot{x} = f(t, x)\f$ for a map \f$f: \mathbb{R} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *
     *  @warning PETSc must be initialized externally.
     *
     *  Solves \f$\dot{x} = f(t, x)\f$ for a map \f$f: \mathbb{R} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$ using a
     *  solver of type TS. Uses Euler's mehtod by default.
     *
     *  @return Convergence code. Positive value indicates the solver converged. See TSConvergedReason.
     *
     *  @param f    The function \f$f: \mathbb{R} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{d}\f$.
     *  @param x0   Initial condition \f$x_{0} \in \mathbb{R}^{d}\f$.
     *  @param xt   The computed solution at the final time \f$t_{1} \in \mathbb{R}\f$. This parameter is used to return the
     *              result.
     *  @param t0   Initial time \f$t_{0} \in \mathbb{R}\f$.
     *  @param t1   End time \f$t_{1} \in \mathbb{R}\f$.
     *  @param d    The dimension \f$d\f$ of \f$\mathbb{R}^{d}\f$.
     *  @param fctx Function context that can be used to pass additional arguments to \f$f\f$ if required. This is passed
     *              to \f$f\f$ on evaluation.
     *  @param ctx  Options for the solver.
     *                                                                                                                            */
    PetscErrorCode odesolve(PetscErrorCode (*f)(TS, PetscReal, Vec, Vec, void*), Vec x0, Vec xt, PetscReal t0, PetscReal t1,
                            int d, void *fctx = NULL, odesolvectx ctx = odesolvectx());
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
