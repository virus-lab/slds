/**********************************************************************************************************************************/
/*! @file sldsutil.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Miscellaneous utilities.
 *
 *  Miscellaneous utilities. Additionally implements matrix creation routines for various applications.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Gets the rows (or columns) of a matrix belonging to the MPI process.
 *                                                                                                                                */
PetscErrorCode slds::util::splitdefault(int *n0, int *n1, int n, int rank)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Calculate ownership range */
    if(n % mpicommsize > rank)
    {
        *n0 = rank*(n/mpicommsize) + rank;
        *n1 = (rank + 1)*(n/mpicommsize) + rank + 1;
    }
    else
    {
        *n0 = rank*(n/mpicommsize) + (n % mpicommsize);
        *n1 = (rank + 1)*(n/mpicommsize) + (n % mpicommsize);
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Sets up a vector for use with PETSc.
 *                                                                                                                                */
PetscErrorCode slds::util::vecsetdefault(Vec *x, int n, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize;
    slds::util::vecctx ctxx;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Recover data */
    if(ctx)
        ctxx = *static_cast<slds::util::vecctx*>(ctx);
    /* Query process rank */
    if(ctxx.local)
        mpirank = 0;
    else
        PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    if(ctxx.local)
        mpicommsize = 1;
    else
        PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Create vector */
    if(ctxx.local)
        PetscCall(VecCreate(PETSC_COMM_SELF, x));
    else
        PetscCall(VecCreate(PETSC_COMM_WORLD, x));
    PetscCall(VecSetSizes(*x, n/mpicommsize + ((n % mpicommsize) > mpirank), n));
    PetscCall(VecSetType(*x, ctxx.type));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets up a matrix for use with PETSc.
 *                                                                                                                                */
PetscErrorCode slds::util::matsetdefault(Mat *A, int n, int m, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize;
    slds::util::matctx ctxx;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Recover data */
    if(ctx)
        ctxx = *static_cast<slds::util::matctx*>(ctx);
    /* Query process rank */
    if(ctxx.local)
        mpirank = 0;
    else
        PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    if(ctxx.local)
        mpicommsize = 1;
    else
        PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Create the matrix */
    if(ctxx.local)
        PetscCall(MatCreate(PETSC_COMM_SELF, A));
    else
        PetscCall(MatCreate(PETSC_COMM_WORLD, A));
    PetscCall(MatSetSizes(*A, n/mpicommsize + ((n % mpicommsize) > mpirank), m/mpicommsize + ((m % mpicommsize) > mpirank),
                          n, m));
    PetscCall(MatSetType(*A, ctxx.type));
    /* Set up */
    if(!ctxx.noset)
        PetscCall(MatSetUp(*A));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets a subvector of a vector.
 *                                                                                                                                */
PetscErrorCode slds::util::subvecset(Vec x, Vec y, int n0, int n1, int p0, PetscReal zerotol)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxxr, idxyr(1, 0);
    std::vector<PetscReal> xr, yr(1, 0);
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    PetscCall(VecBindToCPU(y, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(y, &r0, &r1));
    /* Preallocate */
    idxxr.reserve(n1 - n0);
    xr.reserve(n1 - n0);
    /* Set values of y */
    for(int i = 0; i < n1 - n0; i++)
    {
        /* Determine if the value should be handled by the current process */
        if(n0 + i > r0 - 1 && n0 + i < r1)
        {
            /* Set row indices */
            idxyr[0] = n0 + i;
            /* Get values */
            PetscCall(VecGetValues(y, 1, &idxyr[0], &yr[0]));
            /* Cache nonzero values */
            if(PetscAbsReal(yr[0]) > zerotol)
            {
                idxxr.push_back(p0 + i);
                xr.push_back(yr[0]);
            }
        }
    }
    /* Set values */
    PetscCall(VecSetValues(x, idxxr.size(), &idxxr[0], &xr[0], INSERT_VALUES));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(x));
    PetscCall(VecAssemblyEnd(x));
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    PetscCall(VecBindToCPU(y, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Gets a subvector of a vector.
 *                                                                                                                                */
PetscErrorCode slds::util::subvecget(Vec x, Vec y, int n0, int n1, int p0, PetscReal zerotol)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxxr(1, 0), idxyr;
    std::vector<PetscReal> xr(1, 0), yr;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    PetscCall(VecBindToCPU(y, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x, &r0, &r1));
    /* Preallocate */
    idxyr.reserve(n1 - n0);
    yr.reserve(n1 - n0);
    /* Set values of y */
    for(int i = 0; i < n1 - n0; i++)
    {
        /* Determine if the value should be handled by the current process */
        if(n0 + i > r0 - 1 && n0 + i < r1)
        {
            /* Set row indices */
            idxxr[0] = n0 + i;
            /* Get values */
            PetscCall(VecGetValues(x, 1, &idxxr[0], &xr[0]));
            /* Cache nonzero values */
            if(PetscAbsReal(xr[0]) > zerotol)
            {
                idxyr.push_back(p0 + i);
                yr.push_back(xr[0]);
            }
        }
    }
    /* Set values */
    PetscCall(VecSetValues(y, idxyr.size(), &idxyr[0], &yr[0], INSERT_VALUES));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(y));
    PetscCall(VecAssemblyEnd(y));
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    PetscCall(VecBindToCPU(y, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets a submatrix of a matrix.
 *                                                                                                                                */
PetscErrorCode slds::util::submatset(Mat A, Mat B, int n0, int m0, int n1, int m1, int p0, int q0, PetscReal zerotol)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxar(1, 0), idxac, idxbr(1, 0), idxbc(m1 - m0, 0);
    std::vector<PetscReal> ar, br(m1 - m0, 0);
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind matrices to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    PetscCall(MatBindToCPU(B, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(MatGetOwnershipRange(B, &r0, &r1));
    /* Preallocate memory */
    idxac.reserve(m1 - m0);
    ar.reserve(m1 - m0);
    /* Set column indices */
    for(int i = 0; i < m1 - m0; i++)
        idxbc[i] = m0 + i;
    /* Set rows of A */
    for(int i = 0; i < n1 - n0; i++)
    {
        /* Determine if the row should be handled by the current process */
        if(n0 + i > r0 - 1 && n0 + i < r1)
        {
            /* Set row indices */
            idxar[0] = p0 + i;
            idxbr[0] = n0 + i;
            /* Get values */
            PetscCall(MatGetValues(B, 1, &idxbr[0], m1 - m0, &idxbc[0], &br[0]));
            /* Extract nonzeros */
            for(int j = 0; j < m1 - m0; j++)
            {
                if(PetscAbsReal(br[j]) > zerotol)
                {
                    idxac.push_back(q0 + j);
                    ar.push_back(br[j]);
                }
            }
            /* Set values */
            PetscCall(MatSetValues(A, 1, &idxar[0], idxac.size(), &idxac[0], &ar[0], INSERT_VALUES));
            /* Clear */
            idxac.clear();
            ar.clear();
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
    /* Unbind matrices from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    PetscCall(MatBindToCPU(B, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Gets a submatrix of a matrix.
 *                                                                                                                                */
PetscErrorCode slds::util::submatget(Mat A, Mat B, int n0, int m0, int n1, int m1, int p0, int q0, PetscReal zerotol)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxar(1, 0), idxac(m1 - m0, 0), idxbr(1, 0), idxbc;
    std::vector<PetscReal> ar(m1 - m0, 0), br;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind matrices to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    PetscCall(MatBindToCPU(B, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(MatGetOwnershipRange(A, &r0, &r1));
    /* Preallocate memory */
    idxbc.reserve(m1 - m0);
    br.reserve(m1 - m0);
    /* Set column indices */
    for(int i = 0; i < m1 - m0; i++)
        idxac[i] = m0 + i;
    /* Set rows of B */
    for(int i = 0; i < n1 - n0; i++)
    {
        /* Determine if the row should be handled by the current process */
        if(n0 + i > r0 - 1 && n0 + i < r1)
        {
            /* Set row indices */
            idxar[0] = n0 + i;
            idxbr[0] = p0 + i;
            /* Get values */
            PetscCall(MatGetValues(A, 1, &idxar[0], m1 - m0, &idxac[0], &ar[0]));
            /* Extract nonzeros */
            for(int j = 0; j < m1 - m0; j++)
            {
                if(PetscAbsReal(ar[j]) > zerotol)
                {
                    idxbc.push_back(q0 + j);
                    br.push_back(ar[j]);
                }
            }
            /* Set values */
            PetscCall(MatSetValues(B, 1, &idxbr[0], idxbc.size(), &idxbc[0], &br[0], INSERT_VALUES));
            /* Clear */
            idxbc.clear();
            br.clear();
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(B, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(B, MAT_FINAL_ASSEMBLY));
    /* Unbind matrices from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    PetscCall(MatBindToCPU(B, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets rows of a matrix.
 *                                                                                                                                */
PetscErrorCode slds::util::matrowsset(Mat A, std::vector<Vec> *x, std::vector<int> *idxi, int n, int d, PetscReal zerotol)
{
        /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxar(1, 0), idxac, idxxr(1, 0);
    std::vector<PetscReal> ar, xr(1, 0);
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind matrices to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    /* Preallocate memory */
    idxac.reserve(d);
    ar.reserve(d);
    /* Set columns of A */
    for(int i = 0; i < n; i++)
    {
        /* Bind vectors to CPU */
        PetscCall(VecBindToCPU((*x)[i], PETSC_TRUE));
        /* Set column index */
        idxar[0] = (*idxi)[i];
        /* Get ownership range */
        PetscCall(VecGetOwnershipRange((*x)[i], &r0, &r1));
        /* Set rows */
        for(int j = 0; j < d; j++)
        {
            /* Determine if the value should be handled by the current process */
            if(j > r0 - 1 && j < r1)
            {
                /* Set row indices */
                idxxr[0] = j;
                /* Get values */
                PetscCall(VecGetValues((*x)[i], 1, &idxxr[0], &xr[0]));
                /* Cache nonzero values */
                if(PetscAbsReal(xr[0]) > zerotol)
                {
                    idxac.push_back(j);
                    ar.push_back(xr[0]);
                }
            }
        }
        /* Set values */
        PetscCall(MatSetValues(A, 1, &idxar[0], idxac.size(), &idxac[0], &ar[0], INSERT_VALUES));
        /* Clear */
        idxac.clear();
        ar.clear();
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Unbind vectors from CPU */
        PetscCall(VecBindToCPU((*x)[i], PETSC_FALSE));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
    /* Unbind matrices from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets columns of a matrix.
 *                                                                                                                                */
PetscErrorCode slds::util::matcolumnsset(Mat A, std::vector<Vec> *x, std::vector<int> *idxj, int n, int d, PetscReal zerotol)
{
        /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxar, idxac(1, 0), idxxr(1, 0);
    std::vector<PetscReal> ac, xr(1, 0);
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind matrices to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    /* Preallocate memory */
    idxar.reserve(d);
    ac.reserve(d);
    /* Set columns of A */
    for(int i = 0; i < n; i++)
    {
        /* Bind vectors to CPU */
        PetscCall(VecBindToCPU((*x)[i], PETSC_TRUE));
        /* Set column index */
        idxac[0] = (*idxj)[i];
        /* Get ownership range */
        PetscCall(VecGetOwnershipRange((*x)[i], &r0, &r1));
        /* Set rows */
        for(int j = 0; j < d; j++)
        {
            /* Determine if the value should be handled by the current process */
            if(j > r0 - 1 && j < r1)
            {
                /* Set row indices */
                idxxr[0] = j;
                /* Get values */
                PetscCall(VecGetValues((*x)[i], 1, &idxxr[0], &xr[0]));
                /* Cache nonzero values */
                if(PetscAbsReal(xr[0]) > zerotol)
                {
                    idxar.push_back(j);
                    ac.push_back(xr[0]);
                }
            }
        }
        /* Set values */
        PetscCall(MatSetValues(A, idxar.size(), &idxar[0], 1, &idxac[0], &ac[0], INSERT_VALUES));
        /* Clear */
        idxar.clear();
        ac.clear();
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Unbind vectors from CPU */
        PetscCall(VecBindToCPU((*x)[i], PETSC_FALSE));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
    /* Unbind matrices from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Adds a scalar to the diagonal of a (sub)matrix.
 *                                                                                                                                */
PetscErrorCode slds::util::matdiagonaladd(Mat A, PetscReal v, int d)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    std::vector<int> idxar(1, 0), idxac(1, 0);
    std::vector<PetscReal> ar(1, v);
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind matrices to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(MatGetOwnershipRange(A, &r0, &r1));
    /* Set rows of A */
    for(int i = 0; i < d; i++)
    {
        /* Determine if the row should be handled by the current process */
        if(i > r0 - 1 && i < r1)
        {
            /* Set indices */
            idxar[0] = i;
            idxac[0] = i;
            /* Set values */
            PetscCall(MatSetValues(A, 1, &idxar[0], 1, &idxac[0], &ar[0], ADD_VALUES));
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
    /* Unbind matrices from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
