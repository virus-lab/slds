/**********************************************************************************************************************************/
/*! @file sldsla.hpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Linear algebra routines.
 *
 *  Linear algebra routines. Additionally implements solvers for eigenvalue problems. Based on PETSc (https://petsc.org/) and
 *  SLEPc (https://slepc.upv.es/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsla.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Product of two matrices A and B.
 *                                                                                                                                */
PetscErrorCode slds::la::matmult(Mat A, Mat B, Mat *C, MatProductType type, MatProductAlgorithm alg, PetscReal fill, int cexists)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Create product matrix */
    if(!cexists)
        PetscCall(MatProductCreate(A, B, NULL, C));
    else
        PetscCall(MatProductCreateWithMat(A, B, NULL, *C));
    /* Set type */
    PetscCall(MatProductSetType(*C, type));
    /* Set options */
    PetscCall(MatProductSetAlgorithm(*C, alg));
    if(fill > 0)
        PetscCall(MatProductSetFill(*C, fill));
    /* Set up */
    PetscCall(MatProductSetFromOptions(*C));
    /* Compute product */
    PetscCall(MatProductSymbolic(*C));
    PetscCall(MatProductNumeric(*C));
    /* Clear */
    PetscCall(MatProductClear(*C));
    /* Return */
    return 0;
}
/*!
 *  @brief Sorting function to sort eigenvalues by distance to a target magnitude.
 *                                                                                                                                */
PetscErrorCode slds::la::comparetargetmagnitude(PetscScalar v, PetscScalar iv, PetscScalar w, PetscScalar iw, PetscInt *result,
                                                void *ctx)
{
    /* Set up */
    EPS eps;
    PetscScalar dv, dw, nv, nw;
    PetscScalar t;
    /* Recover data */
    eps = *static_cast<EPS*>(ctx);
    /* Extract target value (assumed to be real since it is a magnitude) */
    PetscCall(EPSGetTarget(eps, &t));
    /* Compute magnitudes */
    nv = PetscSqrtScalar(PetscPowScalar(v, 2) + PetscPowScalar(iv, 2));
    nw = PetscSqrtScalar(PetscPowScalar(w, 2) + PetscPowScalar(iw, 2));
    /* Compute distances to target */
    dv = PetscAbsScalar(nv - t);
    dw = PetscAbsScalar(nw - t);
    /* Set result */
    if(dv < dw)
        *result = -1;
    else if(dw < dv)
        *result = 1;
    else
        *result = 0;
    /* Return */
    return 0;
}
/*!
 *  @brief Eigenpairs of a matrix A in R^(d*d).
 *                                                                                                                                */
PetscErrorCode slds::la::eigen(Mat A, std::vector<PetscReal> *evals, std::vector<PetscReal> *ievals, std::vector<Vec> *evecs,
                               std::vector<Vec> *ievecs, int d, int n, slds::la::eigenctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, npairs;
    EPS eps;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up EPS context */
    PetscCall(EPSCreate(PETSC_COMM_WORLD, &eps));
    /* Set solver options */
    PetscCall(EPSSetOperators(eps, A, NULL));
    PetscCall(EPSSetProblemType(eps, ctx.type));
    PetscCall(EPSSetType(eps, ctx.epstype));
    PetscCall(EPSSetTolerances(eps, ctx.tol, ctx.maxit));
    PetscCall(EPSSetWhichEigenpairs(eps, ctx.select));
    if(ctx.mpd > 0)
        PetscCall(EPSSetDimensions(eps, n, PETSC_DEFAULT, ctx.mpd));
    else
        PetscCall(EPSSetDimensions(eps, n, PETSC_DEFAULT, PETSC_DEFAULT));
    if(ctx.trueres)
        PetscCall(EPSSetTrueResidual(eps, PETSC_TRUE));
    if(ctx.select == EPS_SMALLEST_MAGNITUDE || ctx.abs)
        PetscCall(EPSSetConvergenceTest(eps, EPS_CONV_ABS));
    /* Set target eigenvalue */
    PetscCall(EPSSetTarget(eps, ctx.target));
    /* Set harmonic extraction */
    if(ctx.harmonic && (ctx.select == EPS_TARGET_MAGNITUDE || ctx.select == EPS_TARGET_REAL))
        PetscCall(EPSSetExtraction(eps, EPS_HARMONIC));
    /* Enable balancing for non hermitian problems (LAPACK not supported) */
    if(ctx.balance && ctx.type == EPS_NHEP && strcmp(ctx.epstype, EPSLAPACK))
        PetscCall(EPSSetBalance(eps, EPS_BALANCE_TWOSIDE, PETSC_DEFAULT, PETSC_DEFAULT));
    /* Set deflation subspace */
    if(ctx.deflate && ctx.dsub)
        PetscCall(EPSSetDeflationSpace(eps, (*ctx.dsub).size(), &(*ctx.dsub)[0]));
    /* Set selection by target magnitude */
    if(ctx.setcomparemagnitude)
    {
        PetscCall(EPSSetWhichEigenpairs(eps, EPS_WHICH_USER));
        PetscCall(EPSSetEigenvalueComparison(eps, slds::la::comparetargetmagnitude, &eps));
    }
    /* Call user provided routines if present */
    if(ctx.epsoptions)
        PetscCall(ctx.epsoptions(eps, ctx.epsctx));
    /* Compute eigenpairs */
    PetscCall(EPSSolve(eps));
    /* Extract the number of converged pairs */
    PetscCall(EPSGetConverged(eps, &npairs));
    /* If enough pairs converged, extract them */
    if(npairs > n - 1)
    {
        /* Set up */
        PetscReal v, iv;
        /* Preallocate */
        (*evals).reserve(npairs);
        (*ievals).reserve(npairs);
        if(evecs)
            (*evecs).reserve(npairs);
        if(ievecs && !ctx.returninvsub)
            (*ievecs).reserve(npairs);
        /* Initialize */
        if(evecs)
        {
            for(int i = (*evecs).size(); i < npairs; i++)
            {
                (*evecs).push_back(Vec());
                PetscCall(ctx.vecset(&(*evecs).back(), d, ctx.vecctx));
            }
        }
        if(ievecs && !ctx.returninvsub)
        {
            for(int i = (*ievecs).size(); i < npairs; i++)
            {
                (*ievecs).push_back(Vec());
                PetscCall(ctx.vecset(&(*ievecs).back(), d, ctx.vecctx));
            }
        }
        /* Extract eigenpairs */
        for(int i = 0; i < npairs; i++)
        {
            if(!ctx.returninvsub && evecs && ievecs)
                PetscCall(EPSGetEigenpair(eps, i, &v, &iv, (*evecs)[i], (*ievecs)[i]));
            else if(!ctx.returninvsub && evecs && !ievecs)
                PetscCall(EPSGetEigenpair(eps, i, &v, &iv, (*evecs)[i], NULL));
            else
                PetscCall(EPSGetEigenpair(eps, i, &v, &iv, NULL, NULL));
            (*evals).push_back(v);
            (*ievals).push_back(iv);
        }
        /* Extract invariant subspace */
        if(ctx.returninvsub && evecs)
            PetscCall(EPSGetInvariantSubspace(eps, &(*evecs)[0]));
    }
    /* Clean Up */
    PetscCall(EPSDestroy(&eps));
    /* Return */
    if(npairs < n)
        return -1;
    else
        return 0;
}
/*!
 *  @brief Singular triplets of a matrix A in R^(d*d).
 *                                                                                                                                */
PetscErrorCode slds::la::svd(Mat A, std::vector<PetscReal> *s, std::vector<Vec> *u, std::vector<Vec> *v, int d, int n,
                             slds::la::svdctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, ntrips;
    SVD svd;
    EPS eps;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up SVD context */
    PetscCall(SVDCreate(PETSC_COMM_WORLD, &svd));
    /* Set solver options */
    PetscCall(SVDSetType(svd, ctx.svdtype));
    PetscCall(SVDSetOperators(svd, A, NULL));
    PetscCall(SVDSetProblemType(svd, SVD_STANDARD));
    PetscCall(SVDSetDimensions(svd, n, PETSC_DEFAULT, PETSC_DEFAULT));
    PetscCall(SVDSetTolerances(svd, ctx.tol, ctx.maxit));
    PetscCall(SVDSetWhichSingularTriplets(svd, ctx.select));
    if(ctx.select == SVD_SMALLEST || ctx.abs)
        PetscCall(SVDSetConvergenceTest(svd, SVD_CONV_ABS));
    if(!strcmp(ctx.svdtype, SVDCROSS) || !strcmp(ctx.svdtype, SVDCYCLIC))
    {
        if(!strcmp(ctx.svdtype, SVDCROSS))
            PetscCall(SVDCrossGetEPS(svd, &eps));
        else
            PetscCall(SVDCyclicGetEPS(svd, &eps));
        PetscCall(EPSSetType(eps, ctx.epstype));
        if(ctx.trueres)
            PetscCall(EPSSetTrueResidual(eps, PETSC_TRUE));
        if(ctx.epstarget)
        {
            PetscCall(EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE));
            PetscCall(EPSSetTarget(eps, ctx.target));
            PetscCall(EPSSetExtraction(eps, EPS_HARMONIC));
        }
        if(ctx.epsoptions)
            PetscCall(ctx.epsoptions(eps, ctx.epsctx));
    }
    /* Call user provided routines if present */
    if(ctx.svdoptions)
        PetscCall(ctx.svdoptions(svd, ctx.svdctx));
    /* Compute singular triplets */
    PetscCall(SVDSolve(svd));
    /* Extract the number of converged pairs */
    PetscCall(SVDGetConverged(svd, &ntrips));
    /* If enough pairs converged, extract them */
    if(ntrips > n - 1)
    {
        /* Set up */
        PetscReal sv;
        /* Preallocate */
        (*s).reserve(ntrips);
        if(u)
            (*u).reserve(ntrips);
        if(v)
            (*v).reserve(ntrips);
        /* Initialize */
        if(u)
        {
            for(int i = (*u).size(); i < ntrips; i++)
            {
                (*u).push_back(Vec());
                PetscCall(ctx.vecset(&(*u).back(), d, ctx.vecctx));
            }
        }
        if(v)
        {
            for(int i = (*v).size(); i < ntrips; i++)
            {
                (*v).push_back(Vec());
                PetscCall(ctx.vecset(&(*v).back(), d, ctx.vecctx));
            }
        }
        /* Extract singular triplets */
        for(int i = 0; i < ntrips; i++)
        {
            if(u && v)
                PetscCall(SVDGetSingularTriplet(svd, i, &sv, (*u)[i], (*v)[i]));
            else if (u && !v)
                PetscCall(SVDGetSingularTriplet(svd, i, &sv, (*u)[i], NULL));
            else if (!u && v)
                PetscCall(SVDGetSingularTriplet(svd, i, &sv, NULL, (*v)[i]));
            else
                PetscCall(SVDGetSingularTriplet(svd, i, &sv, NULL, NULL));
            (*s).push_back(sv);
        }
    }
    /* Clean Up */
    PetscCall(SVDDestroy(&svd));
    /* Return */
    if(ntrips < n)
        return -1;
    else
        return 0;
}
/*!
*  @brief Computes the largest eigenvalue of a matrix A in R^(d*d).
*                                                                                                                                */
PetscErrorCode slds::la::spectralr(Mat A, PetscReal *v, PetscReal *iv, slds::la::eigenctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    std::vector<PetscReal> evals, ievals;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set context options */
    ctx.select = EPS_LARGEST_MAGNITUDE;
    /* Compute eigenvalue */
    rcode = slds::la::eigen(A, &evals, &ievals, NULL, NULL, 0, 1, ctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver failed, return INFINITY */
    if(rcode < 0)
    {
        *v = INFINITY;
        *iv = INFINITY;
    }
    /* Else return the eigenvalue */
    else
    {
        *v = evals[0];
        *iv = ievals[0];
    }
    /* Return */
    if(rcode < 0)
        return rcode;
    else
        return 0;
}
/*!
*  @brief Computes the morse index of a matrix A in R^(d*d).
*                                                                                                                                */
PetscErrorCode slds::la::morseposre(Mat A, int *idx, int d, PetscReal evmax, int step0, int step, int computeall, int idxtype,
                                    slds::la::eigenctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode, it, nev, allout;
    std::vector<PetscReal> v, iv;
    std::vector<Vec> s, dsub;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set options */
    it = 0;
    nev = 0;
    if(!computeall)
        ctx.returninvsub = 1;
    if(idxtype == 0)
        ctx.select = EPS_LARGEST_REAL;
    else
        ctx.select = EPS_LARGEST_MAGNITUDE;
    /* Compute dominant eigenvalues in steps of 2^i until one is inside the unit circle */
    for(;;)
    {
        /* Reset indicator */
        allout = true;
        /* If the Morse index is too large, break and exit */
        if(it < 1 && nev + step0 > evmax*d && !computeall)
        {
            *idx = -2;
            break;
        }
        else if(nev + step > evmax*d && !computeall)
        {
            *idx = -2;
            break;
        }
        /* If we already have some eigenvalues set deflation */
        if(!dsub.empty() && !computeall)
        {
            ctx.deflate = 1;
            ctx.dsub = &dsub;
        }
        /* Compute eigenvalues */
        if(computeall)
            rcode = eigen(A, &v, &iv, NULL, NULL, d, d, ctx);
        else if(it < 1)
            rcode = eigen(A, &v, &iv, &s, NULL, d, step0, ctx);
        else
            rcode = eigen(A, &v, &iv, &s, NULL, d, step, ctx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* Exit if the solver failed */
        if(rcode < 0)
        {
            *idx = -3;
            break;
        }
        /* Merge the deflation spaces */
        if(!s.empty() && !computeall)
        {
            dsub.insert(dsub.end(), s.begin(), s.end());
            s.clear();
        }
        /* Check the eigenvalues */
        for(int i = 0; i < v.size(); i++)
        {
            /* Check positive real part */
            if(idxtype == 0)
            {
                if(v[i] < 0)
                {
                    *idx = nev + i;
                    allout = false;
                    break;
                }
            }
            /* Check Morse */
            else
            {
                if(PetscSqrtReal(PetscPowReal(v[i], 2) + PetscPowReal(iv[i], 2)) < 1)
                {
                    *idx = nev + i;
                    allout = false;
                    break;
                }
            }
        }
        /* Set iteration */
        it += 1;
        /* Set number of computed eigenvalues */
        nev += v.size();
        /* Clear the eigenvalues */
        v.clear();
        iv.clear();
        /* If all eigenvalues are outside return index */
        if(allout && nev == d)
        {
            *idx = d;
            break;
        }
        /* If all eigenvalues are outside compute next eigenvalues */
        else if(allout)
            continue;
        /* Else return index */
        else
            break;
    }
    /* Clean up */
    for(int i = 0; i < s.size(); i++)
        PetscCall(VecDestroy(&(s[i])));
    for(int i = 0; i < dsub.size(); i++)
        PetscCall(VecDestroy(&(dsub[i])));
    /* Return */
    return 0;
}
/*!
*  @brief Nullspace of a matrix A in R^(d*d). Requires the dimension to be known.
*                                                                                                                                */
PetscErrorCode slds::la::knull(Mat A, Mat B, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, matassembled;
    int mpirank, rcode, isnull;
    std::vector<PetscReal> s;
    std::vector<Vec> v;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::la::svdctx ctx = *static_cast<slds::la::svdctx*>(ctxx);
    /* Zero matrix if required */
    PetscCall(MatAssembled(B, &matassembled));
    if(matassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(B));
    /* Set context */
    ctx.select = SVD_SMALLEST;
    ctx.epstarget = 1;
    ctx.target = 0;
    /* Compute nullspace as eigenspace of 0 */
    rcode = slds::la::svd(A, &s, NULL, &v, d, k, ctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* Set nullspace */
    if(rcode > -1)
    {
        /* Check if the first k singular values are 0 */
        isnull = true;
        for(int i = 0; i < k; i++)
        {
            if(PetscAbsReal(s[i]) > nulltol)
            {
                isnull = false;
                break;
            }
        }
        /* Set values of B */
        if(isnull)
        {
            /* Set up */
            std::vector<int> idxj(k, 0);
            for(int i = 0; i < k; i++)
                idxj[i] = i;
            /* Set values */
            PetscCall(slds::util::matcolumnsset(B, &v, &idxj, k, d, zerotol));
        }
    }
    /* Clean Up */
    for(int i = 0; i < v.size(); i++)
        PetscCall(VecDestroy(&(v[i])));
    /* Return */
    if(rcode < 0)
        return -1;
    else if(!isnull)
        return -2;
    else
        return 0;
}
/*!
 *  @brief Nullspace of a matrix A in R^(d*d). Requires the dimension to be known.
 *                                                                                                                                */
PetscErrorCode slds::la::knull2(Mat A, Mat B, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, matassembled;
    int mpirank, rcode, rrcode, isnull, fullrank;
    Mat AtA;
    std::vector<PetscReal> v, iv;
    std::vector<Vec> s;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::la::epsnullctx ctx = *static_cast<slds::la::epsnullctx*>(ctxx);
    /* Zero matrix if required */
    if(ctx.setmat)
    {
        PetscCall(MatAssembled(B, &matassembled));
        if(matassembled == PETSC_TRUE)
            PetscCall(MatZeroEntries(B));
    }
    /* Set options */
    ctx.ctx.select = EPS_SMALLEST_MAGNITUDE;
    if(ctx.useinvsub)
        ctx.ctx.returninvsub = 1;
    /* Compute N(A) as N((A^T)A) */
    if(ctx.useata)
    {
        /* Set up */
        if(ctx.AtA)
            AtA = *ctx.AtA;
        /* Compute (A^T)A */
        if(ctx.AtA)
            PetscCall(slds::la::matmult(A, A, &AtA, MATPRODUCT_AtB, ctx.alg, ctx.fill, 1));
        else
            PetscCall(slds::la::matmult(A, A, &AtA, MATPRODUCT_AtB, ctx.alg, ctx.fill, 0));
        /* Set context */
        ctx.ctx.type = EPS_HEP;
        /* Compute nullspace as eigenspace of 0 */
        if(ctx.s)
            rcode = slds::la::eigen(AtA, &v, &iv, ctx.s, NULL, d, k, ctx.ctx);
        else
            rcode = slds::la::eigen(AtA, &v, &iv, &s, NULL, d, k, ctx.ctx);
        /* Clean up */
        if(!ctx.AtA)
            PetscCall(MatDestroy(&AtA));
    }
    /* Compute N(A) directly */
    else
    {
        /* Set context */
        if(ctx.usetarget)
        {
            ctx.ctx.select = EPS_TARGET_MAGNITUDE;
            ctx.ctx.target = 0;
            ctx.ctx.abs = 1;
        }
        /* Compute nullspace as eigenspace of 0 */
        if(ctx.s)
            rcode = slds::la::eigen(A, &v, &iv, ctx.s, NULL, d, k, ctx.ctx);
        else
            rcode = slds::la::eigen(A, &v, &iv, &s, NULL, d, k, ctx.ctx);
    }
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* Set nullspace */
    if(rcode > -1)
    {
        /* Check if the first k eigenvalues are 0 */
        isnull = true;
        for(int i = 0; i < k; i++)
        {
            if(PetscSqrtReal(PetscPowReal(v[i], 2) + PetscPowReal(iv[i], 2)) > nulltol)
            {
                isnull = false;
                break;
            }
        }
        /* Orthonormalize and set values of B */
        if(isnull)
        {
            /* Orthonormalize only if no invariant subspace was computed */
            if(ctx.orthonormalize && !ctx.ctx.returninvsub)
            {
                fullrank = true;
                if(ctx.s)
                    rrcode = slds::la::orthonormalize(ctx.s, d, k, ctx.bv);
                else
                    rrcode = slds::la::orthonormalize(&s, d, k, ctx.bv);
            }
            else
                rrcode = 0;
            /* If orthonormalization succeeded, set result */
            if(rrcode > -1)
            {
                if(ctx.setmat)
                {
                    /* Set up */
                    std::vector<int> idxj(k, 0);
                    for(int i = 0; i < k; i++)
                        idxj[i] = i;
                    /* Set values */
                    if(ctx.s)
                        PetscCall(slds::util::matcolumnsset(B, ctx.s, &idxj, k, d, zerotol));
                    else
                        PetscCall(slds::util::matcolumnsset(B, &s, &idxj, k, d, zerotol));
                }
            }
            /* Else indicate failure */
            else
                fullrank = false;
        }
    }
    /* Clean Up */
    if(!ctx.s)
        for(int i = 0; i < s.size(); i++)
            PetscCall(VecDestroy(&(s[i])));
    /* Return */
    if(rcode < 0)
        return -1;
    else if(!isnull)
        return -2;
    else if(!fullrank)
        return -3;
    else
        return 0;
}
/*!
 *  @brief Determinant of a matrix A in R^(d*d).
 *                                                                                                                                */
PetscErrorCode slds::la::det(Mat A, PetscReal *val)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, info;
    Mat B, F;
    MatType atype;
    MatFactorInfo finfo;
    IS prow, pcol;
    PetscReal re;
    PetscInt exp;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Check matrix type */
    PetscCall(MatGetType(A, &atype));
    /* Convert matrix if required */
    if(strcmp(atype, MATAIJ) && strcmp(atype, MATSEQAIJ) && strcmp(atype, MATMPIAIJ))
    {
        /* Convert matrix */
        PetscCall(MatConvert(A, MATAIJ, MAT_INITIAL_MATRIX, &B));
        /* Set up factor */
        PetscCall(MatGetFactor(B, MATSOLVERMUMPS, MAT_FACTOR_LU, &F));
        PetscCall(MatFactorInfoInitialize(&finfo));
        PetscCall(MatGetOrdering(B, MATORDERINGNATURAL, &prow, &pcol));
        /* Set MUMPS to not use zero pivoting(24), discard factors(31) and to compute the determinant(33) */
        PetscCall(MatMumpsSetIcntl(F, 24, 0));
        PetscCall(MatMumpsSetIcntl(F, 31, 1));
        PetscCall(MatMumpsSetIcntl(F, 33, 1));
        /* Factor the matrix */
        PetscCall(MatLUFactorSymbolic(F, B, prow, pcol, &finfo));
        PetscCall(MatLUFactorNumeric(F, B, &finfo));
        /* Check factorization status */
        PetscCall(MatMumpsGetInfo(F, 1, &info));
        /* Extract real part (12) and exponent (33) of determinant from MUMPS */
        PetscCall(MatMumpsGetRinfog(F, 12, &re));
        PetscCall(MatMumpsGetInfog(F, 34, &exp));
        /* Clean up copy */
        PetscCall(MatDestroy(&B));
    }
    /* Else proceed with the input matrix */
    else
    {
        /* Set up factor */
        PetscCall(MatGetFactor(A, MATSOLVERMUMPS, MAT_FACTOR_LU, &F));
        PetscCall(MatFactorInfoInitialize(&finfo));
        PetscCall(MatGetOrdering(A, MATORDERINGNATURAL, &prow, &pcol));
        /* Set MUMPS to not use zero pivoting(24), discard factors(31) and to compute the determinant(33) */
        PetscCall(MatMumpsSetIcntl(F, 24, 0));
        PetscCall(MatMumpsSetIcntl(F, 31, 1));
        PetscCall(MatMumpsSetIcntl(F, 33, 1));
        /* Factor the matrix */
        PetscCall(MatLUFactorSymbolic(F, A, prow, pcol, &finfo));
        PetscCall(MatLUFactorNumeric(F, A, &finfo));
        /* Check factorization status */
        PetscCall(MatMumpsGetInfo(F, 1, &info));
        /* Extract real part (12) and exponent (33) of determinant from MUMPS */
        PetscCall(MatMumpsGetRinfog(F, 12, &re));
        PetscCall(MatMumpsGetInfog(F, 34, &exp));
    }
    /* If the matrix is singular return 0 */
    if(info == -6 || info == -10)
        *val = 0;
    /* If an error occured return 0 */
    else if(info < 0)
        *val = 0;
    /* Else compute determinant (complex part is ignored since A is real) */
    else
        *val = re*PetscPowReal(2, exp);
    /* Clean up */
    PetscCall(MatDestroy(&F));
    PetscCall(ISDestroy(&prow));
    PetscCall(ISDestroy(&pcol));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the 2-norm for a matrix A in R^(d*d).
 *                                                                                                                                */
PetscErrorCode slds::la::norm2(Mat A, PetscReal *norm, MatProductAlgorithm alg, PetscReal fill, eigenctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    Mat AtA;
    PetscReal v, iv;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute (A^T)A */
    PetscCall(slds::la::matmult(A, A, &AtA, MATPRODUCT_AtB, alg, fill, 0));
    /* Compute the spectral radius of (A^T)A */
    PetscCall(slds::la::spectralr(AtA, &v, &iv, ctx));
    /* Set the 2-norm as the square root of the spectral radius of (A^T)A */
    *norm = PetscSqrtReal(PetscSqrtReal(PetscPowReal(v, 2) + PetscPowReal(iv, 2)));
    /* Clean up */
    PetscCall(MatDestroy(&AtA));
    /* Return */
    return 0;
}
/*!
 *  @brief Orthonormalizes a collection of vectors x in R^d.
 *                                                                                                                                */
PetscErrorCode slds::la::orthonormalize(std::vector<Vec> *x, int d, int n, BV *bv)
{
    /* Set up */
    PetscBool initpetsc, initslepc, lindep;
    int mpirank, fullrank, bvd, bvncol;
    PetscReal norm;
    BV bvx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Only orthogonalize if there is more than one vector */
    if(n > 1)
    {
        /* Initialize */
        if(bv)
            PetscCall(BVGetSizes(*bv, NULL, &bvd, &bvncol));
        if(!bv || bvd != d || bvncol < n)
        {
            PetscCall(BVCreate(PETSC_COMM_WORLD, &bvx));
            PetscCall(BVSetSizes(bvx, PETSC_DECIDE, d, n));
            PetscCall(BVSetFromOptions(bvx));
        }
        else
            bvx = *bv;
        /* Set vectors */
        for(int i = 0; i < n; i++)
            PetscCall(BVInsertVec(bvx, i, (*x)[i]));
        /* Orthonormalize */
        fullrank = true;
        for(int i = 0; i < n; i++)
        {
            PetscCall(BVOrthonormalizeColumn(bvx, i, PETSC_FALSE, &norm, &lindep));
            /* Equality comparison of floats is odd but SLEPc also seems to use this internally (special value?) */
            if(norm == 0.0 || lindep == PETSC_TRUE)
                fullrank = false;
        }
        /* Set result */
        for(int i = 0; i < n; i++)
            PetscCall(BVCopyVec(bvx, i, (*x)[i]));
        /* Clean up */
        if(!bv || bvd != d || bvncol < n)
            PetscCall(BVDestroy(&bvx));
    }
    /* Else normalize the vector */
    else
    {
        /* Normalize */
        fullrank = true;
        PetscCall(VecNormalize((*x)[0], &norm));
        /* Equality comparison of floats is odd but PETSc also seems to use this internally (special value?) */
        if(norm == 0.0)
            fullrank = false;
    }
    /* Return */
    if(!fullrank)
            return -1;
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
