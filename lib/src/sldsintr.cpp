/**********************************************************************************************************************************/
/*! @file sldsintr.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Integration routines.
 *
 *  Integration routines for numeric integration. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <H5Cpp.h>
#include "fastgl.hpp"
#include "sldsintr.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Data for HDF5 output.
 *                                                                                                                                */
class slds::intr::h5node
{
    public:
        int i; /*!< Index. */
        hvl_t h5n; /*!< Handle for writing nodes. */
        h5node(int ii): i(ii) {};
};
/*!
 *  @brief Data for HDF5 output.
 *                                                                                                                                */
class slds::intr::h5weight
{
    public:
        int i; /*!< Index. */
        double w; /*!< Weight. */
        h5weight(int ii, double ww): i(ii), w(ww) {};
};
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Product composite midpoint rule.
 *                                                                                                                                */
PetscErrorCode slds::intr::compositemidpoint(slds::intr::intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d)
{
    /* Set up */
    int n = 1;
    for(int i = 0; i < k; i++)
        n *= nk[i];
    /* Set dimensions */
    Q->d = d;
    Q->k = k;
    Q->n = n;
    /* Reserve memory */
    Q->nodes.reserve(k*n);
    Q->weights.reserve(n);
    /* Initialize the weights */
    for(int i = 0; i < n; i++)
        Q->weights[i] = 1;
    /* Keep track of indices */
    std::vector<int> idx(k, 0);
    /* Calculate nodes and weights */
    for(int i = 0; i < n; i++)
    {
        int p = i, q = n;
        /* Determine current index */
        for(int j = 0; j < k; j++)
        {
            q /= nk[j];
            idx[j] = p/q;
            p %= q;
        }
        /* Set values for the current node and weight */
        for(int kk = 0; kk < k; kk++)
        {
            /* Set node */
            Q->nodes[k*i + kk] = a[kk] + (idx[kk] + 1./2)*(b[kk] - a[kk])/nk[kk];
            /* Set weight */
            Q->weights[i] *= (b[kk] - a[kk])/nk[kk];
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Product composite trapezoidal rule.
 *                                                                                                                                */
PetscErrorCode slds::intr::compositetrapezoid(slds::intr::intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d)
{
    /* Set up */
    int n = 1;
    for(int i = 0; i < k; i++)
        n *= (nk[i] + 1);
    /* Set dimensions */
    Q->d = d;
    Q->k = k;
    Q->n = n;
    /* Reserve memory */
    Q->nodes.reserve(k*n);
    Q->weights.reserve(n);
    /* Initialize the weights */
    for(int i = 0; i < n; i++)
        Q->weights[i] = 1;
    /* Keep track of indices */
    std::vector<int> idx(k, 0);
    /* Calculate nodes and weights */
    for(int i = 0; i < n; i++)
    {
        int p = i, q = n;
        /* Determine current index */
        for(int j = 0; j < k; j++)
        {
            q /= (nk[j] + 1);
            idx[j] = p/q;
            p %= q;
        }
        /* Set values for the current node and weight */
        for(int kk = 0; kk < k; kk++)
        {
            /* Set node */
            Q->nodes[k*i + kk] = a[kk] + idx[kk]*(b[kk] - a[kk])/nk[kk];
            /* Set weight */
            if(idx[kk] % nk[kk] != 0)
                Q->weights[i] *= (b[kk] - a[kk])/nk[kk];
            else
                Q->weights[i] *= (b[kk] - a[kk])/(2*nk[kk]);
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Product composite Simpson's rule.
 *                                                                                                                                */
PetscErrorCode slds::intr::compositesimpson(slds::intr::intrule *Q, PetscReal *a, PetscReal *b, int *nk, int k, int d)
{
    /* Set up */
    int n = 1;
    for(int i = 0; i < k; i++)
        n *= (2*nk[i] + 1);
    /* Set dimensions */
    Q->d = d;
    Q->k = k;
    Q->n = n;
    /* Reserve memory */
    Q->nodes.reserve(k*n);
    Q->weights.reserve(n);
    /* Initialize the weights */
    for(int i = 0; i < n; i++)
        Q->weights[i] = 1;
    /* Keep track of indices */
    std::vector<int> idx(k, 0);
    /* Calculate nodes and weights */
    for(int i = 0; i < n; i++)
    {
        int p = i, q = n;
        /* Determine current index */
        for(int j = 0; j < k; j++)
        {
            q /= (2*nk[j] + 1);
            idx[j] = p/q;
            p %= q;
        }
        /* Set values for the current node and weight */
        for(int kk = 0; kk < k; kk++)
        {
            /* Set node */
            Q->nodes[k*i + kk] = a[kk] + idx[kk]*(b[kk] - a[kk])/(2*nk[kk]);
            /* Set weight */
            if(idx[kk] % (2*nk[kk]) != 0)
            {
                if (idx[kk] % 2 != 0)
                    Q->weights[i] *= 4*(b[kk] - a[kk])/(6*nk[kk]);
                else
                    Q->weights[i] *= 2*(b[kk] - a[kk])/(6*nk[kk]);
            }
            else
                Q->weights[i] *= (b[kk] - a[kk])/(6*nk[kk]);
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Product composite Clenshaw-Curtis rule.
 *                                                                                                                                */
PetscErrorCode slds::intr::compositecc(slds::intr::intrule *Q, PetscReal *a, PetscReal *b, int *nk, int *nr, int k, int d)
{
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Set up */
    int n = 1;
    for(int i = 0; i < k; i++)
        n *= (nr[i] + 1)*nk[i];
    std::vector<std::vector<PetscReal>> basenodes(k, std::vector<PetscReal>()), baseweights(k, std::vector<PetscReal>());
    for(int i = 0; i < k; i++)
    {
        basenodes[i].reserve(nr[i] + 1);
        baseweights[i].reserve(nr[i] + 1);
    }
    /* Set dimensions */
    Q->d = d;
    Q->k = k;
    Q->n = n;
    /* Reserve memory */
    Q->nodes.reserve(k*n);
    Q->weights.reserve(n);
    /* Compute nodes and weights for the rule */
    for(int i = 0; i < k; i++)
    {
        for(int j = 0; j < nr[i] + 1; j++)
        {
            basenodes[i][j] = PetscCosReal(j*pi/nr[i]);
            if(j % nr[i] != 0)
            {
                baseweights[i][j] = 2*(PetscPowReal(nr[i], 2) - 1 - PetscPowReal(-1, j))/(nr[i]*(PetscPowReal(nr[i], 2) - 1));
                for(int l = 1; l < nr[i]/2; l++)
                {
                    baseweights[i][j] -= (4./nr[i])*PetscCosReal(2*j*l*pi/nr[i])/(4*PetscPowReal(l, 2) - 1);
                }
            }
            else
                baseweights[i][j] = 1/(PetscPowReal(nr[i], 2) - 1);
        }
    }
    /* Initialize the weights */
    for(int i = 0; i < n; i++)
        Q->weights[i] = 1;
    /* Keep track of indices */
    std::vector<int> idx(k, 0);
    /* Calculate nodes and weights */
    for(int i = 0; i < n; i++)
    {
        int p = i, q = n;
        /* Determine current index */
        for(int j = 0; j < k; j++)
        {
            q /= (nr[j] + 1)*nk[j];
            idx[j] = p/q;
            p %= q;
        }
        /* Set values for the current node and weight */
        for(int kk = 0; kk < k; kk++)
        {
            /* Set up */
            PetscReal an, bn;
            int in, is;
            /* Find subinterval and index */
            for(int j = 0; j < nk[kk]; j++)
            {
                is = idx[kk]/(nr[kk] + 1);
                in = idx[kk] % (nr[kk] + 1);
            }
            /* Calculate endpoints */
            an = a[kk] + is*(b[kk] - a[kk])/nk[kk];
            bn = a[kk] + (is + 1)*(b[kk] - a[kk])/nk[kk];
            /* Set node */
            Q->nodes[i*k + kk] = basenodes[kk][in]*(bn - an)/2 + (bn + an)/2;
            /* Set weight */
            Q->weights[i] *= (bn - an)*baseweights[kk][in]/2;
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Product composite Gauss-Legendre rule.
 *                                                                                                                                */
PetscErrorCode slds::intr::compositegl(slds::intr::intrule *Q, PetscReal *a, PetscReal *b, int *nk, int *nr, int k, int d)
{
    /* Set up */
    int n = 1;
    for(int i = 0; i < k; i++)
        n *= nr[i]*nk[i];
    std::vector<std::vector<PetscReal>> basenodes(k, std::vector<PetscReal>()), baseweights(k, std::vector<PetscReal>());
    for(int i = 0; i < k; i++)
    {
        basenodes[i].reserve(nr[i]);
        baseweights[i].reserve(nr[i]);
    }
    /* Set dimensions */
    Q->d = d;
    Q->k = k;
    Q->n = n;
    /* Reserve memory */
    Q->nodes.reserve(k*n);
    Q->weights.reserve(n);
    /* Compute nodes and weights for the rule */
    for(int i = 0; i < k; i++)
    {
        for(int j = 0; j < nr[i]; j++)
        {
            fastgl::QuadPair p = fastgl::GLPair (nr[i], j + 1);
            basenodes[i][j] = p.x();
            baseweights[i][j] = p.weight;
        }
    }
    /* Initialize the weights */
    for(int i = 0; i < n; i++)
        Q->weights[i] = 1;
    /* Keep track of indices */
    std::vector<int> idx(k, 0);
    /* Calculate nodes and weights */
    for(int i = 0; i < n; i++)
    {
        int p = i, q = n;
        /* Determine current index */
        for(int j = 0; j < k; j++)
        {
            q /= nr[j]*nk[j];
            idx[j] = p/q;
            p %= q;
        }
        /* Set values for the current node and weight */
        for(int kk = 0; kk < k; kk++)
        {
            /* Set up */
            PetscReal an, bn;
            int in, is;
            /* Find subinterval and index */
            for(int j = 0; j < nk[kk]; j++)
            {
                is = idx[kk]/nr[kk];
                in = idx[kk] % nr[kk];
            }
            /* Calculate endpoints */
            an = a[kk] + is*(b[kk] - a[kk])/nk[kk];
            bn = a[kk] + (is + 1)*(b[kk] - a[kk])/nk[kk];
            /* Set node */
            Q->nodes[i*k + kk] = basenodes[kk][in]*(bn - an)/2 + (bn + an)/2;
            /* Set weight */
            Q->weights[i] *= (bn - an)*baseweights[kk][in]/2;
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Writes the nodes and weights of an integration rule to a HDF5 file.
 *                                                                                                                                */
PetscErrorCode slds::intr::writeh5(slds::intr::intrule *Q, std::string f)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<std::vector<double>> h5nv(Q->n, std::vector<double>(Q->k, 0));
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    H5::H5File file;
    std::vector<slds::intr::h5node> h5n;
    std::vector<slds::intr::h5weight> h5w;
    hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    /* Create HDF5 file on rank 0 */
    if(mpirank == 0)
        file = H5::H5File(f, H5F_ACC_TRUNC);
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Write nodes */
    for(int i = 0; i < Q->n; i++)
    {
        /* Extract node */
        for(int j = 0; j < Q->k; j++)
            h5nv[i][j] = Q->nodes[i*Q->k + j];
        /* Set data only on rank 0 */
        if(mpirank == 0)
            h5n.push_back(slds::intr::h5node(i));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5n.empty())
    {
        /* Define type */
        H5::CompType ntype(sizeof(h5node));
        ntype.insertMember("Index", HOFFSET(slds::intr::h5node, i), H5T_NATIVE_INT);
        ntype.insertMember("Node", HOFFSET(slds::intr::h5node, h5n), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5n.size(); i++)
        {
            h5n[i].h5n.len = h5nv[h5n[i].i].size();
            h5n[i].h5n.p = &h5nv[h5n[i].i][0];
        }
        /* Create dataset */
        hsize_t ndim[1];
        ndim[0] = h5n.size();
        int nrank = sizeof(ndim)/sizeof(hsize_t);
        H5::DataSpace nspace(nrank, ndim);
        H5::DataSet ndataset(file.createDataSet("Nodes", ntype, nspace));
        /* Write dataset */
        ndataset.write(&h5n[0], ntype);
        /* Clear to free up memory */
        h5n.clear();
        h5nv.clear();
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Write weights */
    for(int i = 0; i < Q->n; i++)
    {
        /* Set data only on rank 0 */
        if(mpirank == 0)
            h5w.push_back(slds::intr::h5weight(i, Q->weights[i]));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5w.empty())
    {
        /* Define type */
        H5::CompType wtype(sizeof(h5weight));
        wtype.insertMember("Index", HOFFSET(slds::intr::h5weight, i), H5T_NATIVE_INT);
        wtype.insertMember("Weight", HOFFSET(slds::intr::h5weight, w), H5T_NATIVE_DOUBLE);
        /* Create dataset */
        hsize_t wdim[1];
        wdim[0] = h5w.size();
        int wrank = sizeof(wdim)/sizeof(hsize_t);
        H5::DataSpace wspace(wrank, wdim);
        H5::DataSet wdataset(file.createDataSet("Weights", wtype, wspace));
        /* Write dataset */
        wdataset.write(&h5w[0], wtype);
        /* Clear to free up memory */
        h5w.clear();
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Clean up */
    H5Tclose(h5dblv);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
