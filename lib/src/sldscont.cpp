/**********************************************************************************************************************************/
/*! @file sldscont.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Continuation routines.
 *
 *  Continuation routines. Intended for periodic solutions to periodic difference equations or equilibria of differential equations.
 *  Based on PETSc (https://petsc.org/) and SLEPc (https://slepc.upv.es/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <algorithm>
#include <cmath>
#include <complex>
#include <deque>
#include <numeric>
#include <vector>
#include <H5Cpp.h>
#include "sldscont.hpp"
#include "sldsde.hpp"
#include "sldsdiff.hpp"
#include "sldsla.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Data for default HDF5 output.
 *                                                                                                                                */
class slds::cont::mpch5bpt
{
    public:
        int i; /*!< Index of the bifurcation point. */
        int j; /*!< Index of the iterate. */
        int p; /*!< Period of the bifurcation point. */
        int b; /*!< Index of the branch. */
        int bt; /*!< Type of bifurcation point. */
        hvl_t h5xd; /*!< Handle for writing bifurcation points. */
        mpch5bpt(int ii, int jj, int pp, int bb, int bbt): i(ii), j(jj), p(pp), b(bb), bt(bbt) {};
};
/*!
 *  @brief Data for default HDF5 output.
 *                                                                                                                                */
class slds::cont::mpch5cpt
{
    public:
        int i; /*!< Index of the center. */
        int j; /*!< Index of the iterate. */
        int p; /*!< Period of the point. */
        int b; /*!< Index of the branch. */
        int c; /*!< Specifies if the output is using cyclic matrices. */
        double r; /*!< Radius of the ball. */
        hvl_t h5idx; /*!< Handle for writing indices. */
        hvl_t h5ev; /*!< Handle for writing eigenvalues. */
        hvl_t h5iev; /*!< Handle for writing eigenvalues. */
        hvl_t h5xd; /*!< Handle for writing points. */
        mpch5cpt(int ii, int jj, int pp, int bb, int cc, double rr): i(ii), j(jj), p(pp), b(bb), c(cc), r(rr) {};
};
/*!
 *  @brief Data for default HDF5 output.
 *                                                                                                                                */
class slds::cont::mpch5ppt
{
    public:
        int i; /*!< Index of the point. */
        int j; /*!< Index of the iterate. */
        int p; /*!< Period of the point. */
        int b; /*!< Index of the branch. */
        hvl_t h5xd; /*!< Handle for writing points. */
        mpch5ppt(int ii, int jj, int pp, int bb): i(ii), j(jj), p(pp), b(bb) {};
};
/*!
 *  @brief Data for default HDF5 output.
 *                                                                                                                                */
class slds::cont::mpch5vpt
{
    public:
        int i; /*!< Index of the ball. */
        int j; /*!< Index of the point. */
        hvl_t h5vk; /*!< Handle for writing tangent space coordinates. */
        mpch5vpt(int ii, int jj): i(ii), j(jj) {};
};
/*!
 *  @brief Data for default HDF5 output.
 *                                                                                                                                */
class slds::cont::mpch5tpt
{
    public:
        int i; /*!< Index of the ball. */
        int j; /*!< Index of the basis vector. */
        hvl_t h5xd; /*!< Handle for writing tangent spaces. */
        mpch5tpt(int ii, int jj): i(ii), j(jj) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief System for orthogonal projection onto a manifold. Variant for equilibria.
 *                                                                                                                                */
PetscErrorCode slds::cont::peq(SNES snes, Vec x, Vec fx, void *pctxx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec fxk, v;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(pctxx);
    /* Assign data structures */
    v = pctx.ctx.datactx.yd;
    fxk = pctx.ctx.datactx.yk;
    /* Initialize fx */
    PetscCall(VecSet(fx, 0));
    /* Compute function */
    PetscCall(pctx.feq(snes, x, fx, pctx.feqctx));
    /* Compute last k values of fx */
    if(pctx.xswitch)
        PetscCall(VecAXPBYPCZ(v, 1, -1, 0, x, pctx.x[0]));
    else
    {
        PetscCall(MatMult(pctx.Tx, pctx.vk, v));
        PetscCall(VecAXPBYPCZ(v, 1, -1, -1, x, pctx.x[0]));
    }
    PetscCall(MatMultTranspose(pctx.Tx, v, fxk));
    /* Set last k values of fx */
    PetscCall(slds::util::subvecset(fx, fxk, 0, pctx.k, pctx.d - pctx.k, pctx.ctx.zerotol));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of an orthogonal projection onto a manifold. Variant for equilibria.
 *                                                                                                                                */
PetscErrorCode slds::cont::dpeq(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *pctxx)
{
    /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(pctxx);
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Initialize Dfx (if dfeq is NULL finite differences is used) */
    if(pctx.dfeq)
        PetscCall(pctx.dfeq(snes, x, Dfx, Dfxx, pctx.feqctx));
    else
    {
        /* Set up finite differences context */
        pctx.ctx.slvctx.diffctx.f = pctx.feq;
        pctx.ctx.slvctx.diffctx.fctx = pctx.feqctx;
        /* Evaluate derivative */
        PetscCall(slds::diff::df(NULL, x, Dfx, Dfxx, &pctx.ctx.slvctx.diffctx));
    }
    /* Set last rows of Dfx */
    PetscCall(MatTranspose(pctx.Tx, MAT_INPLACE_MATRIX, &pctx.Tx));
    PetscCall(slds::util::submatset(Dfx, pctx.Tx, 0, 0, pctx.k, pctx.d, pctx.d - pctx.k, 0, pctx.ctx.zerotol));
    PetscCall(MatTranspose(pctx.Tx, MAT_INPLACE_MATRIX, &pctx.Tx));
    /* Return */
    return 0;
}
/*!
 *  @brief System for orthogonal projection onto a manifold. Variant for periodic difference equations.
 *                                                                                                                                */
PetscErrorCode slds::cont::ppd(SNES snes, Vec x, Vec fx, void *pctxx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec v, x0, fxk;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(pctxx);
    /* Assign data structures */
    v = pctx.ctx.datactx.yd;
    x0 = pctx.ctx.datactx.zd;
    fxk = pctx.ctx.datactx.yk;
    /* Initialize x0 and fx */
    PetscCall(VecSet(x0, 0));
    PetscCall(VecSet(fx, 0));
    PetscCall(pctx.ctx.fctx.fcyc(snes, x, fx, pctx.ctx.fctx.fcycctx));
    /* Extract x0 */
    PetscCall(slds::util::subvecget(x, x0, 0, pctx.d - pctx.k, 0, pctx.ctx.zerotol));
    PetscCall(slds::util::subvecget(x, x0, pctx.period*(pctx.d - pctx.k), pctx.period*(pctx.d - pctx.k) + pctx.k,
                                    pctx.d - pctx.k, pctx.ctx.zerotol));
    /* Compute last k values of fx */
    if(pctx.xswitch)
        PetscCall(VecAXPBYPCZ(v, 1, -1, 0, x0, pctx.x[0]));
    else
    {
        PetscCall(MatMult(pctx.Tx, pctx.vk, v));
        PetscCall(VecAXPBYPCZ(v, 1, -1, -1, x0, pctx.x[0]));
    }
    PetscCall(MatMultTranspose(pctx.Tx, v, fxk));
    /* Set last k values of fx */
    PetscCall(slds::util::subvecset(fx, fxk, 0, pctx.k, pctx.period*(pctx.d - pctx.k), pctx.ctx.zerotol));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of an orthogonal projection onto a manifold.
 *                                                                                                                                */
PetscErrorCode slds::cont::dppd(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *pctxx)
{
    /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(pctxx);
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Initialize Dfx (finite difference should be done in dfcyc for efficiency) */
    PetscCall(pctx.ctx.fctx.dfcyc(snes, x, Dfx, Dfxx, pctx.ctx.fctx.fcycctx));
    /* Set last rows of Dfx */
    PetscCall(MatTranspose(pctx.Tx, MAT_INPLACE_MATRIX, &pctx.Tx));
    PetscCall(slds::util::submatset(Dfx, pctx.Tx, 0, 0, pctx.k, pctx.d - pctx.k, pctx.period*(pctx.d - pctx.k), 0,
                                    pctx.ctx.zerotol));
    PetscCall(slds::util::submatset(Dfx, pctx.Tx, 0, pctx.d - pctx.k, pctx.k, pctx.d, pctx.period*(pctx.d - pctx.k),
                                    pctx.period*(pctx.d - pctx.k), pctx.ctx.zerotol));
    PetscCall(MatTranspose(pctx.Tx, MAT_INPLACE_MATRIX, &pctx.Tx));
    /* Return */
    return 0;
}
/*!
 *  @brief Function to compute the tangent space at a given point.
 *                                                                                                                                */
PetscErrorCode slds::cont::txdefault(Mat Dfx, Mat Tx, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, txassembled;
    int mpirank, rcode;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Zero matrix if required */
    PetscCall(MatAssembled(Tx, &txassembled));
    if(txassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Tx));
    /* Compute tangent space as nullspace of the derivative */
    if(pctx.ctx.type == 0)
        rcode = pctx.ctx.fctx.knulleq(Dfx, Tx, pctx.d, pctx.k, pctx.ctx.zerotol, pctx.ctx.nulltol,
                                      pctx.ctx.fctx.knulleqctx);
    else
    {
        /* Compute tangent space via cyclic matrix */
        if(pctx.ctx.usecyclic)
            rcode = pctx.ctx.fctx.knullpd(Dfx, Tx, pctx.period*(pctx.d - pctx.k) + pctx.k, pctx.k,
                                          pctx.ctx.zerotol, pctx.ctx.nulltol, pctx.ctx.fctx.knullpdctx);
        /* Else compute via period operator */
        else
            rcode = pctx.ctx.fctx.knullpd(Dfx, Tx, pctx.d, pctx.k, pctx.ctx.zerotol, pctx.ctx.nulltol,
                                          pctx.ctx.fctx.knullpdctx);
    }
    /* Return */
    return rcode;
}
/*!
 *  @brief Function to compute tangent spaces for equilibria.
 *                                                                                                                                */
PetscErrorCode slds::cont::knulleqdefault(Mat Dfx, Mat Tx, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    slds::cont::mpcpctx *pctxx = static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    if(pctx.ctx.copyectx)
        pctx.ctx.slvctx.knullctx.ctx = pctx.ctx.slvctx.ectx;
    if(pctx.ctx.createbv)
        pctx.ctx.slvctx.knullctx.bv = &pctx.ctx.datactx.bv;
    if(pctx.ctx.slvctx.knullctx.useata)
        pctx.ctx.slvctx.knullctx.AtA = &pctx.ctx.datactx.Pd;
    pctx.ctx.slvctx.knullctx.s = &pctxx->ctx.datactx.sd;
    /* For equilibria compute nullspace of Dfx */
    rcode = slds::la::knull2(Dfx, Tx, d, k, zerotol, nulltol, &pctx.ctx.slvctx.knullctx);
    /* Return */
    return rcode;
}
/*!
 *  @brief Function to compute tangent spaces for periodic difference equations.
 *                                                                                                                                */
PetscErrorCode slds::cont::knullpddefault(Mat Dfx, Mat Tx, int d, int k, PetscReal zerotol, PetscReal nulltol, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode, rrcode;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    slds::cont::mpcpctx *pctxx = static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    if(pctx.ctx.copyectx)
        pctx.ctx.slvctx.knullctx.ctx = pctx.ctx.slvctx.ectx;
    if(pctx.ctx.usecyclic)
    {
        pctx.ctx.slvctx.knullctx.orthonormalize = 0;
        pctx.ctx.slvctx.knullctx.setmat = 0;
        if(pctx.ctx.slvctx.knullctx.useata)
            pctx.ctx.slvctx.knullctx.AtA = &pctx.ctx.datactx.Ppd;
        pctx.ctx.slvctx.knullctx.s = &pctxx->ctx.datactx.spd;
    }
    else
    {
        if(pctx.ctx.createbv)
            pctx.ctx.slvctx.knullctx.bv = &pctx.ctx.datactx.bv;
        if(pctx.ctx.slvctx.knullctx.useata)
            pctx.ctx.slvctx.knullctx.AtA = &pctx.ctx.datactx.Pd;
        pctx.ctx.slvctx.knullctx.s = &pctxx->ctx.datactx.sd;
    }
    /* For periodic difference equations compute nullspace of Dfx - id */
    if(pctx.ctx.usecyclic)
    {
        /* Set Dfx - id */
        PetscCall(slds::util::matdiagonaladd(Dfx, -1, pctx.period*(pctx.d - pctx.k)));
        /* Compute nullspace */
        rcode = slds::la::knull2(Dfx, NULL, pctx.period*(pctx.d - pctx.k) + pctx.k, k, zerotol,
                                 nulltol, &pctx.ctx.slvctx.knullctx);
        /* Restore Dfx - id */
        PetscCall(slds::util::matdiagonaladd(Dfx, 1, pctx.period*(pctx.d - pctx.k)));
    }
    else
    {
        /* Set Dfx - id */
        PetscCall(slds::util::matdiagonaladd(Dfx, -1, pctx.d - pctx.k));
        /* Compute nullspace */
        rcode = slds::la::knull2(Dfx, Tx, pctx.d, k, zerotol, nulltol, &pctx.ctx.slvctx.knullctx);
        /* Restore Dfx - id */
        PetscCall(slds::util::matdiagonaladd(Dfx, 1, pctx.d - pctx.k));
    }
    /* For cyclic matrices extract first component */
    if(rcode > -1 && pctx.ctx.usecyclic)
    {
        /* Set up */
        std::vector<int> idxj(k, 0);
        for(int i = 0; i < k; i++)
            idxj[i] = i;
        /* Expand vectors */
        for(int i = pctxx->ctx.datactx.sd.size(); i < k; i++)
        {
            pctxx->ctx.datactx.sd.push_back(Vec());
            PetscCall(pctx.ctx.fctx.vecset(&pctxx->ctx.datactx.sd.back(), pctx.d, pctx.ctx.fctx.vecctx));
        }
        /* Extract values */
        for(int i = 0; i < k; i++)
        {
            /* Set nonparameter values */
            PetscCall(slds::util::subvecset(pctxx->ctx.datactx.sd[i], pctxx->ctx.datactx.spd[i], 0, pctx.d - pctx.k, 0, -1));
            /* Set parameter values */
            PetscCall(slds::util::subvecset(pctxx->ctx.datactx.sd[i], pctxx->ctx.datactx.spd[i], pctx.period*(pctx.d - pctx.k),
                                            pctx.period*(pctx.d - pctx.k) + pctx.k, pctx.d - pctx.k, -1));
        }
        /* Orthonormalize */
        if(pctx.ctx.createbv)
            rrcode = slds::la::orthonormalize(&pctxx->ctx.datactx.sd, pctx.d, pctx.k, &pctx.ctx.datactx.bv);
        else
            rrcode = slds::la::orthonormalize(&pctxx->ctx.datactx.sd, pctx.d, pctx.k, NULL);
        /* If orthonormalization succeeded, set result */
        if(rrcode > -1)
            PetscCall(slds::util::matcolumnsset(Tx, &pctxx->ctx.datactx.sd, &idxj, pctx.k, pctx.d, -1));
        /* Else indicate failure */
        else
            rcode = -3;
    }
    /* Return */
    return rcode;
}
/*!
 *  @brief Function for bisection line search.
 *                                                                                                                                */
PetscErrorCode slds::cont::lsf(std::vector<Vec> v, Mat Dfv, Mat Dfvp, PetscReal t, int *idx, int idxtype, void *pctxx,
                               void *outctxx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, dfassembled;
    int mpirank, rcode;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(pctxx);
    slds::cont::mpcoutctx *outctx = static_cast<slds::cont::mpcoutctx*>(outctxx);
    /* Zero derivative without parameters if required */
    PetscCall(MatAssembled(Dfvp, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfvp));
    /* Project onto the manifold */
    if(pctx.ctx.type == 0)
        rcode = pctx.ctx.fctx.projecteq(v, Dfv, t, 1, &pctx);
    else
        rcode = pctx.ctx.fctx.projectpd(v, Dfv, t, 1, &pctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver diverged, return -2 */
    if(rcode < 0)
        *idx = -2;
    /* If the solver converged, compute derivative */
    else
    {
        /* Save the sampled point */
        outctx->points.push_back(std::vector<Vec>(pctx.period, Vec()));
        for(int i = 0; i < pctx.period; i++)
        {
            PetscCall(VecDuplicate(v[i], &(outctx->points.back()[i])));
            PetscCall(VecCopy(v[i], outctx->points.back()[i]));
        }
        outctx->pbidx.push_back(pctx.bidx);
        outctx->pparents.push_back(outctx->centers.size());
        outctx->pperiods.push_back(pctx.period);
        /* Extract the submatrix without parameters */
        if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
            PetscCall(slds::util::submatget(Dfv, Dfvp, 0, 0, pctx.period*(pctx.d - pctx.k),
                                            pctx.period*(pctx.d - pctx.k), 0, 0, pctx.ctx.zerotol));
        else
            PetscCall(slds::util::submatget(Dfv, Dfvp, 0, 0, pctx.d - pctx.k, pctx.d - pctx.k, 0, 0, pctx.ctx.zerotol));
        /* Compute index */
        if(pctx.ctx.type == 0 && idxtype == 0)
            PetscCall(slds::cont::idxeqfold(Dfvp, idx, pctx.d - pctx.k));
        else if(pctx.ctx.type == 0 && idxtype == 1)
            PetscCall(slds::cont::idxeqposre(Dfvp, idx, pctx.d - pctx.k, pctx.ctx));
        else if(pctx.ctx.type != 0 && idxtype == 0 && !pctx.ctx.usecyclic)
            PetscCall(slds::cont::idxpdfold(Dfvp, idx, pctx.d - pctx.k));
        else if(pctx.ctx.type != 0 && idxtype == 1 && !pctx.ctx.usecyclic)
            PetscCall(slds::cont::idxpdflip(Dfvp, idx, pctx.d - pctx.k));
        else if(pctx.ctx.type != 0 && idxtype == 2)
        {
            if(pctx.ctx.usecyclic)
            {
                PetscCall(slds::cont::idxpdmorse(Dfvp, idx, pctx.period*(pctx.d - pctx.k), pctx.ctx));
                *idx /= pctx.period;
            }
            else
                PetscCall(slds::cont::idxpdmorse(Dfvp, idx, pctx.d - pctx.k, pctx.ctx));
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Determines type of a bifurcation point. (0 := Fold, 1:= Hopf) for equilibria,
 *         (0 := Fold, 1:= Flip, 2:= Neimark-Sacker) for periodic difference equations.
 *                                                                                                                                */
PetscErrorCode slds::cont::swtype(Mat Dfbp, int *type, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* For equilibria check the eigenvalue with real part closest to 0 */
    if(pctx.ctx.type == 0)
    {
        /* Set up */
        std::vector<PetscReal> v, iv;
        slds::la::eigenctx ectx = pctx.ctx.slvctx.ectx;
        /* Set type */
        ectx.select = EPS_TARGET_REAL;
        ectx.target = 0;
        /* Compute eigenvalue */
        rcode = slds::la::eigen(Dfbp, &v, &iv, NULL, NULL, pctx.d - pctx.k, 1, ectx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If no eigenvalue was found, abort */
        if(rcode < 0)
        {
            *type = -1;
            v = std::vector<PetscReal>(1, std::nan(""));
            iv = std::vector<PetscReal>(1, std::nan(""));
        }
        /* Else determine type */
        else
        {
            /* Check fold */
            if(PetscAbsReal(v[0]) < pctx.ctx.swctx.evtol && PetscAbsReal(iv[0]) < pctx.ctx.swctx.evtol)
                *type = 0;
            /* Check Hopf */
            else if(PetscAbsReal(v[0]) < pctx.ctx.swctx.evtol)
                *type = 1;
            /* If no eigenvalues match, abort */
            else
                *type = -1;
        }
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computed critical eigenvalue (%f, %f) with code %d \n", v[0], iv[0], rcode));
    }
    /* For periodic difference equations check the eigenvalue closest to the unit circle */
    if(pctx.ctx.type != 0)
    {
        /* Set up */
        PetscReal w, iw;
        std::complex<PetscReal> z;
        std::vector<PetscReal> v, iv;
        slds::la::eigenctx ectx = pctx.ctx.slvctx.ectx;
        /* Set type */
        ectx.setcomparemagnitude = 1;
        /* Set target */
        ectx.target = 1;
        /* Compute eigenvalue */
        if(pctx.ctx.usecyclic)
            rcode = slds::la::eigen(Dfbp, &v, &iv, NULL, NULL, pctx.period*(pctx.d - pctx.k), 1, ectx);
        else
            rcode = slds::la::eigen(Dfbp, &v, &iv, NULL, NULL, pctx.d - pctx.k, 1, ectx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If no eigenvalues were found, abort */
        if(rcode < 0)
        {
            *type = -1;
            w = std::nan("");
            iw = std::nan("");
        }
        /* Else determine type */
        else
        {
            /* Extract eigenvalue */
            if(pctx.ctx.usecyclic)
            {
                /* Compute power */
                z = std::complex<PetscReal>(v[0], iv[0]);
                z = std::pow(z, pctx.period);
                w = z.real();
                iw = z.imag();
            }
            else
            {
                w = v[0];
                iw = iv[0];
            }
            /* Check fold */
            if(PetscAbsReal(w - 1) < pctx.ctx.swctx.evtol && PetscAbsReal(iw) < pctx.ctx.swctx.evtol)
                *type = 0;
            /* Check flip */
            else if(PetscAbsReal(w + 1) < pctx.ctx.swctx.evtol && PetscAbsReal(iw) < pctx.ctx.swctx.evtol)
                *type = 1;
            /* Else assume Neimark-Sacker */
            else
                *type = 2;
        }
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                                  "Computed critical eigenvalue (%f, %f) with code %d \n", w, iw, rcode));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Finds the additional nullvector at a bifurcation point.
 *                                                                                                                                */
PetscErrorCode slds::cont::swnull(Vec bnull, Mat Dfb, Mat Tx, Mat Tv, PetscReal t, int d, int k, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    Mat NDfb, Tb;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize */
    PetscCall(pctx.ctx.fctx.matsett(&NDfb, pctx.d, 1, pctx.ctx.fctx.mattctx));
    /* Assign data structures */
    Tb = pctx.ctx.datactx.Tx;
    /* Initialize tangent space */
    PetscCall(MatCopy(Tv, Tb, DIFFERENT_NONZERO_PATTERN));
    /* Interpolate the nullspaces */
    PetscCall(MatAXPY(Tb, -1, Tx, DIFFERENT_NONZERO_PATTERN));
    PetscCall(MatScale(Tb, t));
    PetscCall(MatAXPY(Tb, 1, Tx, DIFFERENT_NONZERO_PATTERN));
    /* Zero parameter columns */
    std::vector<int> idxj(pctx.k, 0);
    std::vector<Vec> dfbc(pctx.k, pctx.ctx.datactx.yd);
    PetscCall(VecSet(pctx.ctx.datactx.yd, 0));
    for(int i = 0; i < pctx.k; i++)
        idxj[i] = pctx.d - pctx.k + i;
    /* Set columns */
    PetscCall(slds::util::matcolumnsset(Dfb, &dfbc, &idxj, pctx.k, pctx.d, -1));
    /* Augment derivative with the interpolated nullspace */
    PetscCall(MatTranspose(Tb, MAT_INPLACE_MATRIX, &Tb));
    PetscCall(slds::util::submatset(Dfb, Tb, 0, 0, pctx.k, pctx.d, pctx.d - pctx.k, 0, -1));
    PetscCall(MatTranspose(Tb, MAT_INPLACE_MATRIX, &Tb));
    /* Find the right null vector */
    if(pctx.ctx.type == 0)
        rcode = pctx.ctx.fctx.knulleq(Dfb, NDfb, pctx.d, 1, pctx.ctx.zerotol, pctx.ctx.nulltol,
                                      pctx.ctx.fctx.knulleqctx);
    else
    {
        /* Compute nullspace via period operator */
        rcode = pctx.ctx.fctx.knullpd(Dfb, NDfb, pctx.d, 1, pctx.ctx.zerotol, pctx.ctx.nulltol,
                                      pctx.ctx.fctx.knullpdctx);
    }
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* Recover nullvector */
    if(rcode > -1)
        PetscCall(MatGetColumnVector(NDfb, bnull, 0));
    /* Clean up */
    PetscCall(MatDestroy(&NDfb));
    /* Return */
    return rcode;
}
/*!
 *  @brief Computes the approximate tangent space for the switching.
 *                                                                                                                                */
PetscErrorCode slds::cont::swtx(Vec xp, Mat Txp, int d, int k, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    Mat Dfxp;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign data structures */
    Dfxp = pctx.ctx.datactx.Dfx;
    /* Compute derivative at the perturbed point */
    PetscCall(slds::cont::df(xp, Dfxp, pctx));
    /* Compute tangent space at perturbed point */
    rcode = pctx.ctx.fctx.tx(Dfxp, Txp, pctx);
    /* Return */
    return rcode;
}
/*!
 *  @brief Computes eigenvalues at a given point.
 *                                                                                                                                */
PetscErrorCode slds::cont::xevsdefault(Mat Dfx, std::vector<PetscReal> *ev, std::vector<PetscReal> *iev, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, dfassembled;
    int mpirank;
    Mat Dfxp;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Only compute eigenvalues if desired */
    if(pctx.ctx.computeev)
    {
        /* Assign data structures */
        if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
            Dfxp = pctx.ctx.datactx.Dfxpdp;
        else
            Dfxp = pctx.ctx.datactx.Dfxp;
        /* Zero derivative without parameters if required */
        PetscCall(MatAssembled(Dfxp, &dfassembled));
        if(dfassembled == PETSC_TRUE)
            PetscCall(MatZeroEntries(Dfxp));
        /* Extract the submatrix without paramteres */
        if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
            PetscCall(slds::util::submatget(Dfx, Dfxp, 0, 0, pctx.period*(pctx.d - pctx.k), pctx.period*(pctx.d - pctx.k),
                                            0, 0, pctx.ctx.zerotol));
        else
            PetscCall(slds::util::submatget(Dfx, Dfxp, 0, 0, pctx.d - pctx.k, pctx.d - pctx.k, 0, 0, pctx.ctx.zerotol));
        /* Set options */
        if(pctx.ctx.type == 0)
            pctx.ctx.slvctx.ectx.select = EPS_LARGEST_REAL;
        else
            pctx.ctx.slvctx.ectx.select = EPS_LARGEST_MAGNITUDE;
        /* Compute eigenvalues */
        if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
            PetscCall(slds::la::eigen(Dfxp, ev, iev, NULL, NULL, pctx.period*(pctx.d - pctx.k), pctx.period*pctx.ctx.nev,
                                    pctx.ctx.slvctx.ectx));
        else
            PetscCall(slds::la::eigen(Dfxp, ev, iev, NULL, NULL, pctx.d - pctx.k, pctx.ctx.nev, pctx.ctx.slvctx.ectx));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Computes indices at a given point.
 *                                                                                                                                */
PetscErrorCode slds::cont::xidxsdefault(Mat Dfx, std::vector<int> *idxx, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc, dfassembled;
    int mpirank;
    Mat Dfxp;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign data structures */
    if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
        Dfxp = pctx.ctx.datactx.Dfxpdp;
    else
        Dfxp = pctx.ctx.datactx.Dfxp;
    /* Zero derivative without parameters if required */
    PetscCall(MatAssembled(Dfxp, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfxp));
    /* Extract the submatrix without paramteres */
    if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
        PetscCall(slds::util::submatget(Dfx, Dfxp, 0, 0, pctx.period*(pctx.d - pctx.k), pctx.period*(pctx.d - pctx.k),
                                        0, 0, pctx.ctx.zerotol));
    else
        PetscCall(slds::util::submatget(Dfx, Dfxp, 0, 0, pctx.d - pctx.k, pctx.d - pctx.k, 0, 0, pctx.ctx.zerotol));
    /* Compute indices */
    if(pctx.ctx.type == 0 && pctx.ctx.idxctx.trackfold)
        PetscCall(slds::cont::idxeqfold(Dfxp, &(*idxx)[0], pctx.d - pctx.k));
    if(pctx.ctx.type == 0 && pctx.ctx.idxctx.trackposre)
        PetscCall(slds::cont::idxeqposre(Dfxp, &(*idxx)[1], pctx.d - pctx.k, pctx.ctx));
    if(pctx.ctx.type != 0 && pctx.ctx.idxctx.trackfold && !pctx.ctx.usecyclic)
        PetscCall(slds::cont::idxpdfold(Dfxp, &(*idxx)[0], pctx.d - pctx.k));
    if(pctx.ctx.type != 0 && pctx.ctx.idxctx.trackflip && !pctx.ctx.usecyclic)
        PetscCall(slds::cont::idxpdflip(Dfxp, &(*idxx)[1], pctx.d - pctx.k));
    if(pctx.ctx.type != 0 && pctx.ctx.idxctx.trackmorse)
    {
        if(pctx.ctx.usecyclic)
        {
            PetscCall(slds::cont::idxpdmorse(Dfxp, &(*idxx)[2], pctx.period*(pctx.d - pctx.k), pctx.ctx));
            (*idxx)[2] /= pctx.period;
        }
        else
            PetscCall(slds::cont::idxpdmorse(Dfxp, &(*idxx)[2], pctx.d - pctx.k, pctx.ctx));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Computes initial indices and tangent spaces.
 *                                                                                                                                */
PetscErrorCode slds::cont::initidxt(std::vector<Vec> x, Vec x0, Vec vk, Mat Dfx, Mat Tx, std::vector<int> *idxx,
                                    std::vector<PetscReal> *ev, std::vector<PetscReal> *iev, slds::cont::mpcpctx pctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    std::vector<Vec> xp(pctx.period, Vec());
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set center */
    for(int i = 0; i < pctx.period; i++)
        PetscCall(VecDuplicate(x[i], &(xp[i])));
    pctx.x = xp;
    PetscCall(VecCopy(x0, pctx.x[0]));
    /* Set predictor direction */
    pctx.vk = vk;
    /* Set tangent space */
    pctx.Tx = Tx;
    /* Project onto the manifold */
    if(pctx.ctx.type == 0)
        rcode = pctx.ctx.fctx.projecteq(x, Dfx, 0, 1, &pctx);
    else
        rcode = pctx.ctx.fctx.projectpd(x, Dfx, 0, 1, &pctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver diverged, return code */
    if(rcode < 0)
        return rcode;
    /* Else compute the tangent space and indices */
    else
    {
        /* Compute tangent space */
        rcode = pctx.ctx.fctx.tx(Dfx, Tx, pctx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If the solver diverged, return code */
        if(rcode < 0)
            return rcode - 100;
        /* Compute indices */
        PetscCall(pctx.ctx.fctx.xidxs(Dfx, idxx, pctx));
        /* Compute eigenvalues */
        (*ev).clear();
        (*iev).clear();
        PetscCall(pctx.ctx.fctx.xevs(Dfx, ev, iev, pctx));
    }
    /* Clean up */
    for(int i = 0; i < xp.size(); i++)
        PetscCall(VecDestroy(&(xp[i])));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes neighbors of a ball.
 *                                                                                                                                */
PetscErrorCode slds::cont::nb(Vec x, std::vector<int> *neighbors, PetscReal *r, int p, int d, int k, slds::cont::mpcctx ctx,
                              slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec diff;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign vector */
    diff = ctx.datactx.yd;
    /* Compute neighbors */
    if(!outctx->centers.empty())
    {
        /* Set up */
        PetscReal dist, distx;
        PetscCall(VecNorm(x, ctx.ntype, &distx));
        /* Filter points via reverse triangle inequality and period */
        for(int i = 0; i < outctx->centers.size(); i++)
        {
            if(PetscAbsReal(distx - (*ctx.datactx.distc)[i]) < *r + outctx->radii[i] + 2*ctx.eps && p == outctx->periods[i])
            {
                PetscCall(VecAXPBYPCZ(diff, 1, -1, 0, x, outctx->centers[i][0]));
                PetscCall(VecNorm(diff, ctx.ntype, &dist));
                /* Check the distances of the centers */
                if(dist < *r + outctx->radii[i] + 2*ctx.eps)
                    (*neighbors).push_back(i);
            }
        }
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Checks if a point is covered.
 *                                                                                                                                */
PetscErrorCode slds::cont::iscovered(Vec p, Vec xj, Mat Txj, PetscReal rj, int *covered, int k,
                                     slds::cont::mpcctx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    PetscReal dist, norm;
    Vec pj, pjk;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign vectors */
    pj = ctx.datactx.zd;
    pjk = ctx.datactx.yk;
    /* Compute inverse projection */
    PetscCall(VecAXPBYPCZ(pj, 1, -1, 0, p, xj));
    PetscCall(MatMultTranspose(Txj, pj, pjk));
    /* Compute norm */
    PetscCall(VecNorm(pjk, ctx.ntype, &norm));
    /* Compute projected distance */
    PetscCall(MatMult(Txj, pjk, pj));
    PetscCall(VecAXPBYPCZ(pj, 1, -1, 1, xj, p));
    PetscCall(VecNorm(pj, ctx.ntype, &dist));
    /* Check if vertex is covered */
    if(norm < rj && dist < ctx.eps)
        *covered = true;
    else
        *covered = false;
    /* Return */
    return 0;
}
/*!
 *  @brief Checks if a point is inside a given region.
 *                                                                                                                                */
PetscErrorCode slds::cont::isinside(Vec x, PetscReal *a, PetscReal *b, int *inside, int d, slds::cont::mpcctx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec xloc;
    VecScatter scatterctx;
    const PetscReal *xxloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign data structures */
    xloc = ctx.datactx.ydloc;
    scatterctx = ctx.datactx.dscatterctx;
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
    /* Initialize inside */
    *inside = true;
    /* Check on all processes */
    PetscCall(VecScatterBegin(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecGetArrayRead(xloc, &xxloc));
    for(int i = 0; i < d; i++)
    {
        /* Break iteration if a component is outside the requested region and mark vertex as outside */
        if(xxloc[i] < a[i] || xxloc[i] > b[i])
        {
            *inside = false;
            break;
        }
    }
    PetscCall(VecRestoreArrayRead(xloc, &xxloc));
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Copies vertices, derivatives and flags.
 *                                                                                                                                */
PetscErrorCode slds::cont::copystep(std::vector<std::vector<Vec>> v, std::vector<Mat> Dfv, std::vector<int> *vadd,
                                    std::vector<std::vector<Vec>> vc, std::vector<Mat> Dfvc, std::vector<int> *vaddc, int d, int k,
                                    slds::cont::mpcpctx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Copy vertices and derivatives */
    for(int i = 0; i < PetscPowReal(2*ctx.ctx.nvertex + 1, k) - 1; i++)
    {
        /* Copy vertex */
        for(int j = 0; j < ctx.period; j++)
            PetscCall(VecCopy(v[i][j], vc[i][j]));
        /* Copy derivative */
        PetscCall(MatCopy(Dfv[i], Dfvc[i], DIFFERENT_NONZERO_PATTERN));
    }
    /* Copy flags */
    std::copy((*vadd).begin(), (*vadd).end(), (*vaddc).begin());
    /* Return */
    return 0;
}
/*!
 *  @brief Computes a step with stepsize control.
 *                                                                                                                                */
PetscErrorCode slds::cont::step(std::vector<Vec> x, Vec vk, Mat Tx, PetscReal *r, PetscReal *a, PetscReal *b, int d, int k,
                                slds::cont::mpcpctx *pctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    std::vector<int> neighbors;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set options */
    pctx->chkdist = 1;
    /* Try to expand radius if possible */
    int decreased = false;
    int expanded = false;
    for(;;)
    {
        /* Print update */
        if(pctx->ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attempting step with radius: %f \n", *r));
        /* Loop over vertices, checking projected distances (this loop has possible restarts) */
        for(int i = 0; i < PetscPowReal(2*pctx->ctx.nvertex + 1, k) - 1; i++)
        {
            /* Set vertex */
            PetscCall(slds::cont::vertexbr(vk, i + 1, *r, pctx->ctx.nvertex, k, pctx->ctx.ntype));
            /* Project onto the manifold */
            if(pctx->ctx.type == 0)
                rcode = pctx->ctx.fctx.projecteq(pctx->ctx.datactx.vs[i], pctx->ctx.datactx.Dfvs[i], 1, 1, pctx);
            else
                rcode = pctx->ctx.fctx.projectpd(pctx->ctx.datactx.vs[i], pctx->ctx.datactx.Dfvs[i], 1, 1, pctx);
            /* Return if there was an error (use PetscCall for clean exit) */
            if(rcode == 100)
                PetscCall(100);
            /* If the solver diverged, decrease radius and try again or break iteration */
            if(rcode < 0)
            {
                /* If we have previously expanded, break */
                if(expanded)
                    break;
                /* Indicate radius has decreased */
                decreased = true;
                /* If new radius is less than the minimum, skip */
                if(*r*pctx->ctx.rdec < pctx->ctx.rmin)
                {
                    *r = pctx->ctx.rmin;
                    continue;
                }
                /* Set new radius */
                *r *= pctx->ctx.rdec;
                /* Reset flags */
                std::fill(pctx->ctx.datactx.vadds.begin(), pctx->ctx.datactx.vadds.end(), false);
                /* Print update */
                if(pctx->ctx.verbose)
                    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attempting step with radius: %f \n", *r));
                /* Restart loop */
                i = -1;
                continue;
            }
            /* If the solver converged check the distance of the projected points and tangent space compatibility */
            else
            {
                /* Save the sampled point */
                outctx->points.push_back(std::vector<Vec>(pctx->period, Vec()));
                for(int j = 0; j < pctx->period; j++)
                {
                    PetscCall(VecDuplicate(pctx->ctx.datactx.vs[i][j], &(outctx->points.back()[j])));
                    PetscCall(VecCopy(pctx->ctx.datactx.vs[i][j], outctx->points.back()[j]));
                }
                outctx->pbidx.push_back(pctx->bidx);
                outctx->pparents.push_back(outctx->centers.size());
                outctx->pperiods.push_back(pctx->period);
                /* If the distance is too large, decrease radius and try again or break iteration */
                if(!pctx->distpass)
                {
                    /* If we have previously expanded, break */
                    if(expanded)
                        break;
                    /* Indicate radius has decreased */
                    decreased = true;
                    /* If new radius is less than the minimum, skip */
                    if(*r*pctx->ctx.rdec < pctx->ctx.rmin)
                    {
                        *r = pctx->ctx.rmin;
                        continue;
                    }
                    /* Set new radius */
                    *r *= pctx->ctx.rdec;
                    /* Reset flags */
                    std::fill(pctx->ctx.datactx.vadds.begin(), pctx->ctx.datactx.vadds.end(), false);
                    /* Print update */
                    if(pctx->ctx.verbose)
                        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attempting step with radius: %f \n", *r));
                    /* Restart loop */
                    i = -1;
                    continue;
                }
                /* If the vertex passed all checks indicate that it should be added to the boundary */
                if(pctx->distpass)
                    pctx->ctx.datactx.vadds[i] = true;
            }
        }
        /* If everything passed and the radius has not decreased, expand and try again */
        if(std::all_of(pctx->ctx.datactx.vadds.begin(), pctx->ctx.datactx.vadds.end(), [](int z){ return z; }) && !decreased)
        {
            /* Indicate we have expanded */
            expanded = true;
            /* Save values */
            PetscCall(slds::cont::copystep(pctx->ctx.datactx.vs, pctx->ctx.datactx.Dfvs, &pctx->ctx.datactx.vadds,
                                           pctx->ctx.datactx.v, pctx->ctx.datactx.Dfv, &pctx->ctx.datactx.vadd, d, k, *pctx));
            /* Reset flags */
            std::fill(pctx->ctx.datactx.vadds.begin(), pctx->ctx.datactx.vadds.end(), false);
            /* Expand radius */
            if((*r)*pctx->ctx.rinc > pctx->ctx.rmax)
                break;
            else
                *r *= pctx->ctx.rinc;
        }
        /* Else exit */
        else
        {
            /* If we have previously expanded, revert radius and exit */
            if(expanded)
            {
                *r /= pctx->ctx.rinc;
                break;
            }
            /* Else save result and exit */
            else
            {
                PetscCall(slds::cont::copystep(pctx->ctx.datactx.vs, pctx->ctx.datactx.Dfvs, &pctx->ctx.datactx.vadds,
                                               pctx->ctx.datactx.v, pctx->ctx.datactx.Dfv, &pctx->ctx.datactx.vadd, d, k, *pctx));
                break;
            }
        }
    }
    /* Update neighbors */
    PetscCall(slds::cont::nb(x[0], &neighbors, r, pctx->period, d, k, pctx->ctx, outctx));
    /* Compute tangents */
    for(int i = 0; i < PetscPowReal(2*pctx->ctx.nvertex + 1, k) - 1; i++)
    {
        /* Set up */
        int inside;
        int covered = false;
        /* Print info */
        if(pctx->ctx.verbose && !pctx->ctx.datactx.vadd[i])
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Projection failed for vertex %d \n", i));
        /* If the vertex has not passed, continue */
        if(!pctx->ctx.datactx.vadd[i])
            continue;
        /* Check if the point is inside */
        PetscCall(slds::cont::isinside(pctx->ctx.datactx.v[i][0], a, b, &inside, pctx->d, pctx->ctx));
        /* If it is not inside, continue */
        if(!inside)
        {
            /* Print info */
            if(pctx->ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Vertex %d is not inside \n", i));
            /* Continue */
            pctx->ctx.datactx.vadd[i] = false;
            continue;
        }
        /* Compute vertex */
        PetscCall(slds::cont::vertexbr(vk, i + 1, *r, pctx->ctx.nvertex, k, pctx->ctx.ntype));
        /* Check if the point is covered */
        if(!neighbors.empty())
        {
            for(int j = 0; j < neighbors.size(); j++)
            {
                /* Check if the periods match */
                if(!(outctx->periods[neighbors[j]] == pctx->period))
                    continue;
                /* Check if the point is covered */
                PetscCall(slds::cont::iscovered(pctx->ctx.datactx.v[i][0], outctx->centers[neighbors[j]][0],
                                                outctx->tangents[neighbors[j]],
                                                outctx->radii[neighbors[j]], &covered, pctx->k, pctx->ctx));
                /* If it is covered, break iteration */
                if(covered)
                    break;
            }
        }
        /* If it is covered, continue */
        if(covered)
        {
            /* Print info */
            if(pctx->ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Vertex %d is covered \n", i));
            /* Continue */
            pctx->ctx.datactx.vadd[i] = false;
            continue;
        }
        /* Compute tangent space */
        rcode = pctx->ctx.fctx.tx(pctx->ctx.datactx.Dfv[i], pctx->ctx.datactx.Tv[i], *pctx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If the solver failed, continue */
        if(rcode < 0)
        {
            /* Print info */
            if(pctx->ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Tangent space computation failed for vertex %d \n", i));
            /* Continue */
            pctx->ctx.datactx.vadd[i] = false;
            continue;
        }
        /* Compute indices */
        PetscCall(pctx->ctx.fctx.xidxs(pctx->ctx.datactx.Dfv[i], &pctx->ctx.datactx.idxv[i], *pctx));
        /* Compute eigenvalues */
        pctx->ctx.datactx.evv[i].clear();
        pctx->ctx.datactx.ievv[i].clear();
        PetscCall(pctx->ctx.fctx.xevs(pctx->ctx.datactx.Dfv[i], &pctx->ctx.datactx.evv[i], &pctx->ctx.datactx.ievv[i], *pctx));
    }
    /* Reset options */
    pctx->chkdist = 0;
    /* Return */
    return 0;
}
/*!
 *  @brief Default cyclic system.
 *                                                                                                                                */
PetscErrorCode slds::cont::fcycdefault(SNES snes, Vec x, Vec fx, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    slds::de::cycctx cctx = slds::de::cycctx(pctx.d, pctx.k, pctx.period, pctx.pdperiod, pctx.ctx.zerotol, pctx.ctx.datactx.xt,
                                             pctx.ctx.datactx.fxt, pctx.ctx.datactx.Dfxt, pctx.ctx.mcyctype,
                                             pctx.ctx.slvctx.diffctx, pctx.fpd, pctx.dfpd, pctx.fpdctx);
    /* Compute cyclic funtion */
    PetscCall(slds::de::fcyc(snes, x, fx, &cctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default cyclic derivative.
 *                                                                                                                                */
PetscErrorCode slds::cont::dfcycdefault(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    slds::de::cycctx cctx = slds::de::cycctx(pctx.d, pctx.k, pctx.period, pctx.pdperiod, pctx.ctx.zerotol, pctx.ctx.datactx.xt,
                                             pctx.ctx.datactx.fxt, pctx.ctx.datactx.Dfxt, pctx.ctx.mcyctype,
                                             pctx.ctx.slvctx.diffctx, pctx.fpd, pctx.dfpd, pctx.fpdctx);
    /* Compute cyclic funtion */
    PetscCall(slds::de::dfcyc(snes, x, Dfx, Dfxx, &cctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default period operator.
 *                                                                                                                                */
PetscErrorCode slds::cont::pfdefault(Vec x, Mat Dfx, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    slds::de::cycctx cctx = slds::de::cycctx(pctx.d, pctx.k, pctx.period, pctx.pdperiod, pctx.ctx.zerotol, pctx.ctx.datactx.xt,
                                             pctx.ctx.datactx.fxt, pctx.ctx.datactx.Dfxt, pctx.ctx.mcyctype,
                                             pctx.ctx.slvctx.diffctx, pctx.fpd, pctx.dfpd, pctx.fpdctx);
    slds::de::pfctx pfctx = slds::de::pfctx(cctx);
    /* Set options */
    pfctx.P = &pctx.ctx.datactx.Pd;
    /* Compute cyclic funtion */
    PetscCall(slds::de::pf(x, Dfx, &pfctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default period operator from iterates.
 *                                                                                                                                */
PetscErrorCode slds::cont::pf2default(std::vector<Vec> *xt, Mat Dfx, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Set data */
    slds::de::cycctx cctx = slds::de::cycctx(pctx.d, pctx.k, pctx.period, pctx.pdperiod, pctx.ctx.zerotol, pctx.ctx.datactx.xt,
                                             pctx.ctx.datactx.fxt, pctx.ctx.datactx.Dfxt, pctx.ctx.mcyctype,
                                             pctx.ctx.slvctx.diffctx, pctx.fpd, pctx.dfpd, pctx.fpdctx);
    slds::de::pfctx pfctx = slds::de::pfctx(cctx);
    /* Set options */
    pfctx.P = &pctx.ctx.datactx.Pd;
    /* Compute cyclic funtion */
    PetscCall(slds::de::pf2(xt, Dfx, &pfctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default vector creation routine.
 *                                                                                                                                */
PetscErrorCode slds::cont::vecsetdefault(Vec *v, int d, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Create vector context */
    slds::util::vecctx vctx = slds::util::vecctx(pctx.ctx.vtype);
    /* Create vector */
    PetscCall(slds::util::vecsetdefault(v, d, &vctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default derivative creation routine.
 *                                                                                                                                */
PetscErrorCode slds::cont::matsetdefault(Mat *M, int n, int m, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Create matrix context */
    slds::util::matctx mctx = slds::util::matctx(pctx.ctx.mtype);
    /* Create matrix */
    PetscCall(slds::util::matsetdefault(M, n, m, &mctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default tangent space creation routine.
 *                                                                                                                                */
PetscErrorCode slds::cont::matsettdefault(Mat *M, int n, int m, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Create matrix context */
    slds::util::matctx mctx = slds::util::matctx(pctx.ctx.mttype);
    /* Create matrix */
    PetscCall(slds::util::matsetdefault(M, n, m, &mctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default cyclic derivative creation routine.
 *                                                                                                                                */
PetscErrorCode slds::cont::matsetcycdefault(Mat *M, int n, int m, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, period;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Calculate period */
    period = (n - pctx.k)/(pctx.d - pctx.k);
    /* Create matrix context */
    slds::de::cycctx cctx = slds::de::cycctx(pctx.d, pctx.k, period, pctx.pdperiod, pctx.ctx.zerotol, Vec(), Vec(), Mat(),
                                             pctx.ctx.mcyctype, slds::diff::dfctx(), NULL, NULL, NULL);
    /* Create matrix */
    PetscCall(slds::de::initdfcycaijdense(M, &cctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default cyclic derivative without parameters creation routine.
 *                                                                                                                                */
PetscErrorCode slds::cont::matsetcycpdefault(Mat *M, int n, int m, void *ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, period;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::cont::mpcpctx pctx = *static_cast<slds::cont::mpcpctx*>(ctx);
    /* Calculate period */
    period = n/(pctx.d - pctx.k);
    /* Create matrix context */
    slds::de::cycctx cctx= slds::de::cycctx(pctx.d - pctx.k, 0, period, pctx.pdperiod, pctx.ctx.zerotol, Vec(), Vec(), Mat(),
                                            pctx.ctx.mcyctype, slds::diff::dfctx(), NULL, NULL, NULL);
    /* Create matrix */
    PetscCall(slds::de::initdfcycaijdense(M, &cctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing bifurcation points.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5defaultb(H5::H5File *file, slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<PetscReal> h5xd(ctx.d, 0);
    std::vector<std::vector<double>> h5xdv;
    const PetscReal *xxloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    std::vector<slds::cont::mpch5bpt> h5b;
    /* Assign data structures */
    Vec xloc = ctx.ctx.datactx.ydloc;
    VecScatter scatterctx = ctx.ctx.datactx.dscatterctx;
    /* Write bifurcation points */
    for(int i = 0; i < outctx->bpoints.size(); i++)
    {
        for(int j = 0; j < outctx->bperiods[i]; j++)
        {
            /* Bind vectors to CPU */
            PetscCall(VecBindToCPU(outctx->bpoints[i][j], PETSC_TRUE));
            PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
            /* Extract point */
            PetscCall(VecScatterBegin(scatterctx, outctx->bpoints[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            PetscCall(VecScatterEnd(scatterctx, outctx->bpoints[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            /* Set data only on rank 0 */
            if(mpirank == 0)
            {
                PetscCall(VecGetArrayRead(xloc, &xxloc));
                for(int k = 0; k < ctx.d; k++)
                    h5xd[k] = xxloc[k];
                PetscCall(VecRestoreArrayRead(xloc, &xxloc));
                h5b.push_back(slds::cont::mpch5bpt(i, j, outctx->bperiods[i], outctx->bbidx[i], outctx->btypes[i]));
                h5xdv.push_back(h5xd);
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Unbind vectors from CPU */
            PetscCall(VecBindToCPU(outctx->bpoints[i][j], PETSC_FALSE));
            PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5b.empty())
    {
        /* Create variable length type */
        hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
        /* Define type */
        H5::CompType btype(sizeof(slds::cont::mpch5bpt));
        btype.insertMember("Index", HOFFSET(slds::cont::mpch5bpt, i), H5T_NATIVE_INT);
        btype.insertMember("Iterate", HOFFSET(slds::cont::mpch5bpt, j), H5T_NATIVE_INT);
        btype.insertMember("Period", HOFFSET(slds::cont::mpch5bpt, p), H5T_NATIVE_INT);
        btype.insertMember("Branch", HOFFSET(slds::cont::mpch5bpt, b), H5T_NATIVE_INT);
        btype.insertMember("Type", HOFFSET(slds::cont::mpch5bpt, bt), H5T_NATIVE_INT);
        btype.insertMember("Point", HOFFSET(slds::cont::mpch5bpt, h5xd), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5b.size(); i++)
        {
            h5b[i].h5xd.len = h5xdv[i].size();
            h5b[i].h5xd.p = &h5xdv[i][0];
        }
        /* Create dataset */
        hsize_t bdim[1];
        bdim[0] = h5b.size();
        int brank = sizeof(bdim)/sizeof(hsize_t);
        H5::DataSpace bspace(brank, bdim);
        H5::DataSet bdataset(file->createDataSet("Bifurcation points", btype, bspace));
        /* Write dataset */
        bdataset.write(&h5b[0], btype);
        /* Clean up */
        H5Tclose(h5dblv);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing output.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5defaultc(H5::H5File *file, slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<PetscReal> h5xd(ctx.d, 0);
    std::vector<std::vector<int>> h5idxv;
    std::vector<std::vector<double>> h5evv, h5ievv, h5xdv;
    const PetscReal *xxloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    std::vector<slds::cont::mpch5cpt> h5c;
    /* Assign data structures */
    Vec xloc = ctx.ctx.datactx.ydloc;
    VecScatter scatterctx = ctx.ctx.datactx.dscatterctx;
    /* Write centers */
    for(int i = 0; i < outctx->centers.size(); i++)
    {
        for(int j = 0; j < outctx->periods[i]; j++)
        {
            /* Bind vectors to CPU */
            PetscCall(VecBindToCPU(outctx->centers[i][j], PETSC_TRUE));
            PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
            /* Extract point */
            PetscCall(VecScatterBegin(scatterctx, outctx->centers[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            PetscCall(VecScatterEnd(scatterctx, outctx->centers[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            /* Set data only on rank 0 */
            if(mpirank == 0)
            {
                PetscCall(VecGetArrayRead(xloc, &xxloc));
                for(int k = 0; k < ctx.d; k++)
                    h5xd[k] = xxloc[k];
                PetscCall(VecRestoreArrayRead(xloc, &xxloc));
                if(ctx.ctx.type != 0 && ctx.ctx.usecyclic)
                    h5c.push_back(slds::cont::mpch5cpt(i, j, outctx->periods[i], outctx->bidx[i], 1, outctx->radii[i]));
                else
                    h5c.push_back(slds::cont::mpch5cpt(i, j, outctx->periods[i], outctx->bidx[i], 0, outctx->radii[i]));
                h5idxv.push_back(outctx->idx[i]);
                if(outctx->ev[i].empty())
                {
                    h5evv.push_back(std::vector<double>(1, std::nan("")));
                    h5ievv.push_back(std::vector<double>(1, std::nan("")));
                }
                else
                {
                    h5evv.push_back(outctx->ev[i]);
                    h5ievv.push_back(outctx->iev[i]);
                }
                h5xdv.push_back(h5xd);
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Unbind vectors from CPU */
            PetscCall(VecBindToCPU(outctx->centers[i][j], PETSC_FALSE));
            PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5c.empty())
    {
        /* Create variable length types */
        hid_t h5intv = H5Tvlen_create(H5T_NATIVE_INT);
        hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
        /* Define type */
        H5::CompType ctype(sizeof(slds::cont::mpch5cpt));
        ctype.insertMember("Index", HOFFSET(slds::cont::mpch5cpt, i), H5T_NATIVE_INT);
        ctype.insertMember("Iterate", HOFFSET(slds::cont::mpch5cpt, j), H5T_NATIVE_INT);
        ctype.insertMember("Period", HOFFSET(slds::cont::mpch5cpt, p), H5T_NATIVE_INT);
        ctype.insertMember("Branch", HOFFSET(slds::cont::mpch5cpt, b), H5T_NATIVE_INT);
        ctype.insertMember("Cyclic", HOFFSET(slds::cont::mpch5cpt, c), H5T_NATIVE_INT);
        ctype.insertMember("Radius", HOFFSET(slds::cont::mpch5cpt, r), H5T_NATIVE_DOUBLE);
        ctype.insertMember("Indices", HOFFSET(slds::cont::mpch5cpt, h5idx), h5intv);
        ctype.insertMember("Eigenvalues (real part)", HOFFSET(slds::cont::mpch5cpt, h5ev), h5dblv);
        ctype.insertMember("Eigenvalues (imaginary part)", HOFFSET(slds::cont::mpch5cpt, h5iev), h5dblv);
        ctype.insertMember("Center", HOFFSET(slds::cont::mpch5cpt, h5xd), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5c.size(); i++)
        {
            h5c[i].h5idx.len = h5idxv[i].size();
            h5c[i].h5idx.p = &h5idxv[i][0];
            h5c[i].h5ev.len = h5evv[i].size();
            h5c[i].h5ev.p = &h5evv[i][0];
            h5c[i].h5iev.len = h5ievv[i].size();
            h5c[i].h5iev.p = &h5ievv[i][0];
            h5c[i].h5xd.len = h5xdv[i].size();
            h5c[i].h5xd.p = &h5xdv[i][0];
        }
        /* Create dataset */
        hsize_t cdim[1];
        cdim[0] = h5c.size();
        int crank = sizeof(cdim)/sizeof(hsize_t);
        H5::DataSpace cspace(crank, cdim);
        H5::DataSet cdataset(file->createDataSet("Balls", ctype, cspace));
        /* Write dataset */
        cdataset.write(&h5c[0], ctype);
        /* Clean up */
        H5Tclose(h5intv);
        H5Tclose(h5dblv);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing output.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5defaultp(H5::H5File *file, slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<PetscReal> h5xd(ctx.d, 0);
    std::vector<std::vector<double>> h5xdv;
    const PetscReal *xxloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    std::vector<slds::cont::mpch5ppt> h5p;
    /* Assign data structures */
    Vec xloc = ctx.ctx.datactx.ydloc;
    VecScatter scatterctx = ctx.ctx.datactx.dscatterctx;
    /* Write points */
    for(int j = 0; j < outctx->points.size(); j++)
    {
        for(int k = 0; k < outctx->pperiods[j]; k++)
        {
            /* Bind vectors to CPU */
            PetscCall(VecBindToCPU(outctx->points[j][k], PETSC_TRUE));
            PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
            /* Extract point */
            PetscCall(VecScatterBegin(scatterctx, outctx->points[j][k], xloc, INSERT_VALUES, SCATTER_FORWARD));
            PetscCall(VecScatterEnd(scatterctx, outctx->points[j][k], xloc, INSERT_VALUES, SCATTER_FORWARD));
            /* Set data only on rank 0 */
            if(mpirank == 0)
            {
                PetscCall(VecGetArrayRead(xloc, &xxloc));
                for(int h = 0; h < ctx.d; h++)
                    h5xd[h] = xxloc[h];
                PetscCall(VecRestoreArrayRead(xloc, &xxloc));
                h5p.push_back(slds::cont::mpch5ppt(j, k, outctx->pperiods[j], outctx->pbidx[j]));
                h5xdv.push_back(h5xd);
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Unbind vectors from CPU */
            PetscCall(VecBindToCPU(outctx->points[j][k], PETSC_FALSE));
            PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5p.empty())
    {
        /* Create variable length type */
        hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
        /* Define type */
        H5::CompType ptype(sizeof(slds::cont::mpch5ppt));
        ptype.insertMember("Index", HOFFSET(slds::cont::mpch5ppt, i), H5T_NATIVE_INT);
        ptype.insertMember("Iterate", HOFFSET(slds::cont::mpch5ppt, j), H5T_NATIVE_INT);
        ptype.insertMember("Period", HOFFSET(slds::cont::mpch5ppt, p), H5T_NATIVE_INT);
        ptype.insertMember("Branch", HOFFSET(slds::cont::mpch5ppt, b), H5T_NATIVE_INT);
        ptype.insertMember("Point", HOFFSET(slds::cont::mpch5ppt, h5xd), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5p.size(); i++)
        {
            h5p[i].h5xd.len = h5xdv[i].size();
            h5p[i].h5xd.p = &h5xdv[i][0];
        }
        /* Create dataset */
        hsize_t pdim[1];
        pdim[0] = h5p.size();
        int prank = sizeof(pdim)/sizeof(hsize_t);
        H5::DataSpace pspace(prank, pdim);
        H5::DataSet pdataset(file->createDataSet("Points", ptype, pspace));
        /* Write dataset */
        pdataset.write(&h5p[0], ptype);
        /* Clean up */
        H5Tclose(h5dblv);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing output.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5defaultv(H5::H5File *file, slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, covered;
    PetscReal dist, distk;
    Vec xkloc;
    VecScatter kscatterctx;
    std::vector<PetscReal> h5vk(ctx.k, 0), distc(outctx->centers.size(), 0), distp(outctx->points.size());
    std::vector<std::vector<double>> h5vkv;
    const PetscReal *xxkloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    std::vector<slds::cont::mpch5vpt> h5v;
    /* Assign data structures */
    Vec diff = ctx.ctx.datactx.yd;
    /* Initialize scatter */
    PetscCall(VecScatterCreateToZero(ctx.ctx.datactx.yk, &kscatterctx, &xkloc));
    /* Compute distances */
    for(int i = 0; i < outctx->centers.size(); i++)
    {
        PetscCall(VecNorm(outctx->centers[i][0], ctx.ctx.ntype, &dist));
        distc[i] = dist;
    }
    for(int i = 0; i < outctx->points.size(); i++)
    {
        PetscCall(VecNorm(outctx->points[i][0], ctx.ctx.ntype, &dist));
        distp[i] = dist;
    }
    /* Write balls */
    for(int i = 0; i < outctx->centers.size(); i++)
    {
        /* Write center */
        std::fill(h5vk.begin(), h5vk.end(), 0);
        h5v.push_back(slds::cont::mpch5vpt(i, -1));
        h5vkv.push_back(h5vk);
        /* Write points in ball */
        for(int j = 0; j < outctx->points.size(); j++)
        {
            /* Check if the periods match */
            if(!(outctx->periods[i] == outctx->pperiods[j]))
                continue;
            /* Filter points via distance */
            if(!(outctx->pparents[j] == i))
            {
                /* Filter via reverse triangle inequality */
                if(PetscAbsReal(distc[i] - distp[j]) > outctx->radii[i] + ctx.ctx.eps)
                    continue;
                /* Check distances */
                PetscCall(VecAXPBYPCZ(diff, 1, -1, 0, outctx->points[j][0], outctx->centers[i][0]));
                PetscCall(VecNorm(diff, ctx.ctx.ntype, &dist));
                if(dist > outctx->radii[i] + ctx.ctx.eps)
                    continue;
            }
            /* Check if the point belongs to the ball */
            PetscCall(slds::cont::iscovered(outctx->points[j][0], outctx->centers[i][0],
                                            outctx->tangents[i],
                                            outctx->radii[i], &covered, ctx.k, ctx.ctx));
            /* Extract norm */
            PetscCall(VecNorm(ctx.ctx.datactx.yk, ctx.ctx.ntype, &distk));
            /* Add the point */
            if((covered && distk > 0) ||
               (outctx->pparents[j] == i && distk < (ctx.ctx.rdec + 1)*outctx->radii[i]/(2*ctx.ctx.rdec)))
            {
                /* Bind vectors to CPU */
                PetscCall(VecBindToCPU(ctx.ctx.datactx.yk, PETSC_TRUE));
                PetscCall(VecBindToCPU(xkloc, PETSC_TRUE));
                /* Extract tangent space coordinates */
                PetscCall(VecScatterBegin(kscatterctx, ctx.ctx.datactx.yk, xkloc, INSERT_VALUES, SCATTER_FORWARD));
                PetscCall(VecScatterEnd(kscatterctx, ctx.ctx.datactx.yk, xkloc, INSERT_VALUES, SCATTER_FORWARD));
                /* Set data only on rank 0 */
                if(mpirank == 0)
                {
                    PetscCall(VecGetArrayRead(xkloc, &xxkloc));
                    for(int k = 0; k < ctx.k; k++)
                        h5vk[k] = xxkloc[k];
                    PetscCall(VecRestoreArrayRead(xkloc, &xxkloc));
                    h5v.push_back(slds::cont::mpch5vpt(i, j));
                    h5vkv.push_back(h5vk);
                }
                /* Sync processes */
                PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
                /* Unbind vectors from CPU */
                PetscCall(VecBindToCPU(ctx.ctx.datactx.yk, PETSC_FALSE));
                PetscCall(VecBindToCPU(xkloc, PETSC_FALSE));
            }
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5v.empty())
    {
        /* Create variable length type */
        hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
        /* Define type */
        H5::CompType vtype(sizeof(slds::cont::mpch5vpt));
        vtype.insertMember("Ball", HOFFSET(slds::cont::mpch5vpt, i), H5T_NATIVE_INT);
        vtype.insertMember("Point", HOFFSET(slds::cont::mpch5vpt, j), H5T_NATIVE_INT);
        vtype.insertMember("v", HOFFSET(slds::cont::mpch5vpt, h5vk), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5v.size(); i++)
        {
            h5v[i].h5vk.len = h5vkv[i].size();
            h5v[i].h5vk.p = &h5vkv[i][0];
        }
        /* Create dataset */
        hsize_t vdim[1];
        vdim[0] = h5v.size();
        int vrank = sizeof(vdim)/sizeof(hsize_t);
        H5::DataSpace vspace(vrank, vdim);
        H5::DataSet vdataset(file->createDataSet("Ballpoints", vtype, vspace));
        /* Write dataset */
        vdataset.write(&h5v[0], vtype);
        /* Clean up */
        H5Tclose(h5dblv);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Clean up */
    PetscCall(VecDestroy(&xkloc));
    PetscCall(VecScatterDestroy(&kscatterctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing output.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5defaultt(H5::H5File *file, slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<PetscReal> h5xd(ctx.d, 0);
    std::vector<std::vector<double>> h5xdv;
    const PetscReal *xxloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    std::vector<slds::cont::mpch5tpt> h5t;
    /* Assign data structures */
    Vec xloc = ctx.ctx.datactx.ydloc;
    VecScatter scatterctx = ctx.ctx.datactx.dscatterctx;
    /* Write tangents */
    for(int i = 0; i < outctx->tangents.size(); i++)
    {
        for(int j = 0; j < ctx.k; j++)
        {
            /* Bind vectors to CPU */
            PetscCall(VecBindToCPU(ctx.ctx.datactx.xd, PETSC_TRUE));
            PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
            /* Get column */
            PetscCall(MatGetColumnVector(outctx->tangents[i], ctx.ctx.datactx.xd, j));
            /* Scatter column */
            PetscCall(VecScatterBegin(scatterctx, ctx.ctx.datactx.xd, xloc, INSERT_VALUES, SCATTER_FORWARD));
            PetscCall(VecScatterEnd(scatterctx, ctx.ctx.datactx.xd, xloc, INSERT_VALUES, SCATTER_FORWARD));
            /* Set data only on rank 0 */
            if(mpirank == 0)
            {
                PetscCall(VecGetArrayRead(xloc, &xxloc));
                for(int k = 0; k < ctx.d; k++)
                    h5xd[k] = xxloc[k];
                PetscCall(VecRestoreArrayRead(xloc, &xxloc));
                h5t.push_back(slds::cont::mpch5tpt(i, j));
                h5xdv.push_back(h5xd);
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Unbind vectors from CPU */
            PetscCall(VecBindToCPU(ctx.ctx.datactx.xd, PETSC_FALSE));
            PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5t.empty())
    {
        /* Create variable length type */
        hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
        /* Define type */
        H5::CompType ttype(sizeof(slds::cont::mpch5tpt));
        ttype.insertMember("Index", HOFFSET(slds::cont::mpch5tpt, i), H5T_NATIVE_INT);
        ttype.insertMember("Element", HOFFSET(slds::cont::mpch5tpt, j), H5T_NATIVE_INT);
        ttype.insertMember("Vector", HOFFSET(slds::cont::mpch5tpt, h5xd), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5t.size(); i++)
        {
            h5t[i].h5xd.len = h5xdv[i].size();
            h5t[i].h5xd.p = &h5xdv[i][0];
        }
        /* Create dataset */
        hsize_t tdim[1];
        tdim[0] = h5t.size();
        int trank = sizeof(tdim)/sizeof(hsize_t);
        H5::DataSpace tspace(trank, tdim);
        H5::DataSet tdataset(file->createDataSet("Tangents", ttype, tspace));
        /* Write dataset */
        tdataset.write(&h5t[0], ttype);
        /* Clean up */
        H5Tclose(h5dblv);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Return */
    return 0;
}
/*!
 *  @brief Default routine for writing output.
 *                                                                                                                                */
PetscErrorCode slds::cont::writeouth5default(slds::cont::mpcpctx ctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    H5::H5File file;
    /* Create HDF5 file on rank 0 */
    if(mpirank == 0)
        file = H5::H5File(ctx.ctx.fname, H5F_ACC_TRUNC);
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Write bifurcation points */
    PetscCall(slds::cont::writeouth5defaultb(&file, ctx, outctx));
    /* Write centers */
    PetscCall(slds::cont::writeouth5defaultc(&file, ctx, outctx));
    /* Write points */
    PetscCall(slds::cont::writeouth5defaultp(&file, ctx, outctx));
    /* Write balls */
    PetscCall(slds::cont::writeouth5defaultv(&file, ctx, outctx));
    /* Write tangents */
    PetscCall(slds::cont::writeouth5defaultt(&file, ctx, outctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets 1-norm nonlinear solver options for mpc.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpcl1snesoptions(SNES snes, void *snesctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::solve::fsolvectx ctx = *static_cast<slds::solve::fsolvectx*>(snesctx);
    /* Set convergence test */
    PetscCall(SNESSetConvergenceTest(snes, slds::solve::snesconvergedl1, NULL, NULL));
    /* Call user provided options routine */
    if(ctx.snesoptions)
        PetscCall(ctx.snesoptions(snes, ctx.snesctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets max-norm nonlinear solver options for mpc.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpclinfsnesoptions(SNES snes, void *snesctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::solve::fsolvectx ctx = *static_cast<slds::solve::fsolvectx*>(snesctx);
    /* Set convergence test */
    PetscCall(SNESSetConvergenceTest(snes, slds::solve::snesconvergedlinf, NULL, NULL));
    /* Call user provided options routine */
    if(ctx.snesoptions)
        PetscCall(ctx.snesoptions(snes, ctx.snesctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Sets up mpc options.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpcsetoptions(slds::cont::mpcpctx *pctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set finite difference parameters and routines */
    pctx->ctx.slvctx.diffctx.d = pctx->d;
    pctx->ctx.slvctx.diffctx.zerotol = pctx->ctx.zerotol;
    if(!pctx->ctx.slvctx.diffctx.initnz)
        pctx->ctx.slvctx.diffctx.initnz = slds::diff::initnzfull;
    /* Set nonlinear solver parameters and routines */
    pctx->ctx.slvctx.fctx.zerotol = pctx->ctx.zerotol;
    if(pctx->ctx.ntype == NORM_1)
    {
        pctx->ctx.slvctx.fctx2 = pctx->ctx.slvctx.fctx;
        pctx->ctx.slvctx.fctx.snesoptions = slds::cont::mpcl1snesoptions;
        pctx->ctx.slvctx.fctx.snesctx = &pctx->ctx.slvctx.fctx2;
    }
    else if(pctx->ctx.ntype == NORM_INFINITY)
    {
        pctx->ctx.slvctx.fctx2 = pctx->ctx.slvctx.fctx;
        pctx->ctx.slvctx.fctx.snesoptions = slds::cont::mpclinfsnesoptions;
        pctx->ctx.slvctx.fctx.snesctx = &pctx->ctx.slvctx.fctx2;
    }
    /* Set routines */
    if(!pctx->ctx.fctx.projecteq)
        pctx->ctx.fctx.projecteq = slds::cont::projecteqdefault;
    if(!pctx->ctx.fctx.projectpd)
        pctx->ctx.fctx.projectpd = slds::cont::projectpddefault;
    if(!pctx->ctx.fctx.tx)
        pctx->ctx.fctx.tx = slds::cont::txdefault;
    if(!pctx->ctx.fctx.knulleq)
    {
        pctx->ctx.fctx.knulleq = slds::cont::knulleqdefault;
        pctx->ctx.fctx.knulleqctx = pctx;
    }
    if(!pctx->ctx.fctx.knullpd)
    {
        pctx->ctx.fctx.knullpd = slds::cont::knullpddefault;
        pctx->ctx.fctx.knullpdctx = pctx;
    }
    if(!pctx->ctx.fctx.fcyc)
    {
        pctx->ctx.fctx.fcyc = slds::cont::fcycdefault;
        pctx->ctx.fctx.dfcyc = slds::cont::dfcycdefault;
        pctx->ctx.fctx.fcycctx = pctx;
    }
    if(!pctx->ctx.fctx.pf)
    {
        pctx->ctx.fctx.pf = slds::cont::pfdefault;
        pctx->ctx.fctx.pf2 = slds::cont::pf2default;
        pctx->ctx.fctx.pfctx = pctx;
    }
    if(!pctx->ctx.fctx.xevs)
        pctx->ctx.fctx.xevs = slds::cont::xevsdefault;
    if(!pctx->ctx.fctx.xidxs)
        pctx->ctx.fctx.xidxs = slds::cont::xidxsdefault;
    if(!pctx->ctx.fctx.vecset)
    {
        pctx->ctx.fctx.vecset = slds::cont::vecsetdefault;
        pctx->ctx.fctx.vecctx = pctx;
    }
    if(!pctx->ctx.fctx.matset)
    {
        pctx->ctx.fctx.matset = slds::cont::matsetdefault;
        pctx->ctx.fctx.matctx = pctx;
    }
    if(!pctx->ctx.fctx.matsetp)
    {
        pctx->ctx.fctx.matsetp = slds::cont::matsetdefault;
        pctx->ctx.fctx.matpctx = pctx;
    }
    if(!pctx->ctx.fctx.matsett)
    {
        pctx->ctx.fctx.matsett = slds::cont::matsettdefault;
        pctx->ctx.fctx.mattctx = pctx;
    }
    if(!pctx->ctx.fctx.matsetcyc)
    {
        pctx->ctx.fctx.matsetcyc = slds::cont::matsetcycdefault;
        pctx->ctx.fctx.matcycctx = pctx;
    }
    if(!pctx->ctx.fctx.matsetcycp)
    {
        pctx->ctx.fctx.matsetcycp = slds::cont::matsetcycpdefault;
        pctx->ctx.fctx.matcycpctx = pctx;
    }
    if(!pctx->ctx.fctx.sw)
        pctx->ctx.fctx.sw = slds::cont::swdefault;
    if(!pctx->ctx.fctx.writeout)
        pctx->ctx.fctx.writeout = slds::cont::writeouth5default;
    /* Set eigenvalue value solver routines */
    pctx->ctx.slvctx.ectx.vecset = pctx->ctx.fctx.vecset;
    pctx->ctx.slvctx.ectx.vecctx = pctx->ctx.fctx.vecctx;
    pctx->ctx.slvctx.knullctx.ctx.vecset = pctx->ctx.fctx.vecset;
    pctx->ctx.slvctx.knullctx.ctx.vecctx = pctx->ctx.fctx.vecctx;
    /* Return */
    return 0;
}
/*!
 *  @brief Sets up mpc data structures.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpcsetdata(slds::cont::mpcpctx *pctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Create data structure for orthonormalization */
    if(pctx->ctx.createbv)
    {
        PetscCall(BVCreate(PETSC_COMM_WORLD, &pctx->ctx.datactx.bv));
        PetscCall(BVSetSizes(pctx->ctx.datactx.bv, PETSC_DECIDE, pctx->d, pctx->k));
        PetscCall(BVSetFromOptions(pctx->ctx.datactx.bv));
    }
    /* Create vectors */
    if(pctx->ctx.type != 0)
    {
        PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.xt, pctx->d, pctx->ctx.fctx.vecctx));
        PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.fxt, pctx->d, pctx->ctx.fctx.vecctx));
        PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.xpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                        pctx->ctx.fctx.vecctx));
        PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.ypd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                        pctx->ctx.fctx.vecctx));
        PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.zpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                        pctx->ctx.fctx.vecctx));
    }
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.vd, pctx->d, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.wd, pctx->d, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.xd, pctx->d, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.yd, pctx->d, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.zd, pctx->d, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.xk, pctx->k, pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.yk, pctx->k, pctx->ctx.fctx.vecctx));
    /* Create matrices */
    if(pctx->ctx.type != 0)
    {
        PetscCall(pctx->ctx.fctx.matset(&pctx->ctx.datactx.Dfxt, pctx->d, pctx->d, pctx->ctx.fctx.matctx));
        PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfxpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                           pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
        if(pctx->ctx.usecyclic)
        {
            PetscCall(pctx->ctx.fctx.matsetcycp(&pctx->ctx.datactx.Dfxpdp, pctx->period*(pctx->d - pctx->k),
                                                pctx->period*(pctx->d - pctx->k), pctx->ctx.fctx.matcycpctx));
            PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Ppd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                               pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
        }
    }
    PetscCall(pctx->ctx.fctx.matset(&pctx->ctx.datactx.Dfx, pctx->d, pctx->d, pctx->ctx.fctx.matctx));
    PetscCall(pctx->ctx.fctx.matset(&pctx->ctx.datactx.Pd, pctx->d, pctx->d, pctx->ctx.fctx.matctx));
    PetscCall(pctx->ctx.fctx.matsetp(&pctx->ctx.datactx.Dfxp, pctx->d - pctx->k, pctx->d - pctx->k, pctx->ctx.fctx.matpctx));
    PetscCall(pctx->ctx.fctx.matsett(&pctx->ctx.datactx.Tx, pctx->d, pctx->k, pctx->ctx.fctx.mattctx));
    /* Create scatters */
    PetscCall(VecScatterCreateToAll(pctx->ctx.datactx.vd, &pctx->ctx.datactx.dscatterctx, &pctx->ctx.datactx.ydloc));
    /* Create vectors */
    pctx->ctx.datactx.v.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.vs.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.Dfv.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.Dfvs.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.vadd.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.vadds.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.Tv.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    pctx->ctx.datactx.idxv.reserve(PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k));
    for(int i = 0; i < PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k) - 1; i++)
    {
        pctx->ctx.datactx.v.push_back(std::vector<Vec>(pctx->period, Vec()));
        pctx->ctx.datactx.vs.push_back(std::vector<Vec>(pctx->period, Vec()));
        for(int j = 0; j < pctx->period; j++)
        {
            PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.v.back()[j], pctx->d, pctx->ctx.fctx.vecctx));
            PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.vs.back()[j], pctx->d, pctx->ctx.fctx.vecctx));
        }
        if(pctx->ctx.type != 0 && pctx->ctx.usecyclic)
        {
            pctx->ctx.datactx.Dfv.push_back(Mat());
            PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfv.back(), pctx->period*(pctx->d - pctx->k) + pctx->k,
                                               pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
            pctx->ctx.datactx.Dfvs.push_back(Mat());
            PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfvs.back(), pctx->period*(pctx->d - pctx->k) + pctx->k,
                                               pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
        }
        else
        {
            pctx->ctx.datactx.Dfv.push_back(Mat());
            PetscCall(pctx->ctx.fctx.matset(&pctx->ctx.datactx.Dfv.back(), pctx->d, pctx->d, pctx->ctx.fctx.matctx));
            pctx->ctx.datactx.Dfvs.push_back(Mat());
            PetscCall(pctx->ctx.fctx.matset(&pctx->ctx.datactx.Dfvs.back(), pctx->d, pctx->d, pctx->ctx.fctx.matctx));
        }
        pctx->ctx.datactx.Tv.push_back(Mat());
        PetscCall(pctx->ctx.fctx.matsett(&pctx->ctx.datactx.Tv.back(), pctx->d, pctx->k, pctx->ctx.fctx.mattctx));
        pctx->ctx.datactx.vadd.push_back(0);
        pctx->ctx.datactx.vadds.push_back(0);
        pctx->ctx.datactx.idxv.push_back(std::vector<int>(3, 0));
        pctx->ctx.datactx.evv.push_back(std::vector<PetscReal>());
        pctx->ctx.datactx.ievv.push_back(std::vector<PetscReal>());
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Updates mpc data structures.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpcupdatedata(slds::cont::mpcpctx *pctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Reset vectors */
    PetscCall(VecDestroy(&pctx->ctx.datactx.xpd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.ypd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.zpd));
    /* Create vectors */
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.xpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                    pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.ypd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                    pctx->ctx.fctx.vecctx));
    PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.zpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                    pctx->ctx.fctx.vecctx));
    /* Reset matrices */
    PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxpd));
    if(pctx->ctx.usecyclic)
    {
        PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxpdp));
        PetscCall(MatDestroy(&pctx->ctx.datactx.Ppd));
    }
    /* Create matrices */
    PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfxpd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                       pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
    if(pctx->ctx.usecyclic)
    {
        PetscCall(pctx->ctx.fctx.matsetcycp(&pctx->ctx.datactx.Dfxpdp, pctx->period*(pctx->d - pctx->k),
                                            pctx->period*(pctx->d - pctx->k), pctx->ctx.fctx.matcycpctx));
        PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Ppd, pctx->period*(pctx->d - pctx->k) + pctx->k,
                                           pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
    }
    /* Reset and create vectors */
    for(int i = 0; i < pctx->ctx.datactx.spd.size(); i++)
        PetscCall(VecDestroy(&pctx->ctx.datactx.spd[i]));
    pctx->ctx.datactx.spd.clear();
    for(int i = 0; i < PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k) - 1; i++)
    {
        /* Extend vectors if required */
        for(int j = pctx->ctx.datactx.v.size(); j < pctx->period; j++)
        {
            pctx->ctx.datactx.v[i].push_back(Vec());
            PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.v[i].back(), pctx->d, pctx->ctx.fctx.vecctx));
            pctx->ctx.datactx.vs[i].push_back(Vec());
            PetscCall(pctx->ctx.fctx.vecset(&pctx->ctx.datactx.vs[i].back(), pctx->d, pctx->ctx.fctx.vecctx));
        }
        /* Only update if cyclic derivatives are used */
        if(pctx->ctx.type != 0 && pctx->ctx.usecyclic)
        {
            PetscCall(MatDestroy(&pctx->ctx.datactx.Dfv[i]));
            PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfv[i], pctx->period*(pctx->d - pctx->k) + pctx->k,
                                               pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
            PetscCall(MatDestroy(&pctx->ctx.datactx.Dfvs[i]));
            PetscCall(pctx->ctx.fctx.matsetcyc(&pctx->ctx.datactx.Dfvs[i], pctx->period*(pctx->d - pctx->k) + pctx->k,
                                               pctx->period*(pctx->d - pctx->k) + pctx->k, pctx->ctx.fctx.matcycctx));
        }
    }
    /* Reset update flag */
    pctx->update = 0;
    /* Return */
    return 0;
}
/*!
 *  @brief Cleans up mpc data structures.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpccleandata(slds::cont::mpcpctx *pctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Destroy data structure for orthonormalization */
    if(pctx->ctx.createbv)
        PetscCall(BVDestroy(&pctx->ctx.datactx.bv));
    /* Destroy vectors */
    if(pctx->ctx.type != 0)
    {
        PetscCall(VecDestroy(&pctx->ctx.datactx.xt));
        PetscCall(VecDestroy(&pctx->ctx.datactx.fxt));
        PetscCall(VecDestroy(&pctx->ctx.datactx.xpd));
        PetscCall(VecDestroy(&pctx->ctx.datactx.ypd));
        PetscCall(VecDestroy(&pctx->ctx.datactx.zpd));
    }
    PetscCall(VecDestroy(&pctx->ctx.datactx.vd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.wd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.xd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.yd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.zd));
    PetscCall(VecDestroy(&pctx->ctx.datactx.xk));
    PetscCall(VecDestroy(&pctx->ctx.datactx.yk));
    /* Destroy matrices */
    if(pctx->ctx.type != 0)
    {
        PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxt));
        PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxpd));
        if(pctx->ctx.usecyclic)
        {
            PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxpdp));
            PetscCall(MatDestroy(&pctx->ctx.datactx.Ppd));
        }
    }
    PetscCall(MatDestroy(&pctx->ctx.datactx.Dfx));
    PetscCall(MatDestroy(&pctx->ctx.datactx.Pd));
    PetscCall(MatDestroy(&pctx->ctx.datactx.Dfxp));
    PetscCall(MatDestroy(&pctx->ctx.datactx.Tx));
    /* Destroy scatters */
    PetscCall(VecDestroy(&pctx->ctx.datactx.ydloc));
    PetscCall(VecScatterDestroy(&pctx->ctx.datactx.dscatterctx));
    /* Destroy vectors */
    for(int i = 0; i < pctx->ctx.datactx.sd.size(); i++)
        PetscCall(VecDestroy(&pctx->ctx.datactx.sd[i]));
    for(int i = 0; i < pctx->ctx.datactx.spd.size(); i++)
        PetscCall(VecDestroy(&pctx->ctx.datactx.spd[i]));
    for(int i = 0; i < PetscPowReal(2*pctx->ctx.nvertex + 1, pctx->k) - 1; i++)
    {
        for(int j = 0; j < pctx->ctx.datactx.v[i].size(); j++)
        {
            PetscCall(VecDestroy(&pctx->ctx.datactx.v[i][j]));
            PetscCall(VecDestroy(&pctx->ctx.datactx.vs[i][j]));
        }
        PetscCall(MatDestroy(&pctx->ctx.datactx.Dfv[i]));
        PetscCall(MatDestroy(&pctx->ctx.datactx.Dfvs[i]));
        PetscCall(MatDestroy(&pctx->ctx.datactx.Tv[i]));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Releases allocated resources for mpcoutctx.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpcoutctx::free()
{
    /* Clean up */
    for(int i = 0; i < centers.size(); i++)
        for(int j = 0; j < centers[i].size(); j++)
            PetscCall(VecDestroy(&centers[i][j]));
    for(int i = 0; i < tangents.size(); i++)
        PetscCall(MatDestroy(&tangents[i]));
    for(int i = 0; i < points.size(); i++)
        for(int j = 0; j < points[i].size(); j++)
            PetscCall(VecDestroy(&points[i][j]));
    for(int i = 0; i < bpoints.size(); i++)
        for(int j = 0; j < bpoints[i].size(); j++)
            PetscCall(VecDestroy(&bpoints[i][j]));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Computes the index function for an equilibrium fold bifurcation.
 *                                                                                                                                */
PetscErrorCode slds::cont::idxeqfold(Mat Dfx, int *idx, int d)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    PetscReal idxx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute determinant */
    PetscCall(slds::la::det(Dfx, &idxx));
    /* Set index */
    *idx = PetscSign(idxx);
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the positive real part index function for a bifurcation.
 *                                                                                                                                */
PetscErrorCode slds::cont::idxeqposre(Mat Dfx, int *idx, int d, slds::cont::mpcctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute index */
    PetscCall(slds::la::morseposre(Dfx, idx, d, ctx.idxctx.evmax, ctx.idxctx.step0, ctx.idxctx.step, ctx.idxctx.computeall, 0,
                                   ctx.slvctx.ectx));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the index function for a fold bifurcation.
 *                                                                                                                                */
PetscErrorCode slds::cont::idxpdfold(Mat Dfx, int *idx, int d)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    PetscReal idxx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute Dfx - id */
    PetscCall(slds::util::matdiagonaladd(Dfx, -1, d));
    /* Compute determinant */
    PetscCall(slds::la::det(Dfx, &idxx));
    /* Set index */
    if(idxx != 0)
        *idx = PetscSign(idxx);
    else
        *idx = -2;
    /* Restore Dfx */
    PetscCall(slds::util::matdiagonaladd(Dfx, 1, d));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the index function for a flip bifurcation.
 *                                                                                                                                */
PetscErrorCode slds::cont::idxpdflip(Mat Dfx, int *idx, int d)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    PetscReal idxx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute Dfx - id */
    PetscCall(slds::util::matdiagonaladd(Dfx, 1, d));
    /* Compute determinant */
    PetscCall(slds::la::det(Dfx, &idxx));
    /* Set index */
    if(idxx != 0)
        *idx = PetscSign(idxx);
    else
        *idx = -2;
    /* Restore Dfx */
    PetscCall(slds::util::matdiagonaladd(Dfx, -1, d));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the morse index function for a bifurcation.
 *                                                                                                                                */
PetscErrorCode slds::cont::idxpdmorse(Mat Dfx, int *idx, int d, slds::cont::mpcctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Compute index */
    PetscCall(slds::la::morseposre(Dfx, idx, d, ctx.idxctx.evmax, ctx.idxctx.step0, ctx.idxctx.step, ctx.idxctx.computeall, 1,
                                   ctx.slvctx.ectx));
    /* Return */
    return 0;
}
/*!
 *  @brief  Compute a vertex of the polyhedron whose vertices are given by all possible linear combinations of the basis vectors.
 *                                                                                                                                */
PetscErrorCode slds::cont::vertexbr(Vec vk, int i, PetscReal r, int n, int k, NormType ntype)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, p, r0, r1;
    PetscReal norm;
    std::vector<int> idxvr;
    std::vector<PetscReal> vr;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(vk, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(vk, &r0, &r1));
    /* Preallocate */
    idxvr.reserve(k);
    vr.reserve(k);
    /* Set vertices */
    for(int j = 0; j < k; j++)
    {
        if(j > r0 - 1 && j < r1)
        {
            p = i % (2*n + 1);
            idxvr.push_back(j);
            if(p % 2 == 0)
                vr.push_back(p/2);
            else
                vr.push_back(-(p + 1)/2);
        }
        i /= (2*n + 1);
    }
    /* Set values */
    PetscCall(VecSetValues(vk, idxvr.size(), &idxvr[0], &vr[0], INSERT_VALUES));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Reassemble vectors */
    PetscCall(VecAssemblyBegin(vk));
    PetscCall(VecAssemblyEnd(vk));
    /* Scale to radius */
    PetscCall(VecNorm(vk, ntype, &norm));
    PetscCall(VecScale(vk, r/norm));
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(vk, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the derivative at a point.
 *                                                                                                                                */
PetscErrorCode slds::cont::df(Vec x, Mat Dfx, slds::cont::mpcpctx ctx)
{
    /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Compute derivative for equilibria */
    if(!ctx.ctx.type)
    {
        if(ctx.dfeq)
            PetscCall(ctx.dfeq(NULL, x, Dfx, NULL, ctx.feqctx));
        else
        {
            /* Set up finite differences context */
            ctx.ctx.slvctx.diffctx.f = ctx.feq;
            ctx.ctx.slvctx.diffctx.fctx = ctx.feqctx;
            /* Evaluate derivative */
            PetscCall(slds::diff::df(NULL, x, Dfx, NULL, &ctx.ctx.slvctx.diffctx));
        }
    }
    /* Compute period operator for periodic difference equations */
    else
    {
        /* Set up */
        std::vector<Vec> xt;
        /* Iterate */
        PetscCall(slds::de::it(ctx.fpd, x, &xt, ctx.period - 1, 0, ctx.pdperiod, ctx.d, ctx.k, ctx.fpdctx));
        /* Compute period operator */
        PetscCall(ctx.ctx.fctx.pf2(&xt, Dfx, ctx.ctx.fctx.pfctx));
        /* Clean up iterates */
        for(int i = 0; i < xt.size(); i++)
            PetscCall(VecDestroy(&(xt[i])));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Projects an initial guess onto a manifold.
 *                                                                                                                                */
PetscErrorCode slds::cont::projecteqdefault(std::vector<Vec> px, Mat Dfx, PetscReal t, int returndfx, slds::cont::mpcpctx *ctxx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    PetscReal dist;
    Vec x, fx, vk;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign context */
    slds::cont::mpcpctx ctx = *ctxx;
    /* Assign data structures */
    x = ctx.ctx.datactx.wd;
    fx = ctx.ctx.datactx.xd;
    vk = ctx.ctx.datactx.xk;
    /* Set initial point */
    if(ctx.xswitch)
        PetscCall(VecCopy(ctx.x[0], x));
    else
    {
        PetscCall(VecCopy(ctx.vk, vk));
        PetscCall(VecScale(vk, t));
        PetscCall(MatMult(ctx.Tx, vk, x));
        PetscCall(VecAXPY(x, 1, ctx.x[0]));
    }
    /* Update context */
    ctx.vk = vk;
    /* Project (if df is NULL finite differences is used) */
    rcode = slds::solve::fsolve(slds::cont::peq, slds::cont::dpeq, x, px[0], fx, Dfx, ctx.d, &ctx, ctx.ctx.slvctx.fctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver converged, proceed */
    if(rcode > -1)
    {
        /* Check projected distance */
        if(ctx.chkdist)
        {
            PetscCall(VecAXPY(x, -1, px[0]));
            PetscCall(VecNorm(x, ctx.ctx.ntype, &dist));
            if(dist < ctx.ctx.eps)
                (*ctxx).distpass = 1;
            else
                (*ctxx).distpass = 0;
            if(!(*ctxx).distpass)
                returndfx = false;
        }
        /* Compute derivative */
        if(returndfx)
        {
            /* Compute derivative (if dfpd is NULL finite differences is used) */
            if(ctx.dfeq)
                PetscCall(ctx.dfeq(NULL, px[0], Dfx, NULL, ctx.feqctx));
            else
            {
                /* Set up finite differences context */
                ctx.ctx.slvctx.diffctx.f = ctx.feq;
                ctx.ctx.slvctx.diffctx.fctx = ctx.feqctx;
                /* Evaluate derivative */
                PetscCall(slds::diff::df(NULL, px[0], Dfx, NULL, &ctx.ctx.slvctx.diffctx));
            }
        }
    }
    /* Return */
    return rcode;
}
/*!
 *  @brief Projects an initial guess onto a manifold.
 *                                                                                                                                */
PetscErrorCode slds::cont::projectpddefault(std::vector<Vec> px, Mat Dfx, PetscReal t, int returndfx, slds::cont::mpcpctx *ctxx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    PetscReal dist;
    Vec x, pxx, x0, fx, vk;
    Mat Dfxx;
    std::vector<Vec> xt;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign context */
    slds::cont::mpcpctx ctx = *ctxx;
    /* Assign data structures */
    x = ctx.ctx.datactx.xpd;
    pxx = ctx.ctx.datactx.ypd;
    fx = ctx.ctx.datactx.zpd;
    x0 = ctx.ctx.datactx.xd;
    vk = ctx.ctx.datactx.xk;
    Dfxx = ctx.ctx.datactx.Dfxpd;
    /* Set initial point */
    if(ctx.xswitch)
        PetscCall(VecCopy(ctx.x[0], x0));
    else
    {
        PetscCall(VecCopy(ctx.vk, vk));
        PetscCall(VecScale(vk, t));
        PetscCall(MatMult(ctx.Tx, vk, x0));
        PetscCall(VecAXPY(x0, 1, ctx.x[0]));
    }
    /* Update context */
    ctx.vk = vk;
    /* Iterate */
    PetscCall(slds::de::it(ctx.fpd, x0, &xt, ctx.period - 1, 0, ctx.pdperiod, ctx.d, ctx.k, ctx.fpdctx));
    /* Initialize initial guess */
    PetscCall(VecSet(x, 0));
    /* Set initial guess */
    for(int i = 0; i < ctx.period; i++)
        PetscCall(slds::util::subvecset(x, xt[i], 0, ctx.d - ctx.k, i*(ctx.d - ctx.k), ctx.ctx.zerotol));
    PetscCall(slds::util::subvecset(x, x0, ctx.d - ctx.k, ctx.d, ctx.period*(ctx.d - ctx.k), ctx.ctx.zerotol));
    /* Project (if df is NULL finite differences is used) */
    rcode = slds::solve::fsolve(slds::cont::ppd, slds::cont::dppd, x, pxx, fx, Dfxx, ctx.period*(ctx.d - ctx.k) + ctx.k, &ctx,
                                ctx.ctx.slvctx.fctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver converged, proceed */
    if(rcode > -1)
    {
        /* Extract px */
        for(int i = 0; i < ctx.period; i++)
        {
            PetscCall(VecSet(px[i], 0));
            PetscCall(slds::util::subvecget(pxx, px[i], i*(ctx.d - ctx.k), (i + 1)*(ctx.d - ctx.k), 0, ctx.ctx.zerotol));
            PetscCall(slds::util::subvecget(pxx, px[i], ctx.period*(ctx.d - ctx.k), ctx.period*(ctx.d - ctx.k) + ctx.k,
                                            ctx.d - ctx.k, ctx.ctx.zerotol));
        }
        /* Check projected distance */
        if(ctx.chkdist)
        {
            PetscCall(VecAXPY(x0, -1, px[0]));
            PetscCall(VecNorm(x0, ctx.ctx.ntype, &dist));
            if(dist < ctx.ctx.eps)
                (*ctxx).distpass = 1;
            else
                (*ctxx).distpass = 0;
            if(!(*ctxx).distpass)
                returndfx = false;
        }
        /* Compute cyclic derivative */
        if(returndfx && ctx.ctx.usecyclic)
        {
            PetscCall(ctx.ctx.fctx.dfcyc(NULL, pxx, Dfx, NULL, ctx.ctx.fctx.fcycctx));
            /* Fix diagonal */
            PetscCall(slds::util::matdiagonaladd(Dfx, 1, ctx.period*(ctx.d - ctx.k)));
        }
        /* Else compute period operator */
        else if(returndfx)
            PetscCall(ctx.ctx.fctx.pf(pxx, Dfx, ctx.ctx.fctx.pfctx));
    }
    /* Clean up iterates */
    for(int i = 0; i < xt.size(); i++)
            PetscCall(VecDestroy(&(xt[i])));
    /* Return */
    return rcode;
}
/*!
 *  @brief Line search to find bifurcation points.
 *                                                                                                                                */
PetscErrorCode slds::cont::lsidxdefault(std::vector<Vec> v, Mat Dfv, Mat Dfvp, PetscReal r, PetscReal *t, int idx0, int idx1,
                                        int idxtype, int d, int k, slds::cont::mpcpctx pctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, idxa, idxb, idxc, converged, srxtype;
    PetscReal ta, tb, tc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Intialize */
    ta = 0;
    tb = 1;
    tc = 0.5;
    idxa = idx0;
    idxb = idx1;
    converged = false;
    /* Return if any of the indices is invalid */
    if(idxa < -1 || idxb < -1)
        return SNES_DIVERGED_FUNCTION_DOMAIN;
    /* Initialize search type */
    if(idxa < idxb)
        srxtype = 0;
    else if(idxb < idxa)
        srxtype = 1;
    /* Return if search type cannot be determined */
    else
        return SNES_DIVERGED_FUNCTION_DOMAIN;
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Staring line search with idx(0) = %d, idx(1) = %d \n", idx0, idx1));
    /* Search */
    for(int i = 0; i < pctx.ctx.idxctx.maxit; i++)
    {
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                                  "Line search iteration: %d, Interval: [%f, %f], idx(%f) = %d, idx(%f) = %d \n",
                                  i, ta, tb, ta, idxa, tb, idxb));
        /* Stop if interval length is less than 2*atol */
        if(r*(tb - ta) < 2*pctx.ctx.idxctx.tol)
        {
            converged = true;
            break;
        }
        /* Compute midpoint index */
        PetscCall(slds::cont::lsf(v, Dfv, Dfvp, tc, &idxc, idxtype, &pctx, outctx));
        /* Return if the index is invalid */
        if(idxc < -1)
            return SNES_DIVERGED_FUNCTION_DOMAIN;
        /* Update for increasing search type */
        if(srxtype == 0)
        {
            if(idxc < idxb)
            {
                ta = tc;
                idxa = idxc;
            }
            else
            {
                tb = tc;
                idxb = idxc;
            }
        }
        /* Update for decreasing search type */
        else
        {
            if(idxc < idxa)
            {
                tb = tc;
                idxb = idxc;
            }
            else
            {
                ta = tc;
                idxa = idxc;
            }
        }
        /* Update midpoint */
        tc = ta + (tb - ta)/2;
    }
    /* Set t */
    *t = tc;
    /* Return */
    if(!converged)
        return SNES_DIVERGED_MAX_IT;
    else
        return SNES_CONVERGED_ITERATING;
}
/*!
 *  @brief Branch switching at bifurcation points.
 *                                                                                                                                */
PetscErrorCode slds::cont::swdefault(Mat Tx, Mat Tv, PetscReal r, int *bidxpp, int idx0, int idx1, int idxtype, int d, int k,
                                     slds::cont::mpcpctx pctx, slds::cont::mpcoutctx *outctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode, type, sdsz;
    PetscReal t, norm;
    Vec bnull, diff, xppd, yppd, zppd;
    Mat Dfb, Dfbp, Dfxp, Dfxppd, Txp;
    std::vector<int> idxxp(3, 0);
    std::vector<PetscReal> evp, ievp;
    std::vector<Vec> xp, b(pctx.period, Vec());
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Assign data structures */
    bnull = pctx.ctx.datactx.vd;
    diff = pctx.ctx.datactx.xd;
    if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
    {
        Dfb = pctx.ctx.datactx.Dfxpd;
        Dfbp = pctx.ctx.datactx.Dfxpdp;
    }
    else
    {
        Dfb = pctx.ctx.datactx.Dfx;
        Dfbp = pctx.ctx.datactx.Dfxp;
    }
    Txp = pctx.ctx.datactx.Tx;
    /* Initialize */
    *bidxpp = false;
    for(int i = 0; i < pctx.period; i++)
        PetscCall(VecDuplicate(pctx.x[0], &(b[i])));
    /* Find the bifurcation point */
    rcode = slds::cont::lsidxdefault(b, Dfb, Dfbp, r, &t, idx0, idx1, idxtype, pctx.d, pctx.k, pctx, outctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver diverged or returned an incorrect value, indicate and abort branch switching */
    if(rcode < 0 || t < 0 || t > 1)
    {
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Line search failed with code: %d \n", rcode));
        /* Clean up */
        for(int j = 0; j < b.size(); j++)
            PetscCall(VecDestroy(&(b[j])));
        /* Return */
        return 0;
    }
    /* Check if the point is close to a known bifurcation point */
    if(!outctx->bpoints.empty())
    {
        /* Set up */
        PetscReal distb;
        PetscCall(VecNorm(b[0], pctx.ctx.ntype, &distb));
        /* Filter points via reverse triangle inequality */
        for(int i = 0; i < outctx->bpoints.size(); i++)
        {
            if(PetscAbsReal(distb - (*pctx.ctx.datactx.distb)[i]) < r*pctx.ctx.swctx.rs)
            {
                /* Check distance of points */
                PetscCall(VecAXPBYPCZ(diff, 1, -1, 0, b[0], outctx->bpoints[i][0]));
                PetscCall(VecNorm(diff, pctx.ctx.ntype, &norm));
                /* If the point is close to a known bifurcation point, abort branch switching */
                if(norm < r*pctx.ctx.swctx.rs)
                {
                    /* Print update */
                    if(pctx.ctx.verbose)
                        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Point is close to a known bifurcation point \n"));
                    /* Clean up */
                    for(int j = 0; j < b.size(); j++)
                        PetscCall(VecDestroy(&(b[j])));
                    /* Return */
                    return 0;
                }
            }
        }
    }
    /* Determine bifurcation type */
    PetscCall(slds::cont::swtype(Dfbp, &type, pctx));
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Bifurcation point is of type %d \n", type));
    /* Save bifurcation point */
    if(type > -1)
    {
        outctx->bpoints.push_back(std::vector<Vec>(pctx.period, Vec()));
        for(int i = 0; i < pctx.period; i++)
        {
            PetscCall(VecDuplicate(b[i], &(outctx->bpoints.back()[i])));
            PetscCall(VecCopy(b[i], outctx->bpoints.back()[i]));
        }
        outctx->btypes.push_back(type);
        outctx->bbidx.push_back(pctx.bidx);
        outctx->bperiods.push_back(pctx.period);
        /* Save distance */
        PetscCall(VecNorm(b[0], pctx.ctx.ntype, &norm));
        (*pctx.ctx.datactx.distb).push_back(norm);
    }
    /* Switch */
    if(pctx.ctx.swctx.sw && ((pctx.ctx.type == 0 && type == 0) || (pctx.ctx.type != 0 && (type == 0 || type == 1))))
    {
        /* Set matrices */
        Dfxp = pctx.ctx.datactx.Dfx;
        /* Set new contexts */
        pctx.ctx.fctx.knullpdctx = &pctx;
        pctx.ctx.fctx.fcycctx = &pctx;
        pctx.ctx.fctx.pfctx = &pctx;
        pctx.ctx.fctx.matcycctx = &pctx;
        pctx.ctx.fctx.matcycpctx = &pctx;
        /* Save size data */
        sdsz = pctx.ctx.datactx.sd.size();
        /* Set options */
        pctx.ctx.usecyclic = 0;
        /* Set up switching */
        if(pctx.ctx.type != 0 && type == 1)
        {
            /* Multiply period */
            pctx.period *= 2;
            /* Create and set vectors with double period */
            PetscCall(pctx.ctx.fctx.vecset(&xppd, pctx.period*(pctx.d - pctx.k) + pctx.k,
                                           pctx.ctx.fctx.vecctx));
            PetscCall(pctx.ctx.fctx.vecset(&yppd, pctx.period*(pctx.d - pctx.k) + pctx.k,
                                           pctx.ctx.fctx.vecctx));
            PetscCall(pctx.ctx.fctx.vecset(&zppd, pctx.period*(pctx.d - pctx.k) + pctx.k,
                                           pctx.ctx.fctx.vecctx));
            pctx.ctx.datactx.xpd = xppd;
            pctx.ctx.datactx.ypd = yppd;
            pctx.ctx.datactx.zpd = zppd;
            /* Create and set matrices with double period */
            PetscCall(pctx.ctx.fctx.matsetcyc(&Dfxppd, pctx.period*(pctx.d - pctx.k) + pctx.k,
                                              pctx.period*(pctx.d - pctx.k) + pctx.k, pctx.ctx.fctx.matcycctx));
            pctx.ctx.datactx.Dfxpd = Dfxppd;
        }
        /* Recompute derivative */
        PetscCall(slds::cont::df(b[0], Dfxp, pctx));
        /* Compute additional nullvector */
        rcode = slds::cont::swnull(bnull, Dfxp, Tx, Tv, t, pctx.d, pctx.k, pctx);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If no nullvector was found, abort branch switching */
        if(rcode < 0)
        {
            /* Print update */
            if(pctx.ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Failed to find addtional nullvector with rcode: %d \n", rcode));
            /* Clean up */
            for(int i = 0; i < b.size(); i++)
                PetscCall(VecDestroy(&(b[i])));
            /* Return */
            return 0;
        }
        /* Initialize solution */
        xp = std::vector<Vec>(pctx.period, Vec());
        for(int i = 0; i < pctx.period; i++)
            PetscCall(VecDuplicate(pctx.x[0], &(xp[i])));
        /* Set tangent and switch options */
        pctx.xswitch = 1;
        pctx.x = xp;
        pctx.Tx = Txp;
        /* Compute nullvector scale */
        PetscCall(VecNorm(bnull, pctx.ctx.ntype, &norm));
        /* Compute first perturbation */
        PetscCall(VecAXPBYPCZ(xp[0], 1, r*pctx.ctx.swctx.ds/norm, 0, b[0], bnull));
        /* Compute tangent space for the first point */
        rcode = slds::cont::swtx(xp[0], Txp, pctx.d, pctx.k, pctx);
        /* If tangent space computation failed, print update */
        if(rcode < 0 && pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Tangent space comutation failed for first point with rcode: %d \n", rcode));
        /* If tangent space computation succeeded, project first point */
        else
        {
            if(pctx.ctx.type == 0)
                rcode = pctx.ctx.fctx.projecteq(xp, Dfxp, 1, 1, &pctx);
            else
                rcode = pctx.ctx.fctx.projectpd(xp, Dfxp, 1, 1, &pctx);
            /* Return if there was an error (use PetscCall for clean exit) */
            if(rcode == 100)
                PetscCall(100);
            /* If the projection failed, print update */
            if(rcode < 0 && pctx.ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Projection failed for first point with rcode: %d \n", rcode));
            /* If the projection succeeded, compute tangent space and indices */
            else
            {
                /* Compute tangent space */
                rcode = pctx.ctx.fctx.tx(Dfxp, Txp, pctx);
                /* Return if there was an error (use PetscCall for clean exit) */
                if(rcode == 100)
                    PetscCall(100);
                /* If the solver failed, print update */
                if(rcode < 0 && pctx.ctx.verbose)
                    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Projection failed for first point with rcode: %d \n", rcode));
                /* Else compute indices and add point to boundary */
                else
                {
                    /* Update branch tracker */
                    if(!(*bidxpp))
                        pctx.bidx = pctx.nb + 1;
                    /* Compute indices */
                    PetscCall(pctx.ctx.fctx.xidxs(Dfxp, &idxxp, pctx));
                    /* Compute eigenvalues */
                    evp.clear();
                    ievp.clear();
                    PetscCall(pctx.ctx.fctx.xevs(Dfxp, &evp, &ievp, pctx));
                    /* Add point to boundary */
                    (*pctx.ctx.datactx.xboundary).push_back(std::vector<Vec>(pctx.period, Vec()));
                    for(int i = 0; i < pctx.period; i++)
                    {
                        PetscCall(VecDuplicate(xp[i], &((*pctx.ctx.datactx.xboundary).back()[i])));
                        PetscCall(VecCopy(xp[i], (*pctx.ctx.datactx.xboundary).back()[i]));
                    }
                    /* Add tangent space to boundary */
                    (*pctx.ctx.datactx.Txboundary).push_back(Mat());
                    PetscCall(MatDuplicate(Txp, MAT_COPY_VALUES, &((*pctx.ctx.datactx.Txboundary).back())));
                    /* Add period to boundary */
                    (*pctx.ctx.datactx.pboundary).push_back(pctx.period);
                    /* Add branch index to boundary */
                    (*pctx.ctx.datactx.bidxboundary).push_back(pctx.bidx);
                    /* Add initial radius to boundary */
                    (*pctx.ctx.datactx.rboundary).push_back(pctx.ctx.rmin);
                    /* Add indices to boundary */
                    (*pctx.ctx.datactx.idxboundary).push_back(idxxp);
                    /* Add eigenvalues to boundary */
                    (*pctx.ctx.datactx.evboundary).push_back(evp);
                    (*pctx.ctx.datactx.ievboundary).push_back(ievp);
                }
            }
        }
        /* Compute second perturbation */
        PetscCall(VecAXPBYPCZ(xp[0], 1, -r*pctx.ctx.swctx.ds/norm, 0, b[0], bnull));
        /* Compute tangent space for the second point */
        rcode = slds::cont::swtx(xp[0], Txp, pctx.d, pctx.k, pctx);
        /* If tangent space computation failed, print update */
        if(rcode < 0 && pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Tangent space comutation failed for second point with rcode: %d \n", rcode));
        /* If tangent space computation succeeded, project second point */
        else
        {
            if(pctx.ctx.type == 0)
                rcode = pctx.ctx.fctx.projecteq(xp, Dfxp, 1, 1, &pctx);
            else
                rcode = pctx.ctx.fctx.projectpd(xp, Dfxp, 1, 1, &pctx);
            /* Return if there was an error (use PetscCall for clean exit) */
            if(rcode == 100)
                PetscCall(100);
            /* If the projection failed, print update */
            if(rcode < 0 && pctx.ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Projection failed for second point with rcode: %d \n", rcode));
            /* If the projection succeeded, compute tangent space and indices */
            else
            {
                /* Compute tangent space */
                rcode = pctx.ctx.fctx.tx(Dfxp, Txp, pctx);
                /* Return if there was an error (use PetscCall for clean exit) */
                if(rcode == 100)
                    PetscCall(100);
                /* If the solver failed, print update */
                if(rcode < 0 && pctx.ctx.verbose)
                    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Projection failed for second point with rcode: %d \n", rcode));
                /* Else compute indices and add point to boundary */
                else
                {
                    /* Update branch tracker */
                    if(!(*bidxpp))
                        pctx.bidx = pctx.nb + 1;
                    /* Compute indices */
                    PetscCall(pctx.ctx.fctx.xidxs(Dfxp, &idxxp, pctx));
                    /* Compute eigenvalues */
                    evp.clear();
                    ievp.clear();
                    PetscCall(pctx.ctx.fctx.xevs(Dfxp, &evp, &ievp, pctx));
                    /* Add point to boundary */
                    (*pctx.ctx.datactx.xboundary).push_back(std::vector<Vec>(pctx.period, Vec()));
                    for(int i = 0; i < pctx.period; i++)
                    {
                        PetscCall(VecDuplicate(xp[i], &((*pctx.ctx.datactx.xboundary).back()[i])));
                        PetscCall(VecCopy(xp[i], (*pctx.ctx.datactx.xboundary).back()[i]));
                    }
                    /* Add tangent space to boundary */
                    (*pctx.ctx.datactx.Txboundary).push_back(Mat());
                    PetscCall(MatDuplicate(Txp, MAT_COPY_VALUES, &((*pctx.ctx.datactx.Txboundary).back())));
                    /* Add period to boundary */
                    (*pctx.ctx.datactx.pboundary).push_back(pctx.period);
                    /* Add branch index to boundary */
                    (*pctx.ctx.datactx.bidxboundary).push_back(pctx.bidx);
                    /* Add initial radius to boundary */
                    (*pctx.ctx.datactx.rboundary).push_back(pctx.ctx.rmin);
                    /* Add indices to boundary */
                    (*pctx.ctx.datactx.idxboundary).push_back(idxxp);
                    /* Add eigenvalues to boundary */
                    (*pctx.ctx.datactx.evboundary).push_back(evp);
                    (*pctx.ctx.datactx.ievboundary).push_back(ievp);
                }
            }
        }
        /* Clean up */
        for(int i = sdsz; i < pctx.ctx.datactx.sd.size(); i++)
            PetscCall(VecDestroy(&pctx.ctx.datactx.sd[i]));
        for(int i = 0; i < xp.size(); i++)
            PetscCall(VecDestroy(&(xp[i])));
        if(pctx.ctx.type != 0 && type == 1)
        {
            PetscCall(VecDestroy(&xppd));
            PetscCall(VecDestroy(&yppd));
            PetscCall(VecDestroy(&zppd));
            PetscCall(MatDestroy(&Dfxppd));
        }
    }
    /* Clean up */
    for(int i = 0; i < b.size(); i++)
        PetscCall(VecDestroy(&(b[i])));
    /* Return */
    return 0;
}
/*!
 *  @brief Numerical continuation.
 *                                                                                                                                */
PetscErrorCode slds::cont::mpc(PetscErrorCode (*feq)(SNES, Vec, Vec, void*), PetscErrorCode (*dfeq)(SNES, Vec, Mat, Mat, void*),
                               PetscErrorCode (**fpd)(SNES, Vec, Vec, void*), PetscErrorCode (**dfpd)(SNES, Vec, Mat, Mat, void*),
                               Vec x0, PetscReal *a, PetscReal *b, int p0, int pd, int d, int k, slds::cont::mpcoutctx *outctx,
                               void *feqctx, void **fpdctx, slds::cont::mpcctx ctx)
{
    /* Set up */
    PetscBool initpetsc, initslepc;
    int mpirank, rcode;
    PetscReal r, norm;
    Vec vk;
    Mat Tx;
    std::vector<int> idxx(3, 0);
    std::vector<PetscReal> ev, iev;
    std::vector<PetscReal> distb, distc;
    std::vector<Vec> x;
    std::deque<int> pboundary, bidxboundary;
    std::deque<PetscReal> rboundary;
    std::deque<std::vector<Vec>> xboundary;
    std::deque<Mat> Txboundary;
    std::deque<std::vector<int>> idxboundary;
    std::deque<std::vector<PetscReal>> evboundary, ievboundary;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    PetscCall(SlepcInitialized(&initslepc));
    if(initslepc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize projection context */
    slds::cont::mpcpctx pctx = slds::cont::mpcpctx(d, k, std::vector<Vec>(), Vec(), Mat(), ctx, feq, dfeq, feqctx, fpd, dfpd,
                                                   fpdctx);
    if(pctx.ctx.type != 0)
    {
        pctx.period = std::lcm(p0, pd);
        pctx.pdperiod = pd;
    }
    else
        pctx.period = 1;
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Initialize mpc */
    PetscCall(slds::cont::mpcsetoptions(&pctx));
    /* Initialize data */
    x = std::vector<Vec>(pctx.period, Vec());
    for(int i = 0; i < pctx.period; i++)
        PetscCall(pctx.ctx.fctx.vecset(&(x[i]), pctx.d, pctx.ctx.fctx.vecctx));
    PetscCall(pctx.ctx.fctx.vecset(&vk, pctx.k, pctx.ctx.fctx.vecctx));
    PetscCall(pctx.ctx.fctx.matsett(&Tx, pctx.d, pctx.k, pctx.ctx.fctx.mattctx));
    PetscCall(slds::cont::mpcsetdata(&pctx));
    /* Set additional context data */
    pctx.ctx.datactx.xboundary = &xboundary;
    pctx.ctx.datactx.Txboundary = &Txboundary;
    pctx.ctx.datactx.pboundary = &pboundary;
    pctx.ctx.datactx.bidxboundary = &bidxboundary;
    pctx.ctx.datactx.rboundary = &rboundary;
    pctx.ctx.datactx.idxboundary = &idxboundary;
    pctx.ctx.datactx.evboundary = &evboundary;
    pctx.ctx.datactx.ievboundary = &ievboundary;
    pctx.ctx.datactx.distb = &distb;
    pctx.ctx.datactx.distc = &distc;
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Initializing boundary... \n"));
    /* Initialize boundary */
    if(pctx.ctx.type != 0 && pctx.ctx.usecyclic)
        rcode = slds::cont::initidxt(x, x0, vk, pctx.ctx.datactx.Dfxpd, Tx, &idxx, &ev, &iev, pctx);
    else
        rcode = slds::cont::initidxt(x, x0, vk, pctx.ctx.datactx.Dfx, Tx, &idxx, &ev, &iev, pctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If initialization failed, clean up and exit */
    if(rcode < 0)
    {
        /* Indicate initialization has failed */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Initialization failed with code: %d \n", rcode));
        /* Clean up */
        for(int i = 0; i < pctx.period; i++)
            PetscCall(VecDestroy(&(x[i])));
        PetscCall(VecDestroy(&vk));
        PetscCall(MatDestroy(&Tx));
        PetscCall(slds::cont::mpccleandata(&pctx));
        /* Return */
        return rcode;
    }
    /* Set boundary point */
    xboundary.push_back(std::vector<Vec>(pctx.period, Vec()));
    for(int i = 0; i < pctx.period; i++)
    {
        PetscCall(VecDuplicate(x[i], &(xboundary.back()[i])));
        PetscCall(VecCopy(x[i], xboundary.back()[i]));
    }
    /* Set boundary tangent space */
    Txboundary.push_back(Mat());
    PetscCall(MatDuplicate(Tx, MAT_COPY_VALUES, &(Txboundary.back())));
    /* Set boundary data */
    pboundary.push_back(pctx.period);
    bidxboundary.push_back(pctx.bidx);
    rboundary.push_back(pctx.ctx.rmin);
    /* Set boundary indices */
    idxboundary.push_back(idxx);
    /* Set boundary eigenvalues */
    evboundary.push_back(ev);
    ievboundary.push_back(iev);
    /* Set context */
    pctx.x = x;
    pctx.vk = vk;
    pctx.Tx = Tx;
    /* Expand on the boundary */
    while(!xboundary.empty())
    {
        /* Indicate a new iteration has started */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Starting new iteration... \n"));
        /* Print info about the current iteration */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cover: %d, Boundary: %d, Period: %d \n", (int)outctx->centers.size(),
                                  (int)xboundary.size(), pboundary.back()));
        /* Get new point from boundary */
        if(pctx.period != pboundary.back())
        {
            /* Mark update */
            pctx.update = 1;
            /* Expand x if required */
            for(int i = x.size(); i < pboundary.back(); i++)
            {
                x.push_back(Vec());
                PetscCall(pctx.ctx.fctx.vecset(&(x.back()), pctx.d, pctx.ctx.fctx.vecctx));
            }
        }
        pctx.period = pboundary.back();
        /* Get point */
        for(int i = 0; i < pctx.period; i++)
            PetscCall(VecCopy(xboundary.back()[i], x[i]));
        PetscCall(MatCopy(Txboundary.back(), Tx, DIFFERENT_NONZERO_PATTERN));
        pctx.bidx = bidxboundary.back();
        r = rboundary.back();
        idxx = idxboundary.back();
        ev = evboundary.back();
        iev = ievboundary.back();
        /* Remove point from boundary */
        for(int i = 0; i < pctx.period; i++)
            PetscCall(VecDestroy(&(xboundary.back()[i])));
        xboundary.pop_back();
        PetscCall(MatDestroy(&(Txboundary.back())));
        Txboundary.pop_back();
        pboundary.pop_back();
        bidxboundary.pop_back();
        rboundary.pop_back();
        idxboundary.pop_back();
        evboundary.pop_back();
        ievboundary.pop_back();
        /* Print update */
        if(pctx.ctx.verbose && pctx.update)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Updating data... \n"));
        /* Update data */
        if(pctx.update)
            PetscCall(slds::cont::mpcupdatedata(&pctx));
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing step... \n"));
        /* Compute step */
        PetscCall(slds::cont::step(x, vk, Tx, &r, a, b, pctx.d, pctx.k, &pctx, outctx));
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Updating boundary... \n"));
        /* Update boundary */
        for(int i = 0; i < PetscPowReal(2*pctx.ctx.nvertex + 1, k) - 1; i++)
        {
            /* Set up */
            int bidxpp;
            /* Continue if the vertex should not be added */
            if(!pctx.ctx.datactx.vadd[i])
                continue;
            /* Set vertex */
            PetscCall(slds::cont::vertexbr(vk, i + 1, r, pctx.ctx.nvertex, k, pctx.ctx.ntype));
            /* Check indices and switch branches */
            if(pctx.ctx.type == 0 && idxx[0] > -2 && pctx.ctx.datactx.idxv[i][0] > -2 && idxx[0] != pctx.ctx.datactx.idxv[i][0])
            {
                pctx.ctx.fctx.sw(Tx, pctx.ctx.datactx.Tv[i], r, &bidxpp, idxx[0], pctx.ctx.datactx.idxv[i][0], 0,
                                 pctx.d, pctx.k, pctx, outctx);
                if(bidxpp)
                    pctx.nb += 1;
            }
            if(pctx.ctx.type == 0 && idxx[1] > -2 && pctx.ctx.datactx.idxv[i][1] > -2 && idxx[1] != pctx.ctx.datactx.idxv[i][1])
            {
                pctx.ctx.fctx.sw(Tx, pctx.ctx.datactx.Tv[i], r, &bidxpp, idxx[1], pctx.ctx.datactx.idxv[i][1], 1,
                                 pctx.d, pctx.k, pctx, outctx);
                if(bidxpp)
                    pctx.nb += 1;
            }
            if(pctx.ctx.type != 0 && idxx[0] > -2 && pctx.ctx.datactx.idxv[i][0] > -2 && idxx[0] != pctx.ctx.datactx.idxv[i][0])
            {
                pctx.ctx.fctx.sw(Tx, pctx.ctx.datactx.Tv[i], r, &bidxpp, idxx[0], pctx.ctx.datactx.idxv[i][0], 0,
                                 pctx.d, pctx.k, pctx, outctx);
                if(bidxpp)
                    pctx.nb += 1;
            }
            if(pctx.ctx.type != 0 && idxx[1] > -2 && pctx.ctx.datactx.idxv[i][1] > -2 && idxx[1] != pctx.ctx.datactx.idxv[i][1])
            {
                pctx.ctx.fctx.sw(Tx, pctx.ctx.datactx.Tv[i], r, &bidxpp, idxx[1], pctx.ctx.datactx.idxv[i][1], 1,
                                 pctx.d, pctx.k, pctx, outctx);
                if(bidxpp)
                    pctx.nb += 1;
            }
            if(pctx.ctx.type != 0 && idxx[2] > -2 && pctx.ctx.datactx.idxv[i][2] > -2 && idxx[2] != pctx.ctx.datactx.idxv[i][2])
            {
                pctx.ctx.fctx.sw(Tx, pctx.ctx.datactx.Tv[i], r, &bidxpp, idxx[2], pctx.ctx.datactx.idxv[i][2], 2,
                                 pctx.d, pctx.k, pctx, outctx);
                if(bidxpp)
                    pctx.nb += 1;
            }
            /* Add vertex to boundary */
            xboundary.push_back(std::vector<Vec>(pctx.period, Vec()));
            for(int j = 0; j < pctx.period; j++)
            {
                PetscCall(VecDuplicate(pctx.ctx.datactx.v[i][j], &(xboundary.back()[j])));
                PetscCall(VecCopy(pctx.ctx.datactx.v[i][j], xboundary.back()[j]));
            }
            /* Add tangent space to boundary */
            Txboundary.push_back(Mat());
            PetscCall(MatDuplicate(pctx.ctx.datactx.Tv[i], MAT_COPY_VALUES, &(Txboundary.back())));
            /* Add data to boundary */
            pboundary.push_back(pctx.period);
            bidxboundary.push_back(pctx.bidx);
            rboundary.push_back(r);
            /* Add indices to boundary */
            idxboundary.push_back(pctx.ctx.datactx.idxv[i]);
            /* Add eigenvalues to boundary */
            evboundary.push_back(pctx.ctx.datactx.evv[i]);
            ievboundary.push_back(pctx.ctx.datactx.ievv[i]);
        }
        /* Print update */
        if(pctx.ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Saving point... \n"));
        /* Add point to cover */
        outctx->centers.push_back(std::vector<Vec>(pctx.period, Vec()));
        for(int i = 0; i < pctx.period; i++)
        {
            PetscCall(VecDuplicate(x[i], &(outctx->centers.back()[i])));
            PetscCall(VecCopy(x[i], outctx->centers.back()[i]));
        }
        /* Add tangent space to cover */
        outctx->tangents.push_back(Mat());
        PetscCall(MatDuplicate(Tx, MAT_COPY_VALUES, &(outctx->tangents.back())));
        /* Add data to cover */
        outctx->radii.push_back(r);
        outctx->bidx.push_back(pctx.bidx);
        outctx->periods.push_back(pctx.period);
        /* Add indices to cover */
        outctx->idx.push_back(idxx);
        /* Add eigenvalues to cover */
        outctx->ev.push_back(ev);
        outctx->iev.push_back(iev);
        /* Add distance */
        PetscCall(VecNorm(x[0], pctx.ctx.ntype, &norm));
        distc.push_back(norm);
    }
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing output... \n"));
    /* Write output */
    PetscCall(pctx.ctx.fctx.writeout(pctx, outctx));
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Clean up */
    for(int i = 0; i < x.size(); i++)
        PetscCall(VecDestroy(&(x[i])));
    PetscCall(VecDestroy(&vk));
    PetscCall(MatDestroy(&Tx));
    PetscCall(slds::cont::mpccleandata(&pctx));
    /* Print update */
    if(pctx.ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished! \n"));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
