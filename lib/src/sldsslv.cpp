/**********************************************************************************************************************************/
/*! @file sldsslv.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Linear and nonlinear solvers for systems of equations.
 *
 *  Linear and nonlinear solvers for systems of equations. Additionally implements linear least squares for under- or
 *  overdetermined systems. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsslv.hpp"
#include "sldsdiff.hpp"
#include "sldsla.hpp"
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Basic monitoring function. Prints current iteration number and norm of the current iterate.
 *                                                                                                                                */
PetscErrorCode snesinfo(SNES snes, int its, PetscReal fnorm, void *ctx)
{
    /* Print info -> PETSC_COMM_WORLD ensures this is only printed once (by the 0 process) */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Iteration: %d \n", its));
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "||Fx||: %f \n", fnorm));
    /* Return */
    return 0;
}
/*!
 *  @brief SNES convergence test for p-norms. Modifies SNESConvergedDefault().
 *                                                                                                                                */
PetscErrorCode snesconvergedlp(SNES snes, PetscInt it, SNESConvergedReason *reason, NormType ntype)
{
    /* Set up */
    void *ctx;
    PetscReal xnorm, snorm, fnorm;
    Vec x, xx, fx, s;
    PetscErrorCode (*f)(SNES, Vec, Vec, void*);
    /* Get solution */
    PetscCall(SNESGetSolution(snes, &x));
    PetscCall(SNESGetSolutionUpdate(snes, &xx));
    /* Get function and context */
    PetscCall(SNESGetFunction(snes, NULL, &f, &ctx));
    /* Compute function and step */
    PetscCall(VecDuplicate(x, &fx));
    PetscCall(VecDuplicate(x, &s));
    PetscCall(f(snes, x, fx, ctx));
    PetscCall(VecAXPBYPCZ(s, 1, -1, 0, x, xx));
    /* Compute norms */
    PetscCall(VecNorm(x, ntype, &xnorm));
    PetscCall(VecNorm(s, ntype, &snorm));
    PetscCall(VecNorm(fx, ntype, &fnorm));
    /* Determine convergence */
    PetscCall(SNESConvergedDefault(snes, it, xnorm, snorm, fnorm, reason, NULL));
    /* Clean up */
    PetscCall(VecDestroy(&fx));
    PetscCall(VecDestroy(&s));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Convergence test for nonlinear solver using the 1-norm.
 *                                                                                                                                */
PetscErrorCode slds::solve::snesconvergedl1(SNES snes, PetscInt it, PetscReal xnorm, PetscReal snorm, PetscReal fnorm,
                                            SNESConvergedReason *reason, void *cctx)
{
    /* Call convergence test with 1-norm */
    PetscCall(snesconvergedlp(snes, it, reason, NORM_1));
    /* Return */
    return 0;
}
/*!
 *  @brief Convergence test for nonlinear solver using the max-norm.
 *                                                                                                                                */
PetscErrorCode slds::solve::snesconvergedlinf(SNES snes, PetscInt it, PetscReal xnorm, PetscReal snorm, PetscReal fnorm,
                                              SNESConvergedReason *reason, void *cctx)
{
    /* Call convergence test with max-norm */
    PetscCall(snesconvergedlp(snes, it, reason, NORM_INFINITY));
    /* Return */
    return 0;
}
/*!
 *  @brief Solves min f(x) s.t. c(x) = 0, h(x) >= 0, l <= x <= u.
 *                                                                                                                                */
PetscErrorCode slds::solve::minsolve(PetscErrorCode (*f)(SNES, Vec, PetscReal, void*),
                                     PetscErrorCode (*gradf)(SNES, Vec, Vec, void*),
                                     PetscErrorCode (*hf)(SNES, Vec, Mat, Mat, void*), PetscErrorCode (*c)(SNES, Vec, Vec, void*),
                                     PetscErrorCode (*dc)(SNES, Vec, Mat, Mat, void*), PetscErrorCode (*h)(SNES, Vec, Vec, void*),
                                     PetscErrorCode (*dh)(SNES, Vec, Mat, Mat, void*), Vec x, Vec l, Vec u, void *fctx,
                                     slds::solve::minsolvectx ctx)
{
    /* Return */
    return 0;
}
/*!
 *  @brief Solves min ||Ax - b||.
 *                                                                                                                                */
PetscErrorCode slds::solve::lsqsolve(Mat A, Vec x, Vec b, slds::solve::lsqsolvectx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec xb, bb;
    Mat P, B;
    MatType atype;
    KSP ksp;
    PC pc;
    KSPConvergedReason kspconv;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up KSP context */
    PetscCall(KSPCreate(PETSC_COMM_WORLD, &ksp));
    PetscCall(KSPGetPC(ksp, &pc));
    PetscCall(KSPSetType(ksp, KSPLSQR));
    PetscCall(PCSetType(pc, ctx.pctype));
    PetscCall(KSPSetTolerances(ksp, ctx.ksprtol, ctx.kspatol, ctx.kspdtol, ctx.kspmaxit));
    /* Call user provided routines if present */
    if(ctx.kspoptions)
        PetscCall(ctx.kspoptions(ksp, ctx.kspctx));
    if(ctx.pcoptions)
        PetscCall(ctx.pcoptions(pc, ctx.pcctx));
    /* Check matrix type */
    PetscCall(MatGetType(A, &atype));
    /* If we use MUMPS LU, convert matrix if required */
    if(strcmp(atype, MATAIJ) && strcmp(atype, MATSEQAIJ) && strcmp(atype, MATMPIAIJ) && ctx.safe)
    {
        /* Set up MUMPS LU with zero and static pivoting */
        PetscCall(PCSetType(pc, PCLU));
        PetscCall(PetscOptionsSetValue(NULL, "-mat_mumps_icntl_4", "0"));
        PetscCall(PetscOptionsSetValue(NULL, "-mat_mumps_icntl_24", "1"));
        PetscCall(PCFactorSetMatSolverType(pc, MATSOLVERMUMPS));
        /* Copy matrix and vectors to MPIAIJ and STANDARD types */
        PetscCall(MatConvert(A, MATAIJ, MAT_INITIAL_MATRIX, &B));
        PetscCall(VecDuplicate(x, &xb));
        PetscCall(VecDuplicate(b, &bb));
        PetscCall(VecSetType(xb, VECSTANDARD));
        PetscCall(VecSetType(bb, VECSTANDARD));
        PetscCall(VecCopy(x, xb));
        PetscCall(VecCopy(b, bb));
        /* Create matrix for preconditioner */
        PetscCall(slds::la::matmult(B, B, &P, MATPRODUCT_AtB, ctx.alg, ctx.fill, 0));
        /* Set operators */
        PetscCall(KSPSetOperators(ksp, B, P));
        /* Solve */
        PetscCall(KSPSolve(ksp, bb, xb));
        /* Copy solution */
        PetscCall(VecCopy(xb, x));
        /* Clean up */
        PetscCall(MatDestroy(&B));
        PetscCall(VecDestroy(&xb));
        PetscCall(VecDestroy(&bb));
    }
    /* Else proceed with the input matrix */
    else
    {
        /* If we use MUMPS LU, set options */
        if(ctx.safe)
        {
            /* Set up MUMPS LU with zero pivoting */
            PetscCall(PCSetType(pc, PCLU));
            PetscCall(PCFactorSetMatSolverType(pc, MATSOLVERMUMPS));
            PetscCall(PetscOptionsSetValue(NULL, "-mat_mumps_icntl_24", "1"));
        }
        /* Create matrix for preconditioner */
        PetscCall(slds::la::matmult(A, A, &P, MATPRODUCT_AtB, ctx.alg, ctx.fill, 0));
        /* Set operators */
        PetscCall(KSPSetOperators(ksp, A, P));
        /* Solve */
        PetscCall(KSPSolve(ksp, b, x));
    }
    /* Check convergence */
    PetscCall(KSPGetConvergedReason(ksp, &kspconv));
    /* Clean up */
    PetscCall(MatDestroy(&P));
    PetscCall(KSPDestroy(&ksp));
    /* Return */
    return kspconv;
}
/*!
 *  @brief Solves Ax = b.
 *                                                                                                                                */
PetscErrorCode slds::solve::lsolve(Mat A, Vec x, Vec b, slds::solve::lsolvectx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    KSP ksp;
    PC pc;
    KSPConvergedReason kspconv;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up KSP context */
    PetscCall(KSPCreate(PETSC_COMM_WORLD, &ksp));
    PetscCall(KSPGetPC(ksp, &pc));
    PetscCall(KSPSetType(ksp, ctx.ksptype));
    PetscCall(PCSetType(pc, ctx.pctype));
    PetscCall(KSPSetTolerances(ksp, ctx.ksprtol, ctx.kspatol, ctx.kspdtol, ctx.kspmaxit));
    /* Call user provided routines if present */
    if(ctx.kspoptions)
        PetscCall(ctx.kspoptions(ksp, ctx.kspctx));
    if(ctx.pcoptions)
        PetscCall(ctx.pcoptions(pc, ctx.pcctx));
    /* Set operators */
    PetscCall(KSPSetOperators(ksp, A, A));
    /* Solve */
    PetscCall(KSPSolve(ksp, b, x));
    /* Check convergence */
    PetscCall(KSPGetConvergedReason(ksp, &kspconv));
    /* Clean up */
    PetscCall(KSPDestroy(&ksp));
    /* Return */
    return kspconv;
}
/*!
 *  @brief Solves f(x) = 0.
 *                                                                                                                                */
PetscErrorCode slds::solve::fsolve(PetscErrorCode (*f)(SNES, Vec, Vec, void*), PetscErrorCode (*df)(SNES, Vec, Mat, Mat, void*),
                                   Vec x0, Vec x, Vec fx, Mat Dfx, int d, void *fctx, slds::solve::fsolvectx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    SNES snes;
    SNESConvergedReason snesconv;
    KSP ksp;
    PC pc;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize solver context */
    PetscCall(SNESCreate(PETSC_COMM_WORLD, &snes));
    /* Set solver options */
    PetscCall(SNESGetKSP(snes, &ksp));
    PetscCall(KSPGetPC(ksp, &pc));
    PetscCall(SNESSetType(snes, ctx.snestype));
    PetscCall(KSPSetType(ksp, ctx.ksptype));
    PetscCall(PCSetType(pc, ctx.pctype));
    PetscCall(SNESSetTolerances(snes, ctx.snesatol, ctx.snesrtol, ctx.snesstol, ctx.snesmaxit, ctx.snesmaxf));
    PetscCall(SNESSetDivergenceTolerance(snes, ctx.snesdtol));
    PetscCall(KSPSetTolerances(ksp, ctx.ksprtol, ctx.kspatol, ctx.kspdtol, ctx.kspmaxit));
    /* Call user provided routines if present */
    if(ctx.snesoptions)
        PetscCall(ctx.snesoptions(snes, ctx.snesctx));
    if(ctx.kspoptions)
        PetscCall(ctx.kspoptions(ksp, ctx.kspctx));
    if(ctx.pcoptions)
        PetscCall(ctx.pcoptions(pc, ctx.pcctx));
    /* Set up monitoring */
    if(ctx.monitorsnes)
    {
        if(ctx.monitor)
            PetscCall(SNESMonitorSet(snes, ctx.monitor, ctx.monitorctx, NULL));
        else
            PetscCall(SNESMonitorSet(snes, snesinfo, NULL, NULL));
    }
    /* Set function evaluation routine */
    PetscCall(SNESSetFunction(snes, fx, f, fctx));
    /* Set derivative evaluation routine */
    if(df)
        PetscCall(SNESSetJacobian(snes, Dfx, Dfx, df, fctx));
    else
    {
        /* Set up finite differences context */
        slds::diff::dfctx dfctxx = slds::diff::dfctx(d, fctx, f, ctx.initnz, ctx.zerotol, ctx.colortype, ctx.split);
        PetscCall(SNESSetJacobian(snes, Dfx, Dfx, slds::diff::df, &dfctxx));
    }
    /* Set inital guess */
    PetscCall(VecCopy(x0, x));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Solve f(x) = 0 */
    PetscCall(SNESSolve(snes, NULL, x));
    /* Check convergence */
    PetscCall(SNESGetConvergedReason(snes, &snesconv));
    /* Clean up */
    PetscCall(SNESDestroy(&snes));
    /* Return */
    return snesconv;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
