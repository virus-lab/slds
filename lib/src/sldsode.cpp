/**********************************************************************************************************************************/
/*! @file sldsode.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2024
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of ordinary differential equations.
 *
 *  Module for numerical analysis of ordinary differential equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsode.hpp"
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Solves an ordinary differential equation.
 *                                                                                                                                */
PetscErrorCode slds::ode::odesolve(PetscErrorCode (*f)(TS, PetscReal, Vec, Vec, void*), Vec x0, Vec xt, PetscReal t0,
                                   PetscReal t1, int d, void *fctx, slds::ode::odesolvectx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec fx;
    TS ts;
    TSConvergedReason tsconv;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize */
    if(!ctx.fx)
        PetscCall(VecDuplicate(x0, &fx));
    else
        fx = ctx.fx;
    /* Initialize solver context */
    if(ctx.local)
        PetscCall(TSCreate(PETSC_COMM_SELF, &ts));
    else
        PetscCall(TSCreate(PETSC_COMM_WORLD, &ts));
    /* Set solver options */
    PetscCall(TSSetType(ts, ctx.tstype));
    PetscCall(TSSetProblemType(ts, ctx.tsptype));
    PetscCall(TSSetTimeStep(ts, ctx.dt));
    PetscCall(TSSetMaxSteps(ts, PETSC_INT_MAX));
    PetscCall(TSSetExactFinalTime(ts, ctx.tsftime));
    PetscCall(TSSetTolerances(ts, ctx.tsatol, NULL, ctx.tsrtol, NULL));
    /* Call user provided routines if present */
    if(ctx.tsoptions)
        PetscCall(ctx.tsoptions(ts, ctx.tsctx));
    /* Set function evaluation routine */
    PetscCall(TSSetRHSFunction(ts, fx, f, fctx));
    /* Set derivative evaluation routine */
    PetscCall(TSSetRHSJacobian(ts, ctx.Dfx, ctx.Dfx, ctx.df, fctx));
    /* Set initial time */
    PetscCall(TSSetTime(ts, t0));
    /* Set final time */
    PetscCall(TSSetMaxTime(ts, t1));
    /* Set initial condition */
    PetscCall(VecCopy(x0, xt));
    PetscCall(TSSetSolution(ts, x0));
    /* Sync processes */
    if(!ctx.local)
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Solve */
    PetscCall(TSSolve(ts, xt));
    /* Check convergence (appears to be broken) */
    /* PetscCall(TSGetConvergedReason(ts, &tsconv)); */
    /* Clean up */
    if(!ctx.fx)
        PetscCall(VecDestroy(&fx));
    PetscCall(TSDestroy(&ts));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
