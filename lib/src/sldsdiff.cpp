/**********************************************************************************************************************************/
/*! @file sldsdiff.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Differentiation routines.
 *
 *  Differentiation routines for numeric differentiation. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsdiff.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Initializes all values of a matrix to 1.
 *                                                                                                                                */
PetscErrorCode slds::diff::initnzfull(Mat A, int d, PetscErrorCode(*split)(int*, int*, int, int))
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize, n0, n1, dnz, onz;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Bind matrix to CPU */
    PetscCall(MatBindToCPU(A, PETSC_TRUE));
    /* Get ownership range */
    PetscCall(split(&n0, &n1, d, mpirank));
    /* Preallocate */
    if(mpicommsize > 1)
    {
        /* Set nz parameters */
        dnz = n1 - n0;
        onz = (d - (n1 - n0))*(n1 - n0 > 0);
        /* Preallocate */
        PetscCall(MatMPIAIJSetPreallocation(A, dnz, NULL, onz, NULL));
    }
    else
        PetscCall(MatSeqAIJSetPreallocation(A, d, NULL));
    /* Prepare entires */
    std::vector<int> idxr(n1 - n0, 0), idxc(d, 0);
    std::vector<PetscReal> v((n1 - n0)*d, 1);
    /* Set indices */
    for(int i = 0; i < n1 - n0; i++)
        idxr[i] = n0 + i;
    for(int i = 0; i < d; i++)
        idxc[i] = i;
    /* Set values */
    PetscCall(MatSetValues(A, idxr.size(), &idxr[0], idxc.size(), &idxc[0], &v[0], INSERT_VALUES));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
    /* Unbind matrix from CPU */
    PetscCall(MatBindToCPU(A, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes Dfx, where f: Rd -> Rd, using finite differences.
 *                                                                                                                                */
PetscErrorCode slds::diff::df(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *ctx)
{
    /* Set up */
    PetscBool initpetsc;
    Mat C, FD;
    MatType dfxtype;
    ISColoring iscoloring;
    MatFDColoring fdcoloring;
    MatColoring coloring;
    slds::util::matctx mctx(MATAIJ);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Recover data */
    dfctx ctxx = *static_cast<dfctx*>(ctx);
    /* Check matrix type */
    PetscCall(MatGetType(Dfx, &dfxtype));
    /* Create a new matrix for finite differences */
    mctx.noset = 1;
    PetscCall(slds::util::matsetdefault(&FD, ctxx.d, ctxx.d, &mctx));
    /* Initialize nonzero structure */
    if(ctxx.initnz)
        PetscCall(ctxx.initnz(FD, ctxx.d, ctxx.split));
    else
        PetscCall(slds::diff::initnzfull(FD, ctxx.d, ctxx.split));
    /* Set up finite difference approximation of DFx using matrix coloring */
    PetscCall(MatColoringCreate(FD, &coloring));
    PetscCall(MatColoringSetType(coloring, ctxx.colortype));
    PetscCall(MatColoringApply(coloring, &iscoloring));
    PetscCall(MatFDColoringCreate(FD, iscoloring, &fdcoloring));
    PetscCall(MatFDColoringSetFromOptions(fdcoloring));
    PetscCall(MatFDColoringSetUp(FD, iscoloring, fdcoloring));
    PetscCall(MatFDColoringSetFunction(fdcoloring, (PetscErrorCode (*)(void))ctxx.f, ctxx.fctx));
    /* Compute derivative */
    PetscCall(MatFDColoringApply(FD, fdcoloring, x, NULL));
    /* Set result */
    if(strcmp(dfxtype, MATAIJ) && strcmp(dfxtype, MATSEQAIJ) && strcmp(dfxtype, MATMPIAIJ))
    {
        /* Convert matrix */
        PetscCall(MatConvert(FD, dfxtype, MAT_INITIAL_MATRIX, &C));
        /* Copy matrix */
        PetscCall(MatCopy(C, Dfx, DIFFERENT_NONZERO_PATTERN));
        /* Clean up */
        PetscCall(MatDestroy(&C));
    }
    else
        PetscCall(MatCopy(FD, Dfx, DIFFERENT_NONZERO_PATTERN));
    /* Clean up */
    PetscCall(MatDestroy(&FD));
    PetscCall(MatColoringDestroy(&coloring));
    PetscCall(ISColoringDestroy(&iscoloring));
    PetscCall(MatFDColoringDestroy(&fdcoloring));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
