/**********************************************************************************************************************************/
/*! @file sldsie.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of integral equations.
 *
 *  Module for numerical analysis of integral equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include "sldsie.hpp"
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Evaluates an Urysohn operator.
 *                                                                                                                                */
PetscErrorCode slds::ie::furysohn(SNES snes, Vec u, Vec fu, void *uctx)
{
    /* Set up */
    const PetscReal *uuloc;
    PetscBool initpetsc;
    int mpirank, mpicommsize, n0, n1;
    Vec uloc;
    VecScatter scatterctx;
    std::vector<int> idx;
    std::vector<PetscReal> fuloc;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(u, PETSC_TRUE));
    PetscCall(VecBindToCPU(fu, PETSC_TRUE));
    /* Recover data */
    slds::ie::urysohnctx ctx = *static_cast<slds::ie::urysohnctx*>(uctx);
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(u, &n0, &n1));
    /* Initialize fu */
    PetscCall(VecSet(fu, 0));
    /* Reuse or create scatter to all processes */
    if(ctx.uloc && ctx.scatterctx)
    {
        uloc = *ctx.uloc;
        scatterctx = *ctx.scatterctx;
    }
    else
        PetscCall(VecScatterCreateToAll(u, &scatterctx, &uloc));
    /* Scatter values to all processes */
    PetscCall(VecScatterBegin(scatterctx, u, uloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(scatterctx, u, uloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecGetArrayRead(uloc, &uuloc));
    /* Extract parameter values */
    std::vector<PetscReal> uk(ctx.fctx.k, 0);
    if(ctx.fctx.k > 0)
    {
        for(int i = 0; i < ctx.fctx.k; i++)
            uk[i] = uuloc[ctx.Q->n*ctx.Q->d + i];
        ctx.fctx.lambda = &uk;
    }
    /* Preallocate memory */
    std::vector<PetscReal> node(ctx.Q->k, 0), nodeint(ctx.Q->k, 0), unodeint(ctx.Q->d, 0), feval(ctx.Q->d, 0),
        result(ctx.Q->d, 0);
    idx.reserve(((n1 - n0)/ctx.Q->d + 1)*ctx.Q->d);
    fuloc.reserve(((n1 - n0)/ctx.Q->d + 1)*ctx.Q->d);
    /* Compute values at the nodes in a parallel loop */
    for(int i = 0; i < ctx.Q->n; i++)
    {
        /* Determine if the node should be handled by the process */
        if(i*ctx.Q->d > n0 - 1 && i*ctx.Q->d < n1)
        {
            /* Set node to compute */
            for(int m = 0; m < ctx.Q->k; m++)
                node[m] = ctx.Q->nodes[i*ctx.Q->k + m];
            /* Null result */
            for(int m = 0; m < ctx.Q->d; m++)
                result[m] = 0;
            /* Compute integral */
            for(int l = 0; l < ctx.Q->n; l++)
            {
                /* Set current nodes */
                for(int m = 0; m < ctx.Q->k; m++)
                    nodeint[m] = ctx.Q->nodes[l*ctx.Q->k + m];
                for(int m = 0; m < ctx.Q->d; m++)
                    unodeint[m] = uuloc[l*ctx.Q->d + m];
                /* Evaluate kernel */
                PetscCall(ctx.f(&node[0], &nodeint[0], &unodeint[0], &feval[0], ctx.Q->k, ctx.Q->d, ctx.fctx));
                /* Add to result */
                for(int m = 0; m < ctx.Q->d; m++)
                    result[m] += ctx.Q->weights[l]*feval[m];
            }
            /* Set result */
            for(int m = 0; m < ctx.Q->d; m++)
            {
                idx.push_back(i*ctx.Q->d + m);
                fuloc.push_back(result[m]);
            }
        }
    }
    PetscCall(VecRestoreArrayRead(uloc, &uuloc));
    /* Set result */
    PetscCall(VecSetValues(fu, idx.size(), &idx[0], &fuloc[0], INSERT_VALUES));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fu));
    PetscCall(VecAssemblyEnd(fu));
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(u, PETSC_FALSE));
    PetscCall(VecBindToCPU(fu, PETSC_FALSE));
    /* Clean up */
    if(!ctx.uloc && !ctx.scatterctx)
    {
        PetscCall(VecDestroy(&uloc));
        PetscCall(VecScatterDestroy(&scatterctx));
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the derivative of an Urysohn operator.
 *                                                                                                                                */
PetscErrorCode slds::ie::dfurysohn(SNES snes, Vec u, Mat Dfu, Mat Dfuu, void *uctx)
{
    /* Set up */
    const PetscReal *uuloc;
    PetscBool initpetsc, dfassembled;
    int mpirank, mpicommsize, n0, n1;
    Vec uloc;
    VecScatter scatterctx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Bind vectors and matrices to CPU */
    PetscCall(VecBindToCPU(u, PETSC_TRUE));
    PetscCall(MatBindToCPU(Dfu, PETSC_TRUE));
    /* Recover data */
    slds::ie::urysohnctx ctx = *static_cast<slds::ie::urysohnctx*>(uctx);
    /* Get ownership range */
    PetscCall(MatGetOwnershipRange(Dfu, &n0, &n1));
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfu, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfu));
    /* Reuse or create scatter to all processes */
    if(ctx.uloc && ctx.scatterctx)
    {
        uloc = *ctx.uloc;
        scatterctx = *ctx.scatterctx;
    }
    else
        PetscCall(VecScatterCreateToAll(u, &scatterctx, &uloc));
    /* Scatter values to all processes */
    PetscCall(VecScatterBegin(scatterctx, u, uloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(scatterctx, u, uloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecGetArrayRead(uloc, &uuloc));
    /* Extract parameter values */
    std::vector<PetscReal> uk(ctx.fctx.k, 0);
    if(ctx.fctx.k > 0)
    {
        for(int i = 0; i < ctx.fctx.k; i++)
            uk[i] = uuloc[ctx.Q->n*ctx.Q->d + i];
        ctx.fctx.lambda = &uk;
    }
    /* Preallocate memory */
    std::vector<PetscReal> node(ctx.Q->k, 0), nodeint(ctx.Q->k, 0), unodeint(ctx.Q->d, 0), d3feval(ctx.Q->d*ctx.Q->d, 0),
        dlfr, dlfeval;
    if(ctx.parameters && ctx.fctx.k > 0)
    {
        dlfr.reserve(ctx.fctx.k*ctx.Q->d);
        dlfeval.reserve(ctx.Q->d);
    }
    /* Initialize vectors to store nonzeros of rows */
    std::vector<std::vector<int>> idxc(ctx.Q->d, std::vector<int>());
    std::vector<std::vector<PetscReal>> dfuc(ctx.Q->d, std::vector<PetscReal>());
    for(int i = 0; i < ctx.Q->d; i++)
    {
        idxc[i].reserve((ctx.Q->n + ctx.fctx.k)*ctx.Q->d);
        dfuc[i].reserve((ctx.Q->n + ctx.fctx.k)*ctx.Q->d);
    }
    /* Set rows of Dfu in parallel */
    for(int i = 0; i < ctx.Q->n; i++)
    {
        /* Determine if the node should be handled by the process */
        if(i*ctx.Q->d > n0 - 1 && i*ctx.Q->d < n1)
        {
            /* Set node corresponding to the row */
            for(int m = 0; m < ctx.Q->k; m++)
                node[m] = ctx.Q->nodes[i*ctx.Q->k + m];
            /* Zero parameter derivatives */
            if(ctx.parameters && ctx.fctx.k > 0)
            {
                for(int j = 0; j < ctx.fctx.k*ctx.Q->d; j++)
                    dlfr[j] = 0;
            }
            /* Set columns */
            for(int j = 0; j < ctx.Q->n; j++)
            {
                /* Set current nodes */
                for(int m = 0; m < ctx.Q->k; m++)
                    nodeint[m] = ctx.Q->nodes[j*ctx.Q->k + m];
                for(int m = 0; m < ctx.Q->d; m++)
                    unodeint[m] = uuloc[j*ctx.Q->d + m];
                /* Compute nonparameter derivatives */
                PetscCall(ctx.d3f(&node[0], &nodeint[0], &unodeint[0], &d3feval[0], ctx.Q->k, ctx.Q->d, ctx.fctx));
                /* Set nonparameter values */
                for(int m = 0; m < ctx.Q->d; m++)
                    for(int l = 0; l < ctx.Q->d; l++)
                    {
                        /* Never explicitly insert zeros to preserve sparsity */
                        if(PetscAbsReal(ctx.Q->weights[j]*d3feval[m*ctx.Q->d + l]) > ctx.zerotol)
                        {
                            idxc[m].push_back(j*ctx.Q->d + l);
                            dfuc[m].push_back(ctx.Q->weights[j]*d3feval[m*ctx.Q->d + l]);
                        }
                    }
                /* Compute parameter derivatives */
                if(ctx.parameters && ctx.fctx.k > 0)
                {
                    for(int m = 0; m < ctx.fctx.k; m++)
                    {
                        PetscCall(ctx.dlf[m](&node[0], &nodeint[0], &unodeint[0], &dlfeval[0], ctx.Q->k, ctx.Q->d, ctx.fctx));
                        for(int l = 0; l < ctx.Q->d; l++)
                            dlfr[m*ctx.Q->d + l] += ctx.Q->weights[j]*dlfeval[l];
                    }
                }
            }
            /* Set parameter values */
            if(ctx.parameters && ctx.fctx.k > 0)
            {
                for(int m = 0; m < ctx.fctx.k; m++)
                    for(int l = 0; l < ctx.Q->d; l++)
                    {
                        /* Never explicitly insert zeros to preserve sparsity */
                        if(PetscAbsReal(dlfr[m*ctx.Q->d + l]) > ctx.zerotol)
                        {
                            idxc[l].push_back(ctx.Q->n*ctx.Q->d + m);
                            dfuc[l].push_back(dlfr[m*ctx.Q->d + l]);
                        }
                    }
            }
            /* Set matrix values */
            for(int m = 0; m < ctx.Q->d; m++)
            {
                int idxr[1];
                idxr[0] = i*ctx.Q->d + m;
                PetscCall(MatSetValues(Dfu, 1, &idxr[0], idxc[m].size(), &idxc[m][0], &dfuc[m][0], INSERT_VALUES));
            }
            /* Clear */
            for(int i = 0; i < ctx.Q->d; i++)
            {
                idxc[i].clear();
                dfuc[i].clear();
            }
        }
    }
    PetscCall(VecRestoreArrayRead(uloc, &uuloc));
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(Dfu, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(Dfu, MAT_FINAL_ASSEMBLY));
    /* Unbind vectors and matrices from CPU */
    PetscCall(VecBindToCPU(u, PETSC_FALSE));
    PetscCall(MatBindToCPU(Dfu, PETSC_FALSE));
    /* Clean up */
    if(!ctx.uloc && !ctx.scatterctx)
    {
        PetscCall(VecDestroy(&uloc));
        PetscCall(VecScatterDestroy(&scatterctx));
    }
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
