/**********************************************************************************************************************************/
/*! @file sldsloca.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2024
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of local attractors.
 *
 *  Module for numerical analysis of local attractors. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2024  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <algorithm>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <H5Cpp.h>
#include "sldsloca.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Data for HDF5 output.
 *                                                                                                                                */
class slds::loca::h5box
{
    public:
        int d; /*!< Dimension. */
        hvl_t h5a; /*!< Handle for writing interval bounds. */
        hvl_t h5b; /*!< Handle for writing interval bounds. */
        h5box(int dd): d(dd) {};
};
/*!
 *  @brief Provides a hash function for a vector of integers.
 *                                                                                                                                */
class hashintvec
{
    public:
        std::size_t operator()(std::vector<int> const& v) const
        {
            std::size_t seed = v.size();
            for(auto x : v) {
                x = ((x >> 16)^x)*0x45d9f3b;
                x = ((x >> 16)^x)*0x45d9f3b;
                x = (x >> 16)^x;
                seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
            }
            return seed;
        }
};
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Samples a random test point in a box.
 *                                                                                                                                */
PetscErrorCode slds::loca::boxtpoint(slds::loca::box box, Vec tx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<int> idx(box.d, 0);
    std::vector<PetscReal> txx(box.d, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize mersenne twister random number engine */
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 mtee(seed);
    /* Sample test point */
    for(int i = 0; i < box.d; i++)
    {
        std::uniform_real_distribution<PetscReal> u(box.a[i], box.b[i]);
        idx[i] = i;
        txx[i] = u(mtee);
    }
    /* Set values */
    PetscCall(VecSetValues(tx, box.d, &idx[0], &txx[0], INSERT_VALUES));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(tx));
    PetscCall(VecAssemblyEnd(tx));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the box a point is contained in.
 *                                                                                                                                */
PetscErrorCode slds::loca::getbox(Vec x, std::vector<int> *idx, int d, std::vector<int> n, slds::loca::box u)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    const PetscReal *xx;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize */
    PetscCall(VecGetArrayRead(x, &xx));
    /* Compute box indices */
    for(int i = 0; i < d; i++)
        (*idx)[i] = PetscFloorReal((xx[i] - u.a[i])*PetscPowInt(2, n[i])/(u.b[i] - u.a[i]));
    /* Restore */
    PetscCall(VecRestoreArrayRead(x, &xx));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the box corresponding to a given index.
 *                                                                                                                                */
PetscErrorCode slds::loca::idxbox(std::vector<int> idx, slds::loca::box *box, int d, std::vector<int> n, slds::loca::box u)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set box bounds */
    for(int i = 0; i < d; i++)
    {
        box->a[i] = u.a[i] + idx[i]*(u.b[i] - u.a[i])/PetscPowInt(2, n[i]);
        box->b[i] = u.a[i] + (idx[i] + 1)*(u.b[i] - u.a[i])/PetscPowInt(2, n[i]);
    }
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the local attractor of a box.
 *                                                                                                                                */
PetscErrorCode slds::loca::loca(PetscErrorCode (*f)(Vec, Vec, void*), slds::loca::box u, std::vector<slds::loca::box> *au, int d,
                                void *fctx, slds::loca::locactx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize;
    Vec tx, ftx;
    std::vector<int> n(d, 0), idx(d, 0);
    std::vector<std::vector<int>> idxau;
    slds::util::vecctx vctx, locvctx(VECSEQ, 1);
    slds::loca::box box(d, std::vector<PetscReal>(d, 0), std::vector<PetscReal>(d, 0));
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Initialize */
    PetscCall(slds::util::vecsetdefault(&tx, d, &locvctx));
    PetscCall(VecDuplicate(tx, &ftx));
    /* Print update */
    if(ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Compute initial subdivisions */
    idxau.push_back(std::vector<int>(d, 0));
    for(int i = 0; i < ctx.nsinit; i++)
    {
        /* Initialize */
        int saxis = 0;
        PetscReal boxl, boxlmax = -1;
        std::vector<std::vector<int>> idxxau(2*idxau.size(), std::vector<int>(d, 0));
        /* Determine the division axis */
        for(int j = 0; j < d; j++)
        {
            boxl = (u.b[j] - u.a[j])/PetscPowInt(2, n[j]);
            if(boxl > boxlmax)
            {
                saxis = j;
                boxlmax = boxl;
            }
        }
        /* Divide boxes */
        for(int j = 0; j < idxau.size(); j++)
        {
            /* Initialize */
            idx = idxau[j];
            /* Set lower index */
            idx[saxis] = 2*idxau[j][saxis];
            idxxau[2*j] = idx;
            /* Set lower index */
            idx[saxis] = 2*idxau[j][saxis] + 1;
            idxxau[2*j + 1] = idx;
        }
        /* Update */
        n[saxis] += 1;
        idxau = idxxau;
    }
    /* Compute subdivision steps */
    for(int i = ctx.nsinit; i < ctx.ns; i++)
    {
        /* Initialize */
        int saxis = 0;
        PetscReal boxl, boxlmax = -1;
        Vec mapau, mapauloc;
        VecScatter scatterctx;
        std::unordered_set<int> sidxxau;
        std::vector<std::vector<int>> idxxau(2*idxau.size(), std::vector<int>(d, 0));
        std::unordered_map<std::vector<int>, int, hashintvec> midxxau;
        const PetscReal *mapaulocc;
        /* Exit if the local attractor is empty */
        if(idxau.empty())
        {
             /* Print update */
            if(ctx.verbose)
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attractor is empty! Aborting... \n"));
            /* Exit */
            break;
        }
        /* Print update */
        if(ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Starting subdivision step %d with %d boxes \n", i, idxau.size()));
        /* Initialize test vectors */
        PetscCall(slds::util::vecsetdefault(&mapau, mpicommsize*idxxau.size(), &vctx));
        PetscCall(VecScatterCreateToAll(mapau, &scatterctx, &mapauloc));
        /* Print update */
        if(ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Dividing boxes... \n"));
        /* Determine the division axis */
        for(int j = 0; j < d; j++)
        {
            boxl = (u.b[j] - u.a[j])/PetscPowInt(2, n[j]);
            if(boxl > boxlmax)
            {
                saxis = j;
                boxlmax = boxl;
            }
        }
        /* Divide boxes */
        for(int j = 0; j < idxau.size(); j++)
        {
            /* Initialize */
            idx = idxau[j];
            /* Set lower index */
            idx[saxis] = 2*idxau[j][saxis];
            idxxau[2*j] = idx;
            /* Set lower index */
            idx[saxis] = 2*idxau[j][saxis] + 1;
            idxxau[2*j + 1] = idx;
        }
        /* Update divisions */
        n[saxis] += 1;
        /* Initialize box map */
        midxxau.reserve(idxxau.size());
        for(int j = 0; j < idxxau.size(); j++)
            midxxau.insert({idxxau[j], j});
        /* Initialize local box index */
        std::vector<int> idxtestauloc(idxxau.size(), 0);
        std::vector<PetscReal> testauloc(idxxau.size(), 0);
        for(int j = 0; j < idxxau.size(); j++)
            idxtestauloc[j] = mpirank*idxxau.size() + j;
        /* Print update */
        if(ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Mapping boxes... \n"));
        /* Map boxes */
        for(int j = 0; j < idxxau.size(); j++)
        {
            /* Skip boxes that do not belong to this process */
            if(j % mpicommsize != mpirank)
                continue;
            /* Compute box */
            PetscCall(slds::loca::idxbox(idxxau[j], &box, d, n, u));
            /* Test box */
            for(int k = 0; k < ctx.nt; k++)
            {
                /* Compute test point */
                PetscCall(boxtpoint(box, tx));
                /* Evaluate test point */
                PetscCall(f(tx, ftx, fctx));
                /* Compute box */
                PetscCall(getbox(ftx, &idx, d, n, u));
                /* Check if the box is contained and update its map value */
                if(midxxau.contains(idx))
                    testauloc[midxxau[idx]] += 1;
            }
        }
        /* Set result */
        PetscCall(VecSetValues(mapau, idxtestauloc.size(), &idxtestauloc[0], &testauloc[0], INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble result */
        PetscCall(VecAssemblyBegin(mapau));
        PetscCall(VecAssemblyEnd(mapau));
        /* Scatter results */
        PetscCall(VecScatterBegin(scatterctx, mapau, mapauloc, INSERT_VALUES, SCATTER_FORWARD));
        PetscCall(VecScatterEnd(scatterctx, mapau, mapauloc, INSERT_VALUES, SCATTER_FORWARD));
        /* Preallocate */
        idxau.clear();
        idxau.reserve(idxxau.size());
        /* Initialize lookup set */
        sidxxau.reserve(idxxau.size());
        PetscCall(VecGetArrayRead(mapauloc, &mapaulocc));
        for(int j = 0; j < idxxau.size(); j++)
            for(int k = 0; k < mpicommsize; k++)
                if(mapaulocc[k*idxxau.size() + j] > 0.5)
                {
                    sidxxau.insert(j);
                    break;
                }
        PetscCall(VecRestoreArrayRead(mapauloc, &mapaulocc));
        /* Update */
        for(int j = 0; j < idxxau.size(); j++)
            if(sidxxau.contains(j))
                idxau.push_back(idxxau[j]);
        /* Clean up */
        PetscCall(VecDestroy(&mapau));
        PetscCall(VecDestroy(&mapauloc));
        PetscCall(VecScatterDestroy(&scatterctx));
    }
    /* Print update */
    if(ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing result... \n"));
    /* Set result */
    for(int i = 0; i < idxau.size(); i++)
    {
        PetscCall(slds::loca::idxbox(idxau[i], &box, d, n, u));
        (*au).push_back(box);
    }
    /* Print update */
    if(ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Clean up */
    PetscCall(VecDestroy(&tx));
    PetscCall(VecDestroy(&ftx));
    /* Print update */
    if(ctx.verbose)
        PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished! \n"));
    /* Return */
    return 0;
}
/*!
 *  @brief Writes a list of boxes to a HDF5 file.
 *                                                                                                                                */
PetscErrorCode slds::loca::writeh5(std::vector<slds::loca::box> *boxes, std::string f)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    std::vector<std::vector<double>> h5av((*boxes).size(), std::vector<double>((*boxes)[0].d, 0)),
                                     h5bv((*boxes).size(), std::vector<double>((*boxes)[0].d, 0));
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    H5::H5File file;
    std::vector<slds::loca::h5box> h5b;
    hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    /* Create HDF5 file on rank 0 */
    if(mpirank == 0)
        file = H5::H5File(f, H5F_ACC_TRUNC);
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
     /* Write interval bounds */
    for(int i = 0; i < (*boxes).size(); i++)
    {
        /* Extract bounds */
        for(int j = 0; j < (*boxes)[0].d; j++)
        {
            h5av[i][j] = (*boxes)[i].a[j];
            h5bv[i][j] = (*boxes)[i].b[j];
        }
        /* Set data only on rank 0 */
        if(mpirank == 0)
            h5b.push_back(slds::loca::h5box((*boxes)[0].d));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5b.empty())
    {
        /* Define type */
        H5::CompType ntype(sizeof(slds::loca::h5box));
        ntype.insertMember("Dimension", HOFFSET(slds::loca::h5box, d), H5T_NATIVE_INT);
        ntype.insertMember("Lower", HOFFSET(slds::loca::h5box, h5a), h5dblv);
        ntype.insertMember("Upper", HOFFSET(slds::loca::h5box, h5b), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5b.size(); i++)
        {
            h5b[i].h5a.len = h5av[i].size();
            h5b[i].h5a.p = &h5av[i][0];
            h5b[i].h5b.len = h5bv[i].size();
            h5b[i].h5b.p = &h5bv[i][0];
        }
        /* Create dataset */
        hsize_t ndim[1];
        ndim[0] = h5b.size();
        int nrank = sizeof(ndim)/sizeof(hsize_t);
        H5::DataSpace nspace(nrank, ndim);
        H5::DataSet ndataset(file.createDataSet("Boxes", ntype, nspace));
        /* Write dataset */
        ndataset.write(&h5b[0], ntype);
        /* Clear to free up memory */
        h5b.clear();
        h5av.clear();
        h5bv.clear();
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Clean up */
    H5Tclose(h5dblv);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
