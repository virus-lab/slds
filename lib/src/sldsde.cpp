/**********************************************************************************************************************************/
/*! @file sldsde.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 11/2022
 *  @copyright GNU Public License

 *  @brief Module for numerical analysis of difference equations.
 *
 *  Module for numerical analysis of difference equations. Based on PETSc (https://petsc.org/).
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <H5Cpp.h>
#include "sldsde.hpp"
#include "sldsla.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Data for HDF5 output.
 *                                                                                                                                */
class slds::de::h5it
{
    public:
        int i; /*!< Index. */
        int j; /*!< Index of the iterate. */
        int p; /*!< Period of the iterate. */
        hvl_t h5x; /*!< Handle for iterate data. */
        h5it(int ii, int jj, int pp): i(ii), j(jj), p(pp) {};
};
/**********************************************************************************************************************************/
/* Implementations */
/**********************************************************************************************************************************/
/*!
 *  @brief Iterates a periodic difference equation.
 *                                                                                                                                */
PetscErrorCode slds::de::it(PetscErrorCode (**f)(SNES, Vec, Vec, void*), Vec x0, std::vector<Vec> *xt, int t, int init, int period,
                            int d, int k, void **fctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, r0, r1;
    Vec x, fx;
    std::vector<int> idxk, idxxr(1, 0);
    std::vector<PetscReal> xk, xr(1, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x0, &r0, &r1));
    /* Initialize */
    PetscCall(VecDuplicate(x0, &x));
    PetscCall(VecDuplicate(x0, &fx));
    /* Preallocate memory */
    idxk.reserve(k);
    xk.reserve(k);
    (*xt).reserve(t + 1);
    /* Copy initial value */
    PetscCall(VecCopy(x0, x));
    /* Extract parameters */
    for(int j = 0; j < k; j++)
    {
        /* Determine if the value should be handled by the current process */
        if(d - k + j > r0 - 1 && d - k + j < r1)
        {
            /* Set index */
            idxxr[0] = d - k + j;
            /* Get values */
            PetscCall(VecGetValues(x0, 1, &idxxr[0], &xr[0]));
            /* Store values */
            idxk.push_back(d - k + j);
            xk.push_back(xr[0]);
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Place initial value */
    if(init < 1)
    {
        (*xt).push_back(Vec());
        PetscCall(VecDuplicate(x, &((*xt).back())));
        PetscCall(VecCopy(x, (*xt).back()));
    }
    /* Iterate */
    for(int i = 0; i < t; i++)
    {
        /* Evaluate */
        PetscCall(f[i % period](NULL, x, fx, fctx[i % period]));
        /* Reset parameters */
        PetscCall(VecSetValues(fx, idxk.size(), &idxk[0], &xk[0], INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble x */
        PetscCall(VecAssemblyBegin(fx));
        PetscCall(VecAssemblyEnd(fx));
        /* Place result */
        if(i > init - 1)
        {
            (*xt).push_back(Vec());
            PetscCall(VecDuplicate(fx, &((*xt).back())));
            PetscCall(VecCopy(fx, (*xt).back()));
        }
        /* Set new value */
        PetscCall(VecCopy(fx, x));
    }
    /* Clean up */
    PetscCall(VecDestroy(&x));
    PetscCall(VecDestroy(&fx));
    /* Return */
    return 0;
}
/*!
 *  @brief Attempts to find the period of a sequence of iterates.
 *                                                                                                                                */
PetscErrorCode slds::de::xtperiod(std::vector<Vec> *xt, int *period, PetscReal eps, NormType ntype)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, pfound;
    PetscReal norm;
    Vec diff;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Return if there are less than 2 iterates */
    if((*xt).size() < 2)
        return -1;
    /* Initialize */
    pfound = false;
    PetscCall(VecDuplicate((*xt)[0], &diff));
    /* Loop over possible periods (C++ rounds towards zero for integer division) */
    for(int i = 1; i < (*xt).size()/2; i++)
    {
        /* Set up */
        int ppass = true;
        /* Check if the iterates are i-periodic */
        for(int j = 1; j*i < (*xt).size(); j++)
        {
            /* Compute difference norm */
            PetscCall(VecAXPBYPCZ(diff, 1, -1, 0, (*xt)[0], (*xt)[j*i]));
            PetscCall(VecNorm(diff, ntype, &norm));
            /* If the iterates are not equal, break */
            if(norm > eps)
            {
                ppass = false;
                break;
            }
        }
        /* If we found a suitable period, return */
        if(ppass)
        {
            *period = i;
            pfound = true;
            break;
        }
    }
    /* If no period was found, return -1 */
    if(!pfound)
        *period = -1;
    /* Clean up */
    PetscCall(VecDestroy(&diff));
    /* Return */
    if(!pfound)
        return -1;
    else
        return 0;
}
/*!
 *  @brief Computes the Feigenbaum diagram for a one parameter family of periodic difference equations.
 *                                                                                                                                */
PetscErrorCode slds::de::feigen(PetscErrorCode (**f)(SNES, Vec, Vec, void*), Vec x0, std::vector<std::vector<Vec>> *xtlambda,
                                PetscReal lambda0, PetscReal lambda1, int period, int d, void **fctx, slds::de::feigenctx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, rcode, p, r0, r1;
    PetscReal s;
    Vec x;
    std::vector<Vec> xt;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x0, &r0, &r1));
    /* Initialize */
    PetscCall(VecDuplicate(x0, &x));
    PetscCall(VecCopy(x0, x));
    /* Preallocate memory */
    (*xtlambda).reserve(ctx.nstep);
    /* Calculate step size */
    s = (lambda1 - lambda0)/ctx.nstep;
    /* Iterate equation for multiple values of lambda */
    for(int i = 0; i < ctx.nstep + 1; i++)
    {
        /* Print update */
        if(ctx.verbose)
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Step %d of %d \n", i, ctx.nstep));
        /* Set parameter value */
        if(d - 1 > r0 - 1 && d - 1 < r1)
            PetscCall(VecSetValue(x, d - 1, lambda0 + i*s, INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble x */
        PetscCall(VecAssemblyBegin(x));
        PetscCall(VecAssemblyEnd(x));
        /* Iterate */
        if(i == 0 || ctx.reset)
            PetscCall(slds::de::it(f, x, &xt, ctx.initit + ctx.t, ctx.initit, period, d, 1, fctx));
        else
            PetscCall(slds::de::it(f, x, &xt, ctx.it + ctx.t, ctx.it, period, d, 1, fctx));
        /* Set new initial value */
        if(ctx.reset)
            PetscCall(VecCopy(x0, x));
        else
            PetscCall(VecCopy(xt.back(), x));
        /* Compute period */
        rcode = xtperiod(&xt, &p, ctx.eps, ctx.ntype);
        /* Return if there was an error (use PetscCall for clean exit) */
        if(rcode == 100)
            PetscCall(100);
        /* If no period was found, store all iterates */
        if(rcode < 0)
            p = ctx.t;
        /* Trim result */
        for(int j = xt.size(); j > p; j--)
        {
            PetscCall(VecDestroy(&(xt.back())));
            xt.pop_back();
        }
        /* Set result */
        (*xtlambda).push_back(xt);
        /* Clear */
        xt.clear();
    }
    /* Clean up */
    PetscCall(VecDestroy(&x));
    /* Return */
    return 0;
}
/*!
 *  @brief Evaluates a cyclic system.
 *                                                                                                                                */
PetscErrorCode slds::de::fcyc(SNES snes, Vec x, Vec fx, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, n0, n1;
    std::vector<int> idxtk, idxxr(1, 0);
    std::vector<PetscReal> xtk, xr(1, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    PetscCall(VecBindToCPU(fx, PETSC_TRUE));
    /* Recover data */
    slds::de::cycctx ctx = *static_cast<slds::de::cycctx*>(ctxx);
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x, &n0, &n1));
    /* Initialize fx */
    PetscCall(VecSet(fx, 0));
    /* Get parameter values */
    for(int j = 0; j < ctx.k; j++)
    {
        /* Determine if the value should be handled by the current process */
        if(ctx.period*(ctx.d - ctx.k) + j > n0 - 1 && ctx.period*(ctx.d - ctx.k) + j < n1)
        {
            /* Set index */
            idxxr[0] = ctx.period*(ctx.d - ctx.k) + j;
            /* Get values */
            PetscCall(VecGetValues(x, 1, &idxxr[0], &xr[0]));
            /* Store values */
            idxtk.push_back(ctx.d - ctx.k + j);
            xtk.push_back(xr[0]);
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Set values of fx */
    for(int i = 0; i < ctx.period; i++)
    {
        /* Initialize xt and fxt */
        PetscCall(VecSet(ctx.xt, 0));
        PetscCall(VecSet(ctx.fxt, 0));
        /* Set parameter values */
        PetscCall(VecSetValues(ctx.xt, idxtk.size(), &idxtk[0], &xtk[0], INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble xt */
        PetscCall(VecAssemblyBegin(ctx.xt));
        PetscCall(VecAssemblyEnd(ctx.xt));
        /* Set nonparameter values */
        if(i != 0)
            PetscCall(slds::util::subvecget(x, ctx.xt, (i - 1)*(ctx.d - ctx.k), i*(ctx.d - ctx.k), 0,
                                            ctx.zerotol));
        else
            PetscCall(slds::util::subvecget(x, ctx.xt, (ctx.period - 1)*(ctx.d - ctx.k), ctx.period*(ctx.d - ctx.k), 0,
                                            ctx.zerotol));
        /* Compute function value */
        if(i != 0)
            PetscCall(ctx.fpd[(i - 1) % ctx.pdperiod](snes, ctx.xt, ctx.fxt, ctx.fpdctx[(i - 1) % ctx.pdperiod]));
        else
            PetscCall(ctx.fpd[(ctx.period - 1) % ctx.pdperiod](snes, ctx.xt, ctx.fxt, ctx.fpdctx[(ctx.period - 1) % ctx.pdperiod]));
        /* Subtract id */
        PetscCall(VecSet(ctx.xt, 0));
        PetscCall(slds::util::subvecget(x, ctx.xt, i*(ctx.d - ctx.k), (i + 1)*(ctx.d - ctx.k),
                                        0, ctx.zerotol));
        PetscCall(VecAXPY(ctx.fxt, -1, ctx.xt));
        /* Set subvector */
        PetscCall(slds::util::subvecset(fx, ctx.fxt, 0, ctx.d - ctx.k, i*(ctx.d - ctx.k), ctx.zerotol));
    }
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    PetscCall(VecBindToCPU(fx, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the derivative of a cyclic system.
 *                                                                                                                                */
PetscErrorCode slds::de::dfcyc(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank, n0, n1;
    std::vector<int> idxtk, idxxr(1, 0);
    std::vector<PetscReal> xtk, xr(1, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors and matrices to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    PetscCall(MatBindToCPU(Dfx, PETSC_TRUE));
    /* Recover data */
    slds::de::cycctx ctx = *static_cast<slds::de::cycctx*>(ctxx);
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x, &n0, &n1));
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Get parameter values */
    for(int j = 0; j < ctx.k; j++)
    {
        /* Determine if the value should be handled by the current process */
        if(ctx.period*(ctx.d - ctx.k) + j > n0 - 1 && ctx.period*(ctx.d - ctx.k) + j < n1)
        {
            /* Set index */
            idxxr[0] = ctx.period*(ctx.d - ctx.k) + j;
            /* Get values */
            PetscCall(VecGetValues(x, 1, &idxxr[0], &xr[0]));
            /* Store values */
            idxtk.push_back(ctx.d - ctx.k + j);
            xtk.push_back(xr[0]);
        }
    }
    /* Set submatrices */
    for(int i = 0; i < ctx.period; i++)
    {
        /* Zero derivative if required */
        PetscCall(MatAssembled(ctx.Dfxt, &dfassembled));
        if(dfassembled == PETSC_TRUE)
            PetscCall(MatZeroEntries(ctx.Dfxt));
        /* Initialize xt */
        PetscCall(VecSet(ctx.xt, 0));
        /* Set parameter values */
        PetscCall(VecSetValues(ctx.xt, idxtk.size(), &idxtk[0], &xtk[0], INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble xt */
        PetscCall(VecAssemblyBegin(ctx.xt));
        PetscCall(VecAssemblyEnd(ctx.xt));
        /* Set nonparameter values */
        if(i != 0)
            PetscCall(slds::util::subvecget(x, ctx.xt, (i - 1)*(ctx.d - ctx.k), i*(ctx.d - ctx.k), 0,
                                            ctx.zerotol));
        else
            PetscCall(slds::util::subvecget(x, ctx.xt, (ctx.period - 1)*(ctx.d - ctx.k), ctx.period*(ctx.d - ctx.k), 0,
                                            ctx.zerotol));
        /* Compute derivative (if dfpd is NULL finite differences is used) */
        if(ctx.dfpd)
        {
            if(i != 0)
                PetscCall(ctx.dfpd[(i - 1) % ctx.pdperiod](NULL, ctx.xt, ctx.Dfxt, ctx.Dfxt, ctx.fpdctx[(i - 1) % ctx.pdperiod]));
            else
                PetscCall(ctx.dfpd[(ctx.period - 1) % ctx.pdperiod](NULL, ctx.xt, ctx.Dfxt, ctx.Dfxt,
                                                                    ctx.fpdctx[(ctx.period - 1) % ctx.pdperiod]));
        }
        else
        {
            if(i != 0)
            {
                /* Set up finite differences context */
                ctx.diffctx.f = ctx.fpd[(i - 1) % ctx.pdperiod];
                ctx.diffctx.fctx = ctx.fpdctx[(i - 1) % ctx.pdperiod];
                /* Evaluate derivative */
                PetscCall(slds::diff::df(NULL, ctx.xt, ctx.Dfxt, ctx.Dfxt, &ctx.diffctx));
            }
            else
            {
                /* Set up finite differences context */
                ctx.diffctx.f = ctx.fpd[(ctx.period - 1) % ctx.pdperiod];
                ctx.diffctx.fctx = ctx.fpdctx[(ctx.period - 1) % ctx.pdperiod];
                /* Evaluate derivative */
                PetscCall(slds::diff::df(NULL, ctx.xt, ctx.Dfxt, ctx.Dfxt, &ctx.diffctx));
            }
        }
        /* Set submatrices */
        if(i != 0)
            PetscCall(slds::util::submatset(Dfx, ctx.Dfxt, 0, 0, ctx.d - ctx.k, ctx.d - ctx.k,
                                            i*(ctx.d - ctx.k), (i - 1)*(ctx.d - ctx.k), ctx.zerotol));
        else
            PetscCall(slds::util::submatset(Dfx, ctx.Dfxt, 0, 0, ctx.d - ctx.k, ctx.d - ctx.k,
                                            i*(ctx.d - ctx.k), (ctx.period - 1)*(ctx.d - ctx.k), ctx.zerotol));
        PetscCall(slds::util::submatset(Dfx, ctx.Dfxt, 0, ctx.d - ctx.k, ctx.d - ctx.k, ctx.d,
                                        i*(ctx.d - ctx.k), ctx.period*(ctx.d - ctx.k), ctx.zerotol));
    }
    /* Add id */
    PetscCall(slds::util::matdiagonaladd(Dfx, -1, ctx.period*(ctx.d - ctx.k)));
    /* Unbind vectors and matrices from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    PetscCall(MatBindToCPU(Dfx, PETSC_FALSE));
    /* Return */
    return 0;
}
/*!
 *  @brief Initializes the derivative of a cyclic system.
 *                                                                                                                                */
PetscErrorCode slds::de::initdfcycaijdense(Mat *Dfx, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, mpicommsize, n0, n1;
    std::vector<int> idxdfr(1, 0), idxdfc;
    std::vector<PetscReal> dfr;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Query number of processes */
    PetscCall(MPI_Comm_size(PETSC_COMM_WORLD, &mpicommsize));
    /* Recover data */
    slds::de::cycctx ctx = *static_cast<slds::de::cycctx*>(ctxx);
    /* Set matrix context */
    slds::util::matctx mctx(ctx.mcyctype);
    /* Determine ownership */
    PetscCall(slds::util::splitdefault(&n0, &n1, ctx.period*(ctx.d - ctx.k) + ctx.k, mpirank));
    /* Set matrix */
    PetscCall(slds::util::matsetdefault(Dfx, ctx.period*(ctx.d - ctx.k) + ctx.k, ctx.period*(ctx.d - ctx.k) + ctx.k, &mctx));
    /* Set preallocation */
    mctx.noset = 1;
    std::vector<int> dnnz = std::vector<int>(n1 - n0, 0);
    std::vector<int> onnz = std::vector<int>(n1 - n0, 0);
    /* Determine preallocation */
    for(int i = 0; i < ctx.period*(ctx.d - ctx.k) + ctx.k; i++)
    {
        /* Determine if the row is owned by the current process */
        if(i > n0 - 1 && i < n1)
        {
            /* Determine iterate index */
            int idx = i/(ctx.d - ctx.k);
            /* Check columns */
            for (int j = 0; j < ctx.period*(ctx.d - ctx.k) + ctx.k; j++)
            {
                /* Determine if the entry is nonzero and its location */
                int isnz = false;
                int isd = false;
                /* If it is on the diagonal it is nonzero */
                if(i == j)
                    isnz = true;
                /* If it is in the parameter block it is nonzero */
                else if(j > ctx.period*(ctx.d - ctx.k) - 1)
                    isnz = true;
                /* If it is in a derivative block it is nonzero */
                else if(idx == 0 && j > (ctx.period - 1)*(ctx.d - ctx.k) - 1 && j < ctx.period*(ctx.d - ctx.k))
                    isnz = true;
                else if(idx != 0 && j > (idx - 1)*(ctx.d - ctx.k) - 1 && j < idx*(ctx.d - ctx.k))
                    isnz = true;
                /* If it is in a parameter row it is nonzero */
                else if(i > ctx.period*(ctx.d - ctx.k) - 1)
                    isnz = true;
                /* Determine if the value is in the diagonal submatrix */
                if(j > n0 - 1 && j < n1)
                    isd = true;
                /* Add nonzero count */
                if(isnz && isd)
                    dnnz[i - n0] += 1;
                else if(isnz)
                    onnz[i - n0] += 1;
            }
        }
    }
    /* Preallocate */
    if(mpicommsize > 1)
        PetscCall(MatMPIAIJSetPreallocation(*Dfx, 1, &dnnz[0], 1, &onnz[0]));
    else
        PetscCall(MatSeqAIJSetPreallocation(*Dfx, 1, &dnnz[0]));
    /* Reserve memory */
    idxdfc.reserve(ctx.period*(ctx.d - ctx.k) + ctx.k);
    dfr.reserve(ctx.period*(ctx.d - ctx.k) + ctx.k);
    /* Fill */
    for(int i = 0; i < ctx.period*(ctx.d - ctx.k) + ctx.k; i++)
    {
        /* Determine if the row is owned by the current process */
        if(i > n0 - 1 && i < n1)
        {
            /* Determine iterate index */
            int idx = i/(ctx.d - ctx.k);
            /* Set row index */
            idxdfr[0] = i;
            /* Check columns */
            for (int j = 0; j < ctx.period*(ctx.d - ctx.k) + ctx.k; j++)
            {
                /* Determine if the entry is nonzero and its location */
                int isnz = false;
                /* If it is on the diagonal it is nonzero */
                if(i == j)
                    isnz = true;
                /* If it is in the parameter block it is nonzero */
                else if(j > ctx.period*(ctx.d - ctx.k) - 1)
                    isnz = true;
                /* If it is in a derivative block it is nonzero */
                else if(idx == 0 && j > (ctx.period - 1)*(ctx.d - ctx.k) - 1 && j < ctx.period*(ctx.d - ctx.k))
                    isnz = true;
                else if(idx != 0 && j > (idx - 1)*(ctx.d - ctx.k) - 1 && j < idx*(ctx.d - ctx.k))
                    isnz = true;
                /* If it is in a parameter row it is nonzero */
                else if(i > ctx.period*(ctx.d - ctx.k) - 1)
                    isnz = true;
                /* If the value is nonzero, fill with zero to reserve space */
                if(isnz)
                {
                    idxdfc.push_back(j);
                    dfr.push_back(1);
                }
            }
            /* Set values */
            PetscCall(MatSetValues(*Dfx, 1, &idxdfr[0], idxdfc.size(), &idxdfc[0], &dfr[0], INSERT_VALUES));
            /* Clear */
            idxdfc.clear();
            dfr.clear();
        }
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble matrix */
    PetscCall(MatAssemblyBegin(*Dfx, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(*Dfx, MAT_FINAL_ASSEMBLY));
    /* Zero matrix */
    PetscCall(MatZeroEntries(*Dfx));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the linearized period operator of a periodic solution.
 *                                                                                                                                */
PetscErrorCode slds::de::pf(Vec x, Mat Dfx, void *ctxx)
{
        /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank, n0, n1;
    Mat P;
    std::vector<int> idxtk, idxxr(1, 0);
    std::vector<PetscReal> xtk, xr(1, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Bind vectors to CPU */
    PetscCall(VecBindToCPU(x, PETSC_TRUE));
    /* Recover data */
    slds::de::pfctx ctx = *static_cast<slds::de::pfctx*>(ctxx);
    /* Get ownership range */
    PetscCall(VecGetOwnershipRange(x, &n0, &n1));
    /* Assign product matrix */
    if(ctx.P)
        P = *ctx.P;
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Get parameter values */
    for(int j = 0; j < ctx.ctx.k; j++)
    {
        /* Determine if the value should be handled by the current process */
        if(ctx.ctx.period*(ctx.ctx.d - ctx.ctx.k) + j > n0 - 1 && ctx.ctx.period*(ctx.ctx.d - ctx.ctx.k) + j < n1)
        {
            /* Set index */
            idxxr[0] = ctx.ctx.period*(ctx.ctx.d - ctx.ctx.k) + j;
            /* Get values */
            PetscCall(VecGetValues(x, 1, &idxxr[0], &xr[0]));
            /* Store values */
            idxtk.push_back(ctx.ctx.d - ctx.ctx.k + j);
            xtk.push_back(xr[0]);
        }
    }
    /* Initialize Dfx as id */
    PetscCall(slds::util::matdiagonaladd(Dfx, 1, ctx.ctx.d));
    /* Compute products */
    for(int i = 0; i < ctx.ctx.period; i++)
    {
        /* Zero derivative if required */
        PetscCall(MatAssembled(ctx.ctx.Dfxt, &dfassembled));
        if(dfassembled == PETSC_TRUE)
            PetscCall(MatZeroEntries(ctx.ctx.Dfxt));
        /* Initialize xt */
        PetscCall(VecSet(ctx.ctx.xt, 0));
        /* Set parameter values */
        PetscCall(VecSetValues(ctx.ctx.xt, idxtk.size(), &idxtk[0], &xtk[0], INSERT_VALUES));
        /* Sync processes */
        PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
        /* Assemble xt */
        PetscCall(VecAssemblyBegin(ctx.ctx.xt));
        PetscCall(VecAssemblyEnd(ctx.ctx.xt));
        /* Set nonparameter values */
        PetscCall(slds::util::subvecget(x, ctx.ctx.xt, i*(ctx.ctx.d - ctx.ctx.k), (i + 1)*(ctx.ctx.d - ctx.ctx.k), 0,
                                        ctx.ctx.zerotol));
        /* Compute derivative (if dfpd is NULL finite differences is used) */
        if(ctx.ctx.dfpd)
            PetscCall(ctx.ctx.dfpd[i % ctx.ctx.pdperiod](NULL, ctx.ctx.xt, ctx.ctx.Dfxt, ctx.ctx.Dfxt,
                                                         ctx.ctx.fpdctx[i % ctx.ctx.pdperiod]));
        else
        {
            /* Set up finite differences context */
            ctx.ctx.diffctx.f = ctx.ctx.fpd[i % ctx.ctx.pdperiod];
            ctx.ctx.diffctx.fctx = ctx.ctx.fpdctx[i % ctx.ctx.pdperiod];
            /* Evaluate derivative */
            PetscCall(slds::diff::df(NULL, ctx.ctx.xt, ctx.ctx.Dfxt, ctx.ctx.Dfxt, &ctx.ctx.diffctx));
        }
        /* Compute product */
        if(i == 0 && !ctx.P)
            PetscCall(slds::la::matmult(ctx.ctx.Dfxt, Dfx, &P, MATPRODUCT_AB, ctx.alg, ctx.fill, 0));
        else
            PetscCall(slds::la::matmult(ctx.ctx.Dfxt, Dfx, &P, MATPRODUCT_AB, ctx.alg, ctx.fill, 1));
        /* Set new value for Dfx */
        PetscCall(MatCopy(P, Dfx, DIFFERENT_NONZERO_PATTERN));
    }
    /* Unbind vectors from CPU */
    PetscCall(VecBindToCPU(x, PETSC_FALSE));
    /* Clean up */
    if(!ctx.P)
        PetscCall(MatDestroy(&P));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes the linearized period operator of a periodic solution.
 *                                                                                                                                */
PetscErrorCode slds::de::pf2(std::vector<Vec> *x, Mat Dfx, void *ctxx)
{
    /* Set up */
    PetscBool initpetsc, dfassembled;
    int mpirank;
    Mat P;
    std::vector<int> idxtk, idxxr(1, 0);
    std::vector<PetscReal> xtk, xr(1, 0);
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    slds::de::pfctx ctx = *static_cast<slds::de::pfctx*>(ctxx);
    /* Assign product matrix */
    if(ctx.P)
        P = *ctx.P;
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Initialize Dfx as id */
    PetscCall(slds::util::matdiagonaladd(Dfx, 1, ctx.ctx.d));
    /* Compute products */
    for(int i = 0; i < ctx.ctx.period; i++)
    {
        /* Zero derivative if required */
        PetscCall(MatAssembled(ctx.ctx.Dfxt, &dfassembled));
        if(dfassembled == PETSC_TRUE)
            PetscCall(MatZeroEntries(ctx.ctx.Dfxt));
        /* Compute derivative (if dfpd is NULL finite differences is used) */
        if(ctx.ctx.dfpd)
            PetscCall(ctx.ctx.dfpd[i % ctx.ctx.pdperiod](NULL, (*x)[i], ctx.ctx.Dfxt, ctx.ctx.Dfxt,
                                                         ctx.ctx.fpdctx[i % ctx.ctx.pdperiod]));
        else
        {
            /* Set up finite differences context */
            ctx.ctx.diffctx.f = ctx.ctx.fpd[i % ctx.ctx.pdperiod];
            ctx.ctx.diffctx.fctx = ctx.ctx.fpdctx[i % ctx.ctx.pdperiod];
            /* Evaluate derivative */
            PetscCall(slds::diff::df(NULL, (*x)[i], ctx.ctx.Dfxt, ctx.ctx.Dfxt, &ctx.ctx.diffctx));
        }
        /* Compute product */
        if(i == 0 && !ctx.P)
            PetscCall(slds::la::matmult(ctx.ctx.Dfxt, Dfx, &P, MATPRODUCT_AB, ctx.alg, ctx.fill, 0));
        else
            PetscCall(slds::la::matmult(ctx.ctx.Dfxt, Dfx, &P, MATPRODUCT_AB, ctx.alg, ctx.fill, 1));
        /* Set new value for Dfx */
        PetscCall(MatCopy(P, Dfx, DIFFERENT_NONZERO_PATTERN));
    }
    /* Clean up */
    if(!ctx.P)
        PetscCall(MatDestroy(&P));
    /* Return */
    return 0;
}
/*!
 *  @brief Computes a periodic solution to a periodic difference equation.
 *                                                                                                                                */
PetscErrorCode slds::de::cycsolve(Vec x0, std::vector<Vec> *xs, slds::de::cycslvctx ctx)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank, rcode;
    Vec x, xt, fx, fxt, xx0;
    Mat Dfx, Dfxt;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Initialize */
    PetscCall(ctx.vecset(&x, ctx.period*(ctx.d - ctx.k) + ctx.k, ctx.vecctx));
    PetscCall(ctx.vecset(&fx, ctx.period*(ctx.d - ctx.k) + ctx.k, ctx.vecctx));
    PetscCall(ctx.vecset(&xx0, ctx.period*(ctx.d - ctx.k) + ctx.k, ctx.vecctx));
    PetscCall(ctx.vecset(&xt, ctx.d, ctx.vecctx));
    PetscCall(ctx.vecset(&fxt, ctx.d, ctx.vecctx));
    PetscCall(ctx.matset(&Dfxt, ctx.d, ctx.d, ctx.matctx));
    slds::de::cycctx cctx(ctx.d, ctx.k, ctx.period, ctx.pdperiod, ctx.zerotol, xt, fxt, Dfxt, ctx.mcyctype, ctx.diffctx,
                          ctx.fpd, ctx.dfpd, ctx.fpdctx);
    /* Initialize cyclic matrix */
    if(ctx.matsetcyc)
        PetscCall(ctx.matsetcyc(&Dfx, ctx.period*(ctx.d - ctx.k) + ctx.k,
                                ctx.period*(ctx.d - ctx.k) + ctx.k, ctx.matcycctx));
    else
        PetscCall(slds::de::initdfcycaijdense(&Dfx, &cctx));
    /* Iterate */
    PetscCall(slds::de::it(ctx.fpd, x0, xs, ctx.period - 1, 0, ctx.pdperiod, ctx.d, ctx.k, ctx.fpdctx));
    /* Initialize initial guess */
    PetscCall(VecSet(x, 0));
    /* Set initial guess */
    for(int i = 0; i < ctx.period; i++)
        PetscCall(slds::util::subvecset(xx0, (*xs)[i], 0, ctx.d - ctx.k, i*(ctx.d - ctx.k), ctx.ctx.zerotol));
    PetscCall(slds::util::subvecset(xx0, x0, ctx.d - ctx.k, ctx.d, ctx.period*(ctx.d - ctx.k), ctx.ctx.zerotol));
    /* Project (if df is NULL finite differences is used) */
    rcode = slds::solve::fsolve(slds::de::fcyc, slds::de::dfcyc, xx0, x, fx, Dfx, ctx.period*(ctx.d - ctx.k), &cctx,
                                ctx.ctx);
    /* Return if there was an error (use PetscCall for clean exit) */
    if(rcode == 100)
        PetscCall(100);
    /* If the solver converged, proceed */
    if(rcode > -1)
    {
        /* Extract solution */
        for(int i = 0; i < ctx.period; i++)
        {
            /* Initialize xt */
            PetscCall(VecSet(xt, 0));
            /* Extract nonparameter values */
            PetscCall(slds::util::subvecget(x, xt, i*(ctx.d - ctx.k), (i + 1)*(ctx.d - ctx.k), 0, ctx.ctx.zerotol));
            /* Extract nonparameter values */
            PetscCall(slds::util::subvecget(x, xt, ctx.period*(ctx.d - ctx.k), ctx.period*(ctx.d - ctx.k) + ctx.k,
                                            ctx.d - ctx.k, ctx.ctx.zerotol));
            /* Copy result */
            PetscCall(VecCopy(xt, (*xs)[i]));
        }
    }
    /* Clean up */
    PetscCall(VecDestroy(&x));
    PetscCall(VecDestroy(&xt));
    PetscCall(VecDestroy(&fx));
    PetscCall(VecDestroy(&fxt));
    PetscCall(VecDestroy(&xx0));
    PetscCall(MatDestroy(&Dfx));
    PetscCall(MatDestroy(&Dfxt));
    /* Return */
    return rcode;
}
/*!
 *  @brief Writes a sequence of iterates to an HDF5 file.
 *                                                                                                                                */
PetscErrorCode slds::de::writeitsh5(std::vector<std::vector<Vec>> xt, int d, std::string f)
{
    /* Set up */
    PetscBool initpetsc;
    int mpirank;
    Vec xloc;
    VecScatter scatterctx;
    std::vector<double> h5xd(d, 0);
    std::vector<std::vector<std::vector<double>>> h5xv(xt.size(), std::vector<std::vector<double>>());
    const PetscReal *xxloc;
    /* Return if Petsc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set up HDF5 output */
    H5::H5File file;
    std::vector<slds::de::h5it> h5i;
    hid_t h5dblv = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    /* Initialize scatter */
    PetscCall(VecScatterCreateToZero(xt[0][0], &scatterctx, &xloc));
    /* Create HDF5 file on rank 0 */
    if(mpirank == 0)
        file = H5::H5File(f, H5F_ACC_TRUNC);
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Write iterates */
    for(int i = 0; i < xt.size(); i++)
    {
        /* Preallocate */
        h5xv[i] = std::vector<std::vector<double>>(xt[i].size(), std::vector<double>());
        /* Set data */
        for(int j = 0; j < xt[i].size(); j++)
        {
            /* Bind vectors to CPU */
            PetscCall(VecBindToCPU(xt[i][j], PETSC_TRUE));
            PetscCall(VecBindToCPU(xloc, PETSC_TRUE));
            /* Extract iterate */
            PetscCall(VecScatterBegin(scatterctx, xt[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            PetscCall(VecScatterEnd(scatterctx, xt[i][j], xloc, INSERT_VALUES, SCATTER_FORWARD));
            /* Set data only on rank 0 */
            if(mpirank == 0)
            {
                PetscCall(VecGetArrayRead(xloc, &xxloc));
                for(int k = 0; k < d; k++)
                    h5xd[k] = xxloc[k];
                PetscCall(VecRestoreArrayRead(xloc, &xxloc));
                h5i.push_back(slds::de::h5it(i, j, xt[i].size()));
                h5xv[i][j] = h5xd;
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Unbind vectors from CPU */
            PetscCall(VecBindToCPU(xt[i][j], PETSC_FALSE));
            PetscCall(VecBindToCPU(xloc, PETSC_FALSE));
        }
    }
    /* Write on rank 0 */
    if(mpirank == 0 && !h5i.empty())
    {
        /* Define type */
        H5::CompType itype(sizeof(h5it));
        itype.insertMember("Index", HOFFSET(slds::de::h5it, i), H5T_NATIVE_INT);
        itype.insertMember("Iterate", HOFFSET(slds::de::h5it, j), H5T_NATIVE_INT);
        itype.insertMember("Period", HOFFSET(slds::de::h5it, p), H5T_NATIVE_INT);
        itype.insertMember("x", HOFFSET(slds::de::h5it, h5x), h5dblv);
        /* Set vector data */
        for(int i = 0; i < h5i.size(); i++)
        {
            h5i[i].h5x.len = h5xv[h5i[i].i][h5i[i].j].size();
            h5i[i].h5x.p = &h5xv[h5i[i].i][h5i[i].j][0];
        }
        /* Create dataset */
        hsize_t idim[1];
        idim[0] = h5i.size();
        int irank = sizeof(idim)/sizeof(hsize_t);
        H5::DataSpace ispace(irank, idim);
        H5::DataSet idataset(file.createDataSet("Iterates", itype, ispace));
        /* Write data */
        idataset.write(&h5i[0], itype);
        /* Clear to free up memory */
        h5i.clear();
        h5xv.clear();
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Clean up */
    H5Tclose(h5dblv);
    PetscCall(VecDestroy(&xloc));
    PetscCall(VecScatterDestroy(&scatterctx));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
