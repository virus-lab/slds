# Continuation

- Revise and refactor if necessary 

# Analysis

- Add data structure reuse for finite differences
- Use time-h-maps for applications to ODEs (time-T-maps for T periodic maps)

# Attractors

- Optimize for open maps by only testing the boundary (invariance of domain)
- Implement a space mixing box division among processes

# Graphics

- Proceed work on greedy surface reconstruction (this might be dead)

# Documentation

- Write a Getting Started guide with examples
- Polished web documentation (Sphinx?)

# Other

- Set up gitlab project
- Write unit tests
- Write bindings with SWIG
- Implement different parallelism levels for continuation and solvers, for example single process solve and multifrontal continuation

