#**********************************************************************************************************************************#
 ## @file sldsmvrec.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 11/2022
 #  @copyright GNU Public License
 #
 #  Minimum weight surface reconstruction. This implements a version using the radius of the cirumsphere as a weight function.

#**********************************************************************************************************************************#
# Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
# Imports
#**********************************************************************************************************************************#
import numpy as np
import scipy as sp
#**********************************************************************************************************************************#
# Classes
#**********************************************************************************************************************************#
##
# @brief Class representing a simplex.
#
class simplex:
    def __init__(self, d: int, n: int,  v: np.ndarray) -> None:
        # Set space dimension #
        self.d = d;
        # Set simplex dimension #
        self.n = n;
        # Set vertices #
        self.v = v;
        # Set span #
        self.A = np.zeros((d, n));
        for i in np.arange(n):
            self.A[:, i] = v[:, i + 1] - v[:, 0];
        # Return #
        return;
#**********************************************************************************************************************************#
# Additional Functions
#**********************************************************************************************************************************#
##
# @brief Function to compute the radius of the circumsphere of a simplex.
#
def simplexrcs(s: simplex) -> tuple[int, float]:
    # Set nonlinear function #
    def f(x: np.ndarray) -> np.ndarray:
        # Initialize #
        fx = np.zeros(s.n + 1);
        # Extract coefficients and radius #
        xc = x[:-1];
        r = x[-1];
        # Compute center #
        c = np.matmul(s.A, xc);
        # Set values #
        for i in np.arange(s.n + 1):
            fx[i] = np.linalg.norm(c - s.v[:, i]) - r;
        # Return #
        return fx;
    # Set initial guess to zero #
    x0 = np.zeros(s.n + 1);
    # Solve f(x) = 0 #
    try:
        # Solve using Anderson mixing #
        x = sp.optimize.anderson(f, x0);
        # Return solution #
        return (0, x[-1]);
    # Return error if no solution was found #
    except:
        return (-1, 0);
##
# @brief Naive implementation to find the closest pair of points in a set of points.
#
def cpp(p: np.ndarray) -> tuple[float, int, int]:
    # Get number of points #
    pn = p.shape[1];
    # Find closest pair of points (naive) #
    mindp = np.inf;
    mini = -1;
    minj = -1;
    for i in np.arange(pn):
        for j in np.arange(i + 1, pn):
            # Compute distance #
            dp = np.linalg.norm(p[:, i] - p[:, j]);
            # Set minimum #
            if(dp < mindp):
                mindp = dp;
                mini = i;
                minj = j;
    # Return #
    return (mindp, mini, minj);
##
# @brief Function to compute the simplex with smallest circumsphere in a set of points.
#
def simplexminrcs(p: np.ndarray, d: int, n: int) -> simplex:
    # Get number of points #
    pn = p.shape[1];
    # Find closest pair of points (naive) #
    mindp, mini, minj = cpp(p);
    # Initialize #
    sn = 2;
    sp = [mini, minj];
    # Expand #
    while(sn != n + 1):
        # Initialize new simplex #
        sv = np.zeros((d, sn + 1));
        for i, v in enumerate(sp):
            sv[:, i] = p[:, v];
        s = simplex(d, sn + 1, sv);
        # Find additional point so that the radius of the circumsphere is minimal #
        minrcs = np.inf;
        mini = -1;
        for i in np.arange(pn):
            # Only check new vertices #
            if(i in sp):
                continue;
            else:
                # Update simplex #
                s.v[:, sn] = p[:, i];
                s.A[:, sn - 1] = p[:, i] - s.v[:, 0];
                # Compute circumsphere #
                rcode, rcs = simplexrcs(s);
                # Set minimum #
                if(rcode > -1):
                    if(rcs < minrcs):
                        minrcs = rcs;
                        mini = i;
        # Update #
        sn += 1;
        sp.append(mini);
    # Create final simplex #
    sv = np.zeros((d, sn));
    for i, v in enumerate(sp):
        sv[:, i] = p[:, v];
    # Return #
    return simplex(d, sn, sv);
#**********************************************************************************************************************************#
# MinRCS
#**********************************************************************************************************************************#
#**********************************************************************************************************************************#
# Main
#**********************************************************************************************************************************#
if __name__ == "__main__":
    print("I am a module, exiting ...");
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
