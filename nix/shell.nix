#**********************************************************************************************************************************#
 ## @file shell.nix
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2024
 #  @copyright GNU Public License
 #
 #  Nix environment for slds++ development.
#**********************************************************************************************************************************#
# Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2023  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
# Main Configuration
#**********************************************************************************************************************************#
##
# @brief Main.
#
{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell
{
  ##
  # @brief Packages.
  #
  nativeBuildInputs = with pkgs; [
    blas
    cmake
    doxygen
    git
    gnumake
    flex
    hdf5
    hdf5-cpp
    lapack
    mpich
    pkg-config
    python3
  ]; # Specifies provided packages #
}
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
