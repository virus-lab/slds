/**********************************************************************************************************************************/
/*! @file idecont.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of a 1D periodic integrodifference equation with slds++.
 *
 *  Example of continuation of a 2D periodic integrodifference equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <numeric>
#include <string>
#include <vector>
#include <petsc.h>
#include <slepc.h>
#include "sldscont.hpp"
#include "sldsde.hpp"
#include "sldsie.hpp"
#include "sldsintr.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the IDE.
 *                                                                                                                                */
class idefctx
{
    public:
        int n;
        PetscReal alpha;
        const PetscReal *a;
        idefctx() {};
        idefctx(int nn, PetscReal a, const PetscReal *aa): n(nn), alpha(a), a(aa) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal bh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*lambda*x/(1 + PetscPowReal(x, n));
}
/*!
 *  @brief Derivative of Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal dxbh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*lambda*(1 - (n - 1)*PetscPowReal(x, n))/PetscPowReal((1 + PetscPowReal(x, n)), 2);
}
/*!
 *  @brief Derivative of Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal dlbh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*x/(1 + PetscPowReal(x, n));
}
/*!
 *  @brief Multivariate independent Laplace dispersal kernel.
 *                                                                                                                                */
inline PetscReal mvlp(const PetscReal *x, const PetscReal *y, const PetscReal *a, int d)
{
    /* Set up */
    PetscReal p = 1;
    /* Compute product */
    for(int i = 0; i < d; i++)
        p *= (a[i]/2)*std::exp(-a[i]*PetscAbsReal(x[i] - y[i]));
    /* Return */
    return p;
}
/*!
 *  @brief Beverton-Holt growth with Laplace dispersion.
 *                                                                                                                                */
PetscErrorCode f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result directly since d = 1 and dim(lambda) = 1 */
    fs[0] = mvlp(s, y, fctx.a, k)*bh(uy[0], fctx.alpha, (*ctx.lambda)[0], fctx.n);
    /* Return */
    return 0;
}
/*!
 *  @brief Third partial derivative of Beverton-Holt growth with Laplace dispersion.
 *                                                                                                                                */
PetscErrorCode d3f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                   slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvlp(s, y, fctx.a, k)*dxbh(uy[0], fctx.alpha, (*ctx.lambda)[0], fctx.n);
    /* Return */
    return 0;
}
/*!
 *  @brief Beverton-Holt growth with Laplace dispersion.
 *                                                                                                                                */
PetscErrorCode dlf(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                   slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvlp(s, y, fctx.a, k)*dlbh(uy[0], fctx.alpha, (*ctx.lambda)[0], fctx.n);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int k = 1;
    const int pf = 3;
    const int qnk = 256;
    const PetscReal amp = 0;
    const PetscReal bmp = 5;
    const PetscReal l0 = 0;
    const PetscReal l1 = 1e-6;
    const PetscReal L = 2;
    const PetscReal alf[pf] = {1, 2, 0.5};
    const int nbh[pf] = {4, 4, 4};
    const PetscReal af[k*pf] = {1, 1, 1};
    /* Set up */
    int mpirank, px, cf, sweep, rcode;
    Vec u0, u0loc;
    VecScatter u0scatterctx;
    std::vector<PetscReal> mpa, mpb;
    std::vector<std::vector<Vec>> ut;
    slds::cont::mpcctx mpctx;
    slds::de::feigenctx fgctx;
    slds::intr::intrule Q;
    std::vector<idefctx> fctx(pf, idefctx());
    std::vector<PetscErrorCode (*)(SNES, Vec, Vec, void*)> fu(pf, slds::ie::furysohn);
    std::vector<PetscErrorCode (*)(SNES, Vec, Mat, Mat, void*)> dfu(pf, slds::ie::dfurysohn);
    std::vector<PetscErrorCode (*)(const PetscReal*, const PetscReal*, const PetscReal*, PetscReal*, int, int,
                                   slds::ie::ukernelctx)> dlfu(1, dlf);
    std::vector<slds::ie::urysohnctx> uctx(pf, slds::ie::urysohnctx());
    std::vector<void*> fctxx(pf, NULL), uctxx(pf, NULL);
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Create integration rule */
    std::vector<PetscReal> a(k, -L/2), b(k, L/2);
    std::vector<int> nk(k, qnk);
    PetscCall(slds::intr::compositetrapezoid(&Q, &a[0], &b[0], &nk[0], k, 1));
    /* Set contexts */
    fgctx.verbose = 1;
    fgctx.reset = 1;
    fgctx.initit = 5000;
    fgctx.it = 5000;
    fgctx.t = 192;
    fgctx.eps = 1e-6;
    fgctx.nstep = PetscCeilReal((l1 - l0)*2000);
    for(int i = 0; i < pf; i++)
    {
        fctx[i] = idefctx(nbh[i], alf[i], &af[k*i]);
        uctx[i].parameters = 1;
        uctx[i].f = f;
        uctx[i].d3f = d3f;
        uctx[i].dlf = &dlfu[0];
        uctx[i].Q = &Q;
        uctx[i].fctx.k = 1;
        uctx[i].fctx.fctx = &(fctx[i]);
        fctxx[i] = &(fctx[i]);
        uctxx[i] = &(uctx[i]);
    }
    mpctx.type = 1;
    mpctx.verbose = 1;
    // mpctx.usecyclic = 0;
    // mpctx.computeev = 1;
    // mpctx.nev = 6;
    mpctx.rmin = 5e-5;
    mpctx.rmax = 1;
    mpctx.rdec = 0.5;
    mpctx.rinc = 2;
    mpctx.eps = 1e-4;
    mpctx.ntype = NORM_INFINITY;
    // mpctx.nulltol = 1;
    mpctx.slvctx.fctx.snesatol = 1e-12;
    mpctx.slvctx.fctx.snesrtol = 1e-12;
    mpctx.slvctx.fctx.snesstol = 1e-12;
    mpctx.slvctx.fctx.kspatol = 1e-12;
    mpctx.slvctx.fctx.ksprtol = 1e-12;
    mpctx.slvctx.fctx.snesmaxit = 1e4;
    mpctx.slvctx.fctx.kspmaxit = 1e4;
    // mpctx.slvctx.ectx.epstype = EPSLAPACK;
    mpctx.slvctx.ectx.tol = 1e-12;
    // mpctx.idxctx.trackfold = 1;
    // mpctx.idxctx.trackflip = 1;
    mpctx.idxctx.trackmorse = 1;
    // mpctx.idxctx.computeall = 1;
    // mpctx.idxctx.evmax = 0.5;
    mpctx.idxctx.tol = 1e-16;
    mpctx.idxctx.step0 = 1;
    mpctx.idxctx.step = 1;
    mpctx.swctx.sw = 1;
    mpctx.swctx.evtol = 1e-6;
    mpctx.swctx.ds = 5e-5;
    mpctx.swctx.rs = 1e-6;
    /* Initialize */
    PetscCall(slds::util::vecsetdefault(&u0, Q.n + 1, NULL));
    PetscCall(VecSet(u0, 1));
    /* Set scatters */
    PetscCall(VecScatterCreateToAll(u0, &u0scatterctx, &u0loc));
    for(int i = 0; i < pf; i++)
    {
        uctx[i].uloc = &u0loc;
        uctx[i].scatterctx = &u0scatterctx;
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing Feigenbaum. \n"));
    /* Compute Feigenbaum diagram */
    PetscCall(slds::de::feigen(&fu[0], u0, &ut, l1, l0, pf, Q.n + 1, &uctxx[0], fgctx));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing continuation. \n"));
    /* Set bounds */
    mpa = std::vector<PetscReal>(Q.n + 1, -INFINITY);
    mpb = std::vector<PetscReal>(Q.n + 1, INFINITY);
    mpa[Q.n] = amp;
    mpb[Q.n] = bmp;
    /* Sweep for starting points and compute continuation */
    cf = 0;
    sweep = 1;
    for(int i = ut.size() - 1; i > -1; i--)
    {
        /* Reset sweep if maximum period is reached */
        if(ut[i].size() > fgctx.t - 1)
            sweep = 1;
        /* Set new starting point and compute continuation */
        if(sweep && ut[i].size() < fgctx.t)
        {
            /* Set filename for output */
            mpctx.fname = "mpcout" + std::to_string(cf) + ".h5";
            /* Set new period */
            px = ut[i].size();
            /* Set up */
            slds::cont::mpcoutctx outctx;
            /* Compute continuation */
            rcode = slds::cont::mpc(NULL, NULL, &fu[0], &dfu[0], ut[i][0], &mpa[0], &mpb[0], px, pf, Q.n + 1, 1, &outctx, NULL, &uctxx[0], mpctx);
            /* If continuation succeeded, disable sweep for stable intervals and increase file counter */
            if(rcode > -1)
            {
                sweep = 0;
                cf++;
            }
            /* Clean up */
            PetscCall(outctx.free());
        }
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing output. \n"));
    /* Write output */
    PetscCall(slds::intr::writeh5(&Q, "intrule.h5"));
    PetscCall(slds::de::writeitsh5(ut, Q.n + 1, "its.h5"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up. \n"));
    /* Clean up */
    PetscCall(VecDestroy(&u0));
    PetscCall(VecDestroy(&u0loc));
    PetscCall(VecScatterDestroy(&u0scatterctx));
    for(int i = 0; i < ut.size(); i++)
        for(int j = 0; j < ut[i].size(); j++)
            PetscCall(VecDestroy(&(ut[i][j])));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
