 #**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings;
import ColorSchemes as css;
import GLMakie as glmk;
import JLD2 as jld;
import LinearAlgebra as la;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Set up #
d = [1, 2];
period = 3;
# Set theme #
theme_latexcycle = glmk.merge(glmk.theme_latexfonts(), glmk.Theme(palette = (color = css.colorschemes[:tab10],),
                                                                  Lines = (linewidth = 4,
                                                                           cycle = glmk.Cycle([:color, :linestyle],
                                                                                              covary = true),),
                                                                  Scatter = (markersize = 21,
                                                                             cycle = glmk.Cycle([:color, :marker],
                                                                                                covary = true),),
                                                                  ScatterLines = (linewidth = 4,
                                                                                  markersize = 21,
                                                                                  cycle = glmk.Cycle([:color, :marker],
                                                                                                     covary = true),)));
glmk.set_theme!(theme_latexcycle);
# Create global line plot #
figall = glmk.Figure(resolution = (3000, 1414), fontsize = 48);
axfigall = [glmk.Axis(figall[1, 1],
                      xlabel = L"$\alpha$",
                      ylabel = L"$\int_{\Omega} \phi(x, \alpha) dx$"),
            glmk.Axis(figall[1, 2],
                      xlabel = L"$\alpha$")];
# Plot for each dimension #
for k in 1:2
    # Read integration rule #
    try
        global fint = jld.jldopen("bf"*string(d[k])*"d_data/intrule.h5", "r");
    catch
        print("Failed to read integration rule!");
        return;
    end
    # Open datasets #
    try
        global fintnodes = fint["Nodes"];
        global fintweights = fint["Weights"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get sizes #
    nnodes = length(fintnodes);
    nweights = length(fintweights);
    # Store data in arrays #
    nodes = zeros(Float64, nnodes, d[k]);
    weights = zeros(Float64, nweights);
    for i in 1:nnodes
        for j in 1:d[k]
            nodes[i, j] = fintnodes[i][2][j];
        end
    end
    for i in 1:nweights
        weights[i] = fintweights[i][2][1];
    end
    # Set file counter #
    nfile = 0;
    # Create global data arrays #
    xfigall = zeros(Float64, 0);
    yfigall = zeros(Float64, 0);
    stabfigall = Vector{Tuple{Symbol, Float64}}(undef, 0);;
    while isfile("bf"*string(d[k])*"d_data/mpcout"*string(nfile)*".h5")
        # Print update #
        print("Reading file "*"mpcout"*string(nfile)*".h5... \n");
        # Read continuation output #
        try
            global fmpc = jld.jldopen("bf"*string(d[k])*"d_data/mpcout"*string(nfile)*".h5", "r");
        catch
            print("Failed to read continuation data! Skipping file...");
            nfile += 1;
            continue;
        end
        # Open datasets #
        try
            global fballs = fmpc["Balls"];
            global fpoints = fmpc["Points"];
            global fballpoints = fmpc["Ballpoints"];
        catch
            print("Failed to read datasets! Skipping file...");
            nfile += 1;
            continue;
        end
        # Get sizes #
        nballs = length(fballs);
        npoints = length(fpoints);
        nballpoints = length(fballpoints);
        # Print update #
        print("Precomputing integrals... \n");
        # Precompute integrals #
        intballs = zeros(Float64, nballs);
        intpoints = zeros(Float64, npoints);
        for i in 1:nballs
            intballs[i] = la.dot(weights, fballs[i][end][1:(end - 1)]);
        end
        for i in 1:npoints
            intpoints[i] = la.dot(weights, fpoints[i][end][1:(end - 1)]);
        end
        # Print update #
        print("Indexing balls and points... \n");
        # Index balls and points to avoid multiple searches #
        idxballs = zeros(Int64, fballs[end][1] + 1);
        idxpoints = zeros(Int64, fpoints[end][1] + 1);
        idx = 0;
        for i in 1:nballs
            if(fballs[i][1] == idx)
                idxballs[idx + 1] = i;
                idx += 1;
            end
        end
        idx = 0;
        for i in 1:npoints
            if(fpoints[i][1] == idx)
                idxpoints[idx + 1] = i;
                idx += 1;
            end
        end
        # Print update #
        print("Plotting 2D lines... \n");
        # Create local line plot #
        figfile = glmk.Figure(resolution = (3000, 1414), fontsize = 48);
        axfigfile = glmk.Axis(figfile[1, 1],
                            xlabel = L"$\alpha$",
                            ylabel = L"$\int_{\Omega} \phi(x, \alpha) dx$");
        # Create local line plot per period #
        figfileperiod = glmk.Figure(resolution = (3000, 1414), fontsize = 48);
        axfigfileperiod = Array{typeof(axfigfile)}(undef, period);
        for i in 1:period
            ylabel =  L"\int_{\Omega} \phi_{3t}(x, \alpha) dx";
            if(i > 1)
                p = i - 1;
                ylabel = L"$\int_{\Omega} \phi_{ 3t + %$p }(x, \alpha) dx$";
            end
            axfigfileperiod[i] = glmk.Axis(figfileperiod[1, i],
                                        xlabel = L"$\alpha$",
                                        ylabel = ylabel);
        end
        # Create local data arrays #
        xfigfile = zeros(Float64, 0);
        yfigfile = zeros(Float64, 0);
        stabfigfile = Vector{Tuple{Symbol, Float64}}(undef, 0);
        xfigfileperiod = Array{typeof(xfigfile)}(undef, period);
        yfigfileperiod = Array{typeof(yfigfile)}(undef, period);
        stabfigfileperiod = Array{typeof(stabfigfile)}(undef, period);
        for i in 1:period
            xfigfileperiod[i] = zeros(Float64, 0);
            yfigfileperiod[i] = zeros(Float64, 0);
            stabfigfileperiod[i] = Vector{Tuple{Symbol, Float64}}(undef, 0);;
        end
        # Plot 2D lines #
        for i in 1:nballpoints
            # Get ball and point #
            idxball = fballpoints[i][1] + 1;
            idxpoint = fballpoints[i][2] + 1;
            # Skip if the line connects the center with itself #
            if(idxpoint < 1)
                continue;
            end
            # Get period and stability #
            periodball = fballs[idxballs[idxball]][3];
            periodpoint = fpoints[idxpoints[idxpoint]][3];
            stabilityball = fballs[idxballs[idxball]][end - 3][end];
            # Plot lines #
            for j in 1:periodball
                # Set color by stability #
                color = (:lime, 1);
                if(stabilityball > 0)
                    color = (:red, 0.5);
                end
                # Append data #
                if(intballs[idxballs[idxball] + (j - 1)] > -1e-9 && intpoints[idxpoints[idxpoint] + (j - 1)] > -1e-9 &&
                    fballs[idxballs[idxball] + (j - 1)][end][end] > 0 &&
                    fballs[idxballs[idxball] + (j - 1)][end][end] < 5 &&
                    fpoints[idxpoints[idxpoint] + (j - 1)][end][end] > 0 &&
                    fpoints[idxpoints[idxpoint] + (j - 1)][end][end] < 5)
                    # Fix broken traces (this is a complete hack) #
                    if((nfile == 0 || nfile == 13 || nfile == 15) && periodball > 12 &&
                        sqrt((fballs[idxballs[idxball] + (j - 1)][end][end] -
                        fpoints[idxpoints[idxpoint] + (j - 1)][end][end])^2 +
                        (intballs[idxballs[idxball] + (j - 1)] -
                        intpoints[idxpoints[idxpoint] + (j - 1)])^2) > 0.2)
                        continue;
                    end
                    # Append global data #
                    append!(xfigall,
                            [fballs[idxballs[idxball] + (j - 1)][end][end],
                            fpoints[idxpoints[idxpoint] + (j - 1)][end][end], NaN]);
                    append!(yfigall, [intballs[idxballs[idxball] + (j - 1)],
                                    intpoints[idxpoints[idxpoint] + (j - 1)], NaN]);
                    append!(stabfigall, [color, color, color]);
                    # Append local data #
                    append!(xfigfile,
                            [fballs[idxballs[idxball] + (j - 1)][end][end],
                            fpoints[idxpoints[idxpoint] + (j - 1)][end][end], NaN]);
                    append!(yfigfile, [intballs[idxballs[idxball] + (j - 1)],
                                    intpoints[idxpoints[idxpoint] + (j - 1)], NaN]);
                    append!(stabfigfile, [color, color, color]);
                    append!(xfigfileperiod[(j - 1)%period + 1],
                            [fballs[idxballs[idxball] + (j - 1)][end][end],
                            fpoints[idxpoints[idxpoint] + (j - 1)][end][end], NaN]);
                    append!(yfigfileperiod[(j - 1)%period + 1],
                            [intballs[idxballs[idxball] + (j - 1)], intpoints[idxpoints[idxpoint] + (j - 1)], NaN]);
                    append!(stabfigfileperiod[(j - 1)%period + 1], [color, color, color]);
                end
            end
        end
        # Print update #
        print("Exporting figures... \n");
        # Plot local #
        glmk.lines!(axfigfile, xfigfile, yfigfile, color = stabfigfile, transparency = true);
        for i in 1:period
            glmk.lines!(axfigfileperiod[i], xfigfileperiod[i], yfigfileperiod[i], color = stabfigfileperiod[i],
                        transparency = true);
        end
        # Save figures #
        glmk.save("bf"*string(d[k])*"d_plots/trace"*string(nfile)*".png", figfile);
        glmk.save("bf"*string(d[k])*"d_plots/periodtrace"*string(nfile)*".png", figfileperiod);
        # Increase file counter #
        nfile += 1;
    end
    # Print update #
    print("Exporting figures... \n");
    # Plot global #
    glmk.lines!(axfigall[k], xfigall, yfigall, color = stabfigall, transparency = true);
end
# Save figures #
glmk.save("bf_plots/all.png", figall);
# Print update #
print("Done! \n");
#**********************************************************************************************************************************#
## End
#**********************************************************************************************************************************#
