#**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings
import GLMakie as glmk;
import ScatteredInterpolation as itp;
import JLD2 as jld;
import LinearAlgebra as la;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Set up #
d = 1;
branch = [1, 2];
period = 3;
branchperiod = [3, 6];
nitp = 100;
# Set latex theme globally #
glmk.set_theme!(glmk.theme_latexfonts())
# Create global line plots per period #
figall = glmk.Figure(resolution = (3000, 4242), fontsize = 48, figure_padding = (150, 0, 0, 0));
figallperiod = figall[1, 1] = glmk.GridLayout();
axfigallperiod = [];
for i in 1:period
    zlabel =  L"$\phi_{3t}(x, \alpha)$";
    if(i > 1)
        p = i - 1;
        zlabel = L"$\phi_{ 3t + %$p }(x, \alpha)$";
    end
    push!(axfigallperiod, glmk.Axis3(figallperiod[i, 1],
                                     xlabel = L"$\alpha$",
                                     ylabel = L"$x$",
                                     zlabel = zlabel,
                                     aspect = :equal,
                                     azimuth = 0.4*pi,
                                     elevation = 0.05*pi,
                                     xlabeloffset = 50,
                                     ylabeloffset = 75,
                                     zlabeloffset = 100));
    push!(axfigallperiod, glmk.Axis3(figallperiod[i, 2],
                                     xlabel = L"$\alpha$",
                                     ylabel = L"$x$",
                                     zlabel = "",
                                     aspect = :equal,
                                     azimuth = 0.4*pi,
                                     elevation = 0.05*pi,
                                     xlabeloffset = 50,
                                     ylabeloffset = 75,
                                     zlabeloffset = 100));
end
glmk.colgap!(figallperiod, 100);
glmk.rowgap!(figallperiod, 0);
for k in 1:2
    # Read integration rule #
    try
        global fint = jld.jldopen("branch"*string(branch[k])*string(d)*"d_data/intrule.h5", "r");
    catch
        print("Failed to read integration rule!");
        return;
    end
    # Open datasets #
    try
        global fintnodes = fint["Nodes"];
        global fintweights = fint["Weights"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get sizes #
    nnodes = length(fintnodes);
    nweights = length(fintweights);
    # Store data in arrays #
    nodes = zeros(Float64, nnodes);
    weights = zeros(Float64, nweights);
    for i in 1:nnodes
        nodes[i] = fintnodes[i][2][1];
    end
    for i in 1:nweights
        weights[i] = fintweights[i][2][1];
    end
    # Create global data arrays #
    xpts = Array{typeof(zeros(Float64, 0))}(undef, branchperiod[k]);
    ypts = Array{typeof(zeros(Float64, 0))}(undef, branchperiod[k]);
    zpts = Array{typeof(zeros(Float64, 0))}(undef, branchperiod[k]);
    for i in 1:branchperiod[k]
        xpts[i] = zeros(Float64, 0);
        ypts[i] = zeros(Float64, 0);
        zpts[i] = zeros(Float64, 0);
    end
    # Read continuation output #
    try
        global fmpc = jld.jldopen("branch"*string(branch[k])*string(d)*"d_data/mpcout.h5", "r");
    catch
        print("Failed to read continuation data!");
        return;
    end
    # Open datasets #
    try
        global fpoints = fmpc["Points"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get sizes #
    npoints = div(length(fpoints), branchperiod[k]);
    # Print update #
    print("Gathering points... \n");
    # Get points for interpolation #
    for i in 1:npoints
        # Sort by integral values per period #
        intpts = zeros(Float64, branchperiod[k]);
        for j in 1:branchperiod[k]
            intpts[j] = la.dot(weights, fpoints[(i - 1)*branchperiod[k] + j][end][1:(end - 1)]);
        end
        sortintpts = zeros(Int64, div(branchperiod[k], period), period);
        for j in 1:period
            sortintpts[:, j] = sortperm(intpts[(begin + (j - 1)):period:end]);
        end
        for j in 1:branchperiod[k]
            sortidx = sortintpts[div(j - 1, period) + 1, (j - 1)%period + 1];
            if(intpts[sortidx] > 0);
                append!(xpts[j],
                        fpoints[(i - 1)*branchperiod[k] + (sortidx - 1)*period + (j - 1)%period + 1][end][end]*ones(Float64,
                                                                                                                nnodes));
                append!(ypts[j], nodes);
                append!(zpts[j], fpoints[(i - 1)*branchperiod[k] + (sortidx - 1)*period + (j - 1)%period + 1][end][1:(end - 1)]);
            end
        end
    end
    # Interpolate #
    xfigallperiod = Array{typeof(zeros(Float64, 0))}(undef, branchperiod[k]);
    yfigallperiod = Array{typeof(zeros(Float64, 0))}(undef, branchperiod[k]);
    zfigallperiod = Array{typeof(zeros(Float64, 0, 0))}(undef, branchperiod[k]);
    for i in 1:branchperiod[k]
        xypts = zeros(Float64, 2, length(xpts[i]));
        xypts[1, :] = xpts[i];
        xypts[2, :] = ypts[i];
        fitp = itp.interpolate(itp.NearestNeighbor(), xypts, zpts[i]);
        xfigallperiod[i] = LinRange(minimum(xpts[i]), maximum(xpts[i]), nitp);
        yfigallperiod[i] = LinRange(minimum(ypts[i]), maximum(ypts[i]), nitp);
        gridx = repeat(xfigallperiod[i], nitp)[:];
        gridy = repeat(yfigallperiod[i]', nitp)[:];
        gridxy = [gridx gridy]';
        zfigallperiod[i] = reshape(itp.evaluate(fitp, gridxy), nitp, nitp);
    end
    # Plot global #
    for i in 1:branchperiod[k]
        if(branch[k] == 1)
            # Plot trivial branch #
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], collect(LinRange(0, 1.7401680454643411, nitp)),
                            collect(LinRange(minimum(ypts[i]), maximum(ypts[i]), nitp)),
                            zeros(Float64, nitp, nitp), color = :lime);
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], xfigallperiod[i], yfigallperiod[i], 0*zfigallperiod[i], color = :red);
            # Find stability change index #
            idxswitch = findfirst(xfigallperiod[i] .> 2.3408579542465207);
            # Plot bifurcating branch with stability #
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], (xfigallperiod[i])[1:idxswitch], yfigallperiod[i],
                            (zfigallperiod[i])[1:idxswitch, :], color = :lime);
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], (xfigallperiod[i])[idxswitch:end], yfigallperiod[i],
                            (zfigallperiod[i])[idxswitch:end, :], color = :red);
        else
            # Find stability change index #
            idxswitch = findfirst(xfigallperiod[i] .> 3.4674377677118575);
            # Plot bifurcating branch with stability #
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], (xfigallperiod[i])[1:idxswitch], yfigallperiod[i],
                            (zfigallperiod[i])[1:idxswitch, :], color = :lime);
            glmk.wireframe!(axfigallperiod[2*((i - 1)%period) + k], (xfigallperiod[i])[idxswitch:end], yfigallperiod[i],
                            (zfigallperiod[i])[idxswitch:end, :], color = :red);
        end
    end
end
# Save figures #
glmk.save("branch_plots/periodall.png", figall);
# Print update #
print("Done! \n");
#**********************************************************************************************************************************#
## End
#**********************************************************************************************************************************#
