#**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings;
import ColorSchemes as css;
import GLMakie as glmk;
import JLD2 as jld;
import LinearAlgebra as la;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Set up #
d = [1, 2];
period = 3;
# Set theme #
theme_latexcycle = glmk.merge(glmk.theme_latexfonts(), glmk.Theme(palette = (color = css.colorschemes[:tab10],),
                                                                  Lines = (linewidth = 2,
                                                                           cycle = glmk.Cycle([:color, :linestyle],
                                                                                              covary = true),),
                                                                  Scatter = (markersize = 4,
                                                                             cycle = glmk.Cycle([:color, :marker],
                                                                                                covary = true),),
                                                                  ScatterLines = (linewidth = 2,
                                                                                  markersize = 4,
                                                                                  cycle = glmk.Cycle([:color, :marker],
                                                                                                     covary = true),)));
glmk.set_theme!(theme_latexcycle);
# Create global line plot #
figall = glmk.Figure(resolution = (3000, 1414), fontsize = 48);
axfigall = [glmk.Axis(figall[1, 1],
                      xlabel = L"$\alpha$",
                      ylabel = L"$\int_{\Omega} \phi(x, \alpha) dx$"),
            glmk.Axis(figall[1, 2],
                      xlabel = L"$\alpha$")];
# Plot Feigenbaum diagrams #
for k in 1:2
    # Create data arrays #
    xfigallperiod = Array{typeof(zeros(Float64, 0))}(undef, period);
    yfigallperiod = Array{typeof(zeros(Float64, 0))}(undef, period);
    for i in 1:period
        xfigallperiod[i] = zeros(Float64, 0);
        yfigallperiod[i] = zeros(Float64, 0);
    end
    # Read integration rule #
    try
        global fint = jld.jldopen("fg"*string(d[k])*"d_data/intrule.h5", "r");
    catch
        print("Failed to read integration rule!");
        return;
    end
    # Open datasets #
    try
        global fintnodes = fint["Nodes"];
        global fintweights = fint["Weights"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get sizes #
    nnodes = length(fintnodes);
    nweights = length(fintweights);
    # Store data in arrays #
    nodes = zeros(Float64, nnodes, d[k]);
    weights = zeros(Float64, nweights);
    for i in 1:nnodes
        for j in 1:d[k]
            nodes[i, j] = fintnodes[i][2][j];
        end
    end
    for i in 1:nweights
        weights[i] = fintweights[i][2][1];
    end
    # Read continuation output #
    try
        global fits = jld.jldopen("fg"*string(d[k])*"d_data/its.h5", "r");
    catch
        print("Failed to read data!");
        return;
    end
    # Open datasets #
    try
        global fpoints = fits["Iterates"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get sizes #
    npoints = length(fpoints);
    # Print update #
    print("Plotting points... \n");
    # Precompute integrals #
    for i in 1:npoints
        pperiod = fpoints[i][2];
        push!(xfigallperiod[pperiod%period + 1], fpoints[i][end][end]);
        push!(yfigallperiod[pperiod%period + 1], la.dot(weights, fpoints[i][end][1:(end - 1)]));
    end
    # Print update #
    print("Exporting figures... \n");
    # Plot global #
    for i in 1:period
        glmk.scatter!(axfigall[k], xfigallperiod[i], yfigallperiod[i]);
    end
end
# Save figures #
glmk.save("fg_plots/all.png", figall);
# Print update #
print("Done! \n");
