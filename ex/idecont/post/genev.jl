#**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings;
import ColorSchemes as css;
import GLMakie as glmk;
import JLD2 as jld;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Set up #
d = [1, 2];
branch = 0;
period = 3;
nev = 6;
# Set theme #
theme_latexcycle = glmk.merge(glmk.theme_latexfonts(), glmk.Theme(palette = (color = css.colorschemes[:tab10],),
                                                                  Lines = (linewidth = 4,
                                                                           cycle = glmk.Cycle([:color, :linestyle],
                                                                                              covary = true),),
                                                                  Scatter = (markersize = 21,
                                                                             cycle = glmk.Cycle([:color, :marker],
                                                                                                covary = true),),
                                                                  ScatterLines = (linewidth = 4,
                                                                                  markersize = 21,
                                                                                  cycle = glmk.Cycle([:color, :marker],
                                                                                                     covary = true),)));
glmk.set_theme!(theme_latexcycle);
# Create global line plot #
fig = glmk.Figure(resolution = (3000, 1414), fontsize = 48);
axfig = [glmk.Axis(fig[1, 1],
                   xlabel = L"$\alpha$",
                   ylabel = L"$\text{Re}(\lambda^{n}_{i}(\alpha))$"),
         glmk.Axis(fig[1, 2],
                   xlabel = L"$\alpha$")];
# Plot eigenvalues #
for i in 1:2
    # Read continuation output #
    try
        global fmpc = jld.jldopen("ev"*string(branch)*string(d[i])*"d_data/mpcout.h5", "r");
    catch
        print("Failed to read data!");
        return;
    end
    # Open datasets #
    try
        global fballs = fmpc["Balls"];
    catch
        print("Failed to read datasets!");
        return;
    end
    # Get number of balls #
    nballs = div(length(fballs), period);
    # Prepare data #
    x = zeros(Float64, nballs);
    y = zeros(Float64, nballs, nev);
    # Print eigenvalue traces #
    for j in 1:nballs
        # Set alpha #
        x[j] = fballs[(j - 1)*period + 1][end][end];
        # Compute eigenvalues #
        reevs = zeros(Float64, nev);
        for k in 1:nev
            c = fballs[(j - 1)*period + 1][end - 2][period*k] + fballs[(j - 1)*period + 1][end - 1][period*k]*im;
            c ^= period;
            reevs[k] = real(c);
        end
        # Sort eigenvalues #
        sort!(reevs, by = abs, rev = true);
        # Save eigenvalues #
        for k in 1:nev
            y[j, k] = reevs[k];
        end
    end
    # Plot eigenvalue traces #
    if(branch == 0)
        glmk.ablines!(axfig[i], 1, 0, color = :black, linewidth = 4);
        for j in 1:nev
            glmk.lines!(axfig[i], x[y[:, j] .< 2], (y[:, j])[y[:, j] .< 2], label = L"$\lambda^{n}_{%$j}$");
        end
    end
    if(branch == 1)
        glmk.ablines!(axfig[i], -1, 0, color = :black, linewidth = 4);
        for j in 1:nev
            xj = x[(y[:, j] .< 1) .&& ((y[:, j] .> -2))];
            yj = (y[:, j])[(y[:, j] .< 1) .&& ((y[:, j] .> -2))];
            idxsort = sortperm(xj);
            glmk.lines!(axfig[i], xj[idxsort], yj[idxsort], label = L"$\lambda^{n}_{%$j}$");
        end
    end
    # Add legend #
    glmk.axislegend(axfig[i]);
end
# Export figure #
glmk.save("ev"*string(branch)*"_plots/evs.png", fig);
