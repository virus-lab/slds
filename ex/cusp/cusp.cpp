/**********************************************************************************************************************************/
/*! @file cusp.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of a cusp bifurcation with slds++.
 *
 *  Example of a cusp bifurcation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <sldsutil.hpp>
#include <vector>
#include <petsc.h>
#include <slepc.h>
#include "sldscont.hpp"
#include "sldsdiff.hpp"
#include "sldsslv.hpp"
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Complexified cusp (x + iy)*((x + iy)^2 + lambda) - mu.
 *                                                                                                                                */
PetscErrorCode f(SNES snes, Vec x, Vec fx, void *fctx)
{
    /* x[0] := x, x[1] := y, x[2] := lambda, x[3] := mu */
    /* Set up */
    int mpirank;
    VecScatter scatterctx;
    Vec xloc;
    int idx[4] = {0,1,2,3};
    PetscReal fxloc[4];
    const PetscReal *xxloc;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Scatter x to zero */
    PetscCall(VecScatterCreateToZero(x, &scatterctx, &xloc));
    PetscCall(VecScatterBegin(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    /* Set values of fx */
    if(mpirank == 0)
    {
        PetscCall(VecGetArrayRead(xloc, &xxloc));
        fxloc[0] = PetscPowReal(xxloc[0], 3) - 3*xxloc[0]*PetscPowReal(xxloc[1], 2) + xxloc[2]*xxloc[0] - xxloc[3];
        fxloc[1] = -PetscPowReal(xxloc[1], 3) + 3*PetscPowReal(xxloc[0], 2)*xxloc[1] + xxloc[2]*xxloc[1];
        fxloc[2] = 0;
        fxloc[3] = 0;
        PetscCall(VecSetValues(fx, 4, idx, fxloc, INSERT_VALUES));
        PetscCall(VecRestoreArrayRead(xloc, &xxloc));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fx));
    PetscCall(VecAssemblyEnd(fx));
    /* Clean up */
    PetscCall(VecDestroy(&xloc));
    PetscCall(VecScatterDestroy(&scatterctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of complexified cusp.
 *                                                                                                                                */
PetscErrorCode df(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *fctx)
{
    /* Set up */
    int mpirank;
    VecScatter scatterctx;
    Vec xloc;
    int idx1[4] = {0,1,2,3};
    int idx2[3] = {0,1,2};
    int idxr1[1] = {0};
    int idxr2[1] = {1};
    PetscReal Dfxloc1[4];
    PetscReal Dfxloc2[3];
    const PetscReal *xxloc;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Zero matrix */
    PetscCall(MatZeroEntries(Dfx));
    /* Scatter x to zero */
    PetscCall(VecScatterCreateToZero(x, &scatterctx, &xloc));
    PetscCall(VecScatterBegin(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(scatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    /* Set non-zero values of Dfx */
    if(mpirank == 0)
    {
        PetscCall(VecGetArrayRead(xloc, &xxloc));
        Dfxloc1[0] = 3*PetscPowReal(xxloc[0], 2) - 3*PetscPowReal(xxloc[1], 2) + xxloc[2];
        Dfxloc1[1] = -6*xxloc[0]*xxloc[1];
        Dfxloc1[2] = xxloc[0];
        Dfxloc1[3] = -1;
        Dfxloc2[0] = 6*xxloc[0]*xxloc[1];
        Dfxloc2[1] = 3*PetscPowReal(xxloc[0], 2) - 3*PetscPowReal(xxloc[1], 2) + xxloc[2];
        Dfxloc2[2] = xxloc[1];
        PetscCall(VecRestoreArrayRead(xloc, &xxloc));
        PetscCall(MatSetValues(Dfx, 1, idxr1, 4, idx1, Dfxloc1, INSERT_VALUES));
        PetscCall(MatSetValues(Dfx, 1, idxr2, 3, idx2, Dfxloc2, INSERT_VALUES));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble Dfx */
    PetscCall(MatAssemblyBegin(Dfx, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(Dfx, MAT_FINAL_ASSEMBLY));
    /* Clean up */
    PetscCall(VecDestroy(&xloc));
    PetscCall(VecScatterDestroy(&scatterctx));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
/*!
 *  @brief Main program.
 *                                                                                                                                */
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int d = 4;
    const int k = 2;
    PetscReal a[4] = {-INFINITY, -INFINITY, -2, -2};
    PetscReal b[4] = {INFINITY, INFINITY, 2, 2};
    /* Set up */
    int mpirank, rcode;
    Vec x, x0, fx;
    Mat Dfx;
    slds::cont::mpcoutctx outctx;
    slds::diff::dfctx dfctxx;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set viewer */
    PetscCall(PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE));
    /* Initialize */
    PetscCall(slds::util::vecsetdefault(&x, d, NULL));
    PetscCall(VecDuplicate(x, &x0));
    PetscCall(VecDuplicate(x, &fx));
    PetscCall(slds::util::matsetdefault(&Dfx, d, d, NULL));
    /* Set point */
    PetscCall(VecSet(x0, 0.5));
    /* Test derivatives */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Exact derivative test... \n"));
    PetscCall(df(NULL, x0, Dfx, Dfx, NULL));
    PetscCall(MatView(Dfx, PETSC_VIEWER_STDOUT_WORLD));
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "FD derivative test... \n"));
    dfctxx.d = 4;
    dfctxx.f = f;
    dfctxx.fctx = NULL;
    PetscCall(slds::diff::df(NULL, x0, Dfx, Dfx, &dfctxx));
    PetscCall(MatView(Dfx, PETSC_VIEWER_STDOUT_WORLD));
    /* Get an initial point on the manifold */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Get inital point \n"));
    /* Set solve options */
    slds::solve::fsolvectx fsctx = slds::solve::fsolvectx();
    /* Use Anderson mixing since the derivative is singular by construction */
    fsctx.snestype = SNESANDERSON;
    /* Solve */
    rcode = slds::solve::fsolve(f, df, x0, x, fx, Dfx, d, NULL, fsctx);
    PetscCall(VecView(x, PETSC_VIEWER_STDOUT_WORLD));
    /* Set options */
    slds::cont::mpcctx ctx = slds::cont::mpcctx();
    ctx.type = 0;
    ctx.verbose = 1;
    ctx.rmin = 1e-2;
    ctx.rmax = 2;
    ctx.eps = 1e-1;
    ctx.ntype = NORM_INFINITY;
    ctx.slvctx.ectx.epstype = EPSLAPACK;
    /* Compute continuation */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Compute continuation \n"));
    PetscCall(slds::cont::mpc(f, df, NULL, NULL, x, a, b, 0, 0, d, k, &outctx, NULL, NULL, ctx));
    /* Clean up */
    PetscCall(VecDestroy(&x0));
    PetscCall(VecDestroy(&x));
    PetscCall(VecDestroy(&fx));
    PetscCall(MatDestroy(&Dfx));
    PetscCall(outctx.free());
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
