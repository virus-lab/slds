#**********************************************************************************************************************************#
## @file genplots.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots

#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
import colorcet as cc
import h5py as h5
import numpy as np
import plotly.graph_objects as pgo
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Read points #
f = h5.File('mpcout.h5', mode = 'r');
h5p = f['Points'];
# Extract points #
x = np.zeros(len(h5p));
y = np.zeros(len(h5p));
z = np.zeros(len(h5p));
for i in np.arange(len(h5p)):
    x[i] = h5p[i][-1][-1];
    y[i] = h5p[i][-1][-2];
    z[i] = h5p[i][-1][0] + h5p[i][-1][1];
# Plot #
figure = pgo.Figure(pgo.Scatter3d(x = x, y = y, z = z, mode = 'markers'));
figure.write_html('points.html');
h5bp = f['Bifurcation points'];
# Extract points #
x = np.zeros(len(h5bp));
y = np.zeros(len(h5bp));
z = np.zeros(len(h5bp));
for i in np.arange(len(h5bp)):
    x[i] = h5bp[i][-1][-1];
    y[i] = h5bp[i][-1][-2];
    z[i] = h5bp[i][-1][0] + h5bp[i][-1][1];
# Plot #
figure = pgo.Figure(pgo.Scatter3d(x = x, y = y, z = z, mode = 'markers'));
figure.write_html('bpoints.html');
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
