/**********************************************************************************************************************************/
/*! @file intr.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of computing some integrals with slds++.
 *
 *  Example of computing some integrals with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <petsc.h>
#include <slepc.h>
#include "sldsintr.hpp"
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Computes e^x*(cos(10x))^2*sin(5x).
 *                                                                                                                                */
inline PetscReal f(PetscReal x)
{
    /* Return value */
    return PetscExpReal(x)*PetscPowReal(PetscCosReal(10*x), 2)*PetscSinReal(5*x);
}
/*!
 *  @brief Computes cos(x)^2*sin(y).
 *                                                                                                                                */
inline PetscReal g(PetscReal x, PetscReal y)
{
    /* Return value */
    return PetscPowReal(PetscCosReal(x), 2)*PetscSinReal(y);
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
/*!
 *  @brief Main program.
 *                                                                                                                                */
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set up */
    int mpirank;
    PetscReal If, Ig;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set viewer */
    PetscCall(PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Set integration rules */
    slds::intr::intrule P, Q;
    PetscReal ap[1] = {-pi};
    PetscReal bp[1] = {0};
    int nkp[1] = {1024};
    int ncp[1] = {4};
    PetscReal aq[2] = {-pi, -pi};
    PetscReal bq[2] = {0, 0};
    int nkq[2] = {512, 512};
    int ncq[2] = {4, 4};
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Midpoint... \n"));
    /* Set nodes and weights */
    PetscCall(slds::intr::compositemidpoint(&P, ap, bp, nkp, 1, 1));
    PetscCall(slds::intr::compositemidpoint(&Q, aq, bq, nkq, 2, 1));
    /* Compute integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) */
    If = 0;
    for(int i = 0; i < P.n; i++)
        If += P.weights[i]*f(P.nodes[i]);
    /* Compute integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) */
    Ig = 0;
    for(int i = 0; i < Q.n; i++)
        Ig += Q.weights[i]*g(Q.nodes[i], Q.nodes[i + 1]);
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) is %f \n", If));
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) is %f \n",
                          Ig));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Trapezoid... \n"));
    /* Set nodes and weights */
    PetscCall(slds::intr::compositetrapezoid(&P, ap, bp, nkp, 1, 1));
    PetscCall(slds::intr::compositetrapezoid(&Q, aq, bq, nkq, 2, 1));
    /* Compute integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) */
    If = 0;
    for(int i = 0; i < P.n; i++)
        If += P.weights[i]*f(P.nodes[i]);
    /* Compute integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) */
    Ig = 0;
    for(int i = 0; i < Q.n; i++)
        Ig += Q.weights[i]*g(Q.nodes[i], Q.nodes[i + 1]);
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) is %f \n", If));
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) is %f \n",
                          Ig));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Simpson... \n"));
    /* Set nodes and weights */
    PetscCall(slds::intr::compositesimpson(&P, ap, bp, nkp, 1, 1));
    PetscCall(slds::intr::compositesimpson(&Q, aq, bq, nkq, 2, 1));
    /* Compute integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) */
    If = 0;
    for(int i = 0; i < P.n; i++)
        If += P.weights[i]*f(P.nodes[i]);
    /* Compute integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) */
    Ig = 0;
    for(int i = 0; i < Q.n; i++)
        Ig += Q.weights[i]*g(Q.nodes[i], Q.nodes[i + 1]);
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) is %f \n", If));
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) is %f \n",
                          Ig));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "CC4... \n"));
    /* Set nodes and weights */
    PetscCall(slds::intr::compositecc(&P, ap, bp, nkp, ncp, 1, 1));
    PetscCall(slds::intr::compositecc(&Q, aq, bq, nkq, ncq, 2, 1));
    /* Compute integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) */
    If = 0;
    for(int i = 0; i < P.n; i++)
        If += P.weights[i]*f(P.nodes[i]);
    /* Compute integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) */
    Ig = 0;
    for(int i = 0; i < Q.n; i++)
        Ig += Q.weights[i]*g(Q.nodes[i], Q.nodes[i + 1]);
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) is %f \n", If));
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) is %f \n",
                          Ig));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "GL4... \n"));
    /* Set nodes and weights */
    PetscCall(slds::intr::compositegl(&P, ap, bp, nkp, ncp, 1, 1));
    PetscCall(slds::intr::compositegl(&Q, aq, bq, nkq, ncq, 2, 1));
    /* Compute integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) */
    If = 0;
    for(int i = 0; i < P.n; i++)
        If += P.weights[i]*f(P.nodes[i]);
    /* Compute integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) */
    Ig = 0;
    for(int i = 0; i < Q.n; i++)
        Ig += Q.weights[i]*g(Q.nodes[i], Q.nodes[i + 1]);
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from -pi to 0 of e^x*(cos(10x))^2*sin(5x) is %f \n", If));
    /* Print results */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Integral from {-pi, -pi} to {0, 0} of cos(x)^2*sin(y) is %f \n",
                          Ig));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
