#**********************************************************************************************************************************#
 ## @file evlaplace.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Eigenvalues of a Laplace kernel operator.
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
import numpy as np
import scipy as sp
from typing import Callable
#**********************************************************************************************************************************#
# Root Finding
#**********************************************************************************************************************************#
##
# @brief Find roots of a function in an interval using sweeping and Brents method.
#
def roots(f: Callable[[float], float], a: float, b: float, n: int, xtol: float, fxtol: float, maxit: int) -> list[float]:
    # Compute step #
    ds = (b - a)/n;
    # Initialize intervals and roots #
    r = list();
    isgn = list();
    # Sweep interval for sign changes #
    for i in np.arange(n):
        if(np.sign(f(a + i*ds)) != np.sign(f(a + (i + 1)*ds))):
            isgn.append(i);
    # Find the roots in the sign change intervals #
    for i in isgn:
        x, conv = sp.optimize.brentq(f, a + i*ds, a + (i + 1)*ds, xtol = xtol, maxiter = maxit, full_output = True,
                                     disp = False);
        if(conv.converged and np.abs(f(x)) < fxtol):
            r.append(x);
    # Retrun #
    return r;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Find eigenvalues for roots in an interval #
def evrint(a: float, b: float, n: int, xtol: float, fxtol: float, maxit: int, aval: float, Lval: float) -> tuple[list[float],
                                                                                                                 list[float],
                                                                                                                 list[float]]:
    # Define Expressions #
    def v1(x: float) -> float:
        return np.tan(aval*Lval*x/2) - 1/x;
    def v2(x: float) -> float:
        return 1/np.tan(aval*Lval*x/2) + 1/x;
    # Find roots #
    r1 = roots(v1, a, b, n, xtol, fxtol, maxit);
    r2 = roots(v2, a, b, n, xtol, fxtol, maxit);
    r = r1 + r2;
    # Compute eigenvalues #
    e = list();
    for x in r:
        e.append(1/(1 + x**2));
    # Return #
    return (e, r1, r2);
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
