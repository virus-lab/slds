/**********************************************************************************************************************************/
/*! @file ideev.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of computing eigenvalues of an integrodifference equation with slds++.
 *
 *  Example of computing eigenvalues of an integrodifference equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <numeric>
#include <vector>
#include <H5Cpp.h>
#include <petsc.h>
#include <slepc.h>
#include "sldsde.hpp"
#include "sldsdiff.hpp"
#include "sldsie.hpp"
#include "sldsintr.hpp"
#include "sldsla.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the IDE.
 *                                                                                                                                */
class idefctx
{
    public:
        int n;
        PetscReal alpha;
        const PetscReal *a;
        idefctx() {};
        idefctx(int nn, PetscReal a, const PetscReal *aa): n(nn), alpha(a), a(aa) {};
};
/*!
 *  @brief Ouput for eigenvalues.
 *                                                                                                                                */
class out
{
    public:
        int n; /*!< The number of nodes. */
        int id; /*!< ID of the integration rule. */
        double ev; /*!< Real part of eigenvalue. */
        double iev; /*!< Imaginary part of eigenvalue. */
        out(int nn, int ii, double evv, double ievv): n(nn), id(ii), ev(evv), iev(ievv) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal bh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*lambda*x/(1 + PetscPowReal(x, n));
}
/*!
 *  @brief Derivative of Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal dxbh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*lambda*(1 - (n - 1)*PetscPowReal(x, n))/PetscPowReal((1 + PetscPowReal(x, n)), 2);
}
/*!
 *  @brief Multivariate independent Laplace dispersal kernel.
 *                                                                                                                                */
inline PetscReal mvlp(const PetscReal *x, const PetscReal *y, const PetscReal *a, int d)
{
    /* Set up */
    PetscReal p = 1;
    /* Compute product */
    for(int i = 0; i < d; i++)
        p *= (a[i]/2)*std::exp(-a[i]*PetscAbsReal(x[i] - y[i]));
    /* Return */
    return p;
}
/*!
 *  @brief Beverton-Holt growth with Laplace dispersion with lambda = 1.
 *                                                                                                                                */
PetscErrorCode f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvlp(s, y, fctx.a, k)*bh(uy[0], fctx.alpha, 1, fctx.n);
    /* Return */
    return 0;
}
/*!
 *  @brief Third partial derivative of Beverton-Holt growth with Laplace dispersion with lambda = 1.
 *                                                                                                                                */
PetscErrorCode d3f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                   slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvlp(s, y, fctx.a, k)*dxbh(uy[0], fctx.alpha, 1, fctx.n);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
/*!
 *  @brief Main program.
 *                                                                                                                                */
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int k = 1;
    const int nev = 1;
    const int px = 3;
    const int pf = 3;
    const int p = std::lcm(px, pf);
    const int qnmax = PetscPowReal(2, 11);
    const int qnstep = 1;
    const int qnstep2 = 1;
    const int qnstep3 = 1;
    const PetscReal L = 2;
    const PetscReal alf[pf] = {1, 2, 0.5};
    const int nbh[pf] = {4, 4, 4};
    const PetscReal af[k*pf] = {1, 1, 1};
    const PetscReal eps = 2.2e-16;
    /* Set up */
    int mpirank, rcode;
    Vec u, ut, fut, utloc;
    Mat Dfu, Dfut;
    VecScatter utscatterctx;
    std::vector<PetscReal> ev, iev;
    std::vector<out> outf;
    slds::intr::intrule Q;
    std::vector<idefctx> fctx(pf, idefctx());
    std::vector<PetscErrorCode (*)(SNES, Vec, Vec, void*)> fu(pf, slds::ie::furysohn);
    std::vector<PetscErrorCode (*)(SNES, Vec, Mat, Mat, void*)> dfu(pf, slds::ie::dfurysohn);
    std::vector<slds::ie::urysohnctx> uctx(pf, slds::ie::urysohnctx());
    std::vector<void*> fctxx(pf, NULL), uctxx(pf, NULL);
    slds::diff::dfctx dfctxx;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set viewer */
    PetscCall(PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Set contexts */
    for(int i = 0; i < pf; i++)
    {
        fctx[i] = idefctx(nbh[i], alf[i], &af[k*i]);
        uctx[i].f = f;
        uctx[i].d3f = d3f;
        uctx[i].dlf = NULL;
        uctx[i].Q = &Q;
        uctx[i].fctx.fctx = &(fctx[i]);
        fctxx[i] = &(fctx[i]);
        uctxx[i] = &(uctx[i]);
    }
    /* Set solver options for eigenvalues */
    slds::la::eigenctx ectx = slds::la::eigenctx();
    // ectx.epstype = EPSLAPACK;
    ectx.select = EPS_LARGEST_MAGNITUDE;
    ectx.tol = eps;
    ectx.maxit = 1e6;
    /* Compute eigenvalues */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing eigenvalues... \n"));
    for(int i = 0; i < 7; i++)
    {
        /* Initialize */
        std::vector<PetscReal> a(k, -L/2), b(k, L/2);
        std::vector<int> nk(k, 1);
        std::vector<int> nc(k, 4);
        /* Compute eigenvalues for various n */
        for(;;)
        {
            /* Set rule */
            if(i == 0)
                PetscCall(slds::intr::compositemidpoint(&Q, &a[0], &b[0], &nk[0], k, 1));
            else if(i == 1)
                PetscCall(slds::intr::compositetrapezoid(&Q, &a[0], &b[0], &nk[0], k, 1));
            else if(i == 2)
                PetscCall(slds::intr::compositesimpson(&Q, &a[0], &b[0], &nk[0], k, 1));
            else if(i == 3)
                PetscCall(slds::intr::compositecc(&Q, &a[0], &b[0], &nk[0], &nc[0], k, 1));
            else if(i == 4)
                PetscCall(slds::intr::compositegl(&Q, &a[0], &b[0], &nk[0], &nc[0], k, 1));
            else if(i == 5)
            {
                std::vector<int> nkk(k, 1);
                std::vector<int> ncc(k, 4*nk[0]);
                PetscCall(slds::intr::compositecc(&Q, &a[0], &b[0], &nkk[0], &ncc[0], k, 1));
            }
            else
            {
                std::vector<int> nkk(k, 1);
                std::vector<int> ncc(k, 4*nk[0]);
                PetscCall(slds::intr::compositegl(&Q, &a[0], &b[0], &nkk[0], &ncc[0], k, 1));
            }
            /* Stop if there are too many nodes */
            if(Q.n > qnmax)
                break;
            /* Initialize */
            PetscCall(slds::util::vecsetdefault(&ut, Q.n, NULL));
            PetscCall(slds::util::vecsetdefault(&fut, Q.n, NULL));
            PetscCall(slds::util::matsetdefault(&Dfut, Q.n, Q.n, NULL));
            /* Set contexts */
            slds::de::cycctx cctx = slds::de::cycctx(Q.n, 0, p, pf, 0, ut, fut, Dfut, MATAIJ,
                                                     dfctxx, &fu[0], &dfu[0], &uctxx[0]);
            /* Initialize */
            PetscCall(slds::util::vecsetdefault(&u, p*Q.n, NULL));
            PetscCall(slds::de::initdfcycaijdense(&Dfu, &cctx));
            /* Set scatters */
            PetscCall(VecScatterCreateToAll(ut, &utscatterctx, &utloc));
            for(int j = 0; j < pf; j++)
            {
                uctx[j].uloc = &utloc;
                uctx[j].scatterctx = &utscatterctx;
            }
            /* Set solution */
            PetscCall(VecSet(u, 0));
            /* Compute derivative */
            PetscCall(slds::de::dfcyc(NULL, u, Dfu, Dfu, &cctx));
            PetscCall(slds::util::matdiagonaladd(Dfu, 1, p*Q.n));
            /* Compute eigenvalues */
            rcode = slds::la::eigen(Dfu, &ev, &iev, NULL, NULL, p*Q.n, p*nev, ectx);
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computed eigenvalues %d, %d with code: %d \n", i, Q.n, rcode));
            /* Write solution */
            if(rcode > -1)
            {
               for(int j = 0; j < p*nev; j++)
                   outf.push_back(out(Q.n, i, ev[j], iev[j]));
            }
            /* Clear */
            ev.clear();
            iev.clear();
            /* Increase subintervals */
            if(i == 0 || i == 1)
            {
                for(int i = 0; i < k; i++)
                    nk[i] += qnstep;
            }
            else if(i == 2)
            {
                for(int i = 0; i < k; i++)
                    nk[i] += qnstep2;
            }
            else
            {
                for(int i = 0; i < k; i++)
                    nk[i] += qnstep3;
            }
            /* Clean up */
            PetscCall(VecDestroy(&u));
            PetscCall(VecDestroy(&ut));
            PetscCall(VecDestroy(&fut));
            PetscCall(VecDestroy(&utloc));
            PetscCall(MatDestroy(&Dfu));
            PetscCall(MatDestroy(&Dfut));
            PetscCall(VecScatterDestroy(&utscatterctx));
        }
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing ouput... \n"));
    /* Set up HDF5 output */
    if(mpirank == 0)
    {
        H5::CompType ctype(sizeof(out));
        ctype.insertMember("N", HOFFSET(out, n), H5T_NATIVE_INT);
        ctype.insertMember("ID", HOFFSET(out, id), H5T_NATIVE_INT);
        ctype.insertMember("EV", HOFFSET(out, ev), H5T_NATIVE_DOUBLE);
        ctype.insertMember("IEV", HOFFSET(out, iev), H5T_NATIVE_DOUBLE);
        hsize_t dim[1];
        dim[0] = outf.size();
        int rank = sizeof(dim)/sizeof(hsize_t);
        H5::DataSpace space(rank, dim);
        H5::H5File file = H5::H5File("ev.h5", H5F_ACC_TRUNC);
        H5::DataSet dataset = H5::DataSet(file.createDataSet("Eigenvalues", ctype, space));
        dataset.write(&outf[0], ctype);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
