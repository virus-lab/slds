/**********************************************************************************************************************************/
/*! @file ieev.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of computing eigenvalues of an integral equation with slds++.
 *
 *  Example of computing eigenvalues of an integral equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <vector>
#include <H5Cpp.h>
#include <petsc.h>
#include <slepc.h>
#include "sldsie.hpp"
#include "sldsintr.hpp"
#include "sldsla.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the IE.
 *                                                                                                                                */
class iefctx
{
    public:
        PetscReal pi; /*!< Constant \f$ \pi \f$. */
        PetscReal g; /*!< The parameter \f$ \gamma \f$ in the kernel. */
        iefctx(PetscReal gg): g(gg) { pi = 4*PetscAtanReal(1); };
};
/*!
 *  @brief Ouput for eigenvalues.
 *                                                                                                                                */
class out
{
    public:
        int n; /*!< The number of nodes. */
        int id; /*!< ID of the integration rule. */
        double ev; /*!< Real part of eigenvalue. */
        double iev; /*!< Imaginary part of eigenvalue. */
        out(int nn, int ii, double evv, double ievv): n(nn), id(ii), ev(evv), iev(ievv) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Kernel function.
 *                                                                                                                                */
inline PetscReal kf(PetscReal x, PetscReal g, PetscReal pi)
{
    /* Return value */
    return (1 - PetscPowReal(g, 2))/(1 + PetscPowReal(g, 2) - 2*g*PetscCosReal(2*pi*x));
}
/*!
 *  @brief Kernel of the integral operator.
 *                                                                                                                                */
PetscErrorCode f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    iefctx fctx = *reinterpret_cast<iefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = kf(s[0] + y[0], fctx.g, fctx.pi)*uy[0];
    /* Return */
    return 0;
}
/*!
 *  @brief Third partial derivative of Logistic growth with Laplace dispersion and lambda = 1.
 *                                                                                                                                */
PetscErrorCode d3f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                   slds::ie::ukernelctx ctx)
{
    /* Recover data */
    iefctx fctx = *reinterpret_cast<iefctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = kf(s[0] + y[0], fctx.g, fctx.pi);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
/*!
 *  @brief Main program.
 *                                                                                                                                */
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int qnmax = PetscPowReal(2, 10);
    const int qnstep = 6;
    const int qnstep2 = 3;
    const int qnstep3 = 1;
    const PetscReal g = 0.5;
    const PetscReal eps = 2.22045e-16;
    /* Set up */
    int mpirank, rcode;
    Vec u, uloc;
    Mat Dfu;
    VecScatter uscatterctx;
    std::vector<PetscReal> ev, iev;
    std::vector<out> outf;
    slds::ie::urysohnctx ctx;
    slds::intr::intrule Q;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set viewer */
    PetscCall(PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Create integration rule */
    PetscReal a[1] = {0};
    PetscReal b[1] = {1};
    int nk[1] = {4};
    int nc[1] = {6};
    /* Set integration rule */
    ctx.Q = &Q;
    /* Set additional kernel paramters */
    iefctx kctx = iefctx(g);
    ctx.fctx.fctx = &kctx;
    /* Set kernel and options */
    ctx.f = f;
    ctx.d3f = d3f;
    /* Set solver options for eigenvalues */
    slds::la::eigenctx ectx = slds::la::eigenctx();
    ectx.epstype = EPSLAPACK;
    ectx.tol = eps;
    ectx.maxit = 1e6;
    /* Compute eigenvalues */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing eigenvalues... \n"));
    for(int i = 0; i < 5; i++)
    {
        /* Initialize */
        nk[0] = 1;
        nc[0] = 6;
        /* Compute eigenvalues for various n */
        for(;;)
        {
            /* Set rule */
            if(i == 0)
                PetscCall(slds::intr::compositemidpoint(&Q, a, b, nk, 1, 1));
            else if(i == 1)
                PetscCall(slds::intr::compositetrapezoid(&Q, a, b, nk, 1, 1));
            else if(i == 2)
                PetscCall(slds::intr::compositesimpson(&Q, a, b, nk, 1, 1));
            else if(i == 3)
                PetscCall(slds::intr::compositecc(&Q, a, b, nk, nc, 1, 1));
            else
                PetscCall(slds::intr::compositegl(&Q, a, b, nk, nc, 1, 1));
            /* Stop if there are too many nodes */
            if(Q.n > qnmax)
                break;
            /* Initialize */
            PetscCall(slds::util::vecsetdefault(&u, Q.n, NULL));
            PetscCall(slds::util::matsetdefault(&Dfu, Q.n, Q.n, NULL));
            /* Set scatters */
            PetscCall(VecScatterCreateToAll(u, &uscatterctx, &uloc));
            ctx.uloc = &uloc;
            ctx.scatterctx = &uscatterctx;
            /* Set solution */
            PetscCall(VecSet(u, 0));
            /* Compute derivative */
            PetscCall(slds::ie::dfurysohn(NULL, u, Dfu, Dfu, &ctx));
            /* Compute eigenvalues */
            rcode = slds::la::eigen(Dfu, &ev, &iev, NULL, NULL, Q.n, Q.n, ectx);
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computed eigenvalues %d, %d with code: %d \n", i, Q.n, rcode));
            /* Write solution */
            if(rcode > -1)
            {
               for(int j = 0; j < Q.n; j++)
                   outf.push_back(out(Q.n, i, ev[j], iev[j]));
            }
            /* Clear */
            ev.clear();
            iev.clear();
            /* Increase subintervals */
            if(i == 0 || i == 1)
                nk[0] += qnstep;
            else if(i == 2)
                nk[0] += qnstep2;
            else
                nk[0] += qnstep3;
            /* Clean up */
            PetscCall(VecDestroy(&u));
            PetscCall(VecDestroy(&uloc));
            PetscCall(MatDestroy(&Dfu));
            PetscCall(VecScatterDestroy(&uscatterctx));
        }
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing ouput... \n"));
    /* Set up HDF5 output */
    if(mpirank == 0)
    {
        H5::CompType ctype(sizeof(out));
        ctype.insertMember("N", HOFFSET(out, n), H5T_NATIVE_INT);
        ctype.insertMember("ID", HOFFSET(out, id), H5T_NATIVE_INT);
        ctype.insertMember("EV", HOFFSET(out, ev), H5T_NATIVE_DOUBLE);
        ctype.insertMember("IEV", HOFFSET(out, iev), H5T_NATIVE_DOUBLE);
        hsize_t dim[1];
        dim[0] = outf.size();
        int rank = sizeof(dim)/sizeof(hsize_t);
        H5::DataSpace space(rank, dim);
        H5::H5File file = H5::H5File("ev.h5", H5F_ACC_TRUNC);
        H5::DataSet dataset = H5::DataSet(file.createDataSet("Eigenvalues", ctype, space));
        dataset.write(&outf[0], ctype);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
