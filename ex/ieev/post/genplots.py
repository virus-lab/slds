 #**********************************************************************************************************************************#
 ## @file genplots.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
import colorcet as cc
import pandas as pd
import plotly.express as px
import numpy as np
import sldsplot as pt
#**********************************************************************************************************************************#
## Additional Functions
#**********************************************************************************************************************************#
##
# @brief Estimates and prints convergence rates.
#
def estcrate(data: pd.DataFrame, i: int) -> float:
    # Select data #
    n = data.loc[data['ID'] == i, 'N'];
    e = data.loc[data['ID'] == i, 'Error'];
    # Estimate slope and return #
    return np.polyfit(np.log(n), np.log(e), 1)[0];
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Define constants #
g = 0.5;
# Read data #
data = pd.read_hdf('ev.h5', key = 'Eigenvalues');
# Append errors #
data['Error'] = 0;
# Compute errors #
for i in np.unique(data['ID']):
    datai = data[data['ID'] == i];
    for n in np.unique(datai['N']):
        ev = datai.loc[datai['N'] == n, 'EV'].to_numpy();
        iev = datai.loc[datai['N'] == n, 'IEV'].to_numpy();
        if(n > 1):
            if(ev[1] < 0 or n < 3):
                error = np.sqrt((ev[1] + g)**2 + iev[1]**2);
            else:
                error = np.sqrt((ev[2] + g)**2 + iev[2]**2);
        else:
            error = np.sqrt((ev[0] - g)**2 + iev[0]**2);
        data.loc[np.logical_and(data['ID'] == i, data['N'] == n), 'Error'] = error;
# Trim data #
data = data.drop(columns = ['EV', 'IEV']);
data = data.drop_duplicates();
data = data[data['Error'] > 5e-15];
# Estimate convergence rates #
print('Estimated convergence rate for CMID is ' + str(estcrate(data, 0)));
print('Estimated convergence rate for CTRP is ' + str(estcrate(data, 1)));
print('Estimated convergence rate for CSMP is ' + str(estcrate(data, 2)));
print('Estimated convergence rate for CCC6 is ' + str(estcrate(data, 3)));
print('Estimated convergence rate for CGL6 is ' + str(estcrate(data, 4)));
# Assign ID names #
datamap = {'N': int, 'ID': str, 'Error': float};
data = data.astype(datamap);
data.loc[data['ID'] == '0', 'ID'] = r'$\text{CMID}$';
data.loc[data['ID'] == '1', 'ID'] = r'$\text{CTRP}$';
data.loc[data['ID'] == '2', 'ID'] = r'$\text{CSMP}$';
data.loc[data['ID'] == '3', 'ID'] = r'$\text{CCC}_{6}$';
data.loc[data['ID'] == '4', 'ID'] = r'$\text{CGL}_{6}$';
# Generate plots #
figure = px.line(data, x = 'N', y = 'Error', color = 'ID', line_dash = 'ID', log_x = True, log_y = True,
                 color_discrete_sequence = cc.glasbey);
figure = pt.layout2d(figure);
# figure = figure.update_xaxes(exponentformat = 'power');
# figure = figure.update_yaxes(exponentformat = 'power');
figure = figure.update_traces(line = dict(width = 1));
figure.update_layout(xaxis_title = r'$n$', yaxis_title = r'$\left| \mu_{n} - \lambda \right|$', legend_title = r'', xaxis_tickprefix = r'$', xaxis_ticksuffix = r'$',
                     yaxis_tickprefix = r'$', yaxis_ticksuffix = r'$');
figure.write_image('everrors.svg', scale = 2);
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
