#**********************************************************************************************************************************#
 ## @file sldsplot.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 11/2022
 #  @copyright GNU Public License
 #
 #  A collection of plotting routines using plotly, pandas and open3d.

#**********************************************************************************************************************************#
# Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
# Imports
#**********************************************************************************************************************************#
import colorcet as cc
import numpy as np
import scipy as sp
import plotly.graph_objects as pgo
#**********************************************************************************************************************************#
# Misc
#**********************************************************************************************************************************#
dash2d = ['solid', 'dot', 'dash', 'longdash', 'dashdot', 'longdashdot'];
#**********************************************************************************************************************************#
# Plotting Routines
#**********************************************************************************************************************************#
##
# @brief Plots the wireframe graph of a 2d function using plotly.
#
def wireframe(points: np.ndarray, intensity: bool = True, color: str = "black", colorscale: list = cc.bgy,
              ls: int = 1) -> pgo.Figure:
    # Get triangles and points #
    gridxy = points[:, 0:2];
    meshxy = sp.spatial.Delaunay(gridxy);
    # Get edges #
    xe = list();
    ye = list();
    ze = list();
    ce = list();
    for t in meshxy.simplices:
        xe += [points[t[0]][0]] + [points[t[1]][0]] + [points[t[2]][0]] + [points[t[0]][0]] + [None];
        ye += [points[t[0]][1]] + [points[t[1]][1]] + [points[t[2]][1]] + [points[t[0]][1]] + [None];
        ze += [points[t[0]][2]] + [points[t[1]][2]] + [points[t[2]][2]] + [points[t[0]][2]] + [None];
        if(intensity):
            ce += [points[t[0]][2]] + [points[t[1]][2]] + [points[t[2]][2]] + [points[t[0]][2]] + [0];
    # Create plots #
    if(not intensity):
        figure = pgo.Figure(pgo.Scatter3d(x = xe, y = ye, z = ze, mode = "lines", name = "", line = dict(color = color,
                                                                                                         width = ls)));
    else:
        figure = pgo.Figure(pgo.Scatter3d(x = xe, y = ye, z = ze, mode = "lines", name = "", line = dict(color = ce,
                                                                                                         colorscale = colorscale,
                                                                                                         width = ls)));
    # Return #
    return figure;
##
# @brief Creates a 2d scatter/line plot using plotly.
#
def scatter2d(x: list[list], y: list[list], names: list, colorscale: list = cc.glasbey, lines: bool = False, markers: bool = True,
              ls: int = 1, ms: int = 1, usegl: bool = False) -> pgo.Figure:
    # Set up #
    if(lines and markers):
        mode = "lines+markers";
    elif(lines):
        mode = "lines";
    else:
        mode = "markers";
    # Initialize figure #
    figure = pgo.Figure();
    # Add traces #
    if(usegl):
        for i in range(len(x)):
            figure.add_trace(pgo.Scattergl(x = x[i], y = y[i], mode = mode, name = names[i],
                                           line = dict(color = colorscale[i], width = ls, dash = dash2d[i % len(dash2d)]),
                                           marker = dict(color = colorscale[i], size = ms)));
    else:
        for i in range(len(x)):
            figure.add_trace(pgo.Scatter(x = x[i], y = y[i], mode = mode, name = names[i],
                                         line = dict(color = colorscale[i], width = ls, dash = dash2d[i % len(dash2d)]),
                                         marker = dict(color = colorscale[i], size = ms)));
    # Return #
    return figure;
#**********************************************************************************************************************************#
# Layout Routines
#**********************************************************************************************************************************#
##
# @brief Sets a default layout for a plotly 2d figure.
#
def layout2d(f: pgo.Figure) -> pgo.Figure:
    # Set layout and return #
    return f.update_layout(template = "plotly_white+presentation", margin = dict(l = 0, r = 0, t = 0, b = 0));
##
# @brief Sets a default layout for a plotly 3d figure.
#
def layout3d(f: pgo.Figure) -> pgo.Figure:
    # Set layout and return #
    return f.update_layout(showlegend = False, template = "plotly_white+presentation", coloraxis_showscale = False,
                           margin = dict(l = 0, r = 0, t = 0, b = 0));
#**********************************************************************************************************************************#
# Main
#**********************************************************************************************************************************#
if __name__ == "__main__":
    print("I am a module, exiting ...");
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
