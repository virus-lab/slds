#**********************************************************************************************************************************#
 ## @file idered.py
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Reduction of the example IDE.
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
from sage.all import *
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Define variables #
x = var('x');
y = var('y');
a = var('a');
L = var('L');
v1 = var('v1');
v2 = var('v2');
# Define Expressions #
e1x = sqrt(2*pi/(L*(pi + 2)))*cos(pi*x/(2*L));
e1y = sqrt(2*pi/(L*(pi + 2)))*cos(pi*y/(2*L));
e2x = sqrt(2*pi/(L*(pi - 2)))*sin(pi*x/(2*L));
e2y = sqrt(2*pi/(L*(pi - 2)))*sin(pi*y/(2*L));
kxy = sqrt((pi + 2)*L/(2*pi))*cos(pi*y/(2*L))*e1x + sqrt((pi - 2)*L/(2*pi))*sin(pi*y/(2*L))*e2x;
uya = v1*e1y + v2*e2y;
fa = pi*a*kxy*uya*(1 - uya)/(4*L);
# Define non-normalized expressions #
b1x = cos(pi*x/(2*L));
b1y = cos(pi*y/(2*L));
b2x = sin(pi*x/(2*L));
b2y = sin(pi*y/(2*L));
kxy2 = b1x*b1y + b2x*b2y;
uya2 = v1*b1y + v2*b2y;
fa2 = pi*a*kxy2*uya2*(1 - uya2)/(4*L);
# Integrate Ansatz #
Ifa = fa.integrate(y, -L/2, L/2);
Ifa2 = fa2.integrate(y, -L/2, L/2);
# Simplify #
Ifas = Ifa.full_simplify();
Ifa2s = Ifa2.full_simplify();
#**********************************************************************************************************************************#
# End
#**********************************************************************************************************************************#
