#**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings;
import ColorSchemes as css;
import GLMakie as glmk;
import JLD2 as jld;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Read error data #
try
    global f = jld.jldopen("errors.h5", "r");
    global ferr = f["Errors"];
catch
    print("Failed to read error data!");
    return;
end
# Read data into vectors #
nerr = length(ferr);
qn = zeros(Int64, nerr);
errs = zeros(Float64, nerr);
id = zeros(Int64, nerr);
for i in 1:nerr
    qn[i] = ferr[i][1];
    errs[i] = ferr[i][3];
    id[i] = ferr[i][2];
end
# Set theme #
theme_latexcycle = glmk.merge(glmk.theme_latexfonts(), glmk.Theme(palette = (color = css.colorschemes[:tab10],),
                                                                  Lines = (linewidth = 4,
                                                                           cycle = glmk.Cycle([:color, :linestyle],
                                                                                              covary = true),),
                                                                  Scatter = (markersize = 21,
                                                                             cycle = glmk.Cycle([:color, :marker],
                                                                                                covary = true),),
                                                                  ScatterLines = (linewidth = 4,
                                                                                  markersize = 21,
                                                                                  cycle = glmk.Cycle([:color, :marker],
                                                                                                     covary = true),)));
glmk.set_theme!(theme_latexcycle);
# Create global line plot #
fig = glmk.Figure(resolution = (2000, 1414), fontsize = 48);
axfig = glmk.Axis(fig[1, 1],
                  xlabel = L"$q_{n}$",
                  ylabel = L"$\text{err}_{n}$",
                  xscale = log10,
                  yscale = log10);
# Filter small errors #
filter = errs .> 5e-15;
qnf = qn[filter];
errsf = errs[filter];
idf = id[filter];
# Filter by index #
qnf0 = qnf[idf .== 0];
qnf1 = qnf[idf .== 1];
qnf2 = qnf[idf .== 2];
qnf3 = qnf[idf .== 3];
qnf4 = qnf[idf .== 4];
qnf5 = qnf[idf .== 5];
qnf6 = qnf[idf .== 6];
errsf0 = errsf[idf .== 0];
errsf1 = errsf[idf .== 1];
errsf2 = errsf[idf .== 2];
errsf3 = errsf[idf .== 3];
errsf4 = errsf[idf .== 4];
errsf5 = errsf[idf .== 5];
errsf6 = errsf[idf .== 6];
# Filter in log steps for cleaner look #
logidxs = LinRange(0, 1, 10);
qn0 = [qnf0[idx] for idx in [round(Int64, length(qnf0)^logidx) for logidx in logidxs]];
errs0 = [errsf0[idx] for idx in [round(Int64, length(errsf0)^logidx) for logidx in logidxs]];
qn1 = [qnf1[idx] for idx in [round(Int64, length(qnf1)^logidx) for logidx in logidxs]];
errs1 = [errsf1[idx] for idx in [round(Int64, length(errsf1)^logidx) for logidx in logidxs]];
qn2 = [qnf2[idx] for idx in [round(Int64, length(qnf2)^logidx) for logidx in logidxs]];
errs2 = [errsf2[idx] for idx in [round(Int64, length(errsf2)^logidx) for logidx in logidxs]];
qn3 = [qnf3[idx] for idx in [round(Int64, length(qnf3)^logidx) for logidx in logidxs]];
errs3 = [errsf3[idx] for idx in [round(Int64, length(errsf3)^logidx) for logidx in logidxs]];
qn4 = [qnf4[idx] for idx in [round(Int64, length(qnf4)^logidx) for logidx in logidxs]];
errs4 = [errsf4[idx] for idx in [round(Int64, length(errsf4)^logidx) for logidx in logidxs]];
qn5 = [qnf5[idx] for idx in [round(Int64, length(qnf5)^logidx) for logidx in logidxs]];
errs5 = [errsf5[idx] for idx in [round(Int64, length(errsf5)^logidx) for logidx in logidxs]];
qn6 = [qnf6[idx] for idx in [round(Int64, length(qnf6)^logidx) for logidx in logidxs]];
errs6 = [errsf6[idx] for idx in [round(Int64, length(errsf6)^logidx) for logidx in logidxs]];
# Plot errors #
glmk.scatterlines!(axfig, qn0, errs0, label = L"$\text{MID}$");
glmk.scatterlines!(axfig, qn1, errs1, label = L"$\text{TRP}$");
glmk.scatterlines!(axfig, qn2, errs2, label = L"$\text{SMP}$");
glmk.scatterlines!(axfig, qn3, errs3, label = L"$\text{CC6}$");
glmk.scatterlines!(axfig, qn4, errs4, label = L"$\text{GL8}$");
# glmk.scatterlines!(axfig, qn5, errs5, label = L"$\text{CC}$");
# glmk.scatterlines!(axfig, qn6, errs6, label = L"$\text{GL}$");
# Add legend #
glmk.axislegend(axfig);
# Export figure #
glmk.save("errors.png", fig);
