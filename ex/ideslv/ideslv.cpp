/**********************************************************************************************************************************/
/*! @file ideslv.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 1.0.0
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of solving a periodic integrodifference equation with slds++.
 *
 *  Example of solving a periodic integrodifference equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <numeric>
#include <vector>
#include <H5Cpp.h>
#include <petsc.h>
#include <slepc.h>
#include "sldsde.hpp"
#include "sldsdiff.hpp"
#include "sldsie.hpp"
#include "sldsintr.hpp"
#include "sldsslv.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the IDE.
 *                                                                                                                                */
class idefctx
{
    public:
        PetscReal a; /*!< The parameter \f$ a \f$ in the Laplace kernel. */
        PetscReal L; /*!< The integration domain. */
        idefctx() {};
        idefctx(PetscReal aa, PetscReal LL): a(aa), L(LL) {};
};
/*!
 *  @brief Ouput for n-error plot.
 *                                                                                                                                */
class out
{
    public:
        int n; /*!< The number of nodes. */
        int id; /*!< ID of the integration rule. */
        double error; /*!< Error. */
        out(int nn, int i, double e): n(nn), id(i), error(e) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Exact IDE.
 *                                                                                                                                */
PetscErrorCode fdg(SNES snes, Vec x, Vec fx, void *fctx)
{
    /* x[0] := v1, x[1] := v2 */
    /* Set up */
    int mpirank;
    VecScatter zeroscatterctx;
    Vec xloc;
    int idx[2] = {0,1};
    PetscReal fxloc[2];
    const PetscReal *xxloc;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    idefctx ctx = *reinterpret_cast<idefctx*>(fctx);
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Scatter x to zero */
    PetscCall(VecScatterCreateToZero(x, &zeroscatterctx, &xloc));
    PetscCall(VecScatterBegin(zeroscatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(zeroscatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    /* Set values of fx */
    if(mpirank == 0)
    {
        PetscCall(VecGetArrayRead(xloc, &xxloc));
        fxloc[0] = -ctx.a*(-3*xxloc[0]*(pi + 2) + 10*PetscSqrtReal(2)*PetscPowReal(xxloc[0], 2) +
                           2*PetscSqrtReal(2)*PetscPowReal(xxloc[1], 2))/24;
        fxloc[1] = -ctx.a*xxloc[1]*(-3*(pi - 2) + 4*PetscSqrtReal(2)*xxloc[0])/24;
        PetscCall(VecSetValues(fx, 2, idx, fxloc, INSERT_VALUES));
        PetscCall(VecRestoreArrayRead(xloc, &xxloc));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fx));
    PetscCall(VecAssemblyEnd(fx));
    /* Clean up */
    PetscCall(VecDestroy(&xloc));
    PetscCall(VecScatterDestroy(&zeroscatterctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of the exact IDE.
 *                                                                                                                                */
PetscErrorCode dfdg(SNES snes, Vec x, Mat Dfx, Mat Dfxx, void *fctx)
{
    /* Set up */
    PetscBool dfassembled;
    int mpirank;
    VecScatter zeroscatterctx;
    Vec xloc;
    int idx1[3] = {0,1};
    int idx2[3] = {0,1};
    int idxr1[1] = {0};
    int idxr2[1] = {1};
    PetscReal Dfxloc1[2];
    PetscReal Dfxloc2[2];
    const PetscReal *xxloc;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    idefctx ctx = *reinterpret_cast<idefctx*>(fctx);
    /* Zero derivative if required */
    PetscCall(MatAssembled(Dfx, &dfassembled));
    if(dfassembled == PETSC_TRUE)
        PetscCall(MatZeroEntries(Dfx));
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Scatter x to zero */
    PetscCall(VecScatterCreateToZero(x, &zeroscatterctx, &xloc));
    PetscCall(VecScatterBegin(zeroscatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    PetscCall(VecScatterEnd(zeroscatterctx, x, xloc, INSERT_VALUES, SCATTER_FORWARD));
    /* Set non-zero values of Dfx */
    if(mpirank == 0)
    {
        PetscCall(VecGetArrayRead(xloc, &xxloc));
        Dfxloc1[0] = -ctx.a*(-3*(pi + 2) + 20*PetscSqrtReal(2)*xxloc[0])/24;
        Dfxloc1[1] = -ctx.a*4*PetscSqrtReal(2)*xxloc[1]/24;
        Dfxloc2[0] = -ctx.a*4*PetscSqrtReal(2)*xxloc[1]/24;
        Dfxloc2[1] = -ctx.a*(-3*(pi - 2) + 4*PetscSqrtReal(2)*xxloc[0])/24;
        PetscCall(VecRestoreArrayRead(xloc, &xxloc));
        PetscCall(MatSetValues(Dfx, 1, idxr1, 2, idx1, Dfxloc1, INSERT_VALUES));
        PetscCall(MatSetValues(Dfx, 1, idxr2, 2, idx2, Dfxloc2, INSERT_VALUES));
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Assemble Dfx */
    PetscCall(MatAssemblyBegin(Dfx, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(Dfx, MAT_FINAL_ASSEMBLY));
    /* Clean up */
    PetscCall(VecDestroy(&xloc));
    PetscCall(VecScatterDestroy(&zeroscatterctx));
    /* Return */
    return 0;
}
/*!
 *  @brief Exact basis function 1.
 *                                                                                                                                */
inline PetscReal e1(PetscReal x, PetscReal L)
{
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Return value */
    return PetscCosReal(pi*x/(2*L));
}
/*!
 *  @brief Exact basis function 2.
 *                                                                                                                                */
inline PetscReal e2(PetscReal x, PetscReal L)
{
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Return value */
    return PetscSinReal(pi*x/(2*L));
}
/*!
 *  @brief Logistic growth function.
 *                                                                                                                                */
inline PetscReal lg(PetscReal x)
{
    /* Return value */
    return x*(1 - x);
}
/*!
 *  @brief Univariate degenerate dispersal kernel.
 *                                                                                                                                */
inline PetscReal uvdg(PetscReal x, PetscReal y, PetscReal L)
{
    /* Set pi */
    const PetscReal pi = 4*PetscAtanReal(1);
    /* Return value */
    return pi*PetscCosReal(pi*(x - y)/(2*L))/(4*L);
}
/*!
 *  @brief Logistic growth with univariate degenerate dispersal.
 *                                                                                                                                */
PetscErrorCode f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result */
    fs[0] = fctx.a*uvdg(s[0], y[0], fctx.L)*lg(uy[0]);
    /* Return */
    return 0;
}
/*!
 *  @brief Partial derivative of logistic growth with univariate degenerate dispersal.
 *                                                                                                                                */
PetscErrorCode d3f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* d3f, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idefctx fctx = *reinterpret_cast<idefctx*>(ctx.fctx);
    /* Set result */
    d3f[0] = fctx.a*uvdg(s[0], y[0], fctx.L)*(1 - 2*uy[0]);
    /* Return */
    return 0;
}
/*!
 *  @brief L-infinity norm convergence test.
 *                                                                                                                                */
PetscErrorCode setlinfconv(SNES snes, void *snesctx)
{
    /* Set convergence test */
    PetscCall(SNESSetConvergenceTest(snes, slds::solve::snesconvergedlinf, NULL, NULL));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
/*!
 *  @brief Main program.
 *                                                                                                                                */
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int nn = PetscPowReal(2, 17);
    const int px = 3;
    const int pf = 3;
    const int p = std::lcm(px, pf);
    const int qnmax = PetscPowReal(2, 10);
    const int qnstep = 6;
    const int qnstep2 = 3;
    const int qnstep3 = 1;
    const PetscReal L = 10;
    const PetscReal eps = 1e-15;
    const PetscReal af[pf] = {4, 6, 8};
    /* Set up */
    int mpirank, rcode;
    Vec u, u0, v0, uloc, u0loc, vloc;
    Mat Dfu, Dfu0, Dfv0;
    VecScatter uscatterctx, u0scatterctx, vscatterctx;
    std::vector<PetscReal> x(nn + 1, 0), vt(2*p, 0);
    std::vector<Vec> ut, vt0, ut0;
    std::vector<std::vector<PetscReal>> fx(p, std::vector<PetscReal>(nn + 1, 0)), fxx(p, std::vector<PetscReal>(nn + 1, 0));
    std::vector<PetscErrorCode (*)(SNES, Vec, Vec, void*)> fex(p, fdg), fu(p, slds::ie::furysohn);
    std::vector<PetscErrorCode (*)(SNES, Vec, Mat, Mat, void*)> dfex(p, dfdg), dfu(p, slds::ie::dfurysohn);
    std::vector<out> outf;
    std::vector<idefctx> fctx(p, idefctx());
    std::vector<slds::ie::urysohnctx> uctx(p, slds::ie::urysohnctx());
    std::vector<void*> fctxx(p, NULL), uctxx(p, NULL);
    slds::diff::dfctx dfctxx;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Set viewer */
    PetscCall(PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Set integration rule */
    slds::intr::intrule Q;
    PetscReal a[1] = {-L/2};
    PetscReal b[1] = {L/2};
    int nk[1] = {7};
    int nc[1] = {4};
    PetscCall(slds::intr::compositetrapezoid(&Q, a, b, nk, 1, 1));
    /* Set contexts */
    for(int i = 0; i < pf; i++)
    {
        fctx[i] = idefctx(af[i], L);
        uctx[i].f = f;
        uctx[i].d3f = d3f;
        uctx[i].Q = &Q;
        uctx[i].fctx.fctx = &(fctx[i]);
        fctxx[i] = &(fctx[i]);
        uctxx[i] = &(uctx[i]);
    }
    /* Set solver options */
    slds::solve::fsolvectx slvctx = slds::solve::fsolvectx();
    slvctx.snesatol = eps;
    slvctx.snesrtol = 0;
    slvctx.snesstol = 0;
    slvctx.snesmaxit = 1e6;
    slvctx.kspatol = eps;
    slvctx.ksprtol = 0;
    slvctx.kspmaxit = 1e6;
    slvctx.pctype = PCNONE;
    slvctx.snesoptions = setlinfconv;
    /* Set cyclic solve contexts */
    slds::de::cycslvctx cdgctx = slds::de::cycslvctx(2, 0, p, pf, 0, MATAIJ, dfctxx, &fex[0], &dfex[0], &fctxx[0]);
    cdgctx.ctx = slvctx;
    slds::de::cycslvctx cuctx = slds::de::cycslvctx(Q.n, 0, p, pf, 0, MATAIJ, dfctxx, &fu[0], &dfu[0], &uctxx[0]);
    cuctx.ctx = slvctx;
    /* Initialize */
    PetscCall(slds::util::vecsetdefault(&v0, 2, NULL));
    PetscCall(slds::util::vecsetdefault(&u0, Q.n, NULL));
    PetscCall(slds::util::matsetdefault(&Dfv0, 2, 2, NULL));
    PetscCall(slds::util::matsetdefault(&Dfu0, Q.n, Q.n, NULL));
    /* Set scatters */
    PetscCall(VecScatterCreateToAll(u0, &u0scatterctx, &u0loc));
    for(int i = 0; i < p; i++)
    {
        uctx[i].uloc = &u0loc;
        uctx[i].scatterctx = &u0scatterctx;
    }
    /* Set test values */
    PetscCall(VecSet(u0, 1));
    PetscCall(VecSet(v0, 0.6));
    /* Test derivatives */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Exact derivative test... \n"));
    PetscCall(dfdg(NULL, v0, Dfv0, Dfv0, fctxx[0]));
    PetscCall(MatView(Dfv0, PETSC_VIEWER_STDOUT_WORLD));
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "FD derivative test... \n"));
    dfctxx.d = 2;
    dfctxx.f = fdg;
    dfctxx.fctx = fctxx[0];
    PetscCall(slds::diff::df(NULL, v0, Dfv0, Dfv0, &dfctxx));
    PetscCall(MatView(Dfv0, PETSC_VIEWER_STDOUT_WORLD));
    /* Solve degenerate IDE */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Solve degenerate IDE... \n"));
    PetscCall(VecSet(v0, 0.6));
    rcode = slds::de::cycsolve(v0, &vt0, cdgctx);
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Solved exact IDE with code: %d \n", rcode));
    for(int i = 0; i < p; i++)
    {
        PetscCall(fdg(NULL, vt0[i], v0, fctxx[i % pf]));
        PetscCall(VecView(vt0[i], PETSC_VIEWER_STDOUT_WORLD));
        PetscCall(VecView(v0, PETSC_VIEWER_STDOUT_WORLD));
    }
    /* Test Urysohn derivatives */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Urysohn exact derivative test... \n"));
    PetscCall(slds::ie::dfurysohn(NULL, u0, Dfu0, Dfu0, uctxx[0]));
    PetscCall(MatView(Dfu0, PETSC_VIEWER_STDOUT_WORLD));
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Urysohn FD derivative test... \n"));
    dfctxx.d = Q.n;
    dfctxx.f = slds::ie::furysohn;
    dfctxx.fctx = uctxx[0];
    PetscCall(slds::diff::df(NULL, u0, Dfu0, Dfu0, &dfctxx));
    PetscCall(MatView(Dfu0, PETSC_VIEWER_STDOUT_WORLD));
    /* Solve Urysohn IDE */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Solve Urysohn IDE... \n"));
    PetscCall(VecSet(u0, 0.5));
    rcode = slds::de::cycsolve(u0, &ut0, cuctx);
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Solved Urysohn IDE with code: %d \n", rcode));
    for(int i = 0; i < p; i++)
    {
        PetscCall(slds::ie::furysohn(NULL, ut0[i], u0, uctxx[i % pf]));
        PetscCall(VecView(ut0[i], PETSC_VIEWER_STDOUT_WORLD));
        PetscCall(VecView(u0, PETSC_VIEWER_STDOUT_WORLD));
    }
    /* Set exact solution */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing exact solution... \n"));
    for(int i = 0; i < nn + 1; i++)
        x[i] = -L/2 + i*L/nn;
    PetscCall(VecScatterCreateToAll(vt0[0], &vscatterctx, &vloc));
    for(int j = 0; j < p; j++)
    {
        /* Set up */
        const PetscReal *vvloc;
        /* Scatter v */
        PetscCall(VecScatterBegin(vscatterctx, vt0[j], vloc, INSERT_VALUES, SCATTER_FORWARD));
        PetscCall(VecScatterEnd(vscatterctx, vt0[j], vloc, INSERT_VALUES, SCATTER_FORWARD));
        /* Set result */
        PetscCall(VecGetArrayRead(vloc, &vvloc));
        vt[2*j] = vvloc[0];
        vt[2*j + 1] = vvloc[1];
        for(int i = 0; i < nn + 1; i++)
            fxx[j][i] = vvloc[0]*e1(x[i], L) + vvloc[1]*e2(x[i], L);
        PetscCall(VecRestoreArrayRead(vloc, &vvloc));
    }
    /* Generate n-error plot */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing errors... \n"));
    for(int i = 0; i < 7; i++)
    {
        /* Initialize */
        nk[0] = 1;
        nc[0] = 4;
        /* Compute solutions for various n */
        for(;;)
        {
            /* Set rule */
            if(i == 0)
                PetscCall(slds::intr::compositemidpoint(&Q, a, b, nk, 1, 1));
            else if(i == 1)
                PetscCall(slds::intr::compositetrapezoid(&Q, a, b, nk, 1, 1));
            else if(i == 2)
                PetscCall(slds::intr::compositesimpson(&Q, a, b, nk, 1, 1));
            else if(i == 3)
                PetscCall(slds::intr::compositecc(&Q, a, b, nk, nc, 1, 1));
            else if(i == 4)
                PetscCall(slds::intr::compositegl(&Q, a, b, nk, nc, 1, 1));
            else if(i == 5)
            {
                if(nk[0] > 50)
                    break;
                int nkk[1] = {1};
                int ncc[1] = {2*nk[0]};
                PetscCall(slds::intr::compositecc(&Q, a, b, nkk, ncc, 1, 1));
            }
            else
            {
                if(nk[0] > 50)
                    break;
                int nkk[1] = {1};
                int ncc[1] = {nk[0]};
                PetscCall(slds::intr::compositegl(&Q, a, b, nkk, ncc, 1, 1));
            }
            /* Stop if there are too many nodes */
            if(Q.n > qnmax)
                break;
            /* Initialize */
            PetscCall(slds::util::vecsetdefault(&u, Q.n, NULL));
            PetscCall(slds::util::matsetdefault(&Dfu, Q.n, Q.n, NULL));
            /* Set context */
            cuctx = slds::de::cycslvctx(Q.n, 0, p, pf, 0, MATAIJ, dfctxx, &fu[0], &dfu[0], &uctxx[0]);
            cuctx.ctx = slvctx;
            /* Set scatters */
            PetscCall(VecScatterCreateToAll(u, &uscatterctx, &uloc));
            for(int j = 0; j < pf; j++)
            {
                uctx[j].uloc = &uloc;
                uctx[j].scatterctx = &uscatterctx;
            }
            /* Set initial guess */
            if(mpirank == 0)
            {
                /* Set up */
                std::vector<int> uuidx(Q.n, 0);
                std::vector<PetscReal> uu(Q.n, 0);
                /* Set initial guess as exact solution */
                for(int j = 0; j < Q.n; j++)
                {
                    uuidx[j] = j;
                    uu[j] = vt[0]*e1(Q.nodes[j], L) + vt[1]*e2(Q.nodes[j], L);
                }
                /* Set values */
                PetscCall(VecSetValues(u, Q.n, &uuidx[0], &uu[0], INSERT_VALUES));
            }
            /* Sync processes */
            PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
            /* Assemble initial guess */
            PetscCall(VecAssemblyBegin(u));
            PetscCall(VecAssemblyEnd(u));
            /* Solve */
            rcode = slds::de::cycsolve(u, &ut, cuctx);
            PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Solved IDE %d, %d with code: %d \n", i, Q.n, rcode));
            /* Set error if the solver converged */
            if(rcode > -1)
            {
                /* Extract Nytröm interpolant */
                for(int j = 0; j < p; j++)
                {
                    /* Set up */
                    const PetscReal *uuloc;
                    /* Scatter u */
                    PetscCall(VecScatterBegin(uscatterctx, ut[j], uloc, INSERT_VALUES, SCATTER_FORWARD));
                    PetscCall(VecScatterEnd(uscatterctx, ut[j], uloc, INSERT_VALUES, SCATTER_FORWARD));
                    /* Set Nyström interpolant */
                    PetscCall(VecGetArrayRead(uloc, &uuloc));
                    for(int h = 0; h < nn + 1; h++)
                    {
                        /* Set up */
                        PetscReal s[1], y[1], uy[1], fs[1];
                        /* Initialize */
                        s[0] = x[h];
                        fx[j][h] = 0;
                        /* Set values */
                        for(int k = 0; k < Q.n; k++)
                        {
                            y[0] = Q.nodes[k];
                            uy[0] = uuloc[k];
                            PetscCall(f(s, y, uy, fs, 1, 1, uctx[j].fctx));
                            fx[j][h] += Q.weights[k]*fs[0];
                        }
                    }
                    PetscCall(VecRestoreArrayRead(uloc, &uuloc));
                }
                /* Compute error */
                PetscReal errmax = -1;
                for(int j = 0; j < p; j++)
                {
                    /* Compute period pair with minimal error */
                    PetscReal errhmin = PetscPowReal(2, 32);
                    for(int h = 0; h < p; h++)
                    {
                        /* Compute error for period pair */
                        PetscReal errkmax = -1;
                        for(int k = 0; k < nn + 1; k++)
                        {
                            PetscReal errk = PetscAbsReal(fx[j][k] - fxx[h][k]);
                            if(errk > errkmax)
                                errkmax = errk;
                        }
                        if(errkmax < errhmin)
                            errhmin = errkmax;
                    }
                    if(errhmin > errmax)
                        errmax = errhmin;
                }
                /* Print result */
                PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Error %d, %d is %f \n", i, Q.n, errmax));
                /* Write result */
                outf.push_back(out(Q.n, i, errmax));
            }
            /* Increase subintervals */
            if(i == 0 || i == 1)
                nk[0] += qnstep;
            else if(i == 2)
                nk[0] += qnstep2;
            else
                nk[0] += qnstep3;
            /* Clean up */
            PetscCall(VecDestroy(&u));
            PetscCall(VecDestroy(&uloc));
            PetscCall(MatDestroy(&Dfu));
            PetscCall(VecScatterDestroy(&uscatterctx));
            for(int i = 0; i < ut.size(); i++)
                PetscCall(VecDestroy(&(ut[i])));
            ut.clear();
        }
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing ouput... \n"));
    /* Set up HDF5 output */
    if(mpirank == 0)
    {
        H5::CompType ctype(sizeof(out));
        ctype.insertMember("N", HOFFSET(out, n), H5T_NATIVE_INT);
        ctype.insertMember("ID", HOFFSET(out, id), H5T_NATIVE_INT);
        ctype.insertMember("Error", HOFFSET(out, error), H5T_NATIVE_DOUBLE);
        hsize_t dim[1];
        dim[0] = outf.size();
        int rank = sizeof(dim)/sizeof(hsize_t);
        H5::DataSpace space(rank, dim);
        H5::H5File file = H5::H5File("errors.h5", H5F_ACC_TRUNC);
        H5::DataSet dataset = H5::DataSet(file.createDataSet("Errors", ctype, space));
        dataset.write(&outf[0], ctype);
    }
    /* Sync processes */
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Clean up */
    PetscCall(VecDestroy(&v0));
    PetscCall(VecDestroy(&u0));
    PetscCall(VecDestroy(&u0loc));
    PetscCall(VecDestroy(&vloc));
    PetscCall(MatDestroy(&Dfv0));
    PetscCall(MatDestroy(&Dfu0));
    PetscCall(VecScatterDestroy(&u0scatterctx));
    PetscCall(VecScatterDestroy(&vscatterctx));
    for(int i = 0; i < vt0.size(); i++)
        PetscCall(VecDestroy(&(vt0[i])));
    for(int i = 0; i < ut0.size(); i++)
        PetscCall(VecDestroy(&(ut0[i])));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
