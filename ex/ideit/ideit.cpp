/**********************************************************************************************************************************/
/*! @file idecont.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of a 1D periodic integrodifference equation with slds++.
 *
 *  Example of continuation of a 2D periodic integrodifference equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <numeric>
#include <string>
#include <vector>
#include <petsc.h>
#include <slepc.h>
#include "sldscont.hpp"
#include "sldsde.hpp"
#include "sldsie.hpp"
#include "sldsintr.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the IDE.
 *                                                                                                                                */
class idegctx
{
    public:
        PetscReal c;
        PetscReal pi;
        const PetscReal *s;
        idegctx() {};
        idegctx(PetscReal cc, const PetscReal *ss): c(cc), s(ss) { pi = 4*PetscAtanReal(1); };
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Proportional growth function.
 *                                                                                                                                */
inline PetscReal cg(PetscReal x, PetscReal c)
{
    /* Return value */
    return c*x;
}
/*!
 *  @brief Beverton-Holt growth function.
 *                                                                                                                                */
inline PetscReal bh(PetscReal x, PetscReal alpha, PetscReal lambda, int n)
{
    /* Return value */
    return alpha*lambda*x/(1 + PetscPowReal(x, n));
}
/*!
 *  @brief Ricker growth function.
 *                                                                                                                                */
inline PetscReal rk(PetscReal x, PetscReal lambda)
{
    /* Return value */
    return x*PetscExpReal(lambda*(1 - x));
}
/*!
 *  @brief Multivariate independent Laplace dispersal kernel.
 *                                                                                                                                */
inline PetscReal mvlp(const PetscReal *x, const PetscReal *y, const PetscReal *a, int d)
{
    /* Set up */
    PetscReal p = 1;
    /* Compute product */
    for(int i = 0; i < d; i++)
        p *= (a[i]/2)*std::exp(-a[i]*PetscAbsReal(x[i] - y[i]));
    /* Return */
    return p;
}
/*!
 *  @brief Multivariate independent Gauss dispersal kernel.
 *                                                                                                                                */
inline PetscReal mvga(const PetscReal *x, const PetscReal *y, const PetscReal *s, PetscReal pi, int d)
{
    /* Set up */
    PetscReal p = 1;
    /* Compute product */
    for(int i = 0; i < d; i++)
        p *= PetscExpReal(-0.5*PetscPowReal((x[i] - y[i])/s[i], 2))/(s[i]*PetscSqrtReal(2*pi));
    /* Return */
    return p;
}
/*!
 *  @brief Proportional growth with Gauss dispersion.
 *                                                                                                                                */
PetscErrorCode g(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idegctx gctx = *reinterpret_cast<idegctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvga(s, y, gctx.s, gctx.pi, k)*cg(uy[0], gctx.c);
    /* Return */
    return 0;
}
/*!
 *  @brief Ricker growth with Gauss dispersion.
 *                                                                                                                                */
PetscErrorCode f(const PetscReal* s, const PetscReal* y, const PetscReal* uy, PetscReal* fs, int k, int d,
                 slds::ie::ukernelctx ctx)
{
    /* Recover data */
    idegctx gctx = *reinterpret_cast<idegctx*>(ctx.fctx);
    /* Set result directly */
    fs[0] = mvga(s, y, gctx.s, gctx.pi, k)*rk(uy[0], (*ctx.lambda)[0]);
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    PetscCall(SlepcInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const int k = 2;
    const int qnk = 25;
    const PetscReal c = 2;
    const PetscReal l0 = 0;
    const PetscReal l1 = 3;
    const PetscReal aa = -1;
    const PetscReal bb = 1;
    const PetscReal sg[k] = {0.2, 0.2};
    /* Set up */
    int mpirank;
    Vec u0, u0loc;
    VecScatter u0scatterctx;
    std::vector<PetscReal> mpa, mpb;
    std::vector<std::vector<Vec>> ut;
    slds::cont::mpcctx mpctx;
    slds::cont::mpcoutctx outctx;
    slds::de::feigenctx fgctx;
    slds::intr::intrule Q;
    std::vector<idegctx> fctx(1, idegctx());
    std::vector<PetscErrorCode (*)(SNES, Vec, Vec, void*)> fu(1, slds::ie::furysohn);
    std::vector<slds::ie::urysohnctx> uctx(1, slds::ie::urysohnctx());
    std::vector<void*> fctxx(1, NULL), uctxx(1, NULL);
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Create integration rule */
    std::vector<PetscReal> a(k, aa), b(k, bb);
    std::vector<int> nk(k, qnk);
    PetscCall(slds::intr::compositetrapezoid(&Q, &a[0], &b[0], &nk[0], k, 1));
    /* Set contexts */
    fgctx.verbose = 1;
    fgctx.initit = 1000;
    fgctx.it = 500;
    fgctx.t = 25;
    fgctx.eps = 1e-9;
    fgctx.reset = 1;
    fgctx.nstep = PetscCeilReal(l1 - l0)*200;
    for(int i = 0; i < 1; i++)
    {
        fctx[i] = idegctx(c, &sg[0]);
        uctx[i].parameters = 1;
        uctx[i].f = g;
        uctx[i].Q = &Q;
        uctx[i].fctx.k = 1;
        uctx[i].fctx.fctx = &(fctx[i]);
        fctxx[i] = &(fctx[i]);
        uctxx[i] = &(uctx[i]);
    }
    /* Initialize */
    PetscCall(slds::util::vecsetdefault(&u0, Q.n + 1, NULL));
    PetscCall(VecSet(u0, 1.25));
    /* Set scatters */
    PetscCall(VecScatterCreateToAll(u0, &u0scatterctx, &u0loc));
    for(int i = 0; i < 1; i++)
    {
        uctx[i].uloc = &u0loc;
        uctx[i].scatterctx = &u0scatterctx;
    }
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Iterating. \n"));
    /* Iterate IDE */
    ut.push_back(std::vector<Vec>());
    PetscCall(slds::de::it(&fu[0], u0, &ut[0], 100, 0, 1, Q.n, 0, &uctxx[0]));
    /* Print update */
    // PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing Feigenbaum. \n"));
    /* Compute Feigenbaum diagram */
    // PetscCall(slds::de::feigen(&fu[0], u0, &ut, l0, l1, 1, Q.n + 1, &uctxx[0], fgctx));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing output. \n"));
    /* Write output */
    PetscCall(slds::intr::writeh5(&Q, "intrule.h5"));
    PetscCall(slds::de::writeitsh5(ut, Q.n + 1, "its.h5"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up. \n"));
    /* Clean up */
    PetscCall(VecDestroy(&u0));
    PetscCall(VecDestroy(&u0loc));
    PetscCall(VecScatterDestroy(&u0scatterctx));
    for(int i = 0; i < ut.size(); i++)
        for(int j = 0; j < ut[i].size(); j++)
            PetscCall(VecDestroy(&(ut[i][j])));
    PetscCall(outctx.free());
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(SlepcFinalize());
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
