/**********************************************************************************************************************************/
/*! @file idecont.cpp
 *
 *  @author David Rackl (somekindofvirus@arctic-fox.net)
 *  @version 0.0.1
 *  @date 01/2023
 *  @copyright GNU Public License

 *  @brief Example of a 1D periodic integrodifference equation with slds++.
 *
 *  Example of continuation of a 2D periodic integrodifference equation with slds++.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Copyright Notice */
/**********************************************************************************************************************************/
/*
 * Copyright (C) 2022  David Rackl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *                                                                                                                                */
/**********************************************************************************************************************************/
/* Imports */
/**********************************************************************************************************************************/
#include <petscts.h>
#include <vector>
#include <petsc.h>
#include "sldsloca.hpp"
#include "sldsode.hpp"
#include "sldsutil.hpp"
/**********************************************************************************************************************************/
/* Additional Classes */
/**********************************************************************************************************************************/
/*!
 *  @brief Function context for the Henon map.
 *                                                                                                                                */
class henonctx
{
    public:
        PetscReal a, b;
        henonctx(PetscReal aa, PetscReal bb): a(aa), b(bb) {};
};
/*!
 *  @brief Function context for the Chua map.
 *                                                                                                                                */
class chuactx
{
    public:
        PetscReal a, b, m0, m1;
        chuactx(PetscReal aa, PetscReal bb, PetscReal mm0, PetscReal mm1): a(aa), b(bb), m0(mm0), m1(mm1) {};
};
/*!
 *  @brief Function context for the time-h-map of the Chua map.
 *                                                                                                                                */
class timehchuactx
{
    public:
        PetscReal h;
        chuactx cctx;
        slds::ode::odesolvectx odectx;
        timehchuactx(PetscReal hh, chuactx cctxx, slds::ode::odesolvectx odectxx): h(hh), cctx(cctxx), odectx(odectxx) {};
};
/*!
 *  @brief Function context for the Chua map.
 *                                                                                                                                */
class lorenzctx
{
    public:
        PetscReal b, r, s;
        lorenzctx(PetscReal bb, PetscReal rr, PetscReal ss): b(bb), r(rr), s(ss) {};
};
/*!
 *  @brief Function context for the time-h-map of the Chua map.
 *                                                                                                                                */
class timehlorenzctx
{
    public:
        PetscReal h;
        lorenzctx lctx;
        slds::ode::odesolvectx odectx;
        timehlorenzctx(PetscReal hh, lorenzctx lctxx, slds::ode::odesolvectx odectxx): h(hh), lctx(lctxx), odectx(odectxx) {};
};
/**********************************************************************************************************************************/
/* Additional Functions */
/**********************************************************************************************************************************/
/*!
 *  @brief Henon map.
 *                                                                                                                                */
PetscErrorCode henon(Vec x, Vec fx, void *ctx)
{
    /* Initialize */
    PetscBool initpetsc;
    int mpirank, idx[2] = {0, 1};
    PetscReal fxx[2];
    const PetscReal *xx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    henonctx hctx = *reinterpret_cast<henonctx*>(ctx);
    /* Set result */
    PetscCall(VecGetArrayRead(x, &xx));
    fxx[0] = 1 - hctx.a*PetscPowReal(xx[0], 2) + xx[1];
    fxx[1] = hctx.b*xx[0];
    PetscCall(VecSetValues(fx, 2, idx, fxx, INSERT_VALUES));
    PetscCall(VecRestoreArrayRead(x, &xx));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fx));
    PetscCall(VecAssemblyEnd(fx));
    /* Return */
    return 0;
}
/*!
 *  @brief Chua map.
 *                                                                                                                                */
PetscErrorCode chua(TS ts, PetscReal t, Vec x, Vec fx, void *ctx)
{
    /* Initialize */
    PetscBool initpetsc;
    int mpirank, idx[3] = {0, 1, 2};
    PetscReal fxx[3];
    const PetscReal *xx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    chuactx cctx = *reinterpret_cast<chuactx*>(ctx);
    /* Set result */
    PetscCall(VecGetArrayRead(x, &xx));
    fxx[0] = cctx.a*(xx[1] - cctx.m0*xx[0] - cctx.m1*PetscPowReal(xx[0], 3)/3);
    fxx[1] = xx[0] - xx[1] + xx[2];
    fxx[2] = -cctx.b*xx[1];
    PetscCall(VecSetValues(fx, 3, idx, fxx, INSERT_VALUES));
    PetscCall(VecRestoreArrayRead(x, &xx));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fx));
    PetscCall(VecAssemblyEnd(fx));
    /* Return */
    return 0;
}
/*!
 *  @brief Lorenz map.
 *                                                                                                                                */
PetscErrorCode lorenz(TS ts, PetscReal t, Vec x, Vec fx, void *ctx)
{
    /* Initialize */
    PetscBool initpetsc;
    int mpirank, idx[3] = {0, 1, 2};
    PetscReal fxx[3];
    const PetscReal *xx;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    lorenzctx lctx = *reinterpret_cast<lorenzctx*>(ctx);
    /* Set result */
    PetscCall(VecGetArrayRead(x, &xx));
    fxx[0] = lctx.s*(xx[1] - xx[0]);
    fxx[1] = lctx.r*xx[0] - xx[1] - xx[0]*xx[2];
    fxx[2] = xx[0]*xx[1] - lctx.b*xx[2];
    PetscCall(VecSetValues(fx, 3, idx, fxx, INSERT_VALUES));
    PetscCall(VecRestoreArrayRead(x, &xx));
    /* Assemble result */
    PetscCall(VecAssemblyBegin(fx));
    PetscCall(VecAssemblyEnd(fx));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of the chua map.
 *                                                                                                                                */
PetscErrorCode dfchua(TS ts, PetscReal t, Vec x, Mat Dfx, Mat Dfxx, void *ctx)
{
    /* Set up */
    int mpirank;
    int idx[3] = {0,1,2};
    PetscReal Dfxxx[9];
    const PetscReal *xx;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    chuactx cctx = *reinterpret_cast<chuactx*>(ctx);
    /* Set values of Dfx */
    PetscCall(VecGetArrayRead(x, &xx));
    Dfxxx[0] = -cctx.a*(cctx.m0 + cctx.m1*PetscPowReal(xx[0], 2));
    Dfxxx[1] = cctx.a*xx[1];
    Dfxxx[2] = 0;
    Dfxxx[3] = 1;
    Dfxxx[4] = -1;
    Dfxxx[5] = 1;
    Dfxxx[6] = 0;
    Dfxxx[7] = -cctx.b;
    Dfxxx[8] = 0;
    PetscCall(VecRestoreArrayRead(x, &xx));
    PetscCall(MatSetValues(Dfx, 3, idx, 3, idx, Dfxxx, INSERT_VALUES));
    /* Assemble Dfx */
    PetscCall(MatAssemblyBegin(Dfx, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(Dfx, MAT_FINAL_ASSEMBLY));
    /* Return */
    return 0;
}
/*!
 *  @brief Derivative of the lorenz map.
 *                                                                                                                                */
PetscErrorCode dflorenz(TS ts, PetscReal t, Vec x, Mat Dfx, Mat Dfxx, void *ctx)
{
    /* Set up */
    int mpirank;
    int idx[3] = {0,1,2};
    PetscReal Dfxxx[9];
    const PetscReal *xx;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    lorenzctx lctx = *reinterpret_cast<lorenzctx*>(ctx);
    /* Set values of Dfx */
    PetscCall(VecGetArrayRead(x, &xx));
    Dfxxx[0] = -lctx.s;
    Dfxxx[1] = lctx.s;
    Dfxxx[2] = 0;
    Dfxxx[3] = lctx.r - xx[2];
    Dfxxx[4] = -1;
    Dfxxx[5] = -xx[0];
    Dfxxx[6] = xx[1];
    Dfxxx[7] = xx[0];
    Dfxxx[8] = -lctx.b;
    PetscCall(VecRestoreArrayRead(x, &xx));
    PetscCall(MatSetValues(Dfx, 3, idx, 3, idx, Dfxxx, INSERT_VALUES));
    /* Assemble Dfx */
    PetscCall(MatAssemblyBegin(Dfx, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(Dfx, MAT_FINAL_ASSEMBLY));
    /* Return */
    return 0;
}
/*!
 *  @brief time-h-map of the Chua map.
 *                                                                                                                                */
PetscErrorCode timehchua(Vec x, Vec fx, void *ctx)
{
    /* Initialize */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    timehchuactx hcctx = *reinterpret_cast<timehchuactx*>(ctx);
    /* Solve ode */
    PetscCall(slds::ode::odesolve(chua, x, fx, 0, hcctx.h, 3, &hcctx.cctx, hcctx.odectx));
    /* Return */
    return 0;
}
/*!
 *  @brief time-h-map of the Lorenz map.
 *                                                                                                                                */
PetscErrorCode timehlorenz(Vec x, Vec fx, void *ctx)
{
    /* Initialize */
    PetscBool initpetsc;
    int mpirank;
    /* Return if Petsc or Slepc is not initialized */
    PetscCall(PetscInitialized(&initpetsc));
    if(initpetsc == PETSC_FALSE)
        return 100;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Recover data */
    timehlorenzctx hlctx = *reinterpret_cast<timehlorenzctx*>(ctx);
    /* Solve ode */
    PetscCall(slds::ode::odesolve(lorenz, x, fx, 0, hlctx.h, 3, &hlctx.lctx, hlctx.odectx));
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* Main */
/**********************************************************************************************************************************/
int main(int argc, char **argv)
{
    /* Initialize Petsc and Slepc */
    PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));
    /* Set parameters */
    const PetscReal ah = 1.4;
    const PetscReal bh = 0.3;
    const PetscReal ac = 18;
    const PetscReal bc = 33;
    const PetscReal m0c = -0.2;
    const PetscReal m1c = 0.01;
    const PetscReal bl = 8/3.;
    const PetscReal rl = 28;
    const PetscReal sl = 10;
    const PetscReal lh[2] = {-2, -2};
    const PetscReal uh[2] = {2, 2};
    const PetscReal lc[3] = {-12, -2.5, -20};
    const PetscReal uc[3] = {12, 2.5, 20};
    const PetscReal ll[3] = {-25, -30, 0};
    const PetscReal ul[3] = {25, 30, 60};
    /* Set up */
    int mpirank;
    Vec fxc, fxl;
    Mat Dfxc, Dfxl;
    slds::loca::box uhbox, ucbox, ulbox;
    slds::loca::locactx ctxh, ctxc, ctxl;
    slds::ode::odesolvectx ctxo, ctxol;
    slds::util::vecctx vctx(VECSEQ, 1);
    slds::util::matctx mctx(MATSEQDENSE, 1);
    std::vector<PetscReal> uha(2, 0), uhb(2, 0), uca(3, 0), ucb(3, 0), ula(3, 0), ulb(3, 0);
    std::vector<slds::loca::box> auhbox, aucbox, aulbox;
    /* Query process rank */
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpirank));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Setting up... \n"));
    /* Set options */
    ctxh.verbose = 1;
    ctxh.ns = 26;
    ctxh.nsinit = 3;
    ctxh.nt = 50;
    ctxc.verbose = 1;
    ctxc.ns = 22;
    ctxc.nsinit = 6;
    ctxc.nt = 50;
    ctxl.verbose = 1;
    ctxl.ns = 24;
    ctxl.nsinit = 6;
    ctxl.nt = 50;
    ctxo.local = 1;
    ctxo.dt = 1e-6;
    ctxol.tsatol = 1e-9;
    ctxol.tsrtol = 1e-9;
    ctxo.tstype = TSRADAU5;
    ctxo.fx = fxc;
    ctxo.Dfx = Dfxc;
    ctxo.df = dfchua;
    ctxol.local = 1;
    ctxol.dt = 1e-6;
    ctxol.tsatol = 1e-9;
    ctxol.tsrtol = 1e-9;
    ctxol.tstype = TSRADAU5;
    ctxol.fx = fxl;
    ctxol.Dfx = Dfxl;
    ctxol.df = dflorenz;
    /* Initialize vectors and matrices */
    PetscCall(slds::util::vecsetdefault(&fxc, 3, &vctx));
    PetscCall(slds::util::vecsetdefault(&fxl, 3, &vctx));
    PetscCall(slds::util::matsetdefault(&Dfxc, 3, 3, &mctx));
    PetscCall(slds::util::matsetdefault(&Dfxl, 3, 3, &mctx));
    /* Initialize box */
    for(int i = 0; i < 2; i++)
    {
        uha[i] = lh[i];
        uhb[i] = uh[i];
    }
    uhbox.d = 2;
    uhbox.a = uha;
    uhbox.b = uhb;
    for(int i = 0; i < 3; i++)
    {
        uca[i] = lc[i];
        ucb[i] = uc[i];
    }
    ucbox.d = 3;
    ucbox.a = uca;
    ucbox.b = ucb;
    for(int i = 0; i < 3; i++)
    {
        ula[i] = ll[i];
        ulb[i] = ul[i];
    }
    ulbox.d = 3;
    ulbox.a = ula;
    ulbox.b = ulb;
    /* Initialize context */
    henonctx hctx(ah, bh);
    chuactx cctx(ac, bc, m0c, m1c);
    lorenzctx lctx(bl, rl, sl);
    timehchuactx hcctx(1, cctx, ctxo);
    timehlorenzctx hlctx(1, lctx, ctxol);
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Computing Henon attrator for a = %f, b = %f on [%f, %f] x [%f, %f]... \n", ah, bh,
                          uhbox.a[0], uhbox.b[0], uhbox.a[1], uhbox.b[1]));
    /* Compute attractor */
    PetscCall(slds::loca::loca(henon, uhbox, &auhbox, 2, &hctx, ctxh));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attractor contains %d boxes after %d steps \n", auhbox.size(), ctxh.ns));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing result... \n"));
    /* Write result */
    PetscCall(slds::loca::writeh5(&auhbox, "henon.h5"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                          "Computing Lorenz attrator for b = %f, r = %f, s = %f on [%f, %f] x [%f, %f] x [%f, %f]... \n",
                          bl, rl, sl, ulbox.a[0], ulbox.b[0], ulbox.a[1], ulbox.b[1], ulbox.a[2], ulbox.b[2]));
    /* Compute attractor */
    PetscCall(slds::loca::loca(timehlorenz, ulbox, &aulbox, 3, &hlctx, ctxl));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attractor contains %d boxes after %d steps \n", aulbox.size(), ctxl.ns));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing result... \n"));
    /* Write result */
    PetscCall(slds::loca::writeh5(&aulbox, "lorenz.h5"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                          "Computing Chua attrator for a = %f, b = %f, m0 = %f, m1 = %f on [%f, %f] x [%f, %f] x [%f, %f]... \n",
                          ac, bc, m0c, m1c, ucbox.a[0], ucbox.b[0], ucbox.a[1], ucbox.b[1], ucbox.a[2], ucbox.b[2]));
    /* Compute attractor */
    PetscCall(slds::loca::loca(timehchua, ucbox, &aucbox, 3, &hcctx, ctxc));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Attractor contains %d boxes after %d steps \n", aucbox.size(), ctxc.ns));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Writing result... \n"));
    /* Write result */
    PetscCall(slds::loca::writeh5(&aucbox, "chua.h5"));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Cleaning up... \n"));
    /* Clean up */
    PetscCall(VecDestroy(&fxc));
    PetscCall(VecDestroy(&fxl));
    PetscCall(MatDestroy(&Dfxc));
    PetscCall(MatDestroy(&Dfxl));
    /* Print update */
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Finished. \n"));
    /* Finalize Petsc and Slepc */
    PetscCall(PetscFinalize());
    /* Return */
    return 0;
}
/**********************************************************************************************************************************/
/* End */
/**********************************************************************************************************************************/
