#**********************************************************************************************************************************#
 ## @file genbfstab.jl
 #
 #  @author David Rackl (somekindofvirus@arctic-fox.net)
 #  @verion 1.0.0
 #  @date 01/2023
 #  @copyright GNU Public License
 #
 #  Script to generate example plots
#**********************************************************************************************************************************#
## Copyright Notice
#**********************************************************************************************************************************#
# Copyright (C) 2022  David Rackl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************************************************************************#
## Imports
#**********************************************************************************************************************************#
using LaTeXStrings;
import ColorSchemes as css;
import CairoMakie as glmk;
import JLD2 as jld;
#**********************************************************************************************************************************#
## Main
#**********************************************************************************************************************************#
# Print update #
print("Setting up... \n");
# Set theme #
theme_latexcycle = glmk.merge(glmk.theme_latexfonts(), glmk.Theme(palette = (color = css.colorschemes[:tab10],)));
glmk.set_theme!(theme_latexcycle);
# Create 2d poly plot #
fig = glmk.Figure(resolution = (3000, 3000), fontsize = 48);
axis = glmk.Axis(fig[1, 1], xlabel = L"$x$", ylabel = L"$y$");
# Read continuation output #
try
    global fh = jld.jldopen("build/henon.h5", "r");
catch
    print("Failed to read data!");
    return;
end
# Open datasets #
try
    global bh = fh["Boxes"];
catch
    print("Failed to read datasets!");
    return;
end
# Plot boxes #
for i in 1:length(bh)
    glmk.poly!(axis, glmk.Point2f[(bh[i][2][1], bh[i][2][2]), (bh[i][3][1], bh[i][2][2]), (bh[i][3][1], bh[i][3][2]),
                                  (bh[i][2][1], bh[i][3][2])], color = :black, strokecolor = :white, strokewidth = 0);
end
# Export figure #
glmk.save("henon.png", fig);
# Create 3d poly plot #
fig3 = glmk.Figure(resolution = (3000, 3000), fontsize = 48);
axis3 = glmk.Axis3(fig3[1, 1], xlabel = L"$x$", ylabel = L"$y$", zlabel = L"$z$");
# Read continuation output #
try
    global fc = jld.jldopen("build/chua.h5", "r");
catch
    print("Failed to read data!");
    return;
end
# Open datasets #
try
    global bc = fc["Boxes"];
catch
    print("Failed to read datasets!");
    return;
end
# Plot boxes #
# for i in 1:length(bc)
#     glmk.mesh!(axis3, glmk.Point3f[(bc[i][2][1], bc[i][2][2], bc[i][2][3]), (bc[i][3][1], bc[i][2][2], bc[i][2][3]),
#                                    (bc[i][2][1], bc[i][2][2], bc[i][3][3]), (bc[i][3][1], bc[i][2][2], bc[i][3][3]),
#                                    (bc[i][2][1], bc[i][3][2], bc[i][2][3]), (bc[i][3][1], bc[i][3][2], bc[i][2][3]),
#                                    (bc[i][2][1], bc[i][3][2], bc[i][3][3]), (bc[i][3][1], bc[i][3][2], bc[i][3][3])],
#                [1 2 3; 2 3 4; 1 2 5; 2 5 6; 2 4 6; 4 6 8; 1 3 5; 3 5 7; 3 4 7; 4 7 8; 5 6 7; 6 7 8;], color = :black);
# end
# Plot box centers #
bcx = zeros(Float64, 0);
bcy = zeros(Float64, 0);
bcz = zeros(Float64, 0);
for i in 1:length(bc)
    push!(bcx, bc[i][2][1] + (bc[i][3][1] - bc[i][2][1])/2);
    push!(bcy, bc[i][2][2] + (bc[i][3][2] - bc[i][2][2])/2);
    push!(bcz, bc[i][2][3] + (bc[i][3][3] - bc[i][2][3])/2);
end
glmk.scatter!(axis3, bcx, bcy, bcz, color = :black);
# Export figure #
glmk.save("chua.png", fig3);
# Create 3d poly plot #
fig3 = glmk.Figure(resolution = (3000, 3000), fontsize = 48);
axis3 = glmk.Axis3(fig3[1, 1], xlabel = L"$x$", ylabel = L"$y$", zlabel = L"$z$");
# Read continuation output #
try
    global fl = jld.jldopen("build/lorenz.h5", "r");
catch
    print("Failed to read data!");
    return;
end
# Open datasets #
try
    global bl = fl["Boxes"];
catch
    print("Failed to read datasets!");
    return;
end
# Plot boxes #
# for i in 1:length(bl)
#     glmk.mesh!(axis3, glmk.Point3f[(bl[i][2][1], bl[i][2][2], bl[i][2][3]), (bl[i][3][1], bl[i][2][2], bl[i][2][3]),
#                                    (bl[i][2][1], bl[i][2][2], bl[i][3][3]), (bl[i][3][1], bl[i][2][2], bl[i][3][3]),
#                                    (bl[i][2][1], bl[i][3][2], bl[i][2][3]), (bl[i][3][1], bl[i][3][2], bl[i][2][3]),
#                                    (bl[i][2][1], bl[i][3][2], bl[i][3][3]), (bl[i][3][1], bl[i][3][2], bl[i][3][3])],
#                [1 2 3; 2 3 4; 1 2 5; 2 5 6; 2 4 6; 4 6 8; 1 3 5; 3 5 7; 3 4 7; 4 7 8; 5 6 7; 6 7 8;], color = :black);
# end
# Plot box centers #
blx = zeros(Float64, 0);
bly = zeros(Float64, 0);
blz = zeros(Float64, 0);
for i in 1:length(bl)
    push!(blx, bl[i][2][1] + (bl[i][3][1] - bl[i][2][1])/2);
    push!(bly, bl[i][2][2] + (bl[i][3][2] - bl[i][2][2])/2);
    push!(blz, bl[i][2][3] + (bl[i][3][3] - bl[i][2][3])/2);
end
glmk.scatter!(axis3, blx, bly, blz, color = :black);
# Export figure #
glmk.save("lorenz.png", fig3);
#**********************************************************************************************************************************#
## End
#**********************************************************************************************************************************#
