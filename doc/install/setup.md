# Installation guide

## Prerequisites

Make sure the following dependencies are installed and up to date.

- C/C++ compiler, for example gcc or clang
- Fortran compiler, for example gfortran or flang
- python3
- flex
- make
- cmake
- git
- doxygen
- BLAS/LAPACK implementation, for example OpenBLAS
- MPI implementation, for example OpenMPI
- HDF5 with C++ libraries

The above prerequisites can be installed via the provided Nix environment. This environment can be activated from the *nix* directory with
```console
nix-shell
```
Alternatively, BLAS/LAPACK, MPI and HDF5 can also be installed during PETSc's installation process. See
```console
./configure --help 
```
below.

## Installing PETSc

Clone the git repository with
```console
git clone -b release https://gitlab.com/petsc/petsc.git PATH/TO/PETSC 
```
To update an existing install run
```console
git pull 
```
from PETSc's installation directory. Configure your installation by running
```console
cd PATH/TO/PETSC
./configure --with-precision=double --with-scalar-type=real --with-debugging=0 --COPTFLAGS="-O2 -march=native" --CXXOPTFLAGS="-O2 -march=native" --FOPTFLAGS="-O2 -march=native" --download-bison=yes --download-metis=yes –-download-mumps=yes –-download-parmetis=yes -–download-ptscotch=yes --download-scalapack=yes --download-slepc=yes 
```
Depending on the applications the above configuration may need to be modified. To see all available configuration options run
```console
./configure --help 
```
Build the configuration with
```console
make all check 
```
For additional information please refer to PETSc's official installation guide.

Note: Installing MUMPS currently requires patching the file *config/BuildSystem/config/packages/MUMPS.py* to remove *ouput1+err1* from the post install phase.

## Installing slds++ 

Export the environment variables
```console
export PETSC_DIR=PATH/TO/PETSC 
```
and
```console
export PETSC_ARCH=arch-linux-c-opt 
```
pointing to the PETSc installation. Note that the value of *PETSC_ARCH* may change depending on your configuration. Then build and install the library by running
```console
 cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=On ../
 cmake --build .
 cmake --install . --prefix PATH/TO/SLDS/lib/install
```
from the *lib/build* directory.

## Linking slds++ with cmake 

Import slds++ via its pkgconfig file by adding
```console
set(SLDS $ENV{SLDS_DIR})
set(ENV{PKG_CONFIG_PATH} ${SLDS}/lib/install/share/pkgconfig)
pkg_search_module(slds REQUIRED IMPORTED_TARGET slds)
target_link_libraries(${PROJECT_NAME} PkgConfig::slds)
```
to the project's *CMakeLists.txt*. When building the project make sure to export the environment variables
```console
export PETSC_DIR=PATH/TO/PETSC
export PETSC_ARCH=arch-linux-c-opt
export SLDS_DIR=PATH/TO/PETSC 
```
For a more detailed explanation please refer to the example cmake files.
